<?php

namespace app\helpers;

class NestedSetsHelper
{
    public static function depthTitle($title, $depth)
    {
        return str_repeat('— ', max($depth - 1, 0))  . $title;
    }
}