<?php

namespace app\controllers;

use app\modules\client\models\Client;
use yii\web\Controller;
use yii\web\ErrorAction;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => ErrorAction::class,
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAbout()
    {
        $clients = Client::find()->active()->orderBy('position')->all();

        return $this->render('about', compact('clients'));
    }

    public function actionContacts()
    {
        return $this->render('contacts');
    }

    public function actionMonitoring()
    {
        return $this->render('monitoring');
    }

    public function actionRepair()
    {
        return $this->render('repair');
    }
}
