<?php

namespace app\traits\check;

/**
 * @property int $check
 */
trait CheckTrait
{
    public function isChecked()
    {
        return $this->check == 1;
    }

    public static function CheckList()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }

    public function getCheckedIcon()
    {
        $icon = 'remove';
        $class = 'text-danger';

        if ($this->isChecked()) {
            $icon = "ok";
            $class = 'text-success';
        }

        return '<i class="glyphicon glyphicon-' . $icon . ' ' . $class . '"></i>';
    }
}