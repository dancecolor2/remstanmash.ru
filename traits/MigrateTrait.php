<?php

namespace app\traits;

use yii\db\Migration;

/**
 * @mixin Migration
 */
trait MigrateTrait
{
    public function columns($columnNames)
    {
        $columns = [];

        foreach ($columnNames as $columnName) {
            switch ($columnName) {
                case 'id' : $columns['id'] = $this->primaryKey(); break;
                case 'created_at' : $columns['created_at'] = $this->integer()->notNull()->comment('Дата создания'); break;
                case 'updated_at' : $columns['updated_at'] = $this->integer()->notNull()->comment('Дата редактирования'); break;
                case 'position' : $columns['position'] = $this->integer()->notNull(); break;
                case 'active' : $columns['active'] = $this->boolean()->defaultValue(1)->comment('Активность'); break;
                case 'title' : $columns['title'] = $this->string(255)->notNull()->comment('Название'); break;
                case 'image' :
                    $columns['image'] = $this->string(500)->comment('Картинка');
                    $columns['image_hash'] = $this->string(255);
                    break;
                case 'icon' :
                    $columns['icon'] = $this->string(500)->comment('Иконка');
                    $columns['icon_hash'] = $this->string(255);
                    break;
                case 'seo' :
                    $columns['meta_d'] = $this->string(255)->comment('Описание страницы');
                    $columns['meta_k'] = $this->string(255)->comment('Ключевые слова');
                    $columns['meta_t'] = $this->string(255)->comment('Заголовок страницы');
                    $columns['h1'] = $this->string(255)->comment('H1');

                    break;
            }
        }

        return $columns;
    }

    public function defaultColumns()
    {
        return $this->columns(['id', 'created_at', 'updated_at', 'position', 'active', 'title']);
    }
}