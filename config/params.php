<?php

use Cekurte\Environment\Environment;

return [
    'email_from' => Environment::get('SMTP_USERNAME'),
    'icon-framework' => \kartik\icons\Icon::TYP,
	'reCaptcha' => [
		'siteKeyV2' => '6Lfdj68ZAAAAACPNbW-1xQtbqRkXOy-anzYwYtvB',
		'secretV2' => '6Lfdj68ZAAAAAG1yAdoe7qnrWPtc2oqjlem0cGvm',
	],
];
