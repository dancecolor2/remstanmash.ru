<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'category',
        'vendor',
        'product',
        'news',
        'machine',
        'page',
    ],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@tests' => '@app/tests',
        '@webroot' => '@app/web',
    ],
    'modules' => [
        'category' => \app\modules\category\Module::class,
        'product' => \app\modules\product\Module::class,
        'eav' => \app\modules\eav\Module::class,
        'vendor' => \app\modules\vendor\Module::class,
        'news' => \app\modules\news\Module::class,
        'machine' => \app\modules\machine\Module::class,
        'page' => \app\modules\page\Module::class,
    ],
    'components' => [
        'authManager' => \yii\rbac\DbManager::class,
        'cache' => \yii\caching\FileCache::class,
        'log' => [
            'targets' => [
                [
                    'class' => \yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
	    'urlManager' => [
		    'baseUrl' => '/',
		    'scriptUrl' => '/index.php',
		    'enablePrettyUrl' => true,
		    'showScriptName' => false,
	    ],
    ],
    'params' => $params,
];

return $config;