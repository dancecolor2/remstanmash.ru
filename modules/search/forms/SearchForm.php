<?php

namespace app\modules\search\forms;

use app\modules\product\models\Product;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SearchForm extends Model
{
    public $q;

    public function rules()
    {
        return [
            ['q', 'required'],
            ['q', 'string', 'max' => 255],
        ];
    }

    public function formName()
    {
        return '';
    }

    public function search($params): ActiveDataProvider
    {
        $query = Product::find()->where(['product.active' => 1]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query->joinWith(['vendor']),
            'sort' => ['defaultOrder' => ['position' => SORT_ASC]],
            'pagination' => ['defaultPageSize' => 40, 'forcePageParam' => false],
        ]);

        $this->load($params);

        $query->andWhere([
            'or',
            ['like', 'product.code', $this->q],
            ['like', 'product.title', $this->q],
            ['like', 'product.meta_k', $this->q],
            ['like', 'vendor.title', $this->q],
        ]);

        return $dataProvider;
    }
}