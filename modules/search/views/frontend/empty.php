<?php

use app\modules\page\components\Pages;
use app\modules\setting\components\Settings;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var string $q
 */

$this->title = 'Поиск';
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs();
$this->registerMetaTag(['name' => 'description', 'content' => 'Поиск']);
$this->registerMetaTag(['name' => 'keywords', 'content' => 'Поиск']);
$this->params['h1'] = 'Ничего не найдено';

?>

<section class="section">
    <div class="container">
        <div class="section__body">
            <div class="grid is-row">
                <div class="col-9 col-md-12">
                    <div class="search">
                        <div class="search__form">
                            <form class="search-form" action="<?= Url::to(['/search/frontend/index']) ?>">
                                <label class="search-form__label" for="search-field">Вы искали</label>
                                <div class="search-form__field">
                                    <input class="search-form__input" id="search-field" type="search" name="q" autofocus="autofocus" value="<?= Html::encode($q) ?>"/>
                                    <p class="search-form__amount">Найдено 0 совпадений</p>
                                </div>
                                <p class="search-form__message">По вашему запросу ничего не найдено! Попробуйте изменить ваш запрос...</p>
                                <div class="search-form__link">
                                    <a class="link is-back is-light" href="/" title="">
                                        <span class="link__label">Вернуться назад</span>
                                        <svg class="link__arrow">
                                            <use xlink:href="/img/sprite.svg#arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section is-accent is-centred with-padding has-callback is-padding-100">
    <div class="container">
        <header class="section__header">
            <h2 class="section__title">Нужна консультация?</h2>
            <p class="section__descr">Вы можете связаться с нами по номеру телефона либо оставить заявку и мы свяжемся с вами в течении 10 минут...</p>
        </header>
        <div class="section__body">
            <div class="callback">
                <div class="callback__contact">
                    <address class="contact is-alt">
                        <div class="contact__group">
                            <div class="contact__worktime">
                                <div class="contact__day">пн-сб</div>
                                <div class="contact__time">08:00 - 18:00</div>
                            </div>
                            <ul class="contact__phones">
                                <li class="contact__phone">
                                <a class="phone" href="tel:<?= Settings::getArray('phones')[0] ?>"><?= Settings::getArray('phones')[0] ?></a>
                                </li>
                            </ul>
                        </div>
                    </address>
                </div>
                <div class="callback__button">
                    <button class="button js-button" area-label="заказать обратный звонок" data-modal="callback">заказать обратный звонок</button>
                </div>
            </div>
        </div>
    </div>
</section>

