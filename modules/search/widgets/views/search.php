<?php

use yii\helpers\Url;

/**
 * @var \app\modules\search\forms\SearchForm $formSearch
 * @var string $q
 */

?>

<form class="header-search js-header-search" action="<?= Url::to(['/search/frontend/index']) ?>">
    <button class="header-search__label js-header-search-label" type="button">
        <span>Поиск</span>
    </button>
    <div class="header-search__inner">
        <input class="header-search__field js-header-search-field" type="text" name="q" placeholder="Поиск по артикулу, товарам и производителю..."/>
        <button class="header-search__close js-header-search-close" aria-label="Закрыть" type="button"></button>
    </div>
</form>