<?php

namespace app\modules\search\widgets;

use yii\base\Widget;

class SearchWidget extends Widget
{
    public function run()
    {
        return $this->render('search');
    }
}