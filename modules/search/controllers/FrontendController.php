<?php

namespace app\modules\search\controllers;

use app\modules\search\forms\SearchForm;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{
    public function actionIndex()
    {
        $searchForm = new SearchForm();

        if ($searchForm->load(Yii::$app->request->get()) && $searchForm->validate()) {
            $dataProvider = $searchForm->search(Yii::$app->request->get());
            $q = $searchForm->q;

            if ($dataProvider->totalCount == 0) {
                return $this->render('empty', compact('q'));
            }

            return $this->render('index', compact('dataProvider', 'q'));
        }

        return $this->render('empty', compact('q'));
    }
}