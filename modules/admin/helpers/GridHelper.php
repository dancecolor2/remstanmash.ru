<?php

namespace app\modules\admin\helpers;

class GridHelper
{
    public static function getActiveIcon($bool)
    {
        $icon = 'remove';
        $class = 'text-danger';

        if ($bool) {
            $icon = "ok";
            $class = 'text-success';
        }

        return '<i class="glyphicon glyphicon-' . $icon . ' ' . $class . '"></i>';
    }
}