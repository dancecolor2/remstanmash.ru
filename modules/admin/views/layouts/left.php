<?php

use app\modules\feedback\factories\FormFactory;
use app\modules\feedback\forms\CallbackForm;
use app\modules\feedback\helpers\Badge;
use dmstr\widgets\Menu;

?>

<aside class="main-sidebar">
    <section class="sidebar">
        <?= Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => 'Главная',
                        'icon' => 'home',
                        'url' => Yii::$app->getHomeUrl(),
                    ],
                    [
                        'label' => 'Страницы',
                        'icon' => 'files-o',
                        'url' => ['/page/backend/default/index'],
                        'active' => Yii::$app->controller->module->id == 'page',
                    ],
                    [
                        'label' => 'Каталог',
                        'icon' => 'folder-o',
                        'items' => [
                            [
                                'label' => 'Товары',
                                'icon' => 'shopping-cart',
                                'url' => ['/product/backend/default/index'],
                                'active' => Yii::$app->controller->module->id == 'product',
                            ],
                            [
                                'label' => 'Категории',
                                'icon' => 'folder-open',
                                'url' => ['/category/backend/index'],
                                'active' => Yii::$app->controller->module->id == 'category',
                            ],
                            [
                                'label' => 'Производители',
                                'icon' => 'building-o',
                                'url' => ['/vendor/backend/index'],
                                'active' => Yii::$app->controller->module->id == 'vendor',
                            ],
                            [
                                'label' => 'Характеристики',
                                'icon' => 'list',
                                'url' => ['/eav/backend/index'],
                                'active' => Yii::$app->controller->module->id == 'eav',
                            ],
							[
								'label' => 'Отзывы',
								'url' => ['/review/backend/index'],
								'icon' => 'exclamation',
								'active' => Yii::$app->controller->module->id == 'repair',
							],
                        ],
                    ],
                    [
                        'label' => 'Моодернизация',
                        'url' => ['/machine/backend/default/index'],
                        'icon' => 'microchip',
                        'items' => [
                            [
                                'label' => 'Станки',
                                'icon' => 'rocket',
                                'url' => ['/machine/backend/default/index'],
                                'active' => (Yii::$app->controller->module->id == 'machine' && Yii::$app->controller->id !== 'backend/type')
                                    || Yii::$app->controller->module->id == 'set'
                                ,
                            ],
                            [
                                'label' => 'Типы станков',
                                'icon' => 'list',
                                'url' => ['/machine/backend/type/index'],
                                'active' => Yii::$app->controller->module->id == 'machine' && Yii::$app->controller->id == 'backend/type',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Контент',
                        'icon' => 'file-text',
                        'items' => [
                            [
                                'label' => 'Новости',
                                'icon' => 'newspaper-o',
                                'url' => ['/news/backend/index'],
                                'active' => Yii::$app->controller->module->id == 'news',
                            ],
                            [
                                'label' => 'Проекты',
                                'icon' => 'folder-open-o',
                                'url' => ['/project/backend/index'],
                                'active' => Yii::$app->controller->module->id == 'project',
                            ],
                            [
                                'label' => 'Клиенты',
                                'icon' => 'handshake-o',
                                'url' => ['/client/backend/default/index'],
                                'active' => Yii::$app->controller->module->id == 'client',
                            ],
                            [
                                'label' => 'Системы мониторинга',
                                'icon' => 'desktop',
                                'url' => ['/system/backend/default/index'],
                                'active' => Yii::$app->controller->module->id == 'system',
                            ],
                            [
                                'label' => 'Ремонт',
                                'url' => ['/repair/backend/default/index'],
                                'icon' => 'wrench',
                                'active' => Yii::$app->controller->module->id == 'repair',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Заявки' . Badge::getSumBadge(),
                        'icon' => 'bell-o',
                        'url' => ['/feedback/backend/default/index', 'type' => CallbackForm::TYPE],
                        'active' => Yii::$app->controller->module->id == 'feedback',
                    ],
                    [
                        'label' => 'Настройки',
                        'icon' => 'cogs',
                        'items' => [
                            [
                                'label' => 'Настройки',
                                'icon' => 'cog',
                                'url' => ['/setting/backend/default/index'],
                                'active' => Yii::$app->controller->module->id == 'setting',
                            ],
                            [
                                'label' => 'Пользователи',
                                'icon' => 'users',
                                'url' => ['/user/backend/index'],
                                'active' => Yii::$app->controller->module->id == 'user',
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>
    </section>
</aside>
