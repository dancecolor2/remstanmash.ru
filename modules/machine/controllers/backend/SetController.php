<?php

namespace app\modules\machine\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\machine\models\Machine;
use app\modules\set\models\Set;
use app\modules\set\models\SetItemSearch;
use app\modules\set\models\SetSearch;
use Yii;

class SetController extends BalletController
{
    public function actionIndex($id)
    {
        $machine = Machine::findOneOrException($id);
        $searchModel = new SetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', compact('machine', 'searchModel', 'dataProvider'));
    }

    public function actionCreate($id)
    {
        $machine = Machine::findOneOrException($id);
        $set = new Set([
            'active' => true,
            'machine_id' => $machine->id,
        ]);

        if ($set->load(Yii::$app->request->post()) && $set->save()) {

            return $this->redirect(['update', 'id' => $set->id]);
        }

        return $this->render('create', compact('set', 'machine'));
    }

    public function actionUpdate($id)
    {
        $set = Set::findOneOrException($id);
        $searchModel = new SetItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['set_id' => $set->id]);

        return $this->render('update', compact('set', 'searchModel', 'dataProvider'));
    }
}