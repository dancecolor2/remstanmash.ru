<?php

namespace app\modules\machine\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\machine\models\Machine;
use app\modules\machine\models\MachineImage;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;

class ImageController extends BalletController
{
    public function actionUpload($id)
    {
        $product = Machine::findOneOrException($id);
        $files = UploadedFile::getInstances($product, 'images');
        foreach ($files as $file) {
            $image = new MachineImage([
                'machine_id' => $product->id,
                'image' => $file,
            ]);
            $image->save();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['message' => 'success'];
    }

    public function actionDelete()
    {
        $image = MachineImage::findOneOrException(Yii::$app->request->post('key'));
        $image->delete();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['message' => 'success'];
    }

    public function actionSort()
    {
        $image = MachineImage::findOneOrException(Yii::$app->request->post('key'));
        $image->position = Yii::$app->request->post('position');
        $image->save();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['message' => 'success'];
    }
}