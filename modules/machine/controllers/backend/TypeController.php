<?php

namespace app\modules\machine\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\machine\models\MachineType;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;

class TypeController extends BalletController
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => MachineType::find()->orderBy('title'),
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    public function actionCreate()
    {
        $machineType = new MachineType();

        if ($machineType->load(Yii::$app->request->post()) && $machineType->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['message' => 'success'];
            } else {
                return $this->redirect(['/machineType/backend/default/update', 'id' => $machineType->id]);
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', compact('machineType'));
        } else {
            return $this->render('create', compact('machineType'));
        }
    }

    public function actionUpdate($id)
    {
        $machineType = MachineType::findOneOrException($id);

        if ($machineType->load(Yii::$app->request->post()) && $machineType->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['message' => 'success'];
            } else {
                return $this->refresh();
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', compact('machineType'));
        } else {
            return $this->render('update', compact('machineType'));
        }
    }

    public function actionDelete($id)
    {
        MachineType::findOneOrException($id)->delete();
    }
}