<?php

namespace app\modules\machine\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\machine\models\Machine;
use app\modules\machine\models\MachineSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Response;
use yiidreamteam\upload\ImageUploadBehavior;

class DefaultController extends BalletController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete' => ['POST'],
            ],
        ];
        return $behaviors;
    }

    public function actionIndex()
    {
        $searchModel = new MachineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', compact('dataProvider', 'searchModel'));
    }

    public function actionCreate()
    {
        $machine = new Machine([
            'active' => true,
        ]);

        if ($machine->load(Yii::$app->request->post()) && $machine->save()) {
            return $this->redirect(Url::to(['update', 'id' => $machine->id]));
        }

        return $this->render('create', compact('machine'));
    }

    public function actionUpdate($id)
    {
        $machine = Machine::findOneOrException($id);

        if ($machine->load(Yii::$app->request->post()) && $machine->save()) {
            return $this->refresh();
        }

        return $this->render('update', compact('machine'));
    }

    public function actionSeo($id)
    {
        $machine = Machine::findOneOrException($id);

        if ($machine->load(Yii::$app->getRequest()->post()) && $machine->save()) {
            return $this->refresh();
        }

        return $this->render('seo', compact('machine'));
    }

    public function actionDelete($id)
    {
        Machine::findOneOrException($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMoveUp($id)
    {
        Machine::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        Machine::findOneOrException($id)->moveNext();
    }
}