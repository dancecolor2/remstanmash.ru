<?php

namespace app\modules\machine\controllers;

use app\modules\filter\forms\FilterMachines;
use app\modules\machine\models\Machine;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class FrontendController extends Controller
{
    public function actionIndex()
    {
        $filterMachines = new FilterMachines();
        $filterMachines->load(Yii::$app->request->get());
        $dataProvider = $filterMachines->filter();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['html' => $this->renderAjax('filter', compact('dataProvider'))];
        }

        return $this->render('index', compact('dataProvider', 'filterMachines'));
    }

    public function actionView($id)
    {
        $machine = Machine::findOneOrException(['id' => $id, 'active' => true]);
        $sets = $machine->getSets()->orderBy('position')->active()->all();

        return $this->render('view', compact('machine', 'sets'));
    }
}