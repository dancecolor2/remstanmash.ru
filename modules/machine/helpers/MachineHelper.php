<?php

namespace app\modules\machine\helpers;

use app\modules\machine\models\MachineType;

class MachineHelper
{
    public static function getTypesList()
    {
        return MachineType::find()->orderBy('title')->select('title')->indexBy('id')->column();
    }
}