<?php

namespace app\modules\machine\widgets;

use app\helpers\FormatHelper;
use app\modules\feedback\forms\MachineForm;
use app\modules\machine\models\Machine;
use yii\base\Widget;
use yii\helpers\Json;

/**
 * @property Machine $machine
 */
class MachineWidget extends Widget
{
    public $machine;

    public function run()
    {
        $minCost = $this->getMinCost();

        $dataInfoJson = Json::encode([
            "id" => $this->machine->id,
            "image" => $this->machine->hasImage() ? $this->machine->images[0]->getThumbFileUrl('image', 'preview') : '',
            "code" => !empty($this->machine->code) ? "Арт. " . $this->machine->code : '',
            "title" => $this->machine->title,
            "cost" => !empty($minCost) ? FormatHelper::number($minCost) . " ₽" : '',
            'type' => MachineForm::TYPE,
        ]);

        return $this->render('machine', [
            'machine' => $this->machine,
            'dataInfoJson' => $dataInfoJson,
            'minCost' => $minCost
        ]);
    }

    public function getMinCost()
    {
        $minCost = 0;

        foreach ($this->machine->sets as $set) {

            if (!empty($set->cost)) {

                if (empty($minCost)) {
                    $minCost = $set->cost;
                } elseif ($minCost > $set->cost) {
                    $minCost = $set->cost;
                }
            }
        }

        return $minCost;
    }
}