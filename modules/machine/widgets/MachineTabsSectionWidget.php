<?php

namespace app\modules\machine\widgets;

use app\modules\machine\models\Machine;
use app\modules\set\models\Set;
use yii\base\Widget;

/**
 * @property Machine $machine
 * @property Set[] $sets
 */
class MachineTabsSectionWidget extends Widget
{
    public $machine;
    public $sets = null;

    public function init()
    {
        if (is_null($this->sets)) {
            $this->sets = $this->machine->getSets()->active()->orderBy('position')->all();
        }
    }

    public function run()
    {
        $hasSetsTab = !empty($this->machine->sets);
        $hasDescriptionTab = !empty($this->machine->description);
        $firstSet = $this->sets[0] ?? new Set();

        return $this->render('machine_tabs_section', [
            'machine' => $this->machine,
            'hasSetsTab' => $hasSetsTab,
            'hasDescriptionTab' => $hasDescriptionTab,
            'sets' => $this->sets,
            'firstSet' => $firstSet,
        ]);
    }
}