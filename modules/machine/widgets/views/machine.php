<?php

use app\helpers\FormatHelper;

/**
 * @var \yii\web\View $this
 * @var \app\modules\machine\models\Machine $machine
 * @var string $dataInfoJson
 * @var integer $minCost
 */

?>

<?php if (!empty($machine)): ?>
    <article class="set">
        <div class="set__cover">
            <?php if (!empty($machine->hasImage())): ?>
                <img class="set__image" src="<?= $machine->images[0]->getThumbFileUrl('image', 'preview') ?>" alt="<?= $machine->title ?>"/>
            <?php endif ?>
        </div>
        <div class="set__body">
            <header class="set__header">
                <?php if (!empty($machine->code)): ?>
                    <p class="set__code">Арт. <?= $machine->code ?></p>
                <?php endif ?>
                <h3 class="set__title">
                    <a class="set__link" href="<?= $machine->getHref() ?>" title="<?= $machine->title ?>"><?= $machine->title ?></a>
                </h3>
            </header>
            <div class="set__text">
                <?= FormatHelper::nText($machine->preview_text) ?>
            </div>
            <footer class="set__footer">
                <div class="set__cost">
                    <?php if (!empty($minCost)): ?>
                        <div class="cost">
                            <div class="cost__caption">Стоимость модернизации:</div>
                            <div class="cost__value">от <?= FormatHelper::number($minCost) ?> <i>₽</i></div>
                        </div>
                    <?php endif ?>
                </div>
                <div class="set__button">
                    <button class="button js-button is-small is-wide" area-label="Модернизировать" data-modal="order" data-info='<?= $dataInfoJson ?>'>Модернизировать</button>
                </div>
            </footer>
        </div>
    </article>
<?php endif ?>