<?php

use app\helpers\FormatHelper;
use app\modules\product\widgets\ProductSimpleWidget;
use app\modules\set\helpers\SetHelper;
use app\modules\set\widgets\SetWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\machine\models\Machine $machine
 * @var boolean $hasSetsTab
 * @var boolean $hasDescriptionTab
 * @var \app\modules\set\models\Set[] $sets
 * @var \app\modules\set\models\Set $firstSet
 */

?>

<?php if ($hasSetsTab || $hasDescriptionTab ): ?>

    <section class="section is-margin-top-50" id="tabs">
        <div class="container">
            <div class="section__body">
                <div class="tabs js-tabs is-equipment" role="tablist">
                    <div class="tabs__header">
                        <?php if ($hasSetsTab): ?>
                            <a class="tabs__label js-tabs-label" href="#equipment" title="Комплектации" role="tab" id="tab-1">Комплектации</a>
                        <?php endif ?>
                        <?php if ($hasDescriptionTab): ?>
                            <a class="tabs__label js-tabs-label" href="#descr" title="Описание" role="tab" id="tab-2">Описание</a>
                        <?php endif ?>
                    </div>
                    <div class="tabs__body">
                        <div class="tabs__section js-tabs-section">
                            <?php if ($hasSetsTab): ?>
                                <div class="tabs__item js-tabs-item" id="equipment" role="tabpanel" aria-labelledby="tab1">
                                    <div class="tab">
                                        <div class="equipment js-equipment" role="tablist">
                                            <div class="equipment__header">
                                                <ul class="equipment__cats">
                                                    <?php foreach ($sets as $key => $set): ?>
                                                        <a class="equipment__cat js-equipment-label" title="Комплектации <?= $set->title ?>" href="#equipment-set-<?= $key ?>" role="tab" id="tab-equipment-<?= $key ?>">
                                                            <?= SetWidget::widget(compact('set')) ?>
                                                        </a>
                                                    <?php endforeach ?>
                                                </ul>
                                            </div>
                                            <div class="equipment__body">
                                                <div class="equipment__section js-equipment-section">
                                                    <?php foreach ($sets as $key => $set): ?>
                                                        <div class="equipment__item js-equipment-item" data-info='<?= SetHelper::getTableInfoJson($set) ?>' id="equipment-set-<?= $key ?>" role="tabpanel" aria-labelledby="tab-equipment-<?= $key ?>">
                                                            <?php if (!empty($set->items)): ?>
                                                                <table class="products-table">
                                                                    <thead>
                                                                    <tr class="products-table__header">
                                                                        <th class="products-table__label">Наименование</th>
                                                                        <th class="products-table__label is-amount">Количество</th>
                                                                        <th class="products-table__label is-cost">Цена</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php foreach ($set->items as $item): ?>
                                                                        <tr class="products-table__item">
                                                                            <td class="products-table__product">
                                                                                <?= ProductSimpleWidget::widget(['product' => $item->product]) ?>
                                                                            </td>
                                                                            <td class="products-table__amount"><?= $item->quantity ?></td>
                                                                            <td class="products-table__cost">
                                                                                <div class="cost is-small">
                                                                                    <?php if (!empty($item->product->price)): ?>
                                                                                        <div class="cost__value"><?= FormatHelper::number($item->product->price) ?> <i>₽</i></div>
                                                                                    <?php else: ?>
                                                                                        <!-- <button class="cost__checkprice">Цену уточняйте →</button> -->
                                                                                    <?php endif ?>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    <?php endforeach ?>
                                                                    </tbody>
                                                                </table>
                                                            <?php endif ?>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                            <div class="equipment__footer">
                                                <div class="grid is-row">
                                                    <div class="col-6">
                                                        <div class="equipment__text text">
                                                            <h4>Обращаем ваше внимание!</h4>
                                                            <p>Финальная стоимость всей модернизации указана ориентировочно и требует уточнения, т.к. может зависеть от курса валют, инфляции и прочих факторов...</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-4 shift-2">
                                                            <div class="equipment__cost">
                                                                <div class="cost is-wide">
                                                                    <div class="cost__caption">Ориентировочная сумма:</div>
                                                                    <div class="cost__value js-equipment-cost">
                                                                        <?php if (!empty($sets[0]->cost)): ?>
                                                                            <?= FormatHelper::number($sets[0]->cost) ?> ₽
                                                                        <?php endif ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <button class="equipment__button button is-wide is-multistring js-equipment-button" data-modal="order" data-info='<?= SetHelper::getButtonInfoJson($sets[0]) ?>'>
                                                            <i class='js-equipment-caption'>набор <?= $sets[0]->title ?></i>заказать модернизацию
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if ($hasDescriptionTab): ?>
                                <div class="tabs__item js-tabs-item" id="descr" role="tabpanel" aria-labelledby="tab2">
                                    <div class="tab">
                                        <div class="text">
                                            <?= $machine->description ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif ?>
