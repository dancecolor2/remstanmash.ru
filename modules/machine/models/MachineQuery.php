<?php

namespace app\modules\machine\models;

use app\traits\active\ActiveQueryTrait;

/**
 * This is the ActiveQuery class for [[Machine]].
 *
 * @see Machine
 */
class MachineQuery extends \yii\db\ActiveQuery
{
    use ActiveQueryTrait;
}
