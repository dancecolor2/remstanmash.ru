<?php

namespace app\modules\machine\models;

use app\modules\admin\behaviors\SeoBehavior;
use app\modules\admin\traits\QueryExceptions;
use app\modules\set\models\Set;
use app\modules\set\models\SetQuery;
use app\traits\active\ActiveTrait;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\helpers\Url;
use yii2tech\ar\position\PositionBehavior;

/**
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $position
 * @property int $active
 * @property string $code
 * @property string $title
 * @property string $preview_text
 * @property string $detail_text
 * @property string $description
 * @property string $meta_d
 * @property string $meta_k
 * @property string $meta_t
 * @property string $h1
 * @property int $type_id
 *
 * @property MachineType $type
 * @property MachineImage[] $images
 * @property Set[] $sets
 *
 * @mixin SeoBehavior
 * @mixin PositionBehavior
 */
class Machine extends \yii\db\ActiveRecord
{
    use QueryExceptions;
    use ActiveTrait;

    const COUNT_ON_PAGE = 4;

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            PositionBehavior::class,
            SeoBehavior::class,
        ];
    }

    public static function tableName()
    {
        return 'machine';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            ['code', 'unique'],
            [['active', 'type_id'], 'integer'],
            [['preview_text', 'detail_text', 'description'], 'string'],
            [['code', 'title', 'meta_d', 'meta_k', 'meta_t', 'h1'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MachineType::class, 'targetAttribute' => ['type_id' => 'id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'active' => 'Активность',
            'code' => 'Артикул',
            'title' => 'Название',
            'preview_text' => 'Превью текст',
            'detail_text' => 'Текст',
            'description' => 'Описание',
            'images' => 'Картинки',
            'h1' => 'H1',
            'meta_t' => 'Заголовок страницы',
            'meta_d' => 'Описание страницы',
            'meta_k' => 'Ключевые слова',
            'type_id' => 'Тип станка',
        ];
    }

    public function getType()
    {
        return $this->hasOne(MachineType::class, ['id' => 'type_id']);
    }

    public function getImages()
    {
        return $this->hasMany(MachineImage::class, ['machine_id' => 'id'])->orderBy('position');
    }

    public static function find()
    {
        return new MachineQuery(get_called_class());
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function hasImage()
    {
        return !empty($this->images) && is_file($this->images[0]->getUploadedFilePath('image'));
    }

    public function getHref()
    {
        return Url::to(['/machine/frontend/view', 'id' => $this->id]);
    }

    /**
     * @return SetQuery
     */
    public function getSets() : ActiveQuery
    {
        return $this->hasMany(Set::class, ['machine_id' => 'id']);
    }
}
