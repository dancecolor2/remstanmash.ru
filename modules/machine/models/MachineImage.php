<?php

namespace app\modules\machine\models;

use app\modules\admin\traits\QueryExceptions;
use PHPThumb\GD;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii2tech\ar\position\PositionBehavior;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * @property integer $id
 * @property integer $machine_id
 * @property string $image
 * @property string $image_hash
 * @property string $position
 *
 * @property Machine $machine
 *
 * @mixin PositionBehavior
 * @mixin ImageUploadBehavior
 */
class MachineImage extends ActiveRecord
{
    use QueryExceptions;

    public function behaviors()
    {
        return [
            [
                'class' => PositionBehavior::class,
                'groupAttributes' => ['machine_id'],
            ],
            [
                'class' => ImageUploadBehavior::class,
                'createThumbsOnRequest' => true,
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['processor' => function (GD $thumb) {
                        $thumb->resize(370, 320);
                    }],
                    'preview' => ['processor' => function (GD $thumb) {
                        $thumb->resize(246, 240)->pad(246, 240);
                    }],
                ],
                'filePath' => '@webroot/uploads/machine_image/[[pk]]_[[attribute_image_hash]].[[extension]]',
                'fileUrl' => '/uploads/machine_image/[[pk]]_[[attribute_image_hash]].[[extension]]',
                'thumbPath' => '@webroot/uploads/cache/machine_image/[[pk]]_[[profile]]_[[attribute_image_hash]].[[extension]]',
                'thumbUrl' => '/uploads/cache/machine_image/[[pk]]_[[profile]]_[[attribute_image_hash]].[[extension]]',
            ],
        ];
    }

    public static function tableName()
    {
        return 'machine_image';
    }

    public function rules()
    {
        return [
            ['machine_id', 'required'],
            ['machine_id', 'integer'],
            ['image', 'image', 'extensions' => ['png', 'jpg', 'jpeg'], 'checkExtensionByMimeType' => false],
            [['image_hash'], 'string', 'max' => 255],
            [['machine_id'], 'exist', 'skipOnError' => true, 'targetClass' => Machine::class, 'targetAttribute' => ['machine_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'machine_id' => 'Станок',
            'image' => 'Изображение',
        ];
    }

    public function getMachines(): ActiveQuery
    {
        return $this->hasOne(Machine::class, ['id' => 'machine_id']);
    }

    public function beforeSave($insert)
    {
        if ($this->image instanceof UploadedFile) {
            $this->image_hash = uniqid();
        }
        return parent::beforeSave($insert);
    }
}
