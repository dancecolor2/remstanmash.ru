<?php

namespace app\modules\machine\models;

use app\modules\admin\traits\QueryExceptions;

/**
 * @property int $id
 * @property string $title
 *
 * @property Machine[] $machines
 */
class MachineType extends \yii\db\ActiveRecord
{
    use QueryExceptions;

    public static function tableName()
    {
        return 'machine_type';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
        ];
    }

    public function getMachines()
    {
        return $this->hasMany(Machine::class, ['type_id' => 'id']);
    }
}
