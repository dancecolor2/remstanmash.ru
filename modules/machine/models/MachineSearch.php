<?php

namespace app\modules\machine\models;

use yii\data\ActiveDataProvider;

class MachineSearch extends Machine
{
    public $parent;

    public function rules()
    {
        return [
            [['id', 'active', 'type_id'], 'integer'],
            [['title', 'code'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = Machine::find()->with('images');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => ['defaultOrder' => ['position' => SORT_ASC]],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['type_id' => $this->type_id]);
        $query->andFilterWhere(['active' => $this->active]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }
}
