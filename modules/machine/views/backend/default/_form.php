<?php

use app\modules\machine\helpers\MachineHelper;
use app\modules\machine\models\Machine;
use app\modules\machine\models\MachineImage;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;


/**
 * @var \yii\web\View $this
 * @var Machine $machine
 */

?>

<?php $form = ActiveForm::begin() ?>
    <div class="box-body">
        <?= $form->field($machine, 'active')->checkbox(); ?>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($machine, 'title') ?>
                <?= $form->field($machine, 'code') ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($machine, 'type_id')->dropDownList(MachineHelper::getTypesList()) ?>
            </div>
        </div>
        <?php if (!$machine->isNewRecord): ?>
            <?= $form->field($machine, 'images[]')->widget(FileInput::class, [
                'pluginOptions' => [
                    'uploadUrl' => Url::to(['/machine/backend/image/upload', 'id' => $machine->id]),
                    'deleteUrl' => Url::to(['/machine/backend/image/delete']),
                    'initialPreview' => array_map(function(MachineImage $image){
                        return $image->getUploadedFileUrl('image');
                    }, $machine->images),
                    'initialPreviewConfig' => array_map(function(MachineImage $image){
                        return [
                            'key' => $image->id,
                            'caption' => $image->image,
                            'size' => filesize($image->getUploadedFilePath('image')),
                            'downloadUrl' => $image->getUploadedFileUrl('image'),
                        ];
                    }, $machine->images),
                    'initialPreviewAsData' => true,
                    'overwriteInitial' => false,
                    'showClose' => false,
                    'browseClass' => 'btn btn-primary text-right',
                    'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                    'browseLabel' =>  'Выберите файл',
                ],
                'pluginEvents' => [
                    'filesorted' => 'function(event, params) { 
                        $.post("' . Url::to(['/machine/backend/image/sort']) . '",
                            { key : params.stack[params.newIndex].key, position : params.newIndex + 1 },
                        )
                    }',
                ],
                'options' => [
                    'multiple' => true,
                ],
            ]) ?>

            <?= $form->field($machine, 'preview_text')->textarea(['rows' => 4]) ?>
            <?= $form->field($machine, 'detail_text')->widget(CKEditor::class) ?>
            <?= $form->field($machine, 'description')->widget(CKEditor::class) ?>

        <?php endif ?>

    </div>
    <div class="box-footer with-border">
        <button class="btn btn-success">Сохранить</button>
    </div>
<?php ActiveForm::end() ?>