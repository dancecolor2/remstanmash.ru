<?php

/**
 * @var yii\web\View $this
 * @var app\modules\machine\models\Machine $machine
 */

$this->title = 'Добавление станка';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="box box-primary">
    <?= $this->render('_form', compact('machine')) ?>
</div>


