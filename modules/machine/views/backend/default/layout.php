<?php

use yii\bootstrap\Tabs;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $content
 * @var \app\modules\machine\models\Machine $machine
 * @var array $breadcrumbs
 */

$this->title = $machine->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Станки', 'url' => ['/machine/backend/default/index']];

if(empty($breadcrumbs)) {
    $this->params['breadcrumbs'][] = $machine->getTitle();
} else {
    $this->params['breadcrumbs'][] = [
        'label' => $machine->getTitle(),
        'url' => ['/machine/backend/default/update', 'id' => $machine->id],
    ];
}

if(!empty($breadcrumbs)) {
    foreach ($breadcrumbs as $breadcrumb) {
        $this->params['breadcrumbs'][] = $breadcrumb;
    }
}

?>
<div class="nav-tabs-custom">
<?= Tabs::widget([
    'items' => [
        [
            'label' => 'Общее',
            'url' => ['/machine/backend/default/update', 'id' => $machine->id],
            'active' => Yii::$app->controller->action->id == 'update'
                && Yii::$app->controller->id == 'backend/default'
                && Yii::$app->controller->module->id == 'machine',
        ],
        [
            'label' => 'Наборы ( ' . $machine->getSets()->count() . ' )',
            'url' => ['/set/backend/default/index', 'id' => $machine->id],
            'active' => Yii::$app->controller->module->id == 'set',
        ],
        [
            'label' => 'SEO',
            'url' => ['/machine/backend/default/seo', 'id' => $machine->id],
            'active' => Yii::$app->controller->action->id == 'seo',
        ],
        [
            'label' => 'Действия',
            'headerOptions' => ['class' => 'pull-right'],
            'items' => [
                [
                    'encode' => false,
                    'label' => '<i class="fa fa-remove text-danger" aria-hidden="true"></i>Удалить станок',
                    'url' => Url::to(['/machine/backend/default/delete', 'id' => $machine->id]),
                    'linkOptions' => [
                        'class' => 'text-danger',
                        'data-method' => 'post',
                        'data-confirm' => 'Вы уверены?',
                    ],
                ],
            ],
        ],
    ]
]) ?>

<?= $content ?>

</div>