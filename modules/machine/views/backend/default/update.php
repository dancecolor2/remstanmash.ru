<?php

/**
 * @var yii\web\View $this
 * @var \app\modules\machine\models\Machine $machine
 * @var yii\widgets\ActiveForm $form
 */

?>

<?php $this->beginContent('@app/modules/machine/views/backend/default/layout.php', ['machine' => $machine]); ?>

    <?= $this->render('_form', compact('machine')) ?>

<?php $this->endContent() ?>

