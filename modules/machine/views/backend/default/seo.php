<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var \app\modules\machine\models\Machine $machine
 */

?>

<?php $this->beginContent('@app/modules/machine/views/backend/default/layout.php', ['machine' => $machine, 'breadcrumbs' => ['SEO']]) ?>
<?php $form = ActiveForm::begin() ?>
    <div class="box-body">
        <?= $form->field($machine, 'h1') ?>
        <?= $form->field($machine, 'meta_t') ?>
        <?= $form->field($machine, 'meta_d')->textarea(['rows' => 5]) ?>
        <?= $form->field($machine, 'meta_k')->hint('Фразы через запятую') ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

<?php $form::end() ?>
<?php $this->endContent() ?>