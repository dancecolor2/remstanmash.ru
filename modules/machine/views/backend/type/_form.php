<?php

use app\modules\machine\models\MachineType;
use yii\bootstrap\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var MachineType $machineType
 */

if (!$isAjax = Yii::$app->request->isAjax) {
    $this->params['breadcrumbs'] = [
        [
            'label' => 'Партнеры',
            'url' => ['/machineType/backend/default/index'],
        ],
        $this->title,
    ];
}

?>

<?php $form = ActiveForm::begin($isAjax ? ['options' => ['id' => 'js-modal-form']] : []) ?>
<div class="modal-body">
    <?= $form->field($machineType, 'title') ?>
</div>
<div class="modal-footer">
    <button class="btn btn-success">Сохранить</button>
    <?php if ($isAjax): ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    <?php endif ?>
</div>

<?php ActiveForm::end() ?>
