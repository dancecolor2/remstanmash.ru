<?php

/**
 * @var yii\web\View $this
 * @var \app\modules\machine\models\MachineType $machineType
 */

$this->title = 'Добавление типа станков';

?>

<?php if (Yii::$app->request->isAjax): ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?= $this->title ?></h4>
    </div>
    <?= $this->render('_form', compact('machineType')); ?>
<?php else: ?>
    <div class="box">
        <div class="box-body">
            <?= $this->render('_form', compact('machineType')); ?>
        </div>
    </div>
<?php endif ?>