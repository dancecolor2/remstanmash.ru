<?php

use app\modules\filter\widgets\FilterMachinesWidget;
use app\modules\machine\widgets\MachineWidget;
use app\modules\page\components\Pages;
use app\modules\project\models\Project;
use app\modules\project\widgets\ProjectsWidget;
use app\widgets\CallbackSectionWidget;
use app\widgets\PaginationWidget;
use app\widgets\TextBottomWidget;

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\filter\forms\FilterMachines $filterMachines
 */



Pages::getCurrentPage()->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs();
$this->params['h1'] = Pages::getCurrentPage()->getH1();
$this->params['headlineClass'] = 'has-cover';
$this->params['headlineImage'] = '/img/upgrade-cover.jpg';
$this->params['headlineText'] = Pages::getElementValue('headline_text');


?>

<section class="section">
    <div class="container">
        <div class="section__body">
            <div class="collection js-collection is-sets">
                <div class="grid is-row">
                    <div class="col-3 col-m-12">
                        <div class="collection__filter">
                           <?= FilterMachinesWidget::widget(compact('filterMachines')) ?>
                        </div>
                    </div>
                    <div class="col-9 col-m-12">
                        <div class="collection__body js-collection-body">
                            <ul class="collection__list">
                                <?php foreach ($dataProvider->models as $machine): ?>
                                    <li class="collection__item">
                                        <?= MachineWidget::widget(compact('machine')) ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                            <?php if ($dataProvider->pagination->pageCount > 1): ?>
                                <div class="collection__pagination">
                                    <?= PaginationWidget::widget(['pagination' => $dataProvider->pagination]) ?>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?= ProjectsWidget::widget([
    'type' => Project::TYPE_MODERNIZATION,
    'caption' => 'Можернизация станков',
]) ?>


<?php if (!empty(Pages::getElementValue('is_show_text_bottom'))): ?>
    <?= TextBottomWidget::widget([
        'title' => Pages::getElementValue('text_bottom_title'),
        'text' => Pages::getElementValue('text_bottom'),
    ]) ?>
<?php endif ?>

<?= CallbackSectionWidget::widget() ?>