<?php

use app\modules\machine\widgets\MachineTabsSectionWidget;
use app\modules\page\components\Pages;
use app\modules\project\models\Project;
use app\modules\project\widgets\ProjectsWidget;
use app\widgets\CallbackSectionWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\machine\models\Machine $machine
 * @var \app\modules\set\models\Set[] $sets
 */

$machine->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs('upgrade');
$this->params['h1'] = $machine->getH1();

?>

<section class="section is-white with-padding is-padding-50">
    <div class="container">
        <div class="section__body">
            <div class="set-card">
                <div class="grid is-row">
                    <div class="col-4">
                        <?php if (!empty($machine->images)): ?>
                            <div class="set-card__slider">
                                <div class="slider js-slider">
                                    <div class="slider__inner">
                                        <div class="slider__container js-slider-container">
                                            <div class="slider__wrapper js-slider-wrapper">
                                                <?php foreach ($machine->images as $image): ?>
                                                    <a class="slider__image js-slider-image" href="<?= $image->getImageFileUrl('image') ?>" style="background-image: url(<?= $image->getThumbFileUrl('image') ?>)"></a>
                                                <?php endforeach ?>
                                            </div>
                                        </div>
                                        <div class="slider__list js-slider-thumbnails">
                                            <ul class="slider__wrapper js-slider-wrapper is-roll">
                                                <?php foreach ($machine->images as $image): ?>
                                                    <li class="slider__item js-slider-item">
                                                        <div class="slider__thumbnail" style="background-image: url(<?= $image->getThumbFileUrl('image') ?>)"></div>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="col-7">
                        <div class="set-card__info">
                            <?php if (!empty($machine->code)): ?>
                                <div class="set-card__code">Арт. <?= $machine->code ?></div>
                            <?php endif ?>
                            <div class="set-card__text">
                                <div class="text">
                                    <?= $machine->detail_text ?>
                                </div>
                            </div>
                            <?php if (empty($sets)): ?>
                                <div class="set-card__variations">
                                    <div class="variations">
                                        <h4 class="variations__title">Возможные наборы по модернизации</h4>
                                        <ul class="variations__list list">
                                            <?php foreach ($sets as $set): ?>
                                                <li class="variations__item list__item">
                                                    <div class="variations__name">Набор комплектации <?= $set->title ?></div>
                                                    <?php if (!empty($set->cost)): ?>
                                                        <div class="variations__cost">от <?= $set->cost ?> ₽</div>
                                                    <?php endif ?>
                                                </li>
                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="set-card__link">
                                    <a class="link is-down" href="#tabs" title="" data-scroll-to="data-scroll-to"><span class="link__label">комплектации по модернизации</span>
                                        <svg class="link__arrow">
                                            <use xlink:href="/img/sprite.svg#arrow-small"></use>
                                        </svg>
                                    </a>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?= MachineTabsSectionWidget::widget(compact('machine', 'sets')) ?>

<?= ProjectsWidget::widget([
    'type' => Project::TYPE_MODERNIZATION,
    'caption' => 'Можернизация станков',
]) ?>

<?= CallbackSectionWidget::widget() ?>
