<?php

namespace app\modules\machine;

use app\modules\page\components\Pages;
use yii\base\BootstrapInterface;
use yii\web\GroupUrlRule;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public function bootstrap($app)
{
    $parentAlias = Pages::getPage('upgrade')->alias;
    if (empty($parentAlias)) {
        $parentAlias = 'upgrade';
    }

    $app->get('urlManager')->rules[] = new GroupUrlRule([
        'rules' => [
            '/' . $parentAlias . '/<id>' => '/machine/frontend/view',
        ]
    ]);
}
}
