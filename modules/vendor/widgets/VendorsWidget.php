<?php

namespace app\modules\vendor\widgets;

use app\modules\vendor\models\Vendor;
use yii\base\Widget;

/**
 * @property boolean $showButtonToVendors
 */
class VendorsWidget extends Widget
{
    public $showButtonToVendors = false;

    public function run()
    {
        $vendors = Vendor::find()->active()->orderBy('position')->all();

        return $this->render('vendors', [
            'vendors' => $vendors,
            'showButtonToVendors' => $this->showButtonToVendors,
        ]);
    }
}