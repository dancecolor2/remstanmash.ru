<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\vendor\models\Vendor $vendor
 */

?>


<a class="vendor" href="<?= $vendor->getHref() ?>" title="<?= $vendor->title ?>">
    <div class="vendor__cover">
        <img class="vendor__logo" src="<?= $vendor->getThumbFileUrl('image') ?>" alt="<?= $vendor->title ?>"/>
    </div>
    <p class="vendor__caption"><?= $vendor->hint ?></p>
</a>