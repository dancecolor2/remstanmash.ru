<?php

use app\modules\page\components\Pages;
use app\modules\vendor\widgets\VendorWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\vendor\models\Vendor[] $vendors
 * @var boolean $showButtonToVendors
 */

?>

<?php if (!empty($vendors)): ?>
    <section class="section is-white has-roll with-padding">
        <div class="container">
            <header class="section__header">
                <h2 class="section__title">Наши партнеры</h2>
            </header>
            <div class="section__body">
                <div class="roll js-roll is-vendors" data-options="{&quot;spaceBetween&quot;:0,&quot;slidesPerGroup&quot;:4}">
                    <div class="roll__control js-roll-control">
                        <div class="control">
                            <button class="control__arrow is-prev js-roll-prev" type="button" area-label="Назад">
                                <span class="arrow is-left">
                                    <svg class="arrow__icon">
                                      <use xlink:href="/img/sprite.svg#arrow-alt"></use>
                                    </svg>
                                </span>
                            </button>
                            <button class="control__arrow is-next js-roll-next" type="button" area-label="Вперёд">
                                <span class="arrow is-right">
                                    <svg class="arrow__icon">
                                      <use xlink:href="/img/sprite.svg#arrow-alt"></use>
                                    </svg>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="roll__inner js-roll-swiper">
                        <ul class="roll__list js-roll-list">
                            <?php foreach ($vendors as $vendor): ?>
                                <li class="roll__item js-roll-item">
                                    <?= VendorWidget::widget(compact('vendor')) ?>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                    <?php if ($showButtonToVendors): ?>
                        <div class="roll__footer">
                            <div class="roll__button"><a class="button js-button is-hollow" area-label="сделать подбор по производителю" href="<?= Pages::getHref('vendors') ?>">сделать подбор по производителю</a>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>
