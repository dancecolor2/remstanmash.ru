<?php

namespace app\modules\vendor\controllers;

use app\modules\filter\forms\FilterInVendor;
use app\modules\vendor\models\Vendor;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class FrontendController extends  Controller
{
    public function actionIndex()
    {
        $vendors = Vendor::find()->active()->orderBy('position')->all();

        return $this->render('index', compact('vendors'));
    }

    public function actionView($alias)
    {
        $vendor = Vendor::findOneOrException([
            'alias' => $alias,
            'active' => 1,
        ]);

        $filterInVendor = new FilterInVendor(compact('vendor'));
        $filterInVendor->load(Yii::$app->request->get());
        $dataProvider = $filterInVendor->filter();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'html' => $this->renderAjax('filter', compact('dataProvider'))
            ];
        }

        return $this->render('view', compact('vendor', 'filterInVendor', 'dataProvider'));
    }
}