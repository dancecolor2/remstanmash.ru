<?php

namespace app\modules\vendor\controllers;

use app\modules\admin\components\BalletController;
use app\modules\vendor\models\Vendor;
use app\modules\vendor\models\VendorSearch;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\Response;

class BackendController extends BalletController
{
    public function actionIndex()
    {
        $searchModel = new VendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    public function actionCreate()
    {
        $vendor = new Vendor([
            'active' => 1,
            'scenario' => Vendor::SCENARIO_CREATE,
        ]);

        if (Yii::$app->request->isAjax && $vendor->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($vendor);
        }

        if ($vendor->load(Yii::$app->request->post()) && $vendor->save()) {
            Yii::$app->session->setFlash('success', 'Производитель создан');
            return $this->redirect(['update', 'id' => $vendor->id]);
        }

        return $this->render('create', compact('vendor'));
    }

    public function actionUpdate($id)
    {
        $vendor = Vendor::findOneOrException($id);

        if (Yii::$app->request->isAjax && $vendor->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($vendor);
        }

        if ($vendor->load(Yii::$app->request->post()) && $vendor->save()) {
            Yii::$app->session->setFlash('success', 'Производитель обновлен');
            return $this->refresh();
        }

        return $this->render('update', compact('vendor'));
    }

    public function actionDelete($id)
    {
        Vendor::findOneOrException($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMoveUp($id)
    {
        Vendor::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        Vendor::findOneOrException($id)->moveNext();
    }

    public function actionSeo($id)
    {
        $vendor = Vendor::findOneOrException($id);

        if ($vendor->load(Yii::$app->request->post()) && $vendor->save()) {
            Yii::$app->session->setFlash('success', 'Производитель обновлен');
            return $this->refresh();
        }

        return $this->render('seo', compact('vendor'));
    }
}