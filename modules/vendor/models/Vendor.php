<?php

namespace app\modules\vendor\models;

use app\modules\admin\behaviors\SeoBehavior;
use app\modules\admin\traits\QueryExceptions;
use app\modules\product\models\Product;
use app\traits\active\ActiveTrait;
use PHPThumb\GD;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii2tech\ar\position\PositionBehavior;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * This is the model class for table "vendor".
 *
 * @property int $id
 * @property string $title Название
 * @property string $alias Алиас
 * @property string $content Контент
 * @property string $hint
 * @property string $h1
 * @property string $meta_t
 * @property string $meta_d
 * @property string $meta_k
 * @property string $image
 * @property string $image_hash
 * @property int $position
 *
 * @property Product[] $products
 *
 * @mixin PositionBehavior
 * @mixin ImageUploadBehavior
 * @mixin SeoBehavior
 */
class Vendor extends ActiveRecord
{
    use QueryExceptions;
    use ActiveTrait;

    const SCENARIO_CREATE = 'scenario_create';

    public function behaviors()
    {
        return [
            SeoBehavior::class,
            PositionBehavior::class,
            'image' => [
                'class' => ImageUploadBehavior::class,
                'attribute' => 'image',
                'createThumbsOnRequest' => true,
                'thumbs' => [
                    'thumb' => [
                        'processor' => function(GD $thumb) {
                            return $thumb->resize(225, 45)->pad(225, 45, [255, 255, 255]);
                        },
                    ],
                ],
                'filePath' => '@webroot/uploads/vendor/[[pk]]_[[attribute_image_hash]].[[extension]]',
                'fileUrl' => '/uploads/vendor/[[pk]]_[[attribute_image_hash]].[[extension]]',
                'thumbPath' => '@webroot/uploads/cache/vendor/[[pk]]_[[attribute_image_hash]]_[[profile]].[[extension]]',
                'thumbUrl' => '/uploads/cache/vendor/[[pk]]_[[attribute_image_hash]]_[[profile]].[[extension]]',
            ],
        ];
    }

    public static function tableName()
    {
        return 'vendor';
    }

    public function rules()
    {
        return [
            [['title', 'alias'], 'required'],
            [['alias'], 'unique'],
            [['content', 'hint'], 'string'],
            [['position', 'active'], 'integer'],
            [['title', 'alias', 'h1', 'meta_k', 'meta_t', 'meta_d'], 'string', 'max' => 255],
            [['image'], 'image', 'skipOnEmpty' => false, 'on' => self::SCENARIO_CREATE, 'extensions' => ['png', 'jpeg', 'jpg']],
            [['image'], 'image', 'extensions' => ['png', 'jpeg', 'jpg']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'alias' => 'Алиас',
            'content' => 'Контент',
            'hint' => 'Краткое описание',
            'image' => 'Картинка для анонса',
            'active' => 'Активность',
            'meta_t' => 'Заголовок страницы',
            'meta_d' => 'Описание страницы',
            'meta_k' => 'Ключевые слова',
            'h1' => 'H1',
        ];
    }

    public static function find()
    {
        return new VendorQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if ($this->image instanceof UploadedFile) {
            $this->image_hash = uniqid();
        }
        return parent::beforeSave($insert);
    }

    public function deleteImage()
    {
        /** @var ImageUploadBehavior $imageBehavior */
        $imageBehavior = $this->getBehavior('image');
        $imageBehavior->cleanFiles();
        $this->updateAttributes(['image' => null, 'image_hash' => null]);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getHref()
    {
        return Url::to(['/vendor/frontend/view', 'alias' => $this->alias]);
    }

    public function getH1()
    {
        return $this->h1 ?? $this->title;
    }

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['vendor_id' => 'id']);
    }
}
