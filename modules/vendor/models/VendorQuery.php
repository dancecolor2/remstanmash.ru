<?php

namespace app\modules\vendor\models;

use app\traits\active\ActiveQueryTrait;
use yii\db\ActiveQuery;

/**
 * @see Vendor
 */
class VendorQuery extends ActiveQuery
{
    use ActiveQueryTrait;
}