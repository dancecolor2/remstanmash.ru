<?php

use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var \app\modules\vendor\models\Vendor $vendor
 */

?>

<?php $form = ActiveForm::begin() ?>
<div class="box-body">
    <div class="row">
        <div class="col-lg-8">
            <?= $form->field($vendor, 'active')->checkbox() ?>
            <?= $form->field($vendor, 'title')->textInput(['class' => 'form-control transIt']) ?>
            <?= $form->field($vendor, 'alias', ['enableAjaxValidation' => true])->textInput( $vendor->isNewRecord ? [
                'class' => 'form-control transTo',
            ] : []) ?>
            <?= $form->field($vendor, 'hint') ?>
        </div>
        <div class="col-lg-4">
            <div class="single-kartik-image">
                <?= $form->field($vendor, 'image')->widget(FileInput::class, [
                    'pluginOptions' => [
                        'fileActionSettings' => [
                            'showDrag' => false,
                            'showZoom' => true,
                            'showUpload' => false,
                            'showDelete' => false,
                            'showDownload' => true,
                        ],
                        'initialPreviewDownloadUrl' => $vendor->getUploadedFileUrl('image'),
                        'layoutTemplates' => ['actionDelete' => ''],
                        'showCaption' => false,
                        'showRemove' => false,
                        'showUpload' => false,
                        'showClose' => false,
                        'showCancel' => false,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                        'browseLabel' =>  'Выберите файл',
                        'initialPreview' => [
                            $vendor->getUploadedFileUrl('image'),
                        ],
                        'initialPreviewAsData' => true,
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <?= $form->field($vendor, 'content')->widget(CKEditor::class) ?>
</div>
<div class="box-footer">
    <button class="btn btn-success">Сохранить</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
</div>

<?php ActiveForm::end() ?>
