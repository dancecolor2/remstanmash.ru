<?php

/**
 * @var yii\web\View $this
 * @var \app\modules\vendor\models\Vendor $vendor
 */

$this->title = 'Добавление производителя';

?>

<div class="box box-primary">
    <?= $this->render('_form', compact('vendor')) ?>
</div>
