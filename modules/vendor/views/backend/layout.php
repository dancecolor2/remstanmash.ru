<?php

use yii\bootstrap\Tabs;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $content
 * @var \app\modules\vendor\models\Vendor $vendor
 * @var array $breadcrumbs
 */

$this->title = 'Редактирование: ' . $vendor->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Производители', 'url' => ['index']];

if(Yii::$app->controller->action->id == 'update') {
    $this->params['breadcrumbs'][] = $vendor->getTitle();
} else {
    $this->params['breadcrumbs'][] = [
        'label' => $vendor->getTitle(),
        'url' => ['/vendor/backend/update', 'id' => $vendor->id],
    ];
}

if(!empty($breadcrumbs)) {
    foreach ($breadcrumbs as $breadcrumb) {
        $this->params['breadcrumbs'][] = $breadcrumb;
    }
}

?>
<div class="nav-tabs-custom">
    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'url' => ['/vendor/backend/update', 'id' => $vendor->id],
                'active' => Yii::$app->controller->action->id == 'update',
            ],
            [
                'label' => 'SEO',
                'url' => ['/vendor/backend/seo', 'id' => $vendor->id],
                'active' => Yii::$app->controller->action->id == 'seo',
            ],
            [
                'label' => 'Действия',
                'headerOptions' => ['class' => 'pull-right'],
                'items' => [
                    [
                        'encode' => false,
                        'label' => '<i class="fa fa-remove text-danger" aria-hidden="true"></i>Удалить',
                        'url' => Url::to(['/vendor/backend/delete', 'id' => $vendor->id]),
                        'linkOptions' => [
                            'class' => 'text-danger',
                            'data-method' => 'post',
                            'data-confirm' => 'Вы уверены?',
                        ],
                    ],
                ],
            ],
        ]
    ]) ?>

    <?= $content ?>

</div>