<?php

use yii\bootstrap\ActiveForm;

/**
 * @var \app\modules\vendor\models\Vendor $vendor
 */

?>

<?php $this->beginContent('@app/modules/vendor/views/backend/layout.php', ['vendor' => $vendor, 'breadcrumbs' => ['SEO']]) ?>
<?php $form = ActiveForm::begin() ?>
    <div class="box-body">
        <?= $form->field($vendor, 'h1') ?>
        <?= $form->field($vendor, 'meta_t') ?>
        <?= $form->field($vendor, 'meta_d')->textarea(['rows' => 5]) ?>
        <?= $form->field($vendor, 'meta_k')->hint('Фразы через запятую') ?>
    </div>
    <div class="box-footer">
        <button class="btn btn-success">Сохранить</button>
    </div>
<?php $form::end() ?>
<?php $this->endContent() ?>