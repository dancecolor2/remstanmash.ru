<?php

/**
 * @var yii\web\View $this
 * @var \app\modules\vendor\models\Vendor $vendor
 */

$this->title = 'Редактирование производителя';

?>

<?php $this->beginContent('@app/modules/vendor/views/backend/layout.php', ['vendor' => $vendor]); ?>

    <?= $this->render('_form', compact('vendor')) ?>

<?php $this->endContent() ?>