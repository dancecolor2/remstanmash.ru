<?php

use app\modules\product\widgets\ProductWidget;
use app\widgets\PaginationWidget;

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

?>

<div class="collection__body js-collection-body">
    <ul class="collection__list">
        <?php foreach ($dataProvider->models as $product): ?>
            <li class="collection__item">
                <?= ProductWidget::widget(compact('product')) ?>
            </li>
        <?php endforeach ?>
    </ul>
    <?php if ($dataProvider->pagination->pageCount > 1): ?>
        <div class="collection__pagination">
            <?= PaginationWidget::widget(['pagination' => $dataProvider->pagination]) ?>
        </div>
    <?php endif ?>
</div>