<?php

use app\modules\page\components\Pages;
use app\modules\vendor\widgets\VendorWidget;
use app\widgets\CallbackSectionWidget;
use app\widgets\TextBottomWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\vendor\models\Vendor[] $vendors
 */

Pages::getCurrentPage()->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs();
$this->params['h1'] = Pages::getCurrentPage()->getH1();

?>

<?php if (!empty($vendors)): ?>
    <section class="section">
        <div class="container">
            <div class="section__body">
                <div class="vendors">
                    <ul class="vendors__list grid">
                        <?php foreach ($vendors as $vendor): ?>
                            <li class="vendors__item col-3 col-m-6 col-s-12">
                                <?= VendorWidget::widget(compact('vendor')) ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?php if (!empty(Pages::getElementValue('is_show_text_bottom'))): ?>
    <?= TextBottomWidget::widget([
        'title' => Pages::getElementValue('text_bottom_title'),
        'text' => Pages::getElementValue('text_bottom'),
    ]) ?>
<?php endif ?>

<?= CallbackSectionWidget::widget() ?>