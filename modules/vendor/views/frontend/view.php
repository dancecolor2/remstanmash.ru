<?php

use app\modules\filter\widgets\FilterInVendorWidget;
use app\modules\page\components\Pages;
use app\modules\product\widgets\ProductWidget;
use app\widgets\CallbackSectionWidget;
use app\widgets\PaginationWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\vendor\models\Vendor $vendor
 * @var \app\modules\filter\forms\FilterInVendor $filterInVendor
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$vendor->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs('vendors');
$this->params['h1'] = $vendor->getH1();

?>

<section class="section with-padding">
    <div class="container">
        <div class="section__body">
            <div class="collection js-collection is-products">
                <div class="grid is-row">
                    <div class="col-3 col-m-12">
                        <div class="collection__filter">
                            <?= FilterInVendorWidget::widget(compact('filterInVendor')) ?>
                        </div>
                    </div>
                    <div class="col-9 col-m-12">
                        <div class="collection__vendor">
                            <div class="vendor is-full">
                                <div class="vendor__cover">
                                    <img class="vendor__logo" src="<?= $vendor->getThumbFileUrl('image') ?>" alt="<?= $vendor->getTitle() ?>">
                                </div>
                                <p class="vendor__caption"><?= $vendor->hint ?></p>
                            </div>
                        </div>
                        <div class="collection__body js-collection-body">
                            <ul class="collection__list">
                                <?php foreach ($dataProvider->models as $product): ?>
                                    <li class="collection__item">
                                        <?= ProductWidget::widget(compact('product')) ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                            <?php if ($dataProvider->pagination->pageCount > 1): ?>
                                <div class="collection__pagination">
                                    <?= PaginationWidget::widget(['pagination' => $dataProvider->pagination]) ?>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if (!empty($vendor->content)): ?>
    <section class="section is-white with-padding is-layout-3">
        <div class="container">
            <div class="grid is-row">
                <div class="col-4 col-m-12">
                    <div class="section__content">
                        <section class="card is-alt has-right-shadow">
                            <h4 class="card__title">ремстанмаш — это решение конкретных задач</h4>
                            <div class="card__body">
                                <p class="card__caption">Исключительно профессиональная и&nbsp;информативная поддержка всех наших клиентов</p>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="shift-1 col-7 col-m-12 shift-m-0">
                    <div class="section__body">
                        <div class="section__text text"><?= $vendor->content ?></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?= CallbackSectionWidget::widget() ?>