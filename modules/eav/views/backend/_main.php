<?php

use app\modules\eav\models\EavAttributeType;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\modules\eav\models\EavAttribute $model
 */

$this->title = 'Обновить ' . ' ' . $model->getLabel();
$this->params['breadcrumbs'][] = ['label' => 'Аттрибуты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->getLabel();

?>

<?php $this->beginContent('@app/modules/eav/views/backend/update.php', compact('model')) ?>
    <?php $form = ActiveForm::begin() ?>

        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-8">
                        <?= $form->field($model, 'title') ?>
                        <?= $form->field($model, 'name') ?>
                        <?= $form->field($model, 'label') ?>
                        <?= $form->field($model, 'unit') ?>
                        <?= $form->field($model, 'type_id')->dropDownList(EavAttributeType::getList()) ?>
                    </div>
                    <div class="col-xs-4">
                        <?= $form->field($model, 'is_important')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>
                        <?= $form->field($model, 'is_child_preview')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-success btn-block" type="submit">Сохранить</button>
                </div>
            </div>
        </div>

    <?php ActiveForm::end() ?>
<?php $this->endContent(); ?>