<?php

use app\modules\eav\models\EavAttributeType;
use yii\bootstrap\ActiveForm;


/**
 * @var yii\web\View $this
 * @var app\modules\eav\models\EavAttribute $model
 */

$this->title = 'Добавление аттрибута';
$this->params['breadcrumbs'][] = ['label' => 'Аттрибуты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $form = ActiveForm::begin() ?>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'title')->textInput(['class' => 'form-control transItName']) ?>
                <?= $form->field($model, 'name')->textInput(['class' => 'form-control transToAlias']) ?>
                <?= $form->field($model, 'label') ?>
                <?= $form->field($model, 'unit') ?>
                <?= $form->field($model, 'type_id')->dropDownList(EavAttributeType::getList()) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'is_important')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>
                <?= $form->field($model, 'is_child_preview')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button class="btn btn-success" type="submit">Сохранить</button>
    </div>
</div>
<?php ActiveForm::end() ?>
