<?php

namespace app\modules\news;

use yii\base\BootstrapInterface;
use yii\web\GroupUrlRule;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->get('urlManager')->rules[] = new GroupUrlRule([
            'rules' => [
                '/news' => '/news/frontend/index',
                '/news/<alias>' => '/news/frontend/view',
            ],
        ]);
    }
}
