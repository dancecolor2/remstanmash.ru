<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\news\models\News $news
 * @var \app\modules\product\models\Product $products
*/

$this->title = 'Создать новость';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="nav-tabs-custom">
    <?= $this->render('_form', compact('news', 'products')) ?>
</div>