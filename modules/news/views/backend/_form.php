<?php

use app\helpers\FileHelper;
use kartik\file\FileInput;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var \app\modules\news\models\News $news
 * @var array $products
 */

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12 col-lg-8">
                <?= $form->field($news, 'active')->checkbox() ?>
                <?= $form->field($news, 'title')->textInput(['class' => 'form-control transIt']) ?>
                <?= $form->field($news, 'alias')->textInput($news->isNewRecord ? [ //, ['enableAjaxValidation' => true]
                    'class' => 'form-control transTo',
                ] : []) ?>
                <?= $form->field($news, 'description')->textarea(['rows' => 5]) ?>
            </div>
            <div class="col-xs-12 col-lg-4">
                <div class="single-kartik-image">
                    <?= $form->field($news, 'image')->widget(FileInput::class, [
                        'pluginOptions' => [
                            'fileActionSettings' => [
                                'showDrag' => false,
                                'showZoom' => true,
                                'showUpload' => false,
                                'showDelete' => false,
                                'showDownload' => true,
                            ],
                            'initialPreviewDownloadUrl' => $news->getUploadedFileUrl('image'),
                            'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'showClose' => false,
                            'showCancel' => false,
                            'browseClass' => 'btn btn-primary btn-block',
                            'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                            'browseLabel' => 'Выберите файл',
                            'deleteUrl' => Url::to(['delete-image', 'id' => $news->id]),
                            'initialPreview' => [
                                $news->getUploadedFileUrl('image'),
                            ],
                            'initialPreviewConfig' => [!$news->hasImage() ? [] :
                                [
                                    'caption' => $news->image,
                                    'size' => filesize($news->getUploadedFilePath('image')),
                                    'filetype'=> FileHelper::getMimeTypeByExtension($news->getUploadedFilePath('image')),
                                ]
                            ],
                            'initialPreviewAsData' => true,
                        ],
                        'options' => ['accept' => 'image/png, image/jpeg, image/pjpeg'],
                    ]) ?>
                </div>
            </div>
        </div>
        <?= $form->field($news, 'content')->widget(CKEditor::class) ?>
        <?= $form->field($news, 'productsIds')->widget(Select2::class, [
            'data' => $products,
            'options' => ['placeholder' => '...', 'autocomplete' => 'off'],
            'showToggleAll' => false,
            'theme' => Select2::THEME_BOOTSTRAP,
            'pluginOptions' => [
                'multiple' => true,
	            'allowClear' => false,
	            'minimumInputLength' => 3,
	            'ajax' => [
		            'url' => Url::to(['/news/backend/products']),
		            'dataType' => 'json',
                    'cache' => false,
	            ],
            ],
        ]) ?>
    </div>
    <div class="box-footer with-border">
        <button class="btn btn-success">Сохранить</button>
        <a class="btn btn-default" href="<?= Url::to(['index']) ?>">Отмена</a>
    </div>
<?php ActiveForm::end() ?>
