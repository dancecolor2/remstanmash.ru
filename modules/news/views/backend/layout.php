<?php

use yii\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var string $content
 * @var \app\modules\news\models\News $news
 */

?>
<div class="nav-tabs-custom">
<?= Tabs::widget([
    'items' => [
        [
            'options' => ['class' => 'pull-left'],
            'label' => 'Общее',
            'url' => ['/news/backend/update', 'id' => $news->id],
            'active' => Yii::$app->controller->action->id == 'update',
        ],
        [

            'label' => 'SEO',
            'url' => ['/news/backend/seo', 'id' => $news->id],
            'active' => Yii::$app->controller->action->id == 'seo',
        ],
        [
            'label' => 'Действия',
            'headerOptions' => ['class' => 'pull-right'],
            'items' => [
                [
                    'encode' => false,
                    'label' => '<i class="fa fa-remove text-danger" aria-hidden="true"></i>Удалить новость',
                    'url' => \yii\helpers\Url::to(['/news/backend/delete', 'id' => $news->id]),
                    'linkOptions' => [
                        'class' => 'text-danger',
                        'data-method' => 'post',
                        'data-confirm' => 'Вы уверены?',
                    ],
                ],
            ],
        ],
    ]
]) ?>

<?= $content ?>

</div>