<?php

/**
 * @var $this yii\web\View
 * @var \app\modules\news\models\News $news
 * @var array $products
 */

$this->title = 'Редактирование новости';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $news->title];

?>

<?php $this->beginContent('@app/modules/news/views/backend/layout.php', compact('news')) ?>

<?= $this->render('_form', compact('news', 'products')) ?>

<?php $this->endContent() ?>