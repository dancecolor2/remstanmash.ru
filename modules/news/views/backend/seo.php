<?php

use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var \app\modules\news\models\News $news
 */

$this->title = 'SEO';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $news->title, 'url' => ['backend/update', 'id' => $news->id]];
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $this->beginContent('@app/modules/news/views/backend/layout.php', compact('news')) ?>

    <?php $form = ActiveForm::begin() ?>

    <div class="box-body">
        <?= $form->field($news, 'h1') ?>
        <?= $form->field($news, 'meta_t') ?>
        <?= $form->field($news, 'meta_d')->textarea(['rows' => 5]) ?>
        <?= $form->field($news, 'meta_k')->hint('Фразы через запятую') ?>
    </div>
    <div class="box-footer with-border">
        <button class="btn btn-success">Сохранить</button>
    </div>

<?php ActiveForm::end() ?>
<?php $this->endContent() ?>
