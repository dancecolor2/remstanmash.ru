<?php

use app\helpers\DateHelper;
use app\widgets\ArticleWidget;
use app\modules\page\components\Pages;
use app\modules\product\widgets\ProductCompactWidget;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \app\modules\news\models\News $news
 * @var \app\modules\product\models\Product[] $products
 */

$news->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs('news');
$this->params['h1'] = $news->getH1();
$this->params['headlineClass'] = 'is-margin-bottom-0 has-cover';
$this->params['headlineImage'] = $news->getThumbFileUrl('image', 'origin');
$this->params['headlineDate'] = DateHelper::forHumanShortMonth($news->created_at);

?>

<div class="article">
    <div class="container">
        <div class="article__body">
            <div class="grid is-row">
                <div class="col-10 shift-1 ">
                    <div class="text">
                        <?= ArticleWidget::widget(['article' => $news->content]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="article__footer">
            <div class="article__back">
                <a class="link is-back" href="<?= Url::to(['/news/frontend/index']) ?>" title="назад к новостям">
                    <span class="link__label">назад к новостям</span>
                    <svg class="link__arrow"><use xlink:href="/img/sprite.svg#arrow"></use></svg>
                </a>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($products)): ?>
    <section class="section has-roll is-grey">
        <div class="container">
            <header class="section__header">
                <p class="section__caption">В данной статье</p>
                <h2 class="section__title">Использовались комплектующие</h2>
            </header>
            <div class="section__body">
                <div class="roll js-roll is-products">
                    <div class="roll__control js-roll-control">
                        <div class="control">
                            <button class="control__arrow is-prev js-roll-prev" type="button" area-label="Назад" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true">
                                <span class="arrow is-left">
                                    <svg class="arrow__icon">
                                        <use xlink:href="/img/sprite.svg#arrow-alt"></use>
                                    </svg>
                                </span>
                            </button>
                            <button class="control__arrow is-next js-roll-next" type="button" area-label="Вперёд" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false">
                                <span class="arrow is-right">
                                    <svg class="arrow__icon">
                                        <use xlink:href="/img/sprite.svg#arrow-alt"></use>
                                    </svg>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="roll__inner js-roll-swiper">
                        <ul class="roll__list js-roll-list" style="transform: translate3d(0px, 0px, 0px);">
                            <?php foreach ($products as $product): ?>
                                <li class="roll__item js-roll-item">
                                    <?= ProductCompactWidget::widget(compact('product')) ?>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>