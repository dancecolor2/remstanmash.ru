<?php

use app\modules\news\widgets\NewsWidget;
use app\modules\page\components\Pages;
use app\widgets\PaginationWidget;

/**
 * @var yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs();
$this->params['h1'] = Pages::getCurrentPage()->getH1();
Pages::getCurrentPage()->generateMetaTags();

?>

<section class="section">
    <div class="container">
        <div class="section__body">
            <div class="posts">
                <ul class="posts__list grid is-columns">
                    <?php foreach ($dataProvider->models as $k => $news):?>
                        <li class="posts__item col-4 col-m-6 col-s-12">
                            <?= NewsWidget::widget(compact('news')) ?>
                        </li>
                    <?php endforeach ?>
                </ul>
                <div class="posts__footer">
                    <div class="posts__pagination">
                        <div class="pagination">
                            <?= PaginationWidget::widget(['pagination' => $dataProvider->pagination]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>