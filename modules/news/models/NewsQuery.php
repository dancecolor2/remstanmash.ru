<?php

namespace app\modules\news\models;

use yii\db\ActiveQuery;

class NewsQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['active' => News::ACTIVE_ON]);
    }
}
