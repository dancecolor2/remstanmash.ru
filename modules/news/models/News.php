<?php

namespace app\modules\news\models;

use app\modules\admin\behaviors\SeoBehavior;
use app\modules\admin\traits\QueryExceptions;
use app\modules\product\models\Product;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii2tech\ar\linkmany\LinkManyBehavior;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $created_at Создано
 * @property int $updated_at Обновлено
 * @property int $active Активность
 * @property int $alias
 * @property string $title Заголовок
 * @property string $image Картинка
 * @property string $description Описание для анонса
 * @property string $content Контент
 * @property string $h1
 * @property string $meta_t
 * @property string $meta_d
 * @property string $meta_k
 *
 * @property Product[] $products
 *
 * @mixin ImageUploadBehavior
 * @mixin SeoBehavior
 */
class News extends ActiveRecord
{
    use QueryExceptions;

    const ACTIVE_ON = 1;
    const ACTIVE_OFF = 0;

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            SeoBehavior::class,
            'image' => [
                'class' => ImageUploadBehavior::class,
                'createThumbsOnRequest' => true,
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 370, 'height' => 220],
                    'origin' => ['width' => 1425, 'height' => 432],
                ],
                'filePath' => '@webroot/uploads/news/image_[[pk]].[[extension]]',
                'fileUrl' => '/uploads/news/image_[[pk]].[[extension]]',
                'thumbPath' => '@webroot/uploads/cache/news/image_[[md5_attribute_image]]_[[profile]]_[[pk]].[[extension]]',
                'thumbUrl' => '/uploads/cache/news/image_[[md5_attribute_image]]_[[profile]]_[[pk]].[[extension]]',
            ],
            [
                'class' => LinkManyBehavior::class,
                'relation' => 'products',
                'relationReferenceAttribute' => 'productsIds',
            ]
        ];
    }

    public static function tableName()
    {
        return 'news';
    }

    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

    public function rules()
    {
        return [
            ['image', 'image', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'jpeg']],
            [['title', 'alias', 'description', 'content'], 'required'],
            ['alias', 'unique'],
            [['active'], 'boolean'],
            [['description', 'content'], 'string'],
            [['title', 'h1', 'meta_t', 'meta_d', 'meta_k'], 'string', 'max' => 255],
            [['productsIds'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'active' => 'Активность',
            'title' => 'Заголовок',
            'alias' => 'Алиас',
            'image' => 'Детальная картинка',
            'description' => 'Описание для анонса',
            'content' => 'Контент',
            'h1' => 'H1',
            'meta_t' => 'Заголовок страницы',
            'meta_d' => 'Описание страницы',
            'meta_k' => 'Ключевые слова',
            'productsIds' => 'Товары',
        ];
    }

    public function getHref()
    {
        return Url::to(['/news/frontend/view', 'alias' => $this->alias]);
    }

    public function hasImage()
    {
        return !empty($this->image) && file_exists($this->getUploadedFilePath('image'));
    }

    public function deleteImage()
    {
        /** @var ImageUploadBehavior $imageBehavior */
        $imageBehavior = $this->getBehavior('image');
        $imageBehavior->cleanFiles();
        $this->updateAttributes(['image' => null]);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function isActive()
    {
        return $this->active == self::ACTIVE_ON;
    }

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id'])->viaTable('news_products', ['news_id' => 'id']);
    }
}
