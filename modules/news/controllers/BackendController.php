<?php

namespace app\modules\news\controllers;

use app\modules\admin\components\BalletController;
use app\modules\news\models\NewsSearch;
use app\modules\news\models\News;
use app\modules\product\models\Product;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;

class BackendController extends BalletController
{
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', compact('dataProvider', 'searchModel'));
    }

    public function actionCreate()
    {
        $news = new News(['active' => true]);

        if (Yii::$app->request->isAjax && $news->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($news);
        }

        if ($news->load(Yii::$app->request->post()) && $news->save()) {
            return $this->redirect(['update', 'id' => $news->id]);
        }

        $products = [];

        return $this->render('create', compact('news', 'products'));
    }

    public function actionUpdate($id)
    {
        $news = News::findOneOrException($id);

        if (Yii::$app->request->isAjax && $news->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($news);
        }

        if ($news->load(Yii::$app->request->post()) && $news->save()) {
            return $this->refresh();
        }

        $products = $news->getProducts()->orderBy(['title' => SORT_ASC])->select(['title'])->indexBy('id')->column();

        return $this->render('update', compact('news', 'products'));
    }

    public function actionDelete($id)
    {
        $news = News::findOneOrException($id);
        $news->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImage($id)
    {
        News::findOneOrException($id)->deleteImage();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['success' => true];
    }

    public function actionSeo($id)
    {
        $news = News::findOneOrException($id);

        if ($news->load(Yii::$app->request->post()) && $news->save()) {
            return $this->refresh();
        }

        return $this->render('seo', compact('news'));
    }

	public function actionProducts($q)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		return [
			'results' => array_map(function($product) {
				return [
					'id' => $product['id'],
					'text' => $product['title'],
				];
			},
			Product::find()->where(['like', 'title', $q])->select(['id', 'title'])->orderBy(['title' => SORT_ASC])->asArray()->all())
		];
    }
}
