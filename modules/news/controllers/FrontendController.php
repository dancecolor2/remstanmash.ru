<?php

namespace app\modules\news\controllers;

use app\modules\news\models\News;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class FrontendController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find()->active(),
            'pagination' => ['defaultPageSize' => 9, 'forcePageParam' => false],
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    public function actionView($alias)
    {
        $news = News::findOneOrException([
            'alias' => $alias,
            'active' => News::ACTIVE_ON,
        ]);

        $products = $news->products;

        return $this->render('view', compact('news', 'products'));
    }
}


