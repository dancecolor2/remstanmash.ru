<?php

namespace app\modules\news\widgets;

use app\modules\news\models\News;
use yii\base\Widget;

class LastNewsWidget extends Widget
{
    public function run()
    {
        $list = News::find()->orderBy(['updated_at' => SORT_DESC])->limit(3)->all();

        return $this->render('last_news', compact('list'));
    }
}