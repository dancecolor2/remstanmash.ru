<?php

use app\modules\news\widgets\NewsWidget;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \app\modules\news\models\News[] $list
 */

?>

<section class="section is-grey is-centred with-padding">
    <div class="container">
        <header class="section__header">
            <p class="section__caption">из последнего</p>
            <h2 class="section__title">Новости и события</h2>
        </header>
        <div class="section__body">
            <div class="posts js-posts">
                <div class="js-posts-swiper">
                    <ul class="posts__list is-swiper js-posts-list">
                        <?php foreach ($list as $news): ?>
                            <li class="posts__item js-posts-item" >
                                <?= NewsWidget::widget(compact('news')) ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                
                <div class="posts__footer">
                    <div class="posts__button">
                        <a class="button js-button is-hollow" area-label="Все новости" href="<?= Url::to(['/news/frontend/index']) ?>">Все новости</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>