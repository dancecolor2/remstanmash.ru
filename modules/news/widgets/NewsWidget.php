<?php

namespace app\modules\news\widgets;

use yii\base\Widget;

/**
 * @property \app\modules\news\models\News $news
 */
class NewsWidget extends Widget
{
    public $news;

    public function run()
    {
        return $this->render('news', [
            'news' => $this->news,
        ]);
    }
}