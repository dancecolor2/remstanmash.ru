<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\client\models\Client $client
 */

?>
<?php if (!empty($client)): ?>
    <div class="client">
        <h5 class="client__title"><?= $client->title ?></h5>
        <?php if (!empty($client->caption)): ?>
            <p class="client__caption"><?= $client->caption ?></p>
        <?php endif ?>
    </div>
<?php endif ?>

