<?php

namespace app\modules\client\widgets;

use app\modules\client\models\Client;
use yii\base\Widget;

/**
 * @property Client $client
 */
class ClientWidget extends Widget
{
    public $client;

    public function run()
    {
        return $this->render('client', [
            'client' => $this->client
        ]);
    }
}