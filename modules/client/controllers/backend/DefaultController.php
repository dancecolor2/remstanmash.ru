<?php

namespace app\modules\client\controllers\backend;

use Yii;
use app\modules\admin\components\BalletController;
use app\modules\client\models\Client;
use app\modules\client\models\ClientSearch;
use yii\filters\VerbFilter;

class DefaultController extends BalletController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    public function actionCreate()
    {
        $client = new Client(['active' => true]);

        if ($client->load(Yii::$app->request->post()) && $client->save()) {
            Yii::$app->session->setFlash('success', 'Клиент создан');

            return $this->redirect(['update', 'id' => $client->id]);
        }

        return $this->render('create', [
            'client' => $client,
        ]);
    }

    public function actionUpdate($id)
    {
        $client = Client::findOneOrException($id);

        if ($client->load(Yii::$app->request->post()) && $client->save()) {

            Yii::$app->session->setFlash('success', 'Клиент обновлен');
            return $this->refresh();
        }

        return $this->render('update', compact('client'));
    }

    public function actionDelete($id)
    {
        Client::findOneOrException($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMoveUp($id)
    {
        Client::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        Client::findOneOrException($id)->moveNext();
    }
}
