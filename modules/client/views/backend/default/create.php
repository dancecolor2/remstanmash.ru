<?php

/**
 * @var yii\web\View $this
 * @var $client app\modules\client\models\Client
 */

$this->title = 'Новый клиент';
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">
    <?= $this->render('_form', compact('client')) ?>
</div>

