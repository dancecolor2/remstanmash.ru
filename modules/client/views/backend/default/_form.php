<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\modules\client\models\Client $client */

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
      <?= $form->field($client, 'active')->checkbox() ?>
      <?= $form->field($client, 'title')->textInput(['maxlength' => true]) ?>
      <?= $form->field($client, 'caption')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="box-footer">
         <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>

