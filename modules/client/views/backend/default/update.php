<?php

/**
 * @var yii\web\View $this
 * @var app\modules\client\models\Client $client
 */

$this->title = $client->getTitle();
?>

<?php $this->beginContent('@app/modules/client/views/backend/default/layout.php', ['client' => $client]); ?>

    <?= $this->render('_form', compact('client')) ?>

<?php $this->endContent() ?>



