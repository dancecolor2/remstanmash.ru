<?php
use yii\bootstrap\Tabs;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $content
 * @var app\modules\client\models\Client $client * @var array $breadcrumbs
 */

$this->title = $client->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['/client/backend/default/index']];

if (Yii::$app->controller->action->id == 'update') {
    $this->params['breadcrumbs'][] = $client->getTitle();
} else {
    $this->params['breadcrumbs'][] = [
        'label' => $client->getTitle(),
        'url' => ['/client/backend/default/update', 'id' => $client->id],
    ];
}

if (!empty($breadcrumbs)) {
    foreach ($breadcrumbs as $breadcrumb) {
        $this->params['breadcrumbs'][] = $breadcrumb;
    }
}

?>
<div class="nav-tabs-custom">
    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'url' => ['/client/backend/default/update', 'id' => $client->id],
                'active' => Yii::$app->controller->action->id == 'update',
            ],
            [
                'label' => 'Действия',
                'headerOptions' => ['class' => 'pull-right'],
                'items' => [
                    [
                        'encode' => false,
                        'label' => '<i class="fa fa-remove text-danger" aria-hidden="true"></i>Удалить клиента',
                        'url' => Url::to(['/client/backend/default/delete', 'id' => $client->id]),
                        'linkOptions' => [
                            'class' => 'text-danger',
                            'data-method' => 'post',
                            'data-confirm' => 'Вы уверены?',
                        ],
                    ],
                ],
            ],
        ]
    ]) ?>

    <?= $content ?>

</div>
