<?php

namespace app\modules\client\models;

use app\traits\active\ActiveQueryTrait;
/**
 * @see Client
 */
class ClientQuery extends \yii\db\ActiveQuery
{
    use ActiveQueryTrait;
}
