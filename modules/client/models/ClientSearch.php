<?php

namespace app\modules\client\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\client\models\Client;

/**
 * ClientSearch represents the model behind the search form of `app\modules\client\models\Client`.
 */
class ClientSearch extends Client
{
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'active', 'id', 'position'], 'integer'],
            [['title', 'caption'], 'safe'],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['position' => SORT_ASC]]
        ]);

        if (!($this->load($params) && !$this->validate())) {

            return $dataProvider;
        }

        $query->andFilterWhere([
            'active' => $this->active,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'caption', $this->caption]);

        return $dataProvider;
    }
}
