<?php

namespace app\modules\client\models;

use yii\db\ActiveRecord;
use app\modules\admin\traits\QueryExceptions;
use yii\behaviors\TimestampBehavior;
use app\traits\active\ActiveTrait;
use yii2tech\ar\position\PositionBehavior;

/**
 * @property int $created_at Дата создания
 * @property int $updated_at Дата редактирования
 * @property int $active активность
 * @property int $id
 * @property string $title Название
 * @property string $caption Подпись
 * @property int $position
 *
 * @mixin PositionBehavior
 */
class Client extends ActiveRecord
{
    use QueryExceptions;
    use ActiveTrait;

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            PositionBehavior::class,
        ];
    }

    public static function tableName()
    {
        return 'client';
    }

    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['title'], 'required'],
            [['title', 'caption'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'active' => 'Активность',
            'id' => 'ID',
            'title' => 'Название',
            'caption' => 'Подпись',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientQuery(get_called_class());
    }

    public function getTitle()
    {
        return $this->title;
    }
}
