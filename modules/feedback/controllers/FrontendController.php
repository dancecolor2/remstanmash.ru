<?php

namespace app\modules\feedback\controllers;

use app\modules\feedback\factories\FormFactory;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{
    public function actionSend()
    {
        try {
            $form = FormFactory::create(Yii::$app->request->post('type'));
        } catch (\Exception $e) {
            return $this->goHome();
        }


        $form->ref = Yii::$app->request->referrer;

        if ($form->load(Yii::$app->request->post()) && $form->insert()) {

            Yii::$app->session->setFlash('success', $form->getSuccessMessage());
            return $this->redirect(['success']);
        }

        Yii::$app->session->setFlash('error', 'Что-то пошло не так');

        return $this->redirect(['error']);
    }

    public function actionSuccess()
    {
        if (Yii::$app->session->hasFlash('success')) {

            return $this->render('success');
        }

        return $this->goHome();
    }

    public function actionError()
    {
        if (Yii::$app->session->getFlash('error')) {

            return $this->render('error');
        }

        return $this->goHome();
    }
}