<?php

/**
 * @var $this \yii\web\View
 */

$this->title = 'Заявка отправлена';

?>

<section class="section is-margin-top-60">
    <div class="container">
        <div class="section__body">
            <div class="grid is-row">
                <div class="col-6 shift-3">
                    <div class="response">
                        <h1 class="response__title">Мы получили вашу заявку</h1>
                        <p class="response__text">В&nbsp;течении 10&nbsp;минут мы свяжемся с&nbsp;вами для&nbsp;обсуждения всех деталей. <br> <strong>Благодарим за&nbsp;доверие!</strong></p>
                        <div class="response__button"><a class="button js-button is-wide" area-label="вернуться на главную" href="/">вернуться на главную</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

