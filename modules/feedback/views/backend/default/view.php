<?php

use app\helpers\DateHelper;
use app\modules\feedback\factories\FormFactory;
use app\modules\feedback\forms\SetForm;
use app\modules\feedback\helpers\StatusHelper;
use app\modules\feedback\models\Feedback;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;

/**
 * @var $this \yii\web\View
 * @var Feedback $feedback
 */

 ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Просмотр заявки</h4>
</div>
<?php $form = ActiveForm::begin(['options' => ['id' => 'js-modal-form']]) ?>
<div class="modal-body">

    <?= $form->field($feedback, 'status')->dropDownList(StatusHelper::getStatusList()) ?>

    <?= DetailView::widget([
        'model' => $feedback,
        'attributes' => [
            'id',
            [
                'attribute' => 'type',
                'value' => $feedback::TITLE,
            ],
            [
                'attribute' => 'item_id',
                'format' => 'raw',
                'visible' => !empty($feedback->item_id),
                'value' => function(Feedback $feedback) {
                    return $feedback->getItemValue();
                }
            ],
            [
                'attribute' => 'set_machine',
                'format' => 'raw',
                'visible' => $feedback::TYPE == SetForm::TYPE,
                'value' => function (SetForm $setForm) {
                    $machineForm = FormFactory::create('machine');
                    $machineForm->item_id = $setForm->set->machine_id;
                    return $machineForm->getItemValue();
                }
            ],
            [
                'attribute' => 'name',
                'visible' => !empty($feedback->name),
            ],
            [
                'attribute' => 'phone',
                'visible' => !empty($feedback->phone),
            ],
            [
                'attribute' => 'email',
                'visible' => !empty($feedback->email),
            ],
            [
                'attribute' => 'organization',
                'visible' => !empty($feedback->organization),
            ],
            [
                'attribute' => 'comment',
                'format' => 'ntext',
                'visible' => !empty($feedback->comment),
            ],
            [
                'attribute' => 'ref',
                'format' => 'raw',
                'visible' => !empty($feedback->ref),
                'value' => Html::a($feedback->ref, $feedback->ref, ['target' => '_blank'])
            ],
            [
                'attribute' => 'created_at',
                'visible' => !empty($feedback->created_at),
                'format' => 'datetime',
            ],
        ],
    ]) ?>
</div>
<div class="modal-footer">
    <button class="btn btn-success">Сохранить</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
</div>
<?php ActiveForm::end() ?>

