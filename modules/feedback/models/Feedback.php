<?php

namespace app\modules\feedback\models;

use app\modules\admin\traits\QueryExceptions;
use app\modules\category\models\Category;
use app\modules\feedback\factories\FormFactory;
use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $type
 * @property int $item_id
 * @property int $item_text
 * @property string $name
 * @property string $phone
 * @property string $organization
 * @property string $email
 * @property string $comment
 * @property string $ref
 * @property int status
 *
 * @property Category $category
 */
class Feedback extends ActiveRecord
{
    use QueryExceptions;

    const STATUS_NEW = 1;
    const STATUS_PROCESS = 2;
    const STATUS_SUCCESS = 3;
    const STATUS_CANCELED = 4;

    const TITLE = 'Заявки';
    const ICON = 'bell-o';
    const TYPE = NULL;

	public $reCaptcha;

    public function init()
    {
        $this->type = static::TYPE;
    }

    public function formName()
    {
        return '';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public static function tableName()
    {
        return 'feedback';
    }

    public function rules()
    {
        return [
            [['phone', 'type', 'item_id'], 'required', 'message' => 'Заполните поле'],
            [['status', 'item_id'], 'integer'],
            [['name', 'phone', 'organization'], 'string', 'max' => 255],
            ['email', 'email', 'message' => 'Некорректный E-mail'],
            ['comment', 'string'],
			[['reCaptcha'], ReCaptchaValidator2::class, 'uncheckedMessage' => 'Пожалуйста, подтвердите, что вы не робот.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата заявки',
            'updated_at' => 'Дата обновления',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'comment' => 'Комментарий',
            'ref' => 'Страница',
            'status' => 'Статус',
            'type' => 'Форма',
            'organization' => 'Организация'
        ];
    }

    public function getItemValue()
    {
        return $this->item_id;
    }

    public function getSuccessMessage()
    {
        return '';
    }

    public static function instantiate($row)
    {
        return FormFactory::create($row['type']);
    }

    public function gridAttrs()
    {
        return ['status', 'created_at', 'name', 'phone'];
    }
}
