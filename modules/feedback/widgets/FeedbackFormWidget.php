<?php

namespace app\modules\feedback\widgets;

use app\modules\feedback\forms\CallbackForm;
use app\modules\feedback\forms\FeedbackForm;
use yii\base\Widget;

/**
 * @property FeedbackForm $feedbackForm
 */
class FeedbackFormWidget extends Widget
{
    public $feedbackForm;

    public function init()
    {
        if (empty($this->feedbackForm)) {
            $this->feedbackForm = new FeedbackForm();
        }
    }

    public function run()
    {
        return $this->render('feedback_form', [
            'feedbackForm' => $this->feedbackForm,
        ]);
    }
}