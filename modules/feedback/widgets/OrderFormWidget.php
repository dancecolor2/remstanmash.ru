<?php

namespace app\modules\feedback\widgets;

use app\modules\feedback\models\Feedback;
use yii\base\Widget;

/**
 * @property Feedback $form
 * @property boolean $inModal
 * @property string $type
 */
class OrderFormWidget extends Widget
{
    public $form = null;
    public $inModal = false;
    public $type;
    public $id;

    public function init()
    {
        if (is_null($this->form)) {
            $this->form = new Feedback();
        }
    }

    public function run()
    {
        return $this->render('order_form', [
            'orderForm' => $this->form,
            'inModal' => $this->inModal,
            'type' => $this->type,
            'id' => $this->id,
        ]);
    }

}