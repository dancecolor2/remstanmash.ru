<?php

use app\modules\page\components\Pages;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

/**
 * @var $this \yii\web\View
 * @var $orderForm \app\modules\feedback\models\Feedback
 * @var boolean $inModal
 * @var string $type
 * @var integer $id
 */

?>

<?php $form = ActiveForm::begin([
    'action' => ['/feedback/frontend/send'],
    'method' => 'POST',
    'validateOnType' => true,
    'requiredCssClass' => 'is-required',
    'options' => ['class' => 'form js-form is-order' . ($inModal ? ' in-modal' : ''), 'role' => "form"],
    'fieldConfig' => [
        'inputOptions' => ['class' => 'input js-form-field form__input'],
        'options' => ['tag' => 'label', 'class' => 'form__group js-form-group'],
        'template' => '<span class="form__field">{input}<span class="form__label">{labelTitle}</span>{error}',
        'errorOptions' => ['class' => 'form__message', 'tag' => 'span'],
    ],
]) ?>

<?php if (!empty($type)): ?>
    <input type="hidden" name="type" value="<?= $type ?>">
<?php endif ?>
<?php if (!empty($id)): ?>
    <input type="hidden" name="item_id" value="<?= $id ?>">
<?php endif ?>

<div class="form__body">
    <div class="grid is-columns">
        <div class="col-6 col-m-12">
            <?= $form->field($orderForm, 'name')->label('Ваше имя'); ?>
        </div>
        <div class="col-6 col-m-12">
            <?= $form->field($orderForm, 'organization')->label('Название организации'); ?>
        </div>
        <div class="col-6 col-m-12">
            <?= $form->field($orderForm, 'phone')->widget(MaskedInput::class, [
                'mask' => '+7 999 999 99 99',
                'clientOptions' => [
                    'showMaskOnHover' => false,
                    'clearIncomplete' => true
                ]
            ])->label('Номер телефона'); ?>
        </div>
        <div class="col-6 col-m-12">
            <?= $form->field($orderForm, 'email')->label('E-mail'); ?>
        </div>
        <div class="col-12">
            <?= $form->field($orderForm, 'comment')
                ->textarea(['class' => 'textarea js-form-field form__input'])
                ->label('Сообщение'); ?>
        </div>
        <div class="col-12">
			<?= $form->field($orderForm, 'reCaptcha', [
				'options' => ['class' => 'form__group', 'style' => 'margin-bottom: 20px'],
				'errorOptions' => ['class' => 'form__message'],
			])->widget(ReCaptcha2::class)->label(false) ?>
        </div>
    </div>
</div>
<div class="form__footer">
    <div class="grid is-row is-middle">
        <div class="col-6 col-m-12">
            <div class="form__agreement">Отправляя сообщение, соглашаюсь с&nbsp;<a href='<?= Pages::getHref('privacy') ?>'>обработкой персональных данных</a></div>
        </div>
        <div class="col-6 col-m-12">
            <div class="form__button">
                <button class="button js-button is-wide is-large is-filled" area-label="Сделать заказ">Сделать заказ</button>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end() ?>

