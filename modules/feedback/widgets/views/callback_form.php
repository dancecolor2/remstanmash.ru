<?php

use app\modules\page\components\Pages;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

/**
 * @var \yii\web\View $this
 * @var \app\modules\feedback\forms\CallbackForm $callbackForm
 */

?>

<?php $form = ActiveForm::begin([
    'action' => ['/feedback/frontend/send'],
    'method' => 'POST',
    'validateOnType' => true,
    'requiredCssClass' => 'is-required',
    'options' => ['class' => 'form js-form is-callback in-modal', 'role' => "form"],
    'fieldConfig' => [
        'inputOptions' => ['class' => 'input js-form-field form__input'],
        'options' => ['tag' => 'label', 'class' => 'form__group js-form-group'],
        'template' => '<span class="form__field">{input}<span class="form__label">{labelTitle}</span>{error}',
        'errorOptions' => ['class' => 'form__message', 'tag' => 'span'],
    ],
]) ?>
    <input type="hidden" name="type" value="<?= $callbackForm::TYPE ?>">

    <div class="form__body">
        <div class="grid is-columns is-form">
            <div class="col-12">
                <?= $form->field($callbackForm, 'name')->label('Ваше имя'); ?>
            </div>
            <div class="col-12">
                <?= $form->field($callbackForm, 'phone')->widget(MaskedInput::class, [
                    'mask' => '+7 999 999 99 99',
                    'clientOptions' => [
                        'showMaskOnHover' => false,
                        'clearIncomplete' => true
                    ]
                ])->label('Номер телефона'); ?>
            </div>
            <div class="col-12">
				<?= $form->field($callbackForm, 'reCaptcha')->widget(ReCaptcha2::class)->label(false) ?>
            </div>
        </div>
    </div>
    <div class="form__footer">
        <div class="grid is-columns is-middle">
            <div class="col-12">
                <div class="form__agreement">Отправляя сообщение, соглашаюсь с&nbsp;<a href='<?= Pages::getHref('privacy') ?>'>обработкой персональных данных</a></div>
            </div>
            <div class="col-12">
                <div class="form__button">
                    <button
                    class="button js-button is-wide is-large is-filled"
                    area-label="Отправить"
                    onclick="ym(50330710,'reachGoal','push_button_contacts');"
                    >Отправить</button>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end() ?>