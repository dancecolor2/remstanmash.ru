<?php

use app\modules\page\components\Pages;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\bootstrap\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var \app\modules\feedback\forms\FeedbackForm $feedbackForm
 */

?>

<?php $form = ActiveForm::begin([
    'action' => ['/feedback/frontend/send'],
    'method' => 'POST',
    'validateOnType' => true,
    'requiredCssClass' => 'is-required',
    'options' => ['class' => 'form js-form is-feedback', 'role' => "form"],
    'fieldConfig' => [
        'inputOptions' => ['class' => 'input js-form-field form__input'],
        'options' => ['tag' => 'label', 'class' => 'form__group js-form-group'],
        'template' => '<span class="form__field">{input}<span class="form__label">{labelTitle}</span>{error}',
        'errorOptions' => ['class' => 'form__message', 'tag' => 'span'],
    ],
]) ?>
<input type="hidden" name="type" value="<?= $feedbackForm::TYPE ?>">
<div class="form__header">
    <h3 class="form__title">Написать нам</h3>
</div>
<div class="form__body">
    <div class="grid is-columns is-form">
        <div class="col-6 col-m-12">
            <?= $form->field($feedbackForm, 'name')->label('Ваше имя'); ?>
        </div>
        <div class="col-6 col-m-12">
            <?= $form->field($feedbackForm, 'email')->label('E-mail'); ?>
        </div>
        <div class="col-12">
            <?= $form->field($feedbackForm, 'comment')->textarea(['class' => 'textarea js-form-field form__input'])->label('Сообщение'); ?>
        </div>
        <div class="col-12">
			<?= $form->field($feedbackForm, 'reCaptcha')->widget(ReCaptcha2::class)->label(false) ?>
        </div>
    </div>
</div>
<div class="form__footer">
    <div class="grid is-row is-middle">
        <div class="col-6 col-m-12">
            <div class="form__agreement">Отправляя сообщение, соглашаюсь с&nbsp;<a href='<?= Pages::getHref('privacy') ?>'>обработкой персональных данных</a></div>
        </div>
        <div class="col-6 col-m-12">
            <div class="form__button">
                <button class="button js-button is-wide is-large is-filled" area-label="Отправить сообщение">Отправить сообщение</button>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end() ?>
