<?php

namespace app\modules\feedback\widgets;

use app\modules\feedback\forms\CallbackForm;
use yii\base\Widget;

/**
 * @property CallbackForm $callbackForm
 */
class CallbackFormWidget extends Widget
{
    public $callbackForm;

    public function init()
    {
        if (empty($this->callbackForm)) {
            $this->callbackForm = new CallbackForm();
        }
    }

    public function run()
    {
        return $this->render('callback_form', [
            'callbackForm' => $this->callbackForm,
        ]);
    }
}