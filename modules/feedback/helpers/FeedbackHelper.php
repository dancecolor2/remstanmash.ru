<?php

namespace app\modules\feedback\helpers;

use app\modules\feedback\factories\FormFactory;
use app\modules\feedback\forms\MachineForm;
use app\modules\feedback\forms\OrderForm;
use app\modules\feedback\forms\ProjectForm;
use app\modules\feedback\forms\SetForm;
use app\modules\feedback\models\Feedback;
use kartik\grid\ActionColumn;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

class FeedbackHelper
{
    private static $isUpdateItemUsed = false;

    public static function getGridColumns($columnNames)
    {
        return array_map(function($name){
            switch ($name) {
                case 'view': return [
                        'class' => ActionColumn::class,
                        'template' => '{view}',
                        'width' => '150px',
                        'mergeHeader' => false,
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                return '<span 
                                    href="'.Url::to(['/feedback/backend/default/view', 'id' => $model->id]).'"
                                    class="btn btn-xs btn-primary"
                                    data-toggle="modal"
                                    data-pjax="0"
                                    data-target="#modal-lg"
                                ><i class="fa fa-eye"></i> Просмотр</span>';
                            },
                        ],
                    ];
                case 'status': return [
                        'class' => DataColumn::class,
                        'vAlign' => GridView::ALIGN_MIDDLE,
                        'attribute' => 'status',
                        'format' => 'raw',
                        'width' => '100px',
                        'filter' => StatusHelper::getStatusList(),
                        'value' => function(Feedback $feedback) {
                            return self::getStatusButton($feedback);
                        }
                    ];
                case 'created_at': return [
                        'class' => DataColumn::class,
                        'vAlign' => GridView::ALIGN_MIDDLE,
                        'label' => 'Дата заявки',
                        'attribute' => 'created_at',
                        'width' => '120px',
                        'value' => function(Feedback $feedback) {
                            return date('d.m.Y H:i', $feedback->created_at);
                        }
                    ];
                case 'name': return [
                        'class' => DataColumn::class,
                        'vAlign' => GridView::ALIGN_MIDDLE,
                        'attribute' => 'name',
                    ];
                case 'email': return [
                    'class' => DataColumn::class,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'attribute' => 'email',
                ];
                case 'phone': return [
                        'class' => DataColumn::class,
                        'vAlign' => GridView::ALIGN_MIDDLE,
                        'attribute' => 'phone',
                    ];
                case 'id': return [
                        'class' => DataColumn::class,
                        'vAlign' => GridView::ALIGN_MIDDLE,
                        'attribute' => 'id',
                        'width' => '50px',
                    ];
                case 'item_id': return [
                        'class' => DataColumn::class,
                        'vAlign' => GridView::ALIGN_MIDDLE,
                        'attribute' => 'item_id',
                        'format' => 'raw',
                        'value' => function (Feedback $form) {
                            return $form->getItemValue();
                        }
                    ];
                case 'set_machine': return [
                        'class' => DataColumn::class,
                        'vAlign' => GridView::ALIGN_MIDDLE,
                        'label' => 'Станок',
                        'format' => 'raw',
                        'width' => '250px',
                        'value' => function (SetForm $setForm) {
                            $machineForm = FormFactory::create('machine');
                            $machineForm->item_id = $setForm->set->machine_id;
                            return $machineForm->getItemValue();
                        }
                    ];
                default: return [
                    'class' => DataColumn::class,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'attribute' => $name,
                ];
            }
        }, $columnNames);
    }

    public static function getMenuItems()
    {

        return array_filter(array_map(function($formClass) {

            if (in_array($formClass, [MachineForm::class, SetForm::class, ProjectForm::class])) {

                return self::getUpdateItem();
            }

            return self::getItemMenu($formClass);
        }, FormFactory::$types));
    }

    public static function getItemMenu($formClass)
    {
        return [
            'label' => $formClass::TITLE . Badge::getBadge($formClass::TYPE),
            'icon' => $formClass::ICON,
            'options' => ['style' => 'position: relative'],
            'url' => ['/feedback/backend/default/index', 'type' => $formClass::TYPE],
        ];
    }

    private static function getUpdateItem ()
    {
        if (self::$isUpdateItemUsed) {
            return null;
        }

        self::$isUpdateItemUsed = true;
        return [
            'label' => 'Модернизация ' . Badge::getUpdateBadgeSum(),
            'icon' => MachineForm::ICON,
            'items' => [
                self::getItemMenu(MachineForm::class),
                self::getItemMenu(SetForm::class),
                self::getItemMenu(ProjectForm::class),
            ],
        ];

    }

    public static function getStatusButton (Feedback $feedback)
    {
        return '<button 
            data-target="#modal-lg"
            data-toggle="modal"
            href="' . Url::to(['/feedback/backend/default/view', 'id' => $feedback->id]) . '" 
            class="btn btn-xs btn-' . StatusHelper::getStatusClass($feedback->status) . '"
        >' . StatusHelper::getStatusText($feedback->status) . '</button>';
    }

}