<?php

namespace app\modules\feedback\factories;

use app\modules\feedback\forms\CallbackForm;
use app\modules\feedback\forms\FeedbackForm;
use app\modules\feedback\forms\MachineForm;
use app\modules\feedback\forms\OrderForm;
use app\modules\feedback\forms\ProjectForm;
use app\modules\feedback\forms\RepairForm;
use app\modules\feedback\forms\SetForm;
use app\modules\feedback\models\Feedback;

class FormFactory
{
    public static $types = [
        CallbackForm::TYPE => CallbackForm::class,
        OrderForm::TYPE => OrderForm::class,
        MachineForm::TYPE => MachineForm::class,
        SetForm::TYPE => SetForm::class,
        ProjectForm::TYPE => ProjectForm::class,
        RepairForm::TYPE => RepairForm::class,
        FeedbackForm::TYPE => FeedbackForm::class,
    ];

    public static function create($type) : Feedback
    {
        $className = self::getClassName($type);
        return new $className();
    }

    public static function getClassName($type)
    {
        if (empty(self::$types[$type])) {
            throw new \Exception();
        }

        return self::$types[$type];
    }
}