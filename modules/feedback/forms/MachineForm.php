<?php

namespace app\modules\feedback\forms;

use app\modules\feedback\models\Feedback;
use app\modules\machine\models\Machine;
use yii\helpers\Url;

/**
 * @property Machine $machine
 */
class MachineForm extends Feedback
{
    const TYPE = 'machine';
    const TITLE = 'Модернизация (станок)';
    const ICON = 'microchip';

    public function gridAttrs()
    {
        return ['created_at', 'status', 'phone', 'name', 'item_id',  'organization'];
    }

    public function attributeLabels()
    {
        return array_merge([
            'item_id' => 'Станок',
        ], parent::attributeLabels());
    }

    public function getMachine()
    {
        return $this->hasOne(Machine::class, ['id' => 'item_id']);
    }

    public function getItemValue()
    {
        if (empty($this->machine)) {
            return $this->item_text;
        }

        $image = '';
        if (!empty($this->machine->images)) {
            $image = '<img src="'. Url::to($this->machine->images[0]->getThumbFileUrl('image', 'preview'), true) .'" alt="' . $this->machine->getTitle() . '" style="max-width: 50px; max-height: 50px; margin-right: 10px">';
        }
        return
            '<table style="margin: 0;" cellpadding="5px">
                <tr>
                    <td style="text-align: center">' . $image . '</td>
                    <td style="vertical-align: middle">
                        <a href="' . Url::to(['/machine/backend/default/update', 'id' => $this->machine->id], true) . '" data-pjax="0" target="_blank">' . $this->machine->title . '</a>
                    </td>
                </tr>
            </table>';
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->item_text = $this->machine->title;
        }

        return parent::beforeSave($insert);
    }

}