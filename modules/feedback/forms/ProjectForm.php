<?php

namespace app\modules\feedback\forms;

use app\modules\feedback\models\Feedback;
use app\modules\project\models\Project;
use yii\helpers\Url;

/**
 * @property Project $project
 */
class ProjectForm extends Feedback
{
    const TYPE = 'project';
    const TITLE = 'Модернизация (проект)';
    const ICON = 'microchip';

    public function gridAttrs()
    {
        return ['created_at', 'status', 'phone', 'name', 'item_id',  'organization'];
    }

    public function attributeLabels()
    {
        return array_merge([
            'item_id' => 'Проект',
        ], parent::attributeLabels());
    }

    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'item_id']);
    }

    public function getItemValue()
    {
        if (empty($this->project)) {
            return $this->item_text;
        }

        $image = '';
        if (!empty($this->project->hasImage())) {
            $image = '<img src="'. Url::to($this->project->getThumbFileUrl('image'), true) .'" alt="' . $this->project->getTitle() . '" style="max-width: 50px; max-height: 50px; margin-right: 10px">';
        }
        return
            '<table style="margin: 0;" cellpadding="5px">
                <tr>
                    <td style="text-align: center">' . $image . '</td>
                    <td style="vertical-align: middle">
                        <a href="' . Url::to(['/project/backend/update', 'id' => $this->project->id], true) . '" data-pjax="0" target="_blank">' . $this->project->title . '</a>
                    </td>
                </tr>
            </table>';
    }

    public function beforeSave($insert)
    {
        if  ($insert) {
            $this->item_text = $this->project->title;
        }

        return parent::beforeSave($insert);
    }

}