<?php

namespace app\modules\feedback\forms;

use app\modules\feedback\models\Feedback;
use app\modules\set\models\Set;
use yii\helpers\Url;

/**
 * @property Set $set
 */
class SetForm extends Feedback
{
    const TYPE = 'set';
    const TITLE = 'Модернизация (комплектация)';
    const ICON = 'microchip';

    public function gridAttrs()
    {
        return ['created_at', 'status', 'phone', 'name', 'item_id', 'organization', 'set_machine'];
    }

    public function attributeLabels()
    {
        return array_merge([
            'item_id' => 'Набор',
            'set_machine' => 'Станок',
        ], parent::attributeLabels());
    }

    public function getSet()
    {
        return $this->hasOne(Set::class, ['id' => 'item_id']);
    }

    public function getItemValue()
    {
        return empty($this->set) ? $this->item_text : '<a href="' . Url::to(['/set/backend/default/update', 'id' => $this->set->id], true) . '" data-pjax="0" target="_blank">' . $this->set->title . '</a>';
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->item_text = $this->set->title . ' ( ' . $this->set->machine->title . ' )';
        }

        return parent::beforeSave($insert);
    }

}