<?php

namespace app\modules\feedback\forms;

use app\modules\feedback\models\Feedback;
use himiklab\yii2\recaptcha\ReCaptchaValidator2;

class CallbackForm extends Feedback
{
    const TYPE = 'callback';
    const TITLE = 'Обратный звонок';
    const ICON = 'phone-square';

	public $reCaptcha;

    public function rules()
    {
        return [
            ['status', 'integer'],
            [['phone'], 'required', 'message' => 'Заполните поле'],
            [['name', 'phone'], 'string', 'max' => 255],
            ['comment', 'string'],
			[['reCaptcha'], ReCaptchaValidator2::class, 'uncheckedMessage' => 'Пожалуйста, подтвердите, что вы не робот.'],
        ];
    }

    public function gridAttrs()
    {
        return ['created_at', 'status', 'phone', 'name'];
    }
}