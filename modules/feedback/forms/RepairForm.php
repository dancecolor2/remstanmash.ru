<?php

namespace app\modules\feedback\forms;

use app\modules\feedback\models\Feedback;
use app\modules\repair\models\Repair;
use yii\helpers\Url;

/**
 * @property Repair $repair
 */
class RepairForm extends Feedback
{
    const TYPE = 'repair';
    const TITLE = 'Ремонт';
    const ICON = 'wrench';

    public function gridAttrs()
    {
        return ['created_at', 'status', 'phone', 'name', 'item_id', 'organization'];
    }

    public function attributeLabels()
    {
        return array_merge([
            'item_id' => 'Услуга',
        ], parent::attributeLabels());
    }

    public function getRepair()
    {
        return $this->hasOne(Repair::class, ['id' => 'item_id']);
    }

    public function getItemValue()
    {
        return empty($this->repair) ? $this->item_text : '<a href="' . Url::to(['/repair/backend/default/update', 'id' => $this->repair->id], true) . '" data-pjax="0" target="_blank">' . $this->repair->title . '</a>';
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->item_text = $this->repair->title;
        }


        return parent::beforeSave($insert);
    }

}