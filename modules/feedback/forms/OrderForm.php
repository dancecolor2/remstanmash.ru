<?php

namespace app\modules\feedback\forms;

use app\modules\feedback\models\Feedback;
use app\modules\product\models\Product;
use yii\helpers\Url;

/**
 * @property Product $product
 */
class OrderForm extends Feedback
{
    const TYPE = 'order';
    const TITLE = 'Заказ товара';
    const ICON = 'shopping-cart';

    public function gridAttrs()
    {
        return ['created_at', 'status',  'item_id', 'phone', 'name', 'organization'];
    }

    public function attributeLabels()
    {
        return array_merge([
            'item_id' => 'Товар',
        ], parent::attributeLabels());
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'item_id']);
    }

    public function getItemValue()
    {
        if (empty($this->product)) {

            return $this->item_text;
        }

        $image = '';
        if (!empty($this->product->images)) {
            $image = '<img src="'.  Url::to($this->product->images[0]->getThumbFileUrl('image', 'preview'), true) .'" alt="' . $this->product->title . '" style="max-width: 50px; max-height: 50px; margin-right: 10px">';
        }
        return
            '<table style="margin: 0" cellpadding="5px">
            <tr>
                <td style="text-align: center">' . $image . '</td>
                <td style="vertical-align: middle">
                    <a href="' . Url::to(['/product/backend/default/update', 'id' => $this->product->id], true) . '" data-pjax="0" target="_blank">' . $this->product->title . '</a>
                </td>
            </tr>
        </table>';
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->item_text = $this->product->title;
        }

        return parent::beforeSave($insert);
    }

}