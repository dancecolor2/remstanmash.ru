<?php

namespace app\modules\feedback\forms;

use app\modules\feedback\models\Feedback;
use himiklab\yii2\recaptcha\ReCaptchaValidator2;

class FeedbackForm extends Feedback
{
    const TYPE = 'feedback';
    const TITLE = 'Написать нам';
    const ICON = 'envelope';

    public $reCaptcha;

    public function rules()
    {
        return [
            ['status', 'integer'],
            [['email', 'name', 'comment'], 'required', 'message' => 'Заполните поле'],
            [['name'], 'string', 'max' => 255],
            ['comment', 'string'],
			[['reCaptcha'], ReCaptchaValidator2::class, 'uncheckedMessage' => 'Пожалуйста, подтвердите, что вы не робот.'],
        ];
    }

    public function gridAttrs()
    {
        return ['created_at', 'status', 'name', 'email', 'comment'];
    }
}