<?php

namespace app\modules\set\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\set\models\Set;
use app\modules\set\models\SetItem;
use app\modules\set\models\SetItemSearch;
use Yii;
use yii\web\Response;

class ItemController extends BalletController
{
    public function actionIndex($id)
    {
        $set = Set::findOneOrException($id);
        $searchModel = new SetItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['set_id' => $set->id]);

        return $this->render('index', compact('set', 'searchModel', 'dataProvider'));
    }

    public function actionCreate($id)
    {
        $set = Set::findOneOrException($id);
        $setItem = new SetItem([
            'quantity' => 1,
            'set_id' => $set->id,
        ]);

        if ($setItem->load(Yii::$app->request->post()) && $setItem->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['message' => 'success'];
            } else {
                return $this->redirect(['update', 'id' => $setItem->id]);
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', compact('setItem'));
        } else {
            return $this->render('create', compact('setItem'));
        }
    }

    public function actionUpdate($id)
    {
        $setItem = SetItem::findOneOrException($id);

        if ($setItem->load(Yii::$app->request->post()) && $setItem->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['message' => 'success'];
            } else {
                return $this->refresh();
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', compact('setItem'));
        } else {
            return $this->render('update', compact('setItem'));
        }
    }

    public function actionDelete($id)
    {
        SetItem::findOneOrException($id)->delete();
    }

    public function actionMoveUp($id)
    {
        SetItem::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        SetItem::findOneOrException($id)->moveNext();
    }
}