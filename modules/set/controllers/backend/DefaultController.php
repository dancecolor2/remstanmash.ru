<?php

namespace app\modules\set\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\machine\models\Machine;
use app\modules\set\models\Set;
use app\modules\set\models\SetSearch;
use Yii;

class DefaultController extends BalletController
{
    public function actionIndex($id)
    {
        $machine = Machine::findOneOrException($id);
        $searchModel = new SetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['machine_id' => $machine->id]);

        return $this->render('index', compact('searchModel', 'dataProvider', 'machine'));
    }

    public function actionCreate($id)
    {
        $machine = Machine::findOneOrException($id);
        $set = new Set([
            'active' => true,
            'machine_id' => $machine->id,
        ]);

        if ($set->load(Yii::$app->request->post()) && $set->save()) {

            return $this->redirect(['update', 'id' => $set->id]);
        }

        return $this->render('create', compact('set'));
    }

    public function actionUpdate($id)
    {
        $set = Set::findOneOrException($id);

        if ($set->load(Yii::$app->request->post()) && $set->save()) {

            return $this->refresh();
        }

        return $this->render('update', compact('set'));
    }

    public function actionDelete($id)
    {
        Set::findOneOrException($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMoveUp($id)
    {
        Set::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        Set::findOneOrException($id)->moveNext();
    }
}