<?php

use app\modules\machine\helpers\SetItemHelper;
use app\modules\set\models\SetItem;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var \yii\web\View $this
 * @var \app\modules\set\models\SetItem $setItem
 */

if (!$isAjax = Yii::$app->request->isAjax) {
    $this->params['breadcrumbs'] = [
        [
            'label' => 'Партнеры',
            'url' => ['/setItem/backend/default/index'],
        ],
        $this->title,
    ];
}

$formatJs = <<< 'JS'
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    
    let markup = '';
    
    if (repo.image) {
        markup = '<img src="' + repo.image + '" style="max-width: 60px; max-height: 60px; margin-right:10px; float:left;">'
    }
    markup += '<b> ' + repo.text + '</b>' 
        + '<br><small>Артикул: <b>' + repo.code + '</b></small>'
        + '<br><small>id: <b>' + repo.id +  '</b></small>';

        
    return '<div style="overflow:hidden; border:1px solid #337ab726; padding: 5px;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return  repo.text;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data) {
    return {
        results: data.results
    };
}
JS;

?>

<?php $form = ActiveForm::begin($isAjax ? ['options' => ['id' => 'js-modal-form']] : []) ?>
<div class="modal-body">
    <?= $form->field($setItem, 'product_id')->widget(Select2::class, [
        'showToggleAll' => false,
        'initValueText' => $setItem->product->title,
        'options' => [
            'placeholder' => 'Выберите товар',
            'multiple' => false,
            'required' => true
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'ajax' => [
                'url' => Url::to(['/product/backend/default/product-list']),
                'dataType' => 'json',
                'processResults' => new JsExpression($resultsJs),
            ],
            'templateResult' => new JsExpression('formatRepo'),
            'templateSelection' => new JsExpression('formatRepoSelection'),
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        ],
    ]); ?>
    <?= $form->field($setItem, 'quantity') ?>
</div>
<div class="modal-footer">
    <button class="btn btn-success">Сохранить</button>
    <?php if ($isAjax): ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    <?php endif ?>
</div>

<?php ActiveForm::end() ?>
