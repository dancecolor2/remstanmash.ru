<?php

use app\modules\set\helpers\SetHelper;
use app\modules\set\widgets\SetItemsGridWidget;
use yii\bootstrap\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var \app\modules\set\models\Set $set
 */

 ?>

<?php $form = ActiveForm::begin() ?>
    <div class="box-body">
        <h3><?= $this->title ?></h3>
        <?= $form->field($set, 'active')->checkbox() ?>
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($set, 'title') ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($set, 'cost') ?>
            </div>
            <?php if (!$set->isNewRecord): ?>
                <div class="col-xs-12">
                    <?= SetItemsGridWidget::widget(compact('set')) ?>
                </div>
            <?php endif ?>
        </div>
    </div>
    <div class="box-footer">
        <button class="btn btn-success">Сохранить</button>
    </div>
<?php ActiveForm::end() ?>






