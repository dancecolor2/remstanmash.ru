<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\set\models\Set $set
 * @var \app\modules\set\models\SetItemSearch $searchModel
 */

$this->title = $set->title;

 ?>

<?php $this->beginContent('@app/modules/machine/views/backend/default/layout.php', ['machine' => $set->machine, 'breadcrumbs' => [$set->title]]); ?>

  <?= $this->render('_form', compact('set')) ?>

<?php $this->endContent() ?>

