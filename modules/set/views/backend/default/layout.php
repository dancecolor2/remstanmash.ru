<?php

use yii\bootstrap\Tabs;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $content
 * @var \app\modules\set\models\Set $set
 * @var array $breadcrumbs
 */

$this->title = $set->title;
$this->params['breadcrumbs'][] = ['label' => 'Комплектации', 'url' => ['/set/backend/default/index']];

if (Yii::$app->controller->action->id == 'update') {
    $this->params['breadcrumbs'][] = $set->title;
} else {
    $this->params['breadcrumbs'][] = [
        'label' => $set->title,
        'url' => ['/set/backend/default/update', 'id' => $set->id],
    ];
}

if (!empty($breadcrumbs)) {
    foreach ($breadcrumbs as $breadcrumb) {
        $this->params['breadcrumbs'][] = $breadcrumb;
    }
}

?>
<div class="nav-tabs-custom">
<?= Tabs::widget([
    'items' => [
        [
            'label' => 'Общее',
            'url' => ['/set/backend/default/update', 'id' => $set->id],
            'active' => Yii::$app->controller->action->id == 'update',
        ],
        [
            'label' => 'Элементы (' . $set->getItems()->count() . ')',
            'url' => ['/set/backend/item/index', 'id' => $set->id],
            'active' => Yii::$app->controller->id == 'backend/item',
        ],
        [
            'label' => 'Действия',
            'headerOptions' => ['class' => 'pull-right'],
            'items' => [
                [
                    'encode' => false,
                    'label' => '<i class="fa fa-remove text-danger" aria-hidden="true"></i>Удалить комплектацию',
                    'url' => Url::to(['/set/backend/default/delete', 'id' => $set->id]),
                    'linkOptions' => [
                        'class' => 'text-danger',
                        'data-method' => 'post',
                        'data-confirm' => 'Вы уверены?',
                    ],
                ],
            ],
        ],
    ]
]) ?>

<?= $content ?>

</div>