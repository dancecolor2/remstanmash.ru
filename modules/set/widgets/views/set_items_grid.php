<?php

use app\modules\set\models\SetItem;
use kartik\grid\ActionColumn;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \app\modules\set\models\Set $set
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\set\models\SetItemSearch $searchModel
 */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summaryOptions' => ['class' => 'text-right'],
    'bordered' => false,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'pjax-widget']],
    'striped' => false,
    'hover' => true,
    'panel' => [
        'heading'=> false,
        'type'=>'success',
        'before' => '',
        'after' => false,
    ],
    'toolbar' => [
        [
            'content' =>
                Html::a(
                    Icon::show('plus-outline') . ' Добавить',
                    ['/set/backend/item/create', 'id' => $set->id],
                    [
                        'data-pjax' => 0,
                        'class' => 'btn btn-success',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal',
                    ]
                ) . ' ' .
                Html::a(
                    Icon::show('arrow-sync-outline'),
                    ['index'],
                    [
                        'data-pjax' => 0,
                        'class' => 'btn btn-default',
                        'title' => Yii::t('app', 'Reset')
                    ]
                )
        ],
        '{toggleData}',
    ],
    'toggleDataOptions' => [
        'all' => [
            'icon' => 'resize-full',
            'label' => 'Показать все',
            'class' => 'btn btn-default',
            'title' => 'Показать все'
        ],
        'page' => [
            'icon' => 'resize-small',
            'label' => 'Страницы',
            'class' => 'btn btn-default',
            'title' => 'Постаничная разбивка'
        ],
    ],
    'columns' => [
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'attribute' => 'id',
            'width' => '50px',
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'hAlign' => GridView::ALIGN_CENTER,
            'value' => function (SetItem $setItem) {
                return
                    Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['/set/backend/item/move-up', 'id' => $setItem->id], [
                        'class' => 'pjax-action'
                    ]) .
                    Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['/set/backend/item/move-down', 'id' => $setItem->id], [
                        'class' => 'pjax-action'
                    ]);
            },
            'format' => 'raw',
            'width' => '40px',
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'width' => '50px',
            'format' => 'image',
            'value' => function(SetItem $setItem) {
                return !empty($setItem->product->images) ?  $setItem->product->images[0]->getThumbFileUrl('image', 'preview') : '';
            }
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'attribute' => 'title',
            'format' => 'raw',
            'label' => 'Название',
            'value' => function(SetItem $setItem) {
                return Html::a(
                    Html::encode($setItem->product->title),
                    ['/set/backend/item/update', 'id' => $setItem->id],
                    [
                        'data-pjax' => '0',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal',
                    ]
                );
            }
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'attribute' => 'quantity',
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'label' => 'Цена',
            'attribute' => 'price',
            'value' => function(SetItem $setItem) {
                return $setItem->product->price;
            }
        ],
        [
            'class' => ActionColumn::class,
            'contentOptions' => ['class' => 'text-nowrap'],
            'template' => '{update} {delete}',
            'width' => '50px',
            'options' => ['class' => 'text-nowrap'],
            'mergeHeader' => false,
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a(
                        '<i class="fa fa-pencil"></i> Редактировать',
                        ['/set/backend/item/update', 'id' => $model->id],
                        [
                            'class' => 'btn btn-xs btn-primary',
                            'data-pjax' => '0',
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                        ]
                    );
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a(
                        '<i class="fa fa-trash-o"></i>',
                        ['/set/backend/item/delete', 'id' => $model->id],
                        [
                            'class' => 'btn btn-xs btn-danger btn-delete pjax-action',
                            'title' => 'Удалить',
                            'data-confirm' => 'Вы уверены?',
                        ]
                    );
                },
            ],
        ],
    ],
]) ?>

