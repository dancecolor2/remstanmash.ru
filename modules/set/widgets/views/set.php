<?php

use app\helpers\FormatHelper;

/**
 * @var \yii\web\View $this
 * @var \app\modules\set\models\Set $set
 */

?>

<article class="cat is-small is-equipment">
    <header class="cat__header">
        <div class="cat__group">
            <h3 class="cat__title">
                <span class="cat__label">Комплектации <?= $set->title ?></span>
            </h3>
            <?php if (!empty($set->cost)): ?>
                <div class="cat__cost">
                    <div class="cost is-small">
                        <div class="cost__value">от <?= FormatHelper::number($set->cost) ?> <i>₽</i></div>
                    </div>
                </div>
            <?php endif ?>
        </div>
        <span class="cat__arrow">
          <svg viewBox="0 0 19.4 22.8">
            <path stroke="currentColor" stroke-width="2" fill="none" d="M9,20.4v-18l9,9L9,20.4z"></path>
            <path stroke="currentColor" stroke-width="2" fill="none" d="M18,11.4H0"></path>
          </svg>
        </span>
    </header>
</article>
