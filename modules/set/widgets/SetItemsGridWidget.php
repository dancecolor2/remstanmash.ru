<?php

namespace app\modules\set\widgets;

use app\modules\set\models\Set;
use app\modules\set\models\SetItemSearch;
use Yii;
use yii\base\Widget;

/**
 * @property Set $set
 */
class SetItemsGridWidget extends Widget
{
    public  $set;

    public function run()
    {
        $searchModel = new SetItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['set_id' => $this->set->id]);

        return $this->render('set_items_grid', [
            'set' => $this->set,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}