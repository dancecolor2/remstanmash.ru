<?php

namespace app\modules\set\widgets;

use app\modules\set\models\Set;
use yii\base\Widget;

/**
 * @property Set $set
 */
class SetWidget extends Widget
{
    public $set;

    public function run()
    {
        return $this->render('set', ['set' => $this->set]);
    }
}