<?php

namespace app\modules\set;

use app\modules\page\components\Pages;
use yii\base\BootstrapInterface;
use yii\web\GroupUrlRule;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $parentAlias = Pages::getPage('upgrade')->alias ?? 'upgrade';
        $app->get('urlManager')->rules[] = new GroupUrlRule([
            'rules' => [
                '/' . $parentAlias . '/<id>' => '/product/frontend/view',
            ]
        ]);
    }
}
