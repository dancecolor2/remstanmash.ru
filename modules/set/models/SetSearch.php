<?php

namespace app\modules\set\models;

use app\modules\set\models\Set;
use yii\data\ActiveDataProvider;

class SetSearch extends Set
{

    public function rules()
    {
        return [
            [['id', 'active', 'machine_id'], 'integer'],
            [['title', 'cost'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = Set::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => ['defaultOrder' => ['position' => SORT_ASC]],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['id' => $this->machine]);
        $query->andFilterWhere(['active' => $this->active]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'cost', $this->cost]);

        return $dataProvider;
    }
}
