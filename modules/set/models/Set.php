<?php

namespace app\modules\set\models;

use app\modules\admin\traits\QueryExceptions;
use app\modules\machine\models\Machine;
use app\modules\machine\models\MachineQuery;
use app\traits\active\ActiveTrait;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii2tech\ar\position\PositionBehavior;

/**
 * This is the model class for table "set".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $machine_id
 * @property int $active
 * @property string $title
 * @property int $position
 * @property int $cost
 *
 * @property Machine $machine
 * @property SetItem[] $items
 *
 * @mixin PositionBehavior
 */
class Set extends \yii\db\ActiveRecord
{
    use QueryExceptions;
    use ActiveTrait;

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => PositionBehavior::class,
                'groupAttributes' => ['machine_id'],
            ],
        ];
    }
    public static function tableName()
    {
        return 'set';
    }

    public function rules()
    {
        return [
            [['machine_id', 'title'], 'required'],
            [['machine_id', 'active', 'cost'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['machine_id'], 'exist', 'skipOnError' => true, 'targetClass' => Machine::class, 'targetAttribute' => ['machine_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'machine_id' => 'Станок',
            'active' => 'Активность',
            'title' => 'Название',
            'cost' => 'Стоимость',
        ];
    }

    /**
     * @return MachineQuery
     */
    public function getMachine() : ActiveQuery
    {
        return $this->hasOne(Machine::class, ['id' => 'machine_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getItems() : ActiveQuery
    {
        return $this->hasMany(SetItem::class, ['set_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SetQuery(get_called_class());
    }
}
