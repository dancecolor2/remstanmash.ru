<?php

namespace app\modules\set\models;

use app\modules\admin\traits\QueryExceptions;
use app\modules\product\models\Product;
use app\modules\set\models\Set;
use yii2tech\ar\position\PositionBehavior;

/**
 * This is the model class for table "set_item".
 *
 * @property int $id
 * @property int $set_id
 * @property int $product_id
 * @property int $position
 * @property int $quantity
 *
 * @property Product $product
 * @property Set $set
 *
 * @mixin PositionBehavior
 */
class SetItem extends \yii\db\ActiveRecord
{
    use QueryExceptions;

    public function behaviors()
    {
        return [
            [
                'class' => PositionBehavior::class,
                'groupAttributes' => ['set_id'],
            ],
        ];
    }

    public static function tableName()
    {
        return 'set_item';
    }

    public function rules()
    {
        return [
            [['set_id', 'product_id', 'quantity'], 'required'],
            [['set_id', 'product_id', 'quantity'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['set_id'], 'exist', 'skipOnError' => true, 'targetClass' => Set::class, 'targetAttribute' => ['set_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'set_id' => 'Комплектация',
            'product_id' => 'Товар',
            'quantity' => 'Количество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSet()
    {
        return $this->hasOne(Set::class, ['id' => 'set_id']);
    }
}
