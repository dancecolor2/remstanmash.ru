<?php

namespace app\modules\set\models;

use app\traits\active\ActiveQueryTrait;

/**
 * This is the ActiveQuery class for [[Set]].
 *
 * @see Set
 */
class SetQuery extends \yii\db\ActiveQuery
{
    use ActiveQueryTrait;
}
