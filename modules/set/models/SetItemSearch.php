<?php

namespace app\modules\set\models;

use app\modules\set\models\SetItem;
use yii\data\ActiveDataProvider;

/**
 * @property string $title
 * @property string $price
 */
class SetItemSearch extends SetItem
{
    public $title;
    public $price;

    public function rules()
    {
        return [
            [['id', 'quantity'], 'integer'],
            [['title', 'price'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = SetItem::find()->joinWith('product');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => [
                'defaultOrder' => ['position' => SORT_ASC],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['set_item.id' => $this->id]);
        $query->andFilterWhere(['set_item.quantity' => $this->quantity]);
        $query->andFilterWhere(['like', 'product.title', $this->title]);
        $query->andFilterWhere(['like', 'product.price', $this->price]);

        return $dataProvider;
    }
}
