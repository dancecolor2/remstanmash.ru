<?php

namespace app\modules\set\helpers;

use app\helpers\FormatHelper;
use app\modules\feedback\forms\SetForm;
use app\modules\machine\models\Machine;
use app\modules\set\models\Set;
use yii\helpers\Json;

class SetHelper
{
    public static function getMachineList()
    {
        return Machine::find()->orderBy('title')->indexBy('id')->select('title')->column();
    }

    public static function getTableInfoJson(Set $set)
    {
        return Json::encode([
            'id'=> $set->id,
            'cost'=> !empty($set->cost) ? FormatHelper::number($set->cost) . ' ₽' : '',
            'caption'=> 'Набор ' . $set->title,
        ]);
    }

    public static function getButtonInfoJson(Set $set)
    {
        return Json::encode([
            'id' => $set->id,
            'title' => 'набор ' . $set->title,
            'type' => SetForm::TYPE,
        ]);
    }


}