<?php

use mihaildev\ckeditor\CKEditor;

/**
 * @var \yii\web\View $this
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\modules\page\models\Page $page
 */

?>
<div class="box box-default box-solid ">
    <div class="box-header with-border">
        <h3 class="box-title">РЕМСТАНМАШ</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-8">
                <?= $form->field($page, 'elements[title_content][value]')->label('Заголовок') ?>
                <?= $form->field($page, 'content')->widget(CKEditor::class) ?>
            </div>
            <div class="col-lg-4">
                <div class="box box-primary box-solid ">
                    <div class="box-body">
                        <?= $form->field($page, 'elements[card_title][value]')->textarea(['rows' => 6])->label('Надпись') ?>
                        <?= $form->field($page, 'elements[card_caption][value]')->textarea(['rows' => 6])->label('Текст') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-default box-solid ">
    <div class="box-header with-border">
        <h3 class="box-title">Услуги</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($page, 'elements[services_capture][value]')->label('Надпись') ?>
                <?= $form->field($page, 'elements[services_title][value]')->label('Заголовок') ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($page, 'elements[services_text][value]')->textarea(['rows' => 5])->label('Текст') ?>
            </div>
        </div>
    </div>
</div>
<div class="box box-default box-solid <?= empty($page->elements['is_show_text_bottom']['value']) ? 'collapsed-box' : '' ?>">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?= $form->field($page, 'elements[is_show_text_bottom][value]')->checkbox([
                'onclick' => "$(this).closest('.box').toggleClass('collapsed-box')"
            ])->label('Нижний текст') ?>
        </h3>
    </div>
    <div class="box-body">
        <?= $form->field($page, 'elements[text_bottom_title][value]')->label('Заголовок') ?>
        <?= $form->field($page, 'elements[text_bottom][value]')->widget(CKEditor::class)->label(false) ?>
    </div>
</div>