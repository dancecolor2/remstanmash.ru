<?php

use mihaildev\ckeditor\CKEditor;

/**
 * @var \yii\web\View $this
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\modules\page\models\Page $page
 */

?>

<?= $form->field($page, 'elements[headline_text][value]')->textarea(['rows' => 4])->label('Текст под заголовком') ?>

<div class="box box-default box-solid <?= empty($page->elements['is_show_text_bottom']['value']) ? 'collapsed-box' : '' ?>">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?= $form->field($page, 'elements[is_show_text_bottom][value]')->checkbox([
                'onclick' => "$(this).closest('.box').toggleClass('collapsed-box')"
            ])->label('Нижний текст') ?>
        </h3>
    </div>
    <div class="box-body">
        <?= $form->field($page, 'elements[text_bottom_title][value]')->label('Заголовок') ?>
        <?= $form->field($page, 'elements[text_bottom][value]')->widget(CKEditor::class)->label(false) ?>
    </div>
</div>