<?php

use mihaildev\ckeditor\CKEditor;

/**
 * @var \yii\web\View $this
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\modules\page\models\Page $page
 */

?>

<?= $form->field($page, 'elements[headline_text][value]')->textarea(['rows' => 4])->label('Текст под заголовком') ?>

<div class="box box-default box-solid ">
    <div class="box-header with-border">
        <h3 class="box-title">Наши стремления</h3>
    </div>
    <div class="box-body">
        <?= $form->field($page, 'elements[title_content][value]')->label('Заголовок') ?>
        <?= $form->field($page, 'content')->widget(CKEditor::class)->label(false) ?>
    </div>
</div>

<div class="box box-default box-solid ">
    <div class="box-header with-border">
        <h3 class="box-title">Что мы делаем</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-8">
                <?= $form->field($page, 'elements[what_we_do_title][value]')->label('Заголовок') ?>
                <?= $form->field($page, 'elements[what_we_do_content][value]')->widget(CKEditor::class)->label(false) ?>
            </div>
            <div class="col-lg-4">
                <div class="box box-primary box-solid ">
                    <div class="box-body">
                        <?= $form->field($page, 'elements[card_title][value]')->textarea(['rows' => 4])->label('Надпись') ?>
                        <?= $form->field($page, 'elements[card_caption][value]')->textarea(['rows' => 4])->label('Текст') ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="box box-default box-solid ">
    <div class="box-header with-border">
        <h3 class="box-title">Клиенты</h3>
    </div>
    <div class="box-body">
        <?= $form->field($page, 'elements[clients_title][value]')->label('Надпись') ?>
        <?= $form->field($page, 'elements[clients_text][value]')->textarea(['rows' => 4])->label('Текст') ?>
    </div>
</div>


