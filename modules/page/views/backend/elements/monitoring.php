<?php

use mihaildev\ckeditor\CKEditor;

/**
 * @var \yii\web\View $this
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\modules\page\models\Page $page
 */

?>

<?= $form->field($page, 'elements[headline_text][value]')->textarea(['rows' => 4])->label('Текст под заголовком') ?>
<?= $form->field($page, 'content')->widget(CKEditor::class) ?>

<div class="box box-default box-solid">
    <div class="box-header">
        <h3 class="box-title">Блок со списками</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($page, 'elements[task_title][value]')->label('Заголовок') ?>
                <?= $form->field($page, 'elements[task_text][value]', [
                    'addon' => ['prepend' => ['content'=>'<i class="fa fa-plus" aria-hidden="true"></i>']]
                ])->textarea(['rows' => 5])->label(false)->hint('Каждый элемент с новой строки', ['class' => 'text-muted']) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($page, 'elements[decision_title][value]')->label('Заголовок') ?>
                <?= $form->field($page, 'elements[decision_text][value]', [
                    'addon' => ['prepend' => ['content'=>'<i class="fa fa-check" aria-hidden="true"></i>']]
                ])->textarea(['rows' => 5])->label(false)->hint('Каждый элемент с новой строки', ['class' => 'text-muted']) ?>
            </div>
        </div>
    </div>
</div>

<div class="box box-default box-solid">
    <div class="box-header">
        <h3 class="box-title">Нижний текст</h3>
    </div>
    <div class="box-body">
        <?= $form->field($page, 'elements[bottom_title][value]')->label('Заголовок') ?>
        <?= $form->field($page, 'elements[bottom_text][value]')->widget(CKEditor::class)->label(false) ?>
    </div>
</div>

