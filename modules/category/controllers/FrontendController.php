<?php

namespace app\modules\category\controllers;

use app\modules\category\models\Category;
use app\modules\filter\forms\FilterForm;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FrontendController extends Controller
{
    public function actionIndex()
    {
        $categories = Category::find()->where(['depth' => 1])->orderBy('lft')->all();

        return $this->render('index', compact('categories'));
    }

    public function actionView($alias)
    {
        $category = $this->findModel($alias);

        $filterForm = new FilterForm(compact('category'));
        $filterForm->load(Yii::$app->request->get());
        $dataProvider = $filterForm->filter();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['html' => $this->renderAjax('filter', compact('dataProvider'))];
        }

        $parents = $category->parents()->andWhere(['>', 'depth', 0])->all();
        $mainCategory = $category->isLeaf() && !empty($parents) ? $parents[count($parents) - 1] : $category;
        $subCategories = $mainCategory->children(1)->all();

        return $this->render('view', compact('category', 'parents', 'dataProvider', 'subCategories', 'filterForm'));
    }

    private function findModel($alias): Category
    {
        $category = Category::findOne(['alias' => $alias]);

        if (null == $category) {
            throw new NotFoundHttpException('The requested model does not exist.');
        }

        return $category;
    }
}
