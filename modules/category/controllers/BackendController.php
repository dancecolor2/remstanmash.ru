<?php

namespace app\modules\category\controllers;

use app\modules\admin\components\BalletController;
use app\modules\category\helpers\CategoryHelper;
use app\modules\category\models\Category;
use app\modules\category\models\CategorySearch;
use Yii;
use InvalidArgumentException;
use yii\bootstrap\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BackendController extends BalletController
{
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->getRequest()->get());

        return $this->render('index', compact('dataProvider', 'searchModel'));
    }

    public function actionCreate()
    {
        $model = new Category();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            $parent = Category::findOne($model->parent_id);

            if (null === $parent) {
                throw new InvalidArgumentException('No category with id ' . $model->parent_id);
            }

            $model->appendTo($parent);

            return $this->redirect(['update', 'id' => $model->id]);
        }

        list($dropDownArray, $dropDownOptionsArray) = CategoryHelper::generateDropDownArrays();

        return $this->render('create', compact('model', 'dropDownArray', 'dropDownOptionsArray'));
    }

    public function actionUpdate($id)
    {
        $category = $this->findModel($id);

        if (Yii::$app->request->isAjax && $category->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($category);
        }

        if ($category->load(Yii::$app->request->post())) {
            $currentParentId = $category->currentParent();

            if ($category->parent_id != $currentParentId) {
                $newParentModel = $this->findModel($category->parent_id);
                $category->appendTo($newParentModel);
            } else {
                $category->save();
            }

            return $this->redirect(['update', 'id' => $category->id]);
        }

        list($dropDownArray, $dropDownOptionsArray) = CategoryHelper::generateDropDownArrays($category);
        $category->parent_id = $category->currentParent();

        return $this->render('_main', compact('category', 'dropDownArray', 'dropDownOptionsArray'));
    }

    public function actionSeo($id)
    {
        $category = $this->findModel($id);

        if ($category->load(Yii::$app->getRequest()->post()) && $category->save()) {
            return $this->refresh();
        }

        return $this->render('_seo', compact('category'));
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithChildren();

        return $this->redirect(['index']);
    }

    public function actionDeleteIcon($id)
    {
        $this->findModel($id)->deleteIcon();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['success' => true];
    }

    public function actionMoveUp($id)
    {
        $model = $this->findModel($id);
        $prev = $model->prev()->one();

        if (null !== $prev) {
            $model->insertBefore($prev);
        }
    }

    public function actionMoveDown($id)
    {
        $model = $this->findModel($id);
        $next = $model->next()->one();

        if (null !== $next) {
            $model->insertAfter($next);
        }
    }

    /**
     * @param int $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): Category
    {
        $model = Category::findOne($id);

        if (null === $model) {
            throw new NotFoundHttpException('The requested model does not exist.');
        }

        return $model;
    }
}
