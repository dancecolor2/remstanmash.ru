<?php

namespace app\modules\category;

use app\modules\category\models\Category;
use enshrined\svgSanitize\Sanitizer;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\web\GroupUrlRule;
use yiidreamteam\upload\FileUploadBehavior;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public function bootstrap($app)
    {
        Event::on(Category::class, FileUploadBehavior::EVENT_AFTER_FILE_SAVE, [$this, 'clearSvg']);

        $app->get('urlManager')->rules[] = new GroupUrlRule([
            'rules' => [
                '/catalog' => '/category/frontend/index',
                '/catalog/<alias>' => '/category/frontend/view',
            ],
        ]);
    }

    public function clearSvg(Event $event)
    {
        /** @var Category $category */
        $category = $event->sender;
        $svgFile = $category->getUploadedFilePath('icon');

        if (is_file($svgFile)) {
            $sanitizer = new Sanitizer();
            $sanitizer->removeRemoteReferences(true);
            $dirtySVG = file_get_contents($svgFile);
            $cleanSVG = $sanitizer->sanitize($dirtySVG);
            file_put_contents($svgFile, $cleanSVG);
        }
    }
}