<?php

namespace app\modules\category\widgets;

use Yii;
use yii\base\Widget;

class LayoutWidget extends Widget
{
	public function run()
	{
		$layout = Yii::$app->request->get('layout', 'tile');

		return $this->render('layout', compact('layout'));
	}
}