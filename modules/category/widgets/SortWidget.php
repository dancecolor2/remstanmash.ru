<?php

namespace app\modules\category\widgets;

use Yii;
use yii\base\Widget;

class SortWidget extends Widget
{
	public function run()
	{
		$sortField = Yii::$app->request->get('sort', 'position');

		return $this->render('sort', compact('sortField'));
	}
}