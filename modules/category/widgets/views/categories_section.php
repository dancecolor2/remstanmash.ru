<?php

use app\modules\category\widgets\CategoryWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\category\models\Category[] $categories
 */

?>

<?php if (!empty($categories)): ?>
    <section class="section is-collapse">
        <div class="container">
            <div class="section__body">
                <div class="cats">
                    <ul class="cats__list grid">
                        <?php foreach ($categories as $category): ?>
                            <li class="cats__item col-4 col-m-6 col-s-12">
                                <?= CategoryWidget::widget(compact('category')) ?>
                            </li>
                        <?php endforeach ?>

                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>