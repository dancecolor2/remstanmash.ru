<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\category\models\Category $category
 */

?>

<?php if (!empty($category)): ?>
    <article class="cat">
        <div class="cat__icon">
            <?php $category->showIcon() ?>
        </div>
        <header class="cat__header">
            <div class="cat__group">
                <h3 class="cat__title">
                    <a class="cat__link" href="<?= $category->getHref() ?>" title="<?= $category->title ?>"><?= $category->title ?></a>
                </h3>
            </div>
            <span class="cat__arrow">
                <svg viewBox="0 0 19.4 22.8">
                    <path stroke="currentColor" stroke-width="2" fill="none" d="M9,20.4v-18l9,9L9,20.4z"></path>
                    <path stroke="currentColor" stroke-width="2" fill="none" d="M18,11.4H0"></path>
                </svg>
            </span>
        </header>
    </article>
<?php endif ?>