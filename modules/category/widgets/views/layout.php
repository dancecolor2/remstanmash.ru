<?php

use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var string $layout
 */

?>

<div class="cats__buttons">
	<p class="cats__description is-right">Отображать</p>
	<a data-type="tile" href="<?= Url::current(['layout' => 'tile']) ?>" class="cats__toggle <?= $layout == 'tile' ? 'is-active disabled' : '' ?>">Плиткой</a>
	<a data-type="row" href="<?= Url::current(['layout' => 'row']) ?>" class="cats__toggle <?= $layout == 'row' ? 'is-active disabled' : '' ?>">Списком</a>
</div>
