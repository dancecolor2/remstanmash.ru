<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var $sortField
 */

$directions = [
	'position' => 'По популярности',
	'title' => 'По названию А-Я',
	'-title' => 'По названию Я-А',
	'price' => 'По возрастанию цены',
	'-price' => 'По убыванию цены',
];

?>

<div class="cats__sort">
	<p class="cats__description">Сортировать</p>
	<div class="cats__sorting">
		<button class="cats__drop is-current js-collection-sorting"><?= ArrayHelper::getValue($directions, $sortField) ?></button>
		<div class="cats__box js-collection-box is-hidden">
			<?php foreach ($directions as $key => $direction): ?>
				<?php if ($key != $sortField): ?>
					<a href="<?= Url::current(['sort' => $key]) ?>" class="cats__drop"><?= $direction ?></a>
				<?php endif ?>
			<?php endforeach ?>
		</div>
	</div>
</div>