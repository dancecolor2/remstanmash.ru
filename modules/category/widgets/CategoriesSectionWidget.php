<?php

namespace app\modules\category\widgets;

use app\modules\category\models\Category;
use yii\base\Widget;

class CategoriesSectionWidget extends Widget
{
    public function run()
    {
        $categories = Category::find()->where(['depth' => 1])->orderBy('lft')->all();

        return $this->render('categories_section', compact('categories'));
    }
}