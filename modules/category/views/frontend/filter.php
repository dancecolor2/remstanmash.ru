<?php

use app\modules\category\widgets\LayoutWidget;
use app\modules\category\widgets\SortWidget;
use app\modules\product\widgets\ProductWidget;
use app\widgets\PaginationWidget;

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

?>


<div class="cats__header">
	<?= SortWidget::widget() ?>
	<?= LayoutWidget::widget() ?>
</div>
<div class="collection__body js-collection-body">
	<?php if (!empty($dataProvider->models)): ?>
		<ul class="collection__list <?= Yii::$app->request->get('layout', 'tile') == 'tile' ? 'grid' : '' ?>">
			<?php foreach ($dataProvider->models as $product): ?>
				<li class="collection__item">
					<?= ProductWidget::widget(compact('product')) ?>
				</li>
			<?php endforeach ?>
		</ul>
	<?php endif ?>
	<?php if ($dataProvider->pagination->pageCount > 1): ?>
		<div class="collection__pagination">
			<?= PaginationWidget::widget(['pagination' => $dataProvider->pagination]) ?>
		</div>
	<?php endif ?>
</div>