<?php

use app\modules\category\widgets\LayoutWidget;
use app\modules\category\widgets\SortWidget;
use app\modules\filter\widgets\FilterWidget;
use app\modules\page\components\Pages;
use app\modules\product\widgets\ProductWidget;
use app\widgets\CallbackSectionWidget;
use app\widgets\PaginationWidget;

/**
 * @var \app\modules\category\models\Category $category
 * @var \app\modules\category\models\Category[] $parents
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\category\models\Category[] $subCategories
 * @var string $viewType
 * @var \app\modules\page\models\Page $page
 * @var \app\modules\filter\forms\FilterForm $filterForm
 */

$category->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs('catalog');
foreach ($parents as $parent) {
    $this->params['breadcrumbs'][] = [
        'label' => $parent->getTitle(),
        'url' => $parent->getHref(),
        'class' => 'breadcrumb__link',
    ];
}
$this->params['h1'] = $category->getH1();

?>

<section class="section with-padding">
    <div class="container">
        <div class="section__body">
            <div class="collection js-collection is-products">
                <div class="grid is-row">
                    <div class="col-12">
                    <?php if (!empty($subCategories)): ?>
                            <div class="collection__cats">
                                <div class="cats js-cats">
                                    <button class="cats__button js-cats-button ">Выбрать подкатегорию</button>
                                    <div class="cats__type js-cats-type">
                                        <?php foreach ($subCategories as $subCategory): ?>
                                            <article class="cat is-small <?= $subCategory->id == $category->id ? 'is-active' : '' ?>">
                                                <header class="cat__header">
                                                    <div class="cat__group">
                                                        <h3 class="cat__title">
                                                            <a class="cat__link" href="<?= $subCategory->getHref() ?>" title="<?= $subCategory->title ?>"><?= $subCategory->title ?></a>
                                                        </h3>
                                                    </div>
                                                    <span class="cat__arrow">
                                                        <svg viewBox="0 0 19.4 22.8">
                                                            <path stroke="currentColor" stroke-width="2" fill="none" d="M9,20.4v-18l9,9L9,20.4z"></path>
                                                            <path stroke="currentColor" stroke-width="2" fill="none" d="M18,11.4H0"></path>
                                                        </svg>
                                                    </span>
                                                </header>
                                            </article>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="col-3 col-m-12">
                        <div class="collection__filter">
                            <?= FilterWidget::widget(compact('filterForm')) ?>
                        </div>

                        <div class="consultation">
                            <div class="consultation__top">
                                <h2 class="consultation__title">Нужна консультация?</h2>
                                <p class="consultation__text">Вы можете связаться с нами по номеру телефона либо оставить заявку и мы свяжемся с вами в течении 10 минут...</p>
                            </div>
                            <div class="consultation__bottom">
                                <div class="consultation__info">
                                    <div class="consultation__info-top">
                                        <span class="consultation__info-text">пн-сб</span>
                                        <span class="consultation__info-text">08:00 - 18:00</span>
                                    </div>
                                    <div class="consultation__info-bottom">
                                        <a href="tel: 8 800 511 02 67" class="consultation__info-link">8 800 511 02 67</a>
                                    </div>
                                </div>
                                <div class="consultation__button">
                                    <button class="consultation__btn" data-modal="callback">обратный звонок</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-9 col-m-12">
	                    <div class="js-collection-list">
		                    <div class="js-collection-body">
			                    <div class="cats__header">
				                    <?= SortWidget::widget() ?>
				                    <?= LayoutWidget::widget() ?>
			                    </div>
			                    <div class="collection__body js-collection-body">
				                    <?php if (!empty($dataProvider->models)): ?>
					                    <ul class="collection__list <?= Yii::$app->request->get('layout', 'tile') == 'tile' ? 'grid' : '' ?>">
						                    <?php foreach ($dataProvider->models as $product): ?>
							                    <li class="collection__item">
								                    <?= ProductWidget::widget(compact('product')) ?>
							                    </li>
						                    <?php endforeach ?>
					                    </ul>
				                    <?php endif ?>
				                    <?php if ($dataProvider->pagination->pageCount > 1): ?>
					                    <div class="collection__pagination">
						                    <?= PaginationWidget::widget(['pagination' => $dataProvider->pagination]) ?>
					                    </div>
				                    <?php endif ?>
			                    </div>
		                    </div>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if (!empty($category->content)): ?>
    <section class="section is-white with-padding is-layout-3">
        <div class="container">
            <div class="grid is-row">
                <div class="col-4 col-m-12">
                    <div class="section__content">
                        <section class="card is-alt has-right-shadow">
                            <h4 class="card__title">ремстанмаш — это решение конкретных задач</h4>
                            <div class="card__body">
                                <p class="card__caption">Исключительно профессиональная и&nbsp;информативная поддержка всех наших клиентов</p>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="shift-1 col-7 col-m-12 shift-m-0">
                    <div class="section__body">
                        <div class="section__text text">
                            <?= $category->content ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?= CallbackSectionWidget::widget() ?>