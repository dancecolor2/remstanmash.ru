<?php

use app\modules\category\widgets\CategoryWidget;
use app\modules\page\components\Pages;
use app\modules\vendor\widgets\VendorsWidget;
use app\widgets\CallbackSectionWidget;

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs();
Pages::getCurrentPage()->generateMetaTags();
$this->params['h1'] = Pages::getCurrentPage()->getH1();

?>

<?php if (!empty($categories)): ?>
    <section class="section">
        <div class="container">
            <div class="section__body">
                <div class="cats">
                    <ul class="cats__list grid">
                        <?php foreach ($categories as $category): ?>
                            <li class="cats__item col-4 ">
                                <?= CategoryWidget::widget(compact('category')) ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?= VendorsWidget::widget(['showButtonToVendors' => true]) ?>

<section class="section is-white with-padding is-layout-3">
    <div class="container">
        <div class="grid is-row">
            <div class="col-4 col-m-12">
                <div class="section__content">
                    <section class="card is-alt has-right-shadow">
                        <h4 class="card__title">ремстанмаш — это решение конкретных задач</h4>
                        <div class="card__body">
                            <p class="card__caption">Исключительно профессиональная и&nbsp;информативная поддержка всех наших клиентов</p>
                        </div>
                    </section>
                </div>
            </div>
            <div class="shift-1 col-7 col-m-12 shift-m-0">
                <div class="section__body">
                    <div class="section__text text">
                        <?= Pages::getCurrentPage()->content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?= CallbackSectionWidget::widget() ?>
