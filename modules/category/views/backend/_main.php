<?php

use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/**
 * @var array $categories
 * @var \app\modules\category\models\Category $category
 * @var array $dropDownArray
 * @var array $dropDownOptionsArray
 */

$this->title = $category->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $this->beginContent('@app/modules/category/views/backend/update.php', compact('category')) ?>
<?php $form = ActiveForm::begin() ?>
    <div class="box-body">
        <div class="row">

            <div class="col-xs-8">
                <div class="row">
                    <div class="col-xs-6"> <?= $form->field($category, 'parent_id')->dropDownList($dropDownArray, $dropDownOptionsArray) ?></div>
                </div>

                <?= $form->field($category, 'title') ?>
                <?= $form->field($category, 'alias', ['enableAjaxValidation' => true]) ?>
            </div>
            <div class="col-xs-4">
                <div class="single-kartik-image">
                    <?= $form->field($category, 'icon')->widget(FileInput::class, [
                        'pluginOptions' => [
                            'fileActionSettings' => [
                                'showDrag' => false,
                                'showZoom' => true,
                                'showUpload' => false,
                                'showDownload' => true,
                            ],
                            'initialPreviewDownloadUrl' => $category->getUploadedFileUrl('icon'),
                            'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'showClose' => false,
                            'showCancel' => false,
                            'browseClass' => 'btn btn-primary btn-block',
                            'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                            'browseLabel' =>  'Выберите файл',
                            'deleteUrl' => Url::to(['/category/backend/delete-icon', 'id' => $category->id]),
                            'initialPreview' => [
                                $category->hasIcon() ? $category->getUploadedFileUrl('icon') : null,
                            ],
                            'initialPreviewConfig' => [
                                $category->hasIcon() ? [
                                    'caption' => $category->icon,
                                    'size' => filesize($category->getUploadedFilePath('icon')),
                                    'downloadUrl' => $category->getUploadedFileUrl('icon'),
                                ] : [],
                            ],
                            'initialPreviewAsData' => true,
                        ],
                        'options' => ['accept' => 'image/svg+xml'],
                    ]);?>
                </div>
            </div>
        </div>

        <?= $form->field($category, 'content')->widget(CKEditor::class) ?>
    </div>

    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

<?php $form::end() ?>
<?php $this->endContent() ?>
