<?php

use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \app\modules\category\models\Category $model
 * @var yii\widgets\ActiveForm $form
 * @var array $dropDownArray
 * @var array $dropDownOptionsArray
 */

$this->title = 'Добавление категории';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="box box-primary">
<?php $form = ActiveForm::begin() ?>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'parent_id')->dropDownList($dropDownArray, $dropDownOptionsArray) ?>
                <?= $form->field($model, 'title')->textInput(['class' => 'form-control transIt']) ?>
                <?= $form->field($model, 'alias', ['enableAjaxValidation' => true])->textInput(['class' => 'form-control transTo']) ?>
            </div>
            <div class="col-xs-4">
                <div class="single-kartik-image">
                    <?= $form->field($model, 'icon')->widget(FileInput::class, [
                        'pluginOptions' => [
                            'fileActionSettings' => [
                                'showDrag' => false,
                                'showZoom' => true,
                                'showUpload' => false,
                                'showDownload' => true,
                            ],
                            'initialPreviewDownloadUrl' => $model->getUploadedFileUrl('icon'),
                            'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'showClose' => false,
                            'showCancel' => false,
                            'browseClass' => 'btn btn-primary btn-block',
                            'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                            'browseLabel' =>  'Выберите файл',
                            'deleteUrl' => Url::to(['/category/backend/delete-icon', 'id' => $model->id]),
                        ],
                        'options' => ['accept' => 'image/svg+xml'],
                    ]);?>
                </div>
            </div>
        </div>


        <?= $form->field($model, 'content')->widget(CKEditor::class) ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end() ?>
</div>