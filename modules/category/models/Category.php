<?php

namespace app\modules\category\models;

use app\modules\admin\behaviors\SeoBehavior;
use app\modules\admin\traits\QueryExceptions;
use app\modules\product\models\Product;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yiidreamteam\upload\FileUploadBehavior;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property int $id
 * @property int $lft
 * @property int $depth
 * @property int $rgt
 * @property string $title
 * @property string $alias
 * @property string $content
 * @property string $meta_t
 * @property string $meta_d
 * @property string $meta_k
 * @property string $h1
 * @property string $icon
 * @property string $icon_hash
 * @property int $show_on_main
 *
 * @property int $parent_id
 * @property Product[] $products
 *
 * @mixin SeoBehavior
 * @mixin NestedSetsBehavior
 * @mixin FileUploadBehavior
 */
class Category extends ActiveRecord
{
    use QueryExceptions;

    public $parent_id;

    public function behaviors()
    {
        return [
            NestedSetsBehavior::class,
            SeoBehavior::class,
            TimestampBehavior::class,
            'icon' => [
                'class' => FileUploadBehavior::class,
                'attribute' => 'icon',
                'filePath' => '@webroot/uploads/category/[[pk]]_[[attribute_icon_hash]].[[extension]]',
                'fileUrl' => '/uploads/category/[[pk]]_[[attribute_icon_hash]].[[extension]]',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return CategoryQuery
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    public static function tableName()
    {
        return 'category';
    }

    public function rules()
    {
        return [
            [['title', 'alias'], 'required'],
            [['alias'], 'unique'],
            ['parent_id', 'integer'],
            ['icon', 'file', 'skipOnEmpty' => true],
            [['title', 'alias', 'meta_t', 'meta_d', 'meta_k', 'h1'], 'string', 'max' => 255],
            [['content'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительская категория',
            'title' => 'Заголовок',
            'alias' => 'Алиас',
            'content' => 'Контент',
            'meta_t' => 'Заголовок страницы',
            'meta_d' => 'Описание страницы',
            'meta_k' => 'Ключевые слова',
            'h1' => 'H1',
            'icon' => 'Иконка',
            'show_on_main' => 'Показывать на главной странице'
        ];
    }

    /**
     * @return int
     */
    public function currentParent()
    {
        $current_parent = $this->parents(1)->one();
        $parent = null === $current_parent ? 0 : (int) $current_parent->id;

        return $parent;
    }

    public function getHref()
    {
        return Url::to(['/category/frontend/view', 'alias' => $this->alias]);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function hasIcon()
    {
        return !empty($this->icon) && file_exists($this->getUploadedFilePath('icon'));
    }

    public function beforeSave($insert)
    {
        if ($this->icon instanceof UploadedFile) {
            $this->icon_hash = uniqid();
        }
        return parent::beforeSave($insert);
    }

    public function deleteIcon()
    {
        /** @var FileUploadBehavior $fileBehavior */
        $fileBehavior = $this->getBehavior('icon');
        $fileBehavior->cleanFiles();
        $this->updateAttributes([
            'icon' => null,
            'icon_hash' => null,
        ]);
    }

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['category_id' => 'id']);
    }

    public function showIcon()
    {
        if ($this->hasIcon()) {
            readfile($this->getUploadedFilePath('icon'));
        }
    }
}
