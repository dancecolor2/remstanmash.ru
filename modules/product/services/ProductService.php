<?php

namespace app\modules\product\services;

use app\modules\product\repositories\ProductRepository;

class ProductService
{
    private $products;

    public function __construct(ProductRepository $products)
    {
        $this->products = $products;
    }

    public function assignChild($id, $childId)
    {
        $parent = $this->products->getById($id);
        $child = $this->products->getById($childId);
        $parent->assignChild($child);
        $this->products->save($parent);
    }

    public function revokeChild($id, $childId)
    {
        $parent = $this->products->getById($id);
        $child = $this->products->getById($childId);
        $parent->revokeChild($child);
        $this->products->save($parent);
    }
}