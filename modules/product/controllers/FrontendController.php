<?php

namespace app\modules\product\controllers;

use app\modules\product\models\Product;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class FrontendController extends Controller
{
    public function actionView($id)
    {
        $product = $this->findModel($id);
        $category = $product->hasParent() ? $product->parent->category : $product->category;
        $parents = $category->parents()->andWhere(['>', 'depth', 0])->all();
        $options = $product->getEavAttributes()->orderBy(['position' => SORT_ASC])->all();

        return $this->render('view', compact('product', 'category', 'parents', 'options'));
    }

    private function findModel($id): Product
    {
        $product = Product::find()->where(['id' => $id])->active()->one();

        if (null === $product) {
            throw new NotFoundHttpException('The requested model does not exist.');
        }

        return $product;
    }
}
