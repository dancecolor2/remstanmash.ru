<?php

namespace app\modules\product\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\product\forms\ProductAssignChildForm;
use app\modules\product\models\Product;
use app\modules\product\services\ProductService;
use DomainException;
use RuntimeException;
use Yii;
use yii\data\ActiveDataProvider;

class ChildsController extends BalletController
{
    private $service;

    public function __construct($id, $module, ProductService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionIndex($id)
    {
        $parent = Product::findOneOrException($id);
        $dataProvider = new ActiveDataProvider([
            'query' => $parent->getChilds()
        ]);

        $assignForm = new ProductAssignChildForm();

        return $this->render('index', compact('parent', 'dataProvider', 'assignForm'));
    }

    public function actionAssign($id)
    {
        $parent = Product::findOneOrException($id);
        $assignForm = new ProductAssignChildForm();

        if ($assignForm->load(Yii::$app->request->post()) && $assignForm->validate()) {
            try {
                $this->service->assignChild($parent->id, $assignForm->id);
                Yii::$app->session->setFlash('success', 'Товар привязан');
            } catch (DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            } catch (RuntimeException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', 'Техническая ошибка');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRevoke($id, $child_id)
    {
        try {
            $this->service->revokeChild($id, $child_id);
            Yii::$app->session->setFlash('success', 'Товар отвязан');
        } catch (DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        } catch (RuntimeException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', 'Техническая ошибка');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionProductList($search = '')
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()
                ->select(['id', 'title', 'code'])
                ->andWhere(['parent_id' => null])
                ->andFilterWhere([
                    'or',
                    ['like', 'title', $search],
                    ['like', 'code', $search],
                    ['id' => $search],
                ]),
            'sort' => ['defaultOrder' => ['title' => SORT_ASC]]
        ]);

        return [
            'results' => array_map(function(Product $product) {
                return [
                    'id' => $product->id,
                    'title' => $product->title,
                    'code' => $product->code ?: '',
                    'preview' => !empty($product->images) ? $product->images[0]->getThumbFileUrl('image', 'preview') : '',
                ];
            }, $dataProvider->getModels()),
            'pagination' => [
                'more' => $dataProvider->pagination->getPage() < $dataProvider->pagination->getPageCount() - 1,
            ],
        ];

    }
}