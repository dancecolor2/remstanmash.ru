<?php

namespace app\modules\product\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\product\models\Product;
use app\modules\product\models\ProductFile;
use app\modules\product\models\ProductFileSearch;
use Yii;
use yii\web\Response;

class FileController extends BalletController
{
    public function actionIndex($id)
    {
        $product = Product::findOneOrException($id);
        $searchModel = new ProductFileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['product_id' => $product->id]);

        return $this->render('index', compact('dataProvider', 'searchModel', 'product'));
    }

    public function actionCreate($id)
    {
        $product = Product::findOneOrException($id);
        $productFile = new ProductFile([
            'product_id' => $product->id,
            'scenario' => ProductFile::SCENARIO_CREATE,
            'active' => true,
        ]);
        if($productFile->load(Yii::$app->request->post()) && $productFile->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['message' => 'success'];
        }

        return $this->renderAjax('create', compact('productFile'));
    }

    public function actionUpdate($id)
    {
        $productFile = ProductFile::findOneOrException($id);
        if($productFile->load(Yii::$app->request->post()) && $productFile->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['message' => 'success'];
        }

        return $this->renderAjax('update', compact('productFile'));
    }

    public function actionDelete($id)
    {
        ProductFile::findOneOrException($id)->delete();
    }

    public function actionMoveUp($id)
    {
        ProductFile::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        ProductFile::findOneOrException($id)->moveNext();
    }
}