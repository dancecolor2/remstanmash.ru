<?php

namespace app\modules\product\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\category\models\Category;
use app\modules\product\forms\MassiveSetCategoryForm;
use app\modules\product\forms\ProductMassiveForm;
use app\modules\product\models\Product;
use Yii;
use yii\web\NotFoundHttpException;

class MassiveController extends BalletController
{
    public function actionDraft()
    {
        $form = new ProductMassiveForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            Product::updateAll(['active' => false], ['id' => $form->ids]);
        }

        return 'success';
    }

    public function actionActivate()
    {
        $form = new ProductMassiveForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            Product::updateAll(['active' => true], ['id' => $form->ids]);
        }
    }

    public function actionSetCategory()
    {
        $form = new MassiveSetCategoryForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            if ($form->categoryId === '0') {
                Product::updateAll(['category_id' => null], ['id' => $form->ids]);
                return;
            }

            if (!$category = Category::findOne($form->categoryId) ) {
                throw new NotFoundHttpException();
            }

            Product::updateAll(['category_id' => $category->id], ['id' => $form->ids]);
        }
    }

    public function actionDelete()
    {
        $form = new ProductMassiveForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            Product::deleteAll(['id' => $form->ids]);
        }
    }

    public function actionCheck()
    {
        $form = new ProductMassiveForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            Product::updateAll(['check' => true], ['id' => $form->ids]);
        }
    }

    public function actionUncheck()
    {
        $form = new ProductMassiveForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            Product::updateAll(['check' => false], ['id' => $form->ids]);
        }
    }
}