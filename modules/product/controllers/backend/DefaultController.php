<?php

namespace app\modules\product\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\eav\traits\EavControllerTrait;
use app\modules\product\models\Product;
use app\modules\product\models\ProductSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Response;
use yiidreamteam\upload\ImageUploadBehavior;

class DefaultController extends BalletController
{
    use EavControllerTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete' => ['POST'],
            ],
        ];
        return $behaviors;
    }

    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', compact('dataProvider', 'searchModel'));
    }

    public function actionCreate()
    {
        $product = new Product([
            'active' => true,
            'in_stock' => true,
        ]);

        if ($product->load(Yii::$app->request->post()) && $product->save()) {
            Yii::$app->session->setFlash('success', 'Товар обновлен(а)');
            return $this->redirect(Url::to(['update', 'id' => $product->id]));
        }

        return $this->render('create', compact('product'));
    }

    public function actionUpdate($id)
    {
        $product = Product::findOneOrException($id);

        if ($product->load(Yii::$app->request->post()) && $product->save()) {
            return $this->refresh();
        }

        return $this->render('update', compact('product'));
    }

    public function actionSeo($id)
    {
        $product = Product::findOneOrException($id);

        if ($product->load(Yii::$app->getRequest()->post()) && $product->save()) {
            return $this->refresh();
        }

        return $this->render('seo', compact('product'));
    }

    public function actionDelete($id)
    {
        Product::findOneOrException($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImage($id)
    {
        $product = Product::findOneOrException($id);
        /** @var ImageUploadBehavior $imageBehavior */
        $imageBehavior = $product->getBehavior('image');
        $imageBehavior->cleanFiles();
        $product->updateAttributes(['image' => null]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['success' => true];
    }

    public function actionOrderDescription($id)
    {
        $product = Product::findOneOrException($id);

        if ($product->load(\Yii::$app->request->post()) && $product->save()) {
            return $this->refresh();
        }

        return $this->render('order_description', compact('product'));
    }

    public function actionFeaturesDescription($id)
    {
        $product = Product::findOneOrException($id);

        if ($product->load(\Yii::$app->request->post()) && $product->save()) {
            return $this->refresh();
        }

        return $this->render('features_description', compact('product'));
    }

    public function actionProductList($q = null, $id = null) {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $q = trim($q);
            $products = Product::find()
                ->select('id, code, title')
                ->with('images')
                ->orWhere(['like', 'title', $q])
                ->orWhere(['like', 'code', $q])
                ->limit(20)->all();

            foreach ($products as &$product) {
                $image = !empty($product->images) ? $product->images[0]->getThumbFileUrl('image', 'preview') : '';
                $product = $product->toArray();
                $product['text'] = $product['title'];
                $product['image'] = $image;
            }
            $out['results'] = array_values($products);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Product::findOne($id)->title];
        }
        return $out;
    }

    public function actionMoveUp($id)
    {
        Product::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        Product::findOneOrException($id)->moveNext();
    }
}
