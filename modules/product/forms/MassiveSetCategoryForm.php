<?php

namespace app\modules\product\forms;

use yii\base\Model;

class MassiveSetCategoryForm extends Model
{
    public $ids;
    public $categoryId;

    public function formName()
    {
        return '';
    }

    public function rules()
    {
        return [
            [['ids', 'categoryId'], 'required'],
            ['ids', 'each', 'rule' => ['integer']],
            ['categoryId', 'integer'],
        ];
    }
}