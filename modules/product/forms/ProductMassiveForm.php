<?php

namespace app\modules\product\forms;

use yii\base\Model;

class ProductMassiveForm extends Model
{
    public $ids;

    public function formName()
    {
        return '';
    }

    public function rules()
    {
        return [
            ['ids', 'required'],
            ['ids', 'each', 'rule' => ['integer']],
        ];
    }
}