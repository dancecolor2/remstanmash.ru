<?php

namespace app\modules\product\forms;

use yii\base\Model;

class ProductAssignChildForm extends Model
{
    public $id;

    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer']
        ];
    }
}