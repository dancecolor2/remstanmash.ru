<?php

namespace app\modules\product\models;

use app\helpers\NestedSetsHelper;
use app\modules\category\models\Category;
use yii\data\ActiveDataProvider;
use yii\db\Query;

class ProductSearch extends Product
{
    public $parent;
	public $has_image;

    public function rules()
    {
        return [
            [['id', 'parent', 'active', 'has_image', 'check', 'vendor_id'], 'integer'],
            [['title', 'code'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = Product::find()->with('images');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => ['defaultOrder' => [
                'category_id' => SORT_ASC,
                'position' => SORT_ASC,
            ]],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->parent) {
            /** @var Category $parentCategory */
            $parentCategory = Category::findOne($this->parent);
            $ids = $parentCategory->children()->select(['id'])->column();
            $ids[] = $parentCategory->id;
            $query->andFilterWhere(['category_id' => $ids]);
        }

	    if ($this->has_image == 1) {
		    $query->andWhere(['id' => (new Query)->from('product_image')->select(['product_id'])->column()]);
	    } elseif ($this->has_image == 2) {
		    $query->andWhere(['not', ['id' => (new Query)->from('product_image')->select(['product_id'])->column()]]);
	    }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['vendor_id' => $this->vendor_id]);
        $query->andFilterWhere(['active' => $this->active]);
        $query->andFilterWhere(['check' => $this->check]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }

    public function getCategoriesDropDown()
    {
        $data = Category::find()
            ->select(['title', 'depth', 'id'])
            ->indexBy('id')
            ->orderBy('lft')
            ->andWhere(['>', 'depth', 0])
            ->asArray()
            ->all();

        return array_map(function (array $row) {
            return NestedSetsHelper::depthTitle($row['title'], $row['depth']);
        }, $data);
    }
}
