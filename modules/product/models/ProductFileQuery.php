<?php

namespace app\modules\product\models;

use app\traits\active\ActiveQueryTrait;

/**
 * @see ProductFile
 */
class ProductFileQuery extends \yii\db\ActiveQuery
{
    use ActiveQueryTrait;

}
