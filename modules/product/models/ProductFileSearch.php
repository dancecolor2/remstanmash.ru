<?php

namespace app\modules\product\models;

use yii\data\ActiveDataProvider;

class ProductFileSearch extends ProductFile
{
    public function rules()
    {
        return [
            [['id', 'active'], 'integer'],
            [['title'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = ProductFile::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['position' => SORT_ASC]],
            'pagination' => ['defaultPageSize' => 50],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['active' => $this->active]);
        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}