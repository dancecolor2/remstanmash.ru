<?php

namespace app\modules\product\models;

use app\modules\admin\behaviors\OptionsBehavior;
use app\modules\admin\behaviors\SeoBehavior;
use app\modules\admin\traits\QueryExceptions;
use app\modules\category\models\Category;
use app\modules\eav\EavBehavior;
use app\modules\eav\EavTrait;
use app\modules\eav\models\EavAttribute;
use app\modules\eav\models\EavAttributeQuery;
use app\modules\eav\models\EavAttributeValue;
use app\modules\vendor\models\Vendor;
use app\traits\active\ActiveTrait;
use app\traits\check\CheckTrait;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii2tech\ar\linkmany\LinkManyBehavior;
use yii2tech\ar\position\PositionBehavior;
use yiidreamteam\upload\FileUploadBehavior;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $category_id
 * @property int $vendor_id
 * @property int $price
 * @property string $code
 * @property string $title
 * @property string $h1
 * @property string $meta_t
 * @property string $meta_d
 * @property string $meta_k
 * @property int $active
 * @property int $check
 * @property int $in_stock
 * @property string $description
 * @property string $order_description
 * @property string $features_description
 * @property string $text_bottom
 * @property string $text_bottom_title
 * @property int $options_mask
 *
 * @property Category $category
 * @property EavAttribute[] $eavAttributes
 * @property ProductImage[] $images
 * @property ProductFile[] $files
 * @property Product[] $products
 * @property Vendor $vendor
 * @property Product $parent
 * @property Product[] $childs
 * @property int $parent_id [int(11)]
 * @property int $position [int(11)]
 *
 * @property array $childIds
 *
 * @mixin SeoBehavior
 * @mixin FileUploadBehavior
 * @mixin EavBehavior
 * @mixin ImageUploadBehavior
 * @mixin PositionBehavior
 */
class Product extends ActiveRecord
{
    use EavTrait;
    use ActiveTrait;
    use CheckTrait;
    use QueryExceptions;

    const COUNT_ON_PAGE = 30;

    public $options = [
        'is_show_text_bottom' => false,
    ];

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            SeoBehavior::class,
            OptionsBehavior::class,
            [
                'class' => PositionBehavior::class,
                'groupAttributes' => ['category_id'],
            ],
            [
                'class' => EavBehavior::class,
                'valueClass' => EavAttributeValue::class,
            ],
            [
                'class' => LinkManyBehavior::class,
                'relation' => 'products',
                'relationReferenceAttribute' => 'productIds',
            ],
            [
                'class' => LinkManyBehavior::class,
                'relation' => 'childs',
                'relationReferenceAttribute' => 'childIds',
            ],
        ];
    }

    public function hasParent(): bool
    {
        return !empty($this->parent_id);
    }

    public function hasPrice()
    {
        return !empty($this->price);
    }

    public function canHaveChilds()
    {
        return !$this->hasParent();
    }

    public function assignChild(Product $product)
    {
        if (!$this->canHaveChilds()) {
            throw new \DomainException('Нельзя привезать к данному товару.');
        }

        if ($product->id == $this->id) {
            throw new \DomainException('Нельзя привезать самого себя.');
        }

        if ($product->hasChilds()) {
            throw new \DomainException('Товар имеет подтовары.');
        }

        if ($product->hasParent()) {
            throw new \DomainException('Товар привязан к другому товару.');
        }

        if ($this->hasChild($product)) {
            throw new \DomainException('Товар уже привязан');
        }

        $this->childIds = array_merge($this->childIds, [$product->id]);
    }

    public function revokeChild(Product $product)
    {
        if (!$this->hasChild($product)) {
            throw new \DomainException('Товар не привязан');
        }

        $ids = $this->childIds;
        unset($ids[array_search($product->id, $ids)]);
        $this->childIds = $ids;
    }

    public function hasChilds()
    {
        return !empty($this->childIds);
    }

    public function hasChild(Product $product)
    {
        return in_array($product->id, $this->childIds);
    }

    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    public static function tableName()
    {
        return 'product';
    }

    public function rules()
    {
        return [
            [['title', 'category_id'], 'required'],
            [['code'], 'unique'],
            [['title', 'code', 'h1', 'meta_t', 'meta_d', 'meta_k', 'text_bottom_title'], 'string', 'max' => 255],
            [['category_id', 'price', 'vendor_id'], 'integer'],
            [['active', 'in_stock', 'check'], 'boolean'],
            [['productIds', 'options'], 'safe'],
            [['description', 'order_description', 'features_description', 'text_bottom'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата добавления',
            'updated_at' => 'Дата редактирования',
            'code' => 'Артикул',
            'category_id' => 'Категория',
            'images' => 'Фото',
            'title' => 'Название',
            'description' => 'Описание',
            'meta_t' => 'Заголовок страницы',
            'meta_d' => 'Описание страницы',
            'meta_k' => 'Ключевые слова',
            'h1' => 'H1',
            'active' => 'Активность',
	        'check' => 'Проверено',
            'price' => 'Цена',
            'vendor_id' => 'Производитель',
            'productIds' => "Может так же понадобиться",
            'in_stock' => 'В наличии'
        ];
    }

    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @return EavAttributeQuery
     */
    public function getEavAttributes(): ActiveQuery
    {
        return $this->hasMany(EavAttribute::class, ['id' => 'attribute_id'])
            ->viaTable(EavAttributeValue::tableName(), ['entity_id' => 'id']);
    }

    public function getHref($scheme = false)
    {
        return Url::to(['/product/frontend/view', 'id' => $this->id], $scheme);
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getImages(): ActiveQuery
    {
        return $this->hasMany(ProductImage::class, ['product_id' => 'id'])->orderBy('position');
    }

    public function getFiles() : ProductFileQuery
    {
        /** @var ProductFileQuery $query */
        $query = $this->hasMany(ProductFile::class, ['product_id' => 'id']);
        return $query->orderBy(['position' => SORT_ASC])->active();
    }

    public function getProducts() : ActiveQuery
    {
        return $this->hasMany(Product::class, ['id' => 'relation_id'])->viaTable('product_relation', ['product_id' => 'id']);
    }

    public function getVendor() : ActiveQuery
    {
        return $this->hasOne(Vendor::class, ['id' => 'vendor_id']);
    }

    public function hasImage()
    {
        return !empty($this->images) && is_file($this->images[0]->getUploadedFilePath('image'));
    }

    public function getParent()
    {
        return $this->hasOne(Product::class, ['id' => 'parent_id']);
    }

    /**
     * @return ProductQuery
     */
    public function getChilds(): ActiveQuery
    {
        return $this->hasMany(Product::class, ['parent_id' => 'id'])->orderBy('title');
    }

}
