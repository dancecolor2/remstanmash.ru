<?php

namespace app\modules\product\models;

use app\modules\category\models\Category;
use app\traits\active\ActiveQueryTrait;
use yii\db\ActiveQuery;

class ProductQuery extends ActiveQuery
{
   use ActiveQueryTrait;

    /**
     * @param Category $category
     * @return static
     */
   public function inCategory(Category $category)
   {
       $catIds = $category->children()->select('id')->column();
       array_push($catIds, $category->id);
       return $this->andWhere(['category_id' => $catIds]);
   }
}