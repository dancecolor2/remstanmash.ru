<?php

use app\modules\page\components\Pages;
use app\modules\product\helpers\ProductHelper;
use app\modules\product\widgets\ProductCardSliderWidget;
use app\modules\product\widgets\ProductChildsWidget;
use app\modules\product\widgets\ProductFileWidget;
use app\modules\product\widgets\ProductRelationsSectionWidget;
use app\modules\review\widgets\ReviewFormWidget;
use app\modules\review\widgets\ReviewsWidget;
use app\widgets\CallbackSectionWidget;
use app\widgets\TextBottomWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\Product $product
 * @var \app\modules\product\models\Product[] $bestsellers
 * @var \app\modules\product\models\ProductImage[] $images
 * @var \app\modules\category\models\Category $category
 * @var \app\modules\category\models\Category[] $parents
 * @var \app\modules\eav\models\EavAttribute[] $options
 * @var \app\modules\product\models\ProductFile $productFiles
 * @var \app\modules\page\models\Page $productionPage
 */

$product->generateMetaTags();
$this->params['h1'] = $product->getH1();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs('catalog');
foreach ($parents as $parent) {
    $this->params['breadcrumbs'][] = [
        'label' => $parent->getTitle(),
        'url' => $parent->getHref(),
        'class' => 'breadcrumb__link',
    ];
}
$this->params['breadcrumbs'][] = [
    'label' => $category->getTitle(),
    'url' => $category->getHref(),
    'class' => 'breadcrumb__link',
];
if ($product->hasParent()) {
    $this->params['breadcrumbs'][] = [
        'label' => $product->parent->getTitle(),
        'url' => $product->parent->getHref(),
        'class' => 'breadcrumb__link',
    ];
}

?>

<section class="section is-white with-padding is-padding-50">
    <div class="container">
        <div class="section__body">
            <div class="product-card">
                <div class="grid is-row">
                    <div class="col-4 col-md-6 col-m-12">
                        <div class="product-card__slider">
                            <?= ProductCardSliderWidget::widget(compact('product')) ?>
                        </div>
                    </div>
                    <div class="col-4 col-md-6 col-m-12">
                        <div class="product-card__info">
                            <?php if (!empty($product->code)): ?>
                                <div class="product-card__code">Арт. <?= $product->code ?></div>
                            <?php endif ?>
                            <?php if (!empty($options)): ?>
                                <div class="product-card__params">
                                    <div class="params js-params">
                                        <dl class="params__list js-params-list">
                                            <?php foreach ($options as $option): ?>
                                                <div class="params__item js-params-item">
                                                    <dt class="params__key"><?= $option->title ?></dt>
                                                    <dd class="params__value"><?= trim($product->{$option->name}->getRealValue() . ' ' . $option->unit) ?></dd>
                                                </div>
                                            <?php endforeach ?>
                                        </dl>
                                        <?php if (count($options) > 10): ?>
                                            <div class="is-visible-with-js">
                                                <button class="params__toggle js-params-toggle">
                                                    Все характеристики
                                                    <span class="params__arrow">
                                                        <span class="arrow is-down is-small">
                                                            <svg class="arrow__icon">
                                                                <use xlink:href="/img/sprite.svg#arrow-small"></use>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </button>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="col-4 col-md-12">
                        <div class="product-card__order">
                            <div class="product-order">
                                <?php if (!empty($product->vendor)): ?>
                                    <div class="product-order__vendor">
                                        <div class="vendor is-clear">
                                            <div class="vendor__cover">
                                                <img class="vendor__logo" src="<?= $product->vendor->getThumbFileUrl('image') ?>" alt="<?= $product->vendor->title ?>"/></div>
                                        </div>
                                    </div>
                                <?php endif ?>
                                <div class="product-order__instock">Под заказ</div>
                                <div class="product-order__cost">
                                    <?php if (!empty($product->price)): ?>
                                        <div class="cost is-wide is-large">
                                            <div class="cost__caption">Цена (без НДС)</div>
                                            <div class="cost__value"><?= $product->price ?> <i>₽</i></div>
                                        </div>
                                    <?php endif ?>
                                </div>
                                <div class="product-order__button">
                                    <button
                                    class="button js-button is-small is-wide"
                                    area-label="Сделать заказать"
                                    data-modal="order"
                                    onclick="ym(50330710,'reachGoal','push_button');"
                                    data-info='<?= ProductHelper::getOrderInfoJson($product) ?>'>Сделать заказ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php /*
<section class="section is-accent with-padding is-padding-50">
    <div class="container">
        <div class="section__body">
            <div class="features">
                <ul class="features__list grid is-row">
                    <li class="features__item col-4">
                        <div class="feature"><img class="feature__icon" src="/img/feature-1.svg" alt="alt"/>
                            <p class="feature__text">Точный расчет стоимости доставки производится на этапе <strong>оформления заказа</strong>.</p>
                        </div>
                    </li>
                    <li class="features__item col-4">
                        <div class="feature"><img class="feature__icon" src="/img/feature-2.svg" alt="alt"/>
                            <p class="feature__text">Заказ поступит в исполнение после <strong>100% оплаты</strong>. Оплатить нужно в течении 5 дней.</p>
                        </div>
                    </li>
                    <li class="features__item col-4">
                        <div class="feature"><img class="feature__icon" src="/img/feature-3.svg" alt="alt"/>
                            <p class="feature__text"><strong>Оплатить</strong> можно банковской картой, по безналичному или банковскому переводу.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
 */ ?>

<?php

$hasTabDescription = !empty($product->description) || !empty($options);
$hasTabFiles = !empty($product->files);
$hasTabOrder = !empty($product->order_description);
$hasTabFeatures = !empty($product->features_description);
$hasTabChilds = $product->hasChilds();

if ($hasTabDescription || $hasTabFiles || $hasTabOrder || $hasTabFeatures || $hasTabChilds): ?>
    <section class="section is-margin-top-50">
        <div class="container">
            <div class="section__body">
                <div class="tabs js-tabs" role="tablist">
                    <div class="tabs__header">
                        <?php if ($hasTabChilds): ?>
                            <a class="tabs__label js-tabs-label" href="#specific" title="Описание" role="tab" id="tab-5">другие спецификации</a>
                        <?php endif ?>
                        <?php if ($hasTabDescription): ?>
                            <a class="tabs__label js-tabs-label" href="#descr" title="Описание" role="tab" id="tab-1">Описание</a>
                        <?php endif ?>
                        <?php if ($hasTabFiles): ?>
                            <a class="tabs__label js-tabs-label" href="#files" title="Файлы" role="tab" id="tab-2">Файлы</a>
                        <?php endif ?>
                        <?php if ($hasTabOrder): ?>
                            <a class="tabs__label js-tabs-label" href="#order" title="Данные для заказа" role="tab" id="tab-3">Данные для заказа</a>
                        <?php endif ?>
                        <?php if ($hasTabFeatures): ?>
                            <a class="tabs__label js-tabs-label" href="#features" title="Технические характеристики" role="tab" id="tab-4">Технические характеристики</a>
                        <?php endif ?>
                        <a class="tabs__label js-tabs-label" href="#review" title="Отзывы" role="tab" id="tab-5">Отзывы</a>
                    </div>
                    <div class="tabs__body">
                        <div class="tabs__section js-tabs-section">
                            <?php if ($hasTabChilds): ?>
                                <div class="tabs__item js-tabs-item" id="specific" role="tabpanel" aria-labelledby="tab5">
                                <div class="tab">
                                    <?= ProductChildsWidget::widget(compact('product')) ?>
                                </div>
                            </div>
                            <?php endif ?>
                            <?php if ($hasTabDescription): ?>
                                <div class="tabs__item js-tabs-item" id="descr" role="tabpanel" aria-labelledby="tab1">
                                    <div class="tab">
                                        <div class="grid is-row">
                                            <div class="col-8 col-m-12">
                                                <div class="widget">
                                                    <h4 class="widget__title"><?= $product->title ?></h4>
                                                    <div class="widget__body">
                                                        <div class="text">
                                                            <?= $product->description ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if (!empty($options)): ?>
                                                <div class="col-4 col-m-12">
                                                    <div class="widget">
                                                        <h4 class="widget__title">Характеристики</h4>
                                                        <div class="widget__body">
                                                            <div class="params is-wide">
                                                                <dl class="params__list">
                                                                    <?php foreach ($options as $option): ?>
                                                                        <div class="params__item">
                                                                            <dt class="params__key"><?= $option->title ?></dt>
                                                                            <dd class="params__value"><?= trim($product->{$option->name}->getRealValue() . ' ' . $option->unit) ?></dd>
                                                                        </div>
                                                                    <?php endforeach ?>
                                                                </dl>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if ($hasTabFiles): ?>
                                <div class="tabs__item js-tabs-item" id="files" role="tabpanel" aria-labelledby="tab2">
                                    <div class="tab">
                                        <div class="docs">
                                            <ul class="docs__list grid is-columns">
                                                <?php foreach ($product->files as $productFile): ?>
                                                    <li class="docs__item col-4">
                                                        <?= ProductFileWidget::widget(compact('productFile')) ?>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if ($hasTabOrder): ?>
                                <div class="tabs__item js-tabs-item" id="order" role="tabpanel" aria-labelledby="tab3">
                                    <div class="tab">
                                        <div class="text is-order">
                                            <?= $product->order_description ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if ($hasTabFeatures): ?>
                                <div class="tabs__item js-tabs-item" id="features" role="tabpanel" aria-labelledby="tab4">
                                    <div class="tab">
                                        <?= $product->features_description ?>
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="tabs__item js-tabs-item" id="review" role="tabpanel" aria-labelledby="tab5">
                                <div class="tab">
                                    <div class="grid is-row">
                                        <div class="col-8">
                                            <?= ReviewsWidget::widget(['product_id' => $product->id]) ?>
                                        </div>
                                        <div class="col-4">
                                            <?= ReviewFormWidget::widget(['product_id' => $product->id]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?= ProductRelationsSectionWidget::widget(compact('product')) ?>

<?php if (!empty($product->options['is_show_text_bottom'])): ?>
    <?= TextBottomWidget::widget([
        'title' => $product->text_bottom_title,
        'text' => $product->text_bottom,
    ]) ?>
<?php endif ?>


<?= CallbackSectionWidget::widget() ?>
