<?php

use kartik\helpers\Html;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\Product $product
 */

?>

<?php $this->beginContent('@app/modules/product/views/backend/default/layout.php', ['product' => $product, 'breadcrumbs' => ['Особенности применения']]) ?>
    <?php $form = ActiveForm::begin() ?>
        <div class="box-body">
            <?= $form->field($product, 'features_description')->widget(CKEditor::class)->label(false) ?>
        </div>
        <div class="box-footer">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    <?php $form::end() ?>
<?php $this->endContent() ?>