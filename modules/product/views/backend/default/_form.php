<?php

use app\modules\product\helpers\ProductHelper;
use app\modules\product\models\Product;
use app\modules\product\models\ProductImage;
use kartik\file\FileInput;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var \yii\web\View $this
 * @var Product $product
 */

list($dropDownArray, $dropDownOptionsArray) = ProductHelper::generateDropDownArrays();

$formatJs = <<< 'JS'
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    
    let markup = '';
    
    if (repo.image) {
        markup = '<img src="' + repo.image + '" style="max-width: 60px; max-height: 60px; margin-right:10px; float:left;">'
    }
    markup += '<b> ' + repo.text + '</b>' 
        + '<br><small>Артикул: <b>' + repo.code + '</b></small>'
        + '<br><small>id: <b>' + repo.id +  '</b></small>';

        
    return '<div style="overflow:hidden; border:1px solid #337ab726; padding: 5px;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return  repo.text;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data) {
    return {
        results: data.results
    };
}
JS;

?>

<?php $form = ActiveForm::begin() ?>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($product, 'check')->checkbox(); ?>
                <?= $form->field($product, 'active')->checkbox(); ?>
                <?= $form->field($product, 'in_stock')->checkbox(); ?>
            </div>
            <div class="col-lg-6">
                <?php if ($product->hasParent()): ?>
                    Товар привязан к <?= Html::a($product->parent->title, ['/product/backend/childs/index', 'id' => $product->parent->id]) ?>
                <?php endif ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($product, 'title') ?>
                <?= $form->field($product, 'code') ?>
                <?= $form->field($product, 'price') ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($product, 'category_id')->dropDownList($dropDownArray, $dropDownOptionsArray) ?>
                <?= $form->field($product, 'vendor_id')->dropDownList(ProductHelper::getVendorList(), ['prompt' => '']) ?>

                <?php if (!$product->isNewRecord): ?>
                    <?= $form->field($product, 'productIds')->widget(Select2::class, [
                        'showToggleAll' => false,
                        'initValueText' => ProductHelper::getInitValueTexts($product),
                        'options' => ['placeholder' => 'Выберите товары', 'multiple' => true],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'ajax' => [
                                'url' => Url::to(['product-list']),
                                'dataType' => 'json',
                                'processResults' => new JsExpression($resultsJs),
                            ],
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        ],
                    ]); ?>
                <?php endif ?>

            </div>
        </div>
        <?php if (!$product->isNewRecord): ?>
            <?= $form->field($product, 'images[]')->widget(FileInput::class, [
                'pluginOptions' => [
                    'uploadUrl' => Url::to(['/product/backend/image/upload', 'id' => $product->id]),
                    'deleteUrl' => Url::to(['/product/backend/image/delete']),
                    'initialPreview' => array_map(function(ProductImage $image){
                        return $image->getUploadedFileUrl('image');
                    }, $product->images),
                    'initialPreviewConfig' => array_map(function(ProductImage $image){
                        return [
                            'key' => $image->id,
                            'caption' => $image->image,
                            //'size' => filesize($image->getUploadedFilePath('image')),
                            'downloadUrl' => $image->getUploadedFileUrl('image'),
                        ];
                    }, $product->images),
                    'initialPreviewAsData' => true,
                    'overwriteInitial' => false,
                    'showClose' => false,
                    'browseClass' => 'btn btn-primary text-right',
                    'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                    'browseLabel' =>  'Выберите файл',
                ],
                'pluginEvents' => [
                    'filesorted' => 'function(event, params) { 
                        $.post("' . Url::to(['/product/backend/image/sort']) . '",
                            { key : params.stack[params.newIndex].key, position : params.newIndex + 1 },
                        )
                    }',
                ],
                'options' => [
                    'multiple' => true,
                ],
            ]) ?>

            <?= $this->render('eav/attributes', compact('product')) ?>
            <?= $form->field($product, 'description')->widget(CKEditor::class) ?>

        <?php endif ?>

        <div class="box box-default box-solid <?= empty($product->options['is_show_text_bottom']) ? 'collapsed-box' : '' ?>">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <?= $form->field($product, 'options[is_show_text_bottom]')->checkbox([
                        'onclick' => "$(this).closest('.box').toggleClass('collapsed-box')"
                    ])->label('Нижний текст') ?>
                </h3>
            </div>
            <div class="box-body">
                <?= $form->field($product, 'text_bottom_title')->label('Заголовок') ?>
                <?= $form->field($product, 'text_bottom')->widget(CKEditor::class)->label(false) ?>
            </div>
        </div>

    </div>
    <div class="box-footer with-border">
        <button class="btn btn-success">Сохранить</button>
    </div>
<?php ActiveForm::end() ?>