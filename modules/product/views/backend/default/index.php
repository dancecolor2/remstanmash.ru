<?php

use app\modules\product\helpers\ProductHelper;
use app\modules\product\models\Product;
use kartik\grid\ActionColumn;
use kartik\grid\CheckboxColumn;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\product\models\ProductSearch $searchModel
 */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;

?>

<?= GridView::widget([
    'id' => 'grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summaryOptions' => ['class' => 'text-right'],
    'bordered' => false,
    'pjax' => true,
    'pjaxSettings' => [
        'id' => 'grid',
        'options' => [
            'id' => 'grid',
        ],
    ],
    'striped' => false,
    'hover' => true,
    'panel' => [
        'before',
        'after' => false,
    ],
    'toolbar' => [
        [
            'content' =>
                Html::a(
                    Icon::show('plus-outline') . ' Добавить',
                    ['create'],
                    [
                        'data-pjax' => 0,
                        'class' => 'btn btn-success'
                    ]
                ) . ' ' .
                Html::a(
                    Icon::show('arrow-sync-outline'),
                    ['index'],
                    [
                        'data-pjax' => 0,
                        'class' => 'btn btn-default',
                        'title' => Yii::t('app', 'Reset')
                    ]
                )
        ],
        '{toggleData}',
    ],
    'toggleDataOptions' => [
        'all' => [
            'icon' => 'resize-full',
            'label' => 'Показать все',
            'class' => 'btn btn-default',
            'title' => 'Показать все'
        ],
        'page' => [
            'icon' => 'resize-small',
            'label' => 'Страницы',
            'class' => 'btn btn-default',
            'title' => 'Постаничная разбивка'
        ],
    ],
    'columns' => [
        [
            'class' => CheckboxColumn::class,
            'checkboxOptions' => ['onchange' => 'appMassiveActions.updateActions()'],
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'attribute' => 'id',
            'width' => '100px',
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'hAlign' => GridView::ALIGN_CENTER,
            'value' => function (Product $product) {
                return
                    Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => $product->id], [
                        'class' => 'pjax-action'
                    ]) .
                    Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => $product->id], [
                        'class' => 'pjax-action'
                    ]);
            },
            'format' => 'raw',
            'width' => '40px',
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'format' => 'raw',
            'width' => '50px',
            'value' => function (Product $product) {
                return !empty($product->images) ? Html::img($product->images[0]->getThumbFileUrl('image')) : '';
            },
            'attribute' => 'has_image',
            'label' => '',
            'filter' => [1 => 'Есть', 2 => 'Нет'],
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'attribute' => 'code',
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'format' => 'raw',
            'attribute' => 'title',
            'value' => function (Product $product) {
                return Html::a($product->title, ['update', 'id' => $product->id], ['data-pjax' => '0']);
            }
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'attribute' => 'parent',
            'label' => 'Категория',
            'filter' => $searchModel->getCategoriesDropDown(),
            'value' => function (Product $product) {
                if (!empty($product->category)) {
                    return $product->category->getTitle();
                }

                return null;
            },
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'attribute' => 'vendor_id',
            'label' => 'Производитель',
            'filter' => ProductHelper::getVendorList(),
            'value' => function (Product $product) {
                return !empty($product->vendor) ? $product->vendor->title : '-';
            },
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'label' => 'Активный',
            'attribute' => 'active',
            'format' => 'raw',
            'filter' => Product::activeList(),
            'value' => function (Product $product) {
                return $product->getActiveIcon();
            },
        ],
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'label' => 'Проверен',
            'attribute' => 'check',
            'format' => 'raw',
            'filter' => Product::CheckList(),
            'value' => function (Product $product) {
                return $product->getCheckedIcon();
            },
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update}',
            'width' => '50px',
            'mergeHeader' => false,
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-pencil"></i> Редактировать', $url, [
                        'class' => 'btn btn-xs btn-primary',
                        'data-pjax' => '0',
                    ]);
                },
            ],
        ],
    ],
]) ?>

    <style xmlns:v-bind="http://www.w3.org/1999/xhtml">
        #actions {
            visibility: hidden;
            padding: 5px;
            background: rgb(248, 248, 248);
            border: 1px solid rgb(221, 221, 221);
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            z-index: 2000;
        }
    </style>

    <div id="actions" :style="{ visibility: isActive ? 'visible' : 'hidden' }" class="form-inline">
        <div class="container">
            <div class="form-group" style="width: 100px;">
                <span>Выбрано:</span>
                <b class="form-control-static">{{ ids.length }}</b>
            </div>
            <select v-bind:disabled="!isActive" v-model="action" class="form-control input-sm">
                <option disabled selected hidden :value="null">- Действия -</option>
                <option v-for="(action, index) in actions" v-bind:value="index">{{ action.title }}</option>
            </select>
            <div class="form-group">
                <select v-if="showCategoryList" v-model="categoryId" class="form-control input-sm">.
                    <option disabled selected hidden :value="null">- Выберите категорию -</option>
                    <?php foreach ($searchModel->getCategoriesDropDown() as $id => $title): ?>
                        <option value="<?= $id ?>"><?= $title ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <button v-if="showButton" type="button" @click="send" class="btn btn-sm btn-primary">Применить</button>
        </div>

    </div>


<?php $js = <<<JS
    
   appMassiveActions = new Vue({
      el: '#actions',
      data: {
          isReady: true,
        categoryId: null,
        action: null,
        ids: [],
        actions: [
            {
                'title': 'Активировать',
                'url': '/product/backend/massive/activate'
            },
            {
                'title': 'Заблокировать',
                'url': '/product/backend/massive/draft',
            },
            {
                'title': 'Выбрать категорию',
                'url': '/product/backend/massive/set-category',
                'is_categoryList' : true,
            },
            {
                'title': 'Проверено',
                'url': '/product/backend/massive/check'
            },
            {
                'title': 'Не проверено',
                'url': '/product/backend/massive/uncheck'
            },
            {
                'title': 'Удалить',
                'url': '/product/backend/massive/delete',
                'confirm': 'Подтвердите удаление',
            },
        ],
      },
      
      computed: {
          showButton: function() {
              let action =  this.actions[this.action];
              return this.isActive && action && (action.is_categoryList ? this.categoryId : true);
          },
          showCategoryList: function() {
              let action =  this.actions[this.action];
              return this.isActive && action && action.is_categoryList;
          },
          isActive: function() {
              return this.ids.length > 0;
          }
      },
      
      methods: {
          updateActions: function() {
              this.ids = $("#grid").yiiGridView("getSelectedRows");
          },
          send: function() {
              let _this = this;
              let action =  this.actions[this.action];
              let data = {ids: this.ids};
              
              if (action.is_categoryList) {
                  data.categoryId = this.categoryId;
              }
              
              if (!action.confirm || confirm(action.confirm)) {
                   $.post(action.url, data, function(data) {
                        $.pjax.reload('#grid');
                        _this.ids = [];
                        _this.action = null;
                  });
              }
             
          }
      }
    });
    $(document).on('change', '.select-on-check-all', appMassiveActions.updateActions);
    $(document).on('pjax:complete', appMassiveActions.updateActions);
JS
?>

<?php $this->registerJs($js, \yii\web\View::POS_END) ?>