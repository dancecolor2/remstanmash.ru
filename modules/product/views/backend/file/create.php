<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\ProductFile $productFile
 */

$this->title = 'Добавить файл';

?>

<?= $this->render('_form', compact('productFile')); ?>
