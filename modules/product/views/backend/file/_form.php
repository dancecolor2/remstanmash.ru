<?php

use app\helpers\FileHelper;
use app\modules\product\models\ProductFile;
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var ProductFile $productFile
 */

?>

<?php $form = ActiveForm::begin(['options' => ['id' => 'js-modal-form']]) ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?= $this->title ?></h4>
</div>
<div class="modal-body">
    <?= $form->field($productFile, 'active')->checkbox() ?>
    <?= $form->field($productFile, 'title') ?>
    <?= $form->field($productFile, 'file')->widget( FileInput::class, [
        'pluginOptions' => [
            'fileActionSettings' => [
                'showDelete' => false,
                'showDrag' => false,
                'showUpload' => false,
            ],
            'layoutTemplates' => ['actionDelete' => ''],
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'showClose' => false,
            'showCancel' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
            'browseLabel' =>  'Выберите файл',
            'pluginOptions' => ['previewFileType' => 'any'],
            'initialPreviewConfig' => [
                $productFile->hasFile() ? [
                    'type'=> FileHelper::getType($productFile->getUploadedFileUrl('file')),
                    'caption' => $productFile->file,
                    'downloadUrl' => $productFile->getUploadedFileUrl('file'),
                    'size' => filesize($productFile->getUploadedFilePath('file')),
                ] : [],
            ],
            'initialPreview'=> $productFile->hasFile() ? [$productFile->getUploadedFileUrl('file')] : [],
            'initialPreviewAsData'=>true,
        ],
    ]);?>
</div>
<div class="modal-footer">
    <button class="btn btn-success">Сохранить</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
</div>
<?php ActiveForm::end() ?>
