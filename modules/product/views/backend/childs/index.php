<?php

use app\modules\product\models\Product;
use kartik\form\ActiveForm;
use kartik\grid\ActionColumn;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\Product $parent
 * @var \yii\data\DataProviderInterface $dataProvider
 * @var \app\modules\product\forms\ProductAssignChildForm $assignForm
 */

$this->title = $parent->getTitle();

$formatJs = <<< 'JS'
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    
    let markup = '';
    
    if (repo.preview) {
        markup = '<img src="' + repo.preview + '" style="max-width: 60px; max-height: 60px; margin-right:10px; float:left;">'
    }
    markup += '<b> ' + repo.title + '</b>' 
        + '<br><small>Артикул: <b>' + repo.code + '</b></small>'
        + '<br><small>id: <b>' + repo.id +  '</b></small>';

        
    return '<div style="overflow:hidden; border:1px solid #337ab726; padding: 5px;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return  repo.text;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data) {
    return {
        results: data.results
    };
}
JS;

?>

<?php $this->beginContent('@app/modules/product/views/backend/default/layout.php', ['product' => $parent, ['breadcrumbs' => ['Подтовары']]]); ?>

<div class="box-body">
    <div class="row">
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin([
                'action' => ['assign', 'id' => $parent->id],
                'type' => ActiveForm::TYPE_INLINE,
                'options' => ['style' => 'display: flex; flex-direction: row; align-content: flex-start; margin-bottom: 15px; height: 34px;']

            ]) ?>

            <?= $form->field($assignForm, 'id', ['options' => ['style' => 'flex: 1 1 100%']])->widget(Select2::class, [
                'theme' => Select2::THEME_BOOTSTRAP,
                'data' => [''],
                'pluginOptions' => [
                    'ajax' => [
                        'url' => Url::to(['product-list']),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { return {search:params.term, page: params.page || 1}; }'),
                    ],
                    'templateResult' => new JsExpression('function(repo) { return window.formatRepo(repo); }'),
                    'templateSelection' => new JsExpression('function(repo) { return repo.id ? "["+ repo.id +"] " + repo.title : "... Введите Название, ID или Артикул "; }'),
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                ],
                'options' => [
                    'placeholder' => '...',
                ]
            ]) ?>

            <?= Html::submitButton('Привязать товар', ['class' => 'btn btn-success btn-xs']) ?>

            <?php ActiveForm::end() ?>
        </div>
    </div>
    <?= GridView::widget([
        'id' => 'grid',
        'dataProvider' => $dataProvider,
        'summaryOptions' => ['class' => 'text-right'],
        'bordered' => false,
        'pjax' => false,
        'striped' => false,
        'hover' => true,
        'toggleDataOptions' => [
            'all' => [
                'icon' => 'resize-full',
                'label' => 'Показать все',
                'class' => 'btn btn-default',
                'title' => 'Показать все'
            ],
            'page' => [
                'icon' => 'resize-small',
                'label' => 'Страницы',
                'class' => 'btn btn-default',
                'title' => 'Постаничная разбивка'
            ],
        ],
        'columns' => [
            [
                'class' => DataColumn::class,
                'vAlign' => GridView::ALIGN_MIDDLE,
                'attribute' => 'id',
                'width' => '100px',
            ],
            [
                'class' => DataColumn::class,
                'vAlign' => GridView::ALIGN_MIDDLE,
                'format' => 'raw',
                'width' => '50px',
                'value' => function (Product $product) {
                    return !empty($product->images) ? Html::img($product->images[0]->getThumbFileUrl('image')) : '';
                },
                'attribute' => 'has_image',
                'label' => '',
                'filter' => [1 => 'Есть', 2 => 'Нет'],
            ],
            [
                'class' => DataColumn::class,
                'vAlign' => GridView::ALIGN_MIDDLE,
                'attribute' => 'code',
            ],
            [
                'class' => DataColumn::class,
                'vAlign' => GridView::ALIGN_MIDDLE,
                'format' => 'raw',
                'attribute' => 'title',
                'value' => function (Product $product) {
                    return Html::a($product->title, ['/product/backend/default/update', 'id' => $product->id], ['data-pjax' => '0']);
                }
            ],
            [
                'class' => DataColumn::class,
                'vAlign' => GridView::ALIGN_MIDDLE,
                'label' => 'Активный',
                'attribute' => 'active',
                'format' => 'raw',
                'filter' => Product::activeList(),
                'value' => function (Product $product) {
                    return $product->getActiveIcon();
                },
            ],
            [
                'class' => DataColumn::class,
                'vAlign' => GridView::ALIGN_MIDDLE,
                'label' => 'Проверен',
                'attribute' => 'check',
                'format' => 'raw',
                'filter' => Product::CheckList(),
                'value' => function (Product $product) {
                    return $product->getCheckedIcon();
                },
            ],
            [
                'class' => DataColumn::class,
                'width' => '50px',
                'label' => 'Действия',
                'format' => 'raw',
                'value' => function(Product $product) {
                    return Html::button('Отвязать', [
                        'href' => Url::to(['revoke', 'id' => $product->parent_id, 'child_id' => $product->id]),
                        'data-method' => 'post',
                        'class' => 'btn btn-danger btn-xs'
                    ]);
                }
            ],
        ],
    ]) ?>
</div>
<?php $this->endContent() ?>
