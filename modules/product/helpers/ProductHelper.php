<?php

namespace app\modules\product\helpers;

use app\modules\category\models\Category;
use app\modules\eav\models\EavAttributeOption;
use app\modules\eav\models\EavAttributeValue;
use app\modules\feedback\forms\OrderForm;
use app\modules\product\models\Product;
use app\modules\vendor\models\Vendor;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class ProductHelper
{
    public static function generateDropDownArrays()
    {
        /** @var Category[] $categories */
        $categories = Category::find()->andWhere(['>', 'depth', 0])->orderBy(['lft' => SORT_ASC])->all();
        $dropDownArray = [];
        $dropDownOptionsArray = ['encodeSpaces' => false, 'prompt' => ''];

        foreach ($categories as $item) {
            $title = str_repeat('.', max((int) $item->depth - 1, 0));
            $title .= ' ' . $item->getTitle();
            $dropDownArray[$item->id] = $title;

            if (!$item->isLeaf()) {
                $dropDownOptionsArray['options'][$item->id]['disabled'] = true;
            }
        }

        return [$dropDownArray, $dropDownOptionsArray];
    }

    public static function getFilterDropDown()
    {
        $list = [];
        /** @var Category[] $categories */
        $categories = Category::find()->where(['>', 'depth', 0])->orderBy(['lft' => SORT_ASC])->all();

        foreach ($categories as $category) {
            $title = str_repeat('.', max((int) $category->depth - 1, 0));
            $title .= ' ' . $category->getTitle();
            $list[$category->id] = $title;
        }

        return $list;
    }

    public static function getOptionsList($attributeId, Category $category)
    {
        $categoryIds = array_merge([$category->id], $category->children()->select(['id'])->column());
        $optionIds = EavAttributeValue::find()
            ->joinWith(['entity'])
            ->where(['products.category_id' => $categoryIds, 'attribute_id' => $attributeId])
            ->select(['option_id'])
            ->column();

        /** @var EavAttributeOption[] $options */
        $options = EavAttributeOption::find()
            ->where(['id' => $optionIds])
            ->indexBy('id')
            ->orderBy(['value' => SORT_ASC])
            ->all();

        $list = ArrayHelper::map($options, 'id', function(EavAttributeOption $option) {
            return trim(implode(' ', [$option->value, $option->eavAttribute->unit]));
        });

        natcasesort($list);

        return $list;
    }

    public static function getOrderInfoJson(Product $product)
    {
        $image = !empty($product->images) ? $product->images[0]->getThumbFileUrl('image', 'preview') : '';

        return Json::encode([
            'id' => $product->id,
            'image' => $image,
            'code' => !empty($product->code) ? 'Арт. ' . $product->code : '',
            'title' => $product->title,
            'cost'=> !empty($product->price) ? $product->price . ' ₽' : '',
            'link' => $product->getHref(),
            'type' => OrderForm::TYPE,
        ]);
    }

    public static function getInitValueTexts(Product $product)
    {
        return array_map(function(Product $product) {
            return $product->title;
        }, $product->getProducts()->all());

    }

    public static function getVendorList()
    {
        return Vendor::find()->orderBy('title')->indexBy('id')->select('title')->column();
    }


}
