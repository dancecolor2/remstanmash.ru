<?php

namespace app\modules\product\readModels;

use app\modules\eav\models\EavAttribute;
use app\modules\product\models\Product;
use app\modules\product\models\ProductQuery;
use yii\helpers\ArrayHelper;

class ProductReadRepository
{
    public function getChildSpecifications(Product $product)
    {
        $child = $product->childs[0];
        var_dump($child->getEavAttributes()); die();

    }

    /**
     * @param Product[] $childs
     * @return EavAttribute[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getChildOptions(array $childs): array
    {
        $ids = ArrayHelper::getColumn($childs, 'id');

        return EavAttribute::find()
            ->alias('a')
            ->andWhere(['a.is_child_preview' => true])
            ->innerJoinWith(['products p' => function(ProductQuery $query) use ($ids) {
                return $query->andWhere(['p.id' => $ids])->active();
            }])
            ->limit(3)
            ->orderBy(['a.position' => SORT_ASC])
            ->groupBy('a.id')
            ->all();
    }

    /**
     * @param Product $product
     * @return Product[]
     */
    public function getChilds(Product $product): array
    {
        return $product->getChilds()
            ->active()
            ->orderBy('title')
            ->all();
    }
}