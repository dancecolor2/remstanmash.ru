<?php

namespace app\modules\product\repositories;

use app\modules\product\models\Product;

class ProductRepository
{
    public function getById($id): Product
    {
        if (!$product = Product::findOne($id)) {
            throw new \DomainException('Товар не найден');
        }

        return $product;
    }

    public function save(Product $product): void
    {
        if (!$product->save()) {
            throw new \RuntimeException('Product saving error');
        }
    }

    /**
     * @return iterable|Product[]
     */
    public function getIteratorForYandexMarket(): iterable
    {
        $query = Product::find()->andWhere(['not', ['or', ['price' => null], ['price' => 0]]]);

        foreach ($query->each() as $product) {
            yield $product;
        }
    }
}