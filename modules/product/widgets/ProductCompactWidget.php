<?php

namespace app\modules\product\widgets;

use app\modules\product\models\Product;
use yii\base\Widget;

/**
 * @property Product $product
 */
class ProductCompactWidget extends Widget
{
    public $product;

    public function run()
    {
        return $this->render('product_compact', ['product' => $this->product]);
    }
}