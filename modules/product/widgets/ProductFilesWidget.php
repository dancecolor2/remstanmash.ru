<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 08.02.2019
 * Time: 15:44
 */

namespace app\modules\product\widgets;

use app\modules\product\models\Product;
use yii\base\Widget;

/**
 * @property Product $product
 */
class ProductFilesWidget extends Widget
{
    public $product;

    public function run()
    {
        $files = $this->product->getFiles()->active()->all();

        return $this->render('product_files', compact('files'));
    }
}