<?php

use app\helpers\FormatHelper;

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\ProductFile $productFile
 */

?>

<?php if ($productFile->hasFile()): ?>
    <a class="doc" href="<?= $productFile->getUploadedFileUrl('file') ?>" title="<?= $productFile->title ?>" download="<?= $productFile->file ?>">
        <div class="doc__type" data-type="<?= pathinfo($productFile->getUploadedFileUrl('file'), PATHINFO_EXTENSION ) ?>"></div>
        <div class="doc__group">
            <h4 class="doc__title"><?= $productFile->title ?></h4>
            <p class="doc__size"><?= FormatHelper::fileSize($productFile->getUploadedFilePath('file')) ?></p>
        </div>
    </a>
<?php endif ?>

