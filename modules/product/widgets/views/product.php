<?php

use app\helpers\FormatHelper;
use app\modules\product\helpers\ProductHelper;

/**
 * @var \app\modules\product\models\Product $product
 * @var array $optionsChunks
 * @var string $image
 * @var array $orderInfo
 */

?>

<?php if (!empty($product)): ?>
    <article class="product">
        <div class="product__cover">
            <?php if (!empty($image)): ?>
                <img class="product__image" src="<?= $image ?>" alt="<?= $product->title ?>"/>
            <?php endif ?>
        </div>
        <div class="product__body">
            <header class="product__header">
                <?php if (!empty($product->code)): ?>
                    <p class="product__code">Арт. <?= $product->code ?></p>
                <?php endif ?>
                <h3 class="product__title">
                    <a class="product__link" href="<?= $product->getHref() ?>" title="<?= $product->title ?>"><?= $product->title ?></a>
                </h3>
            </header>
	        <?php if (!empty($optionsChunks)): ?>
		        <div class="product__params">
			        <div class="params">
				        <?php foreach ($optionsChunks as $options): ?>
					        <dl class="params__list is-small">
						        <?php foreach ($options as $option): ?>
							        <div class="params__item" style="height: <?= sizeof($options) == 1 ? '100' : '50' ?>%">
								        <dt class="params__key"><?= $option->title ?></dt>
								        <dd class="params__value"><?= trim($product->{$option->name}->getRealValue() . ' ' . $option->unit) ?></dd>
							        </div>
						        <?php endforeach ?>
					        </dl>
				        <?php endforeach ?>
			        </div>
		        </div>
	        <?php endif ?>
        </div>
        <footer class="product__footer">
            <div class="product__instock"><?= $product->in_stock ? 'Товар в наличии' : 'Под заказ' ?></div>
            <div class="product__cost">
                <div class="cost">
                    <?php if (!empty($product->price)): ?>
                        <div class="cost__caption">Цена</div>
                        <div class="cost__value"><?= FormatHelper::number($product->price) ?> <i>₽</i></div>
                    <?php endif ?>
                </div>
            </div>
            <div class="product__button">
                <button class="button js-button is-small is-wide" area-label="Заказать" data-modal="order" data-info='<?= ProductHelper::getOrderInfoJson($product) ?>'>Заказать</button>
            </div>
        </footer>
    </article>
<?php endif ?>
