<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\Product $product
 */

?>
<?php if (!empty($product->images)): ?>
    <div class="slider js-slider">
        <div class="slider__inner">
            <div class="slider__container js-slider-container">
                <div class="slider__wrapper js-slider-wrapper">
                    <?php foreach ($product->images as $image): ?>
                        <a class="slider__image js-slider-image" href="<?= $image->getImageFileUrl('image') ?>" style="background-image: url(<?= $image->getThumbFileUrl('image') ?>)"></a>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="slider__list js-slider-thumbnails">
                <ul class="slider__wrapper js-slider-wrapper is-roll">
                    <?php foreach ($product->images as $image): ?>
                        <li class="slider__item js-slider-item">
                            <div class="slider__thumbnail" style="background-image: url(<?= $image->getThumbFileUrl('image') ?>)"></div>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>
<?php endif ?>

