<?php

use app\modules\product\helpers\ProductHelper;

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\Product $product
 */

?>

<?php if (!empty($product)): ?>
    <article class="product is-compact">
        <div class="product__cover">
            <?php if (!empty($product->images)): ?>
                <img class="product__image" src="<?= $product->images[0]->getThumbFileUrl('image', 'preview') ?>" alt="<?= $product->title ?>"/>
            <?php endif ?>
        </div>
        <div class="product__body">
            <header class="product__header">
                <?php if (!empty($product->code)): ?>
                    <p class="product__code">Арт. <?= $product->code ?></p>
                <?php endif ?>
                <h3 class="product__title">
                    <a class="product__link" href="<?= $product->getHref() ?>" title="<?= $product->title ?>"><?= $product->title ?></a>
                </h3>
            </header>
        </div>
        <footer class="product__footer">
            <div class="product__instock"><?= $product->in_stock ? 'Товар в наличии' : 'Под заказ' ?></div>
            <div class="product__cost">
                <div class="cost is-wide">
                    <?php if (!empty($product->price)): ?>
                        <div class="cost__caption">Цена</div>
                        <div class="cost__value"><?= $product->price ?> <i>₽</i></div>
                    <?php endif ?>
                </div>
            </div>
            <div class="product__button">
                <button class="button js-button is-small is-wide" area-label="Заказать" data-modal="order" data-info='<?= ProductHelper::getOrderInfoJson($product) ?>'>Заказать</button>
            </div>
        </footer>
    </article>
<?php endif ?>