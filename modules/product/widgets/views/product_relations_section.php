<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\Product[] $products
 */

use app\modules\product\widgets\ProductCompactWidget;

?>

<?php if (!empty($products)): ?>
<section class="section has-roll is-grey">
    <div class="container">
        <header class="section__header">
            <p class="section__caption">комплектация</p>
            <h2 class="section__title">Может так же понадобиться</h2>
        </header>
        <div class="section__body">
            <div class="roll js-roll is-products">
                <div class="roll__control js-roll-control">
                    <div class="control">
                        <button class="control__arrow is-prev js-roll-prev" type="button" area-label="Назад"><span class="arrow is-left">
                        <svg class="arrow__icon">
                          <use xlink:href="/img/sprite.svg#arrow-alt"></use>
                        </svg></span>
                        </button>
                        <button class="control__arrow is-next js-roll-next" type="button" area-label="Вперёд"><span class="arrow is-right">
                        <svg class="arrow__icon">
                          <use xlink:href="/img/sprite.svg#arrow-alt"></use>
                        </svg></span>
                        </button>
                    </div>
                </div>
                <div class="roll__inner js-roll-swiper">
                    <ul class="roll__list js-roll-list">
                        <?php foreach ($products as $product): ?>
                            <li class="roll__item js-roll-item">
                                <?= ProductCompactWidget::widget(compact('product')) ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif ?>