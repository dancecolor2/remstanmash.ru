<?php

use app\modules\product\helpers\ProductHelper;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\Product $product
 * @var \app\modules\category\models\Category $category
 * @var \app\modules\eav\models\EavAttribute[] $options
 * @var bool $hasPrice
 */

?>

<div class="specifications">
    <div class="specifications__item">
        <img src="/img/spec-2.svg" alt="" class="specifications__img">
        <p class="specifications__title">Наименование</p>
    </div>
    <?php foreach ($options as $option): ?>
        <div class="specifications__item">
            <img src="/img/spec-2.svg" alt="" class="specifications__img">
            <p class="specifications__title"><?= $option->getLabel() ?><?= !empty($option->unit) ? ', ' . $option->unit : '' ?></p>
        </div>
    <?php endforeach ?>

<!--    <div class="specifications__item">-->
<!--        <img src="/img/spec-2.svg" alt="" class="specifications__img">-->
<!--        <p class="specifications__title">Тип</p>-->
<!--    </div>-->
<!--    <div class="specifications__item">-->
<!--        <img src="/img/spec-1.svg" alt="" class="specifications__img">-->
<!--        <p class="specifications__title">ID номер<br> для заказа</p>-->
<!--    </div>-->
    <?php if ($hasPrice): ?>
        <div class="specifications__item">
            <img src="/img/spec-3.svg" alt="" class="specifications__img">
            <p class="specifications__title">Цена</p>
        </div>
    <?php endif ?>
</div>
<div class="product-row__list">
<?php foreach ($product->childs as $child): ?>
    <div class="product-row">
        <div class="product-row__name">
            <?php if ($category->hasIcon()): ?>
                <img
                    class="product-row__image"
                    src="<?= $category->getUploadedFileUrl('icon') ?>"
                    width="24"
                    height="24"
                    alt="RA ~1Vpp 07 6">
            <?php endif ?>
            <p class="product-row__subname">Наименование</p>
            <p><a href="<?= Html::encode($child->getHref()) ?>"><?= Html::encode($child->title) ?></a></p>
        </div>
        <div class="product-row__body">
            <?php foreach ($options as $option): ?>
                <?php if ($value = $child->{$option->name}->getRealValue()): ?>
                    <p class="product-row__subname">Наименование</p>
                    <p><?= trim($value) ?></p>
                <?php else: ?>
                    <p class="product-row__subname">Наименование</p>
                    <p>-</p>
                <?php endif ?>
            <?php endforeach ?>
        </div>
        <div class="product-row__order">
            <?php if ($hasPrice): ?>
                <p class="product-row__subname">Наименование</p>
                <p class="product-row__price"><?= $child->hasPrice() ? $child->price . ' ₽' : '-' ?></p>
            <?php endif ?>
            <div class="product-row__button">
                <button
                    class="button js-button is-small is-wide"
                    area-label="заказать"
                    data-modal="order"
                    data-info='<?= ProductHelper::getOrderInfoJson($child) ?>'
                >заказать</button>
            </div>
        </div>
    </div>
<?php endforeach ?>

</div>