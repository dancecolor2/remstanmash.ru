<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\product\models\Product $product
 */

?>

<?php if (!empty($product)): ?>
    <article class="product is-simple">
        <div class="product__cover">
            <?php if ($product->hasImage()): ?>
                <img class="product__image" src="<?= $product->images[0]->getThumbFileUrl('image', 'preview') ?>" alt="<?= $product->title ?>"/>
            <?php endif ?>
        </div>
        <div class="product__body">
            <header class="product__header">
                <?php if (!empty($product->code)): ?>
                    <p class="product__code">Арт. <?= $product->code ?></p>
                <?php endif ?>
                <h3 class="product__title">
                    <a class="product__link" href="<?= $product->getHref() ?>" title="<?= $product->title ?>"><?= $product->title ?></a>
                </h3>
            </header>
        </div>
    </article>
<?php endif ?>
