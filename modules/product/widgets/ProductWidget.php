<?php

namespace app\modules\product\widgets;

use app\modules\product\models\Product;
use yii\base\Widget;

/**
 * @property Product $product
 */
class ProductWidget extends Widget
{
    public $product;

    public function run()
    {
        $product = $this->product;
        $image = !empty($product->images) ? $product->images[0]->getThumbFileUrl('image', 'preview') : '';
        $optionsChunks = $this->getOptionsChanks($product);

        return $this->render('product', compact('product', 'image', 'optionsChunks'));
    }

	private function getOptionsChanks(Product $product)
	{
		return $product->eavAttributes
			? array_chunk(array_slice($product->eavAttributes, 0, 4), ceil(sizeof($product->eavAttributes)/2))
			: [];
	}
}
