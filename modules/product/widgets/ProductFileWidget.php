<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 08.02.2019
 * Time: 15:44
 */

namespace app\modules\product\widgets;

use app\modules\product\models\Product;
use app\modules\product\models\ProductFile;
use yii\base\Widget;

/**
 * @property ProductFile $productFile
 */
class ProductFileWidget extends Widget
{
    public $productFile;

    public function run()
    {
        return $this->render('product_file', ['productFile' => $this->productFile]);
    }
}