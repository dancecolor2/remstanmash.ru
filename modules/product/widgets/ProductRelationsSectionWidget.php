<?php

namespace app\modules\product\widgets;

use app\modules\product\models\Product;
use yii\base\Widget;

/**
 * @property Product $product
 */
class ProductRelationsSectionWidget extends Widget
{
    public $product;

    public function run()
    {
        return $this->render('product_relations_section', ['products' => $this->product->products]);
    }
}