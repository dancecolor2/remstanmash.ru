<?php

namespace app\modules\product\widgets;

use app\modules\product\models\Product;
use app\modules\product\readModels\ProductReadRepository;
use yii\base\Widget;

class ProductChildsWidget extends Widget
{
    /**
     * @var Product
     */
    public $product;

    private $products;

    public function __construct(ProductReadRepository $products, $config = [])
    {
        parent::__construct($config);
        $this->products = $products;
    }

    public function run()
    {
        $childs = $this->products->getChilds($this->product);
        $options = $this->products->getChildOptions($childs);
        $category = $this->product->category->parents()->andWhere(['depth' => 1])->one();
        if (empty($category)) {
            $category = $this->product->category;
        }

        $hasPrice = false;

        foreach ($childs as $child) {
            if ($child->hasPrice()) {
                $hasPrice = true;
                break;
            }
        }

        return $this->render('product_childs', [
            'product' => $this->product,
            'options' => $options,
            'category' => $category,
            'hasPrice' => $hasPrice
        ]);
    }
}