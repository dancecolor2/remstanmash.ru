<?php

namespace app\modules\product\widgets;

use app\modules\product\models\Product;
use yii\base\Widget;

/**
 * @property Product $product
 */
class ProductCardSliderWidget extends Widget
{
    public $product;

    public function run()
    {
        return $this->render('product_card_slider', ['product' => $this->product]);
    }
}