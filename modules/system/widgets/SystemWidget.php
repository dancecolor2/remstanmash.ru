<?php

namespace app\modules\system\widgets;

use app\modules\system\models\System;
use yii\base\Widget;

/**
 * @property System $system
 */
class SystemWidget extends Widget
{
    public $system;

    public function run()
    {
        return $this->render('system', [
            'system' => $this->system,
        ]);
    }
}