<?php

use app\helpers\FormatHelper;

/**
 * @var \yii\web\View $this
 * @var \app\modules\system\models\SystemFile $file
 */

?>

<?php if (!empty($file)): ?>
    <a class="doc" href="<?= $file->getUploadedFileUrl('file') ?>" title="<?= $file->title ?>" download="<?= $file->file ?>">
        <div class="doc__type" data-type="<?= pathinfo($file->file, PATHINFO_EXTENSION) ?>"></div>
        <div class="doc__group">
            <h4 class="doc__title"><?= $file->title ?></h4>
            <p class="doc__size"><?= FormatHelper::fileSize($file->getUploadedFilePath('file')) ?></p>
        </div>
    </a>
<?php endif ?>