<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\system\models\System $system
 */

use app\helpers\FormatHelper;
use app\modules\eav\helpers\Html;
use app\modules\system\widgets\SystemFileWidget;
use yii\helpers\Json;

?>

<?php if (!empty($system)): ?>

<article class="system">
    <header class="system__header">
        <h3 class="system__title is-hide"><?= $system->title ?></h3>
        <?php if ($system->hasImage()): ?>
            <div class="system__vendor">
                <div class="vendor is-clear">
                    <div class="vendor__cover">
                        <img class="vendor__logo" src="<?= $system->getThumbFileUrl('image') ?>" alt="<?= $system->title ?>"/>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </header>
    <?php if (!empty($system->content)): ?>
        <div class="system__text">
            <div class="text">
                <?= $system->content ?>
            </div>
        </div>
    <?php endif ?>
    <?php if (!empty($system->list)): ?>
        <div class="system__group">
            <?php if (!empty($system->subtitle)): ?>
                <h4 class="system__subtitle"><?= $system->subtitle ?></h4>
            <?php endif ?>
            <div class="system__list">
                <ul class="list">
                    <?php foreach (\app\helpers\ContentHelper::splitText($system->list) as $item): ?>
                        <li class="list__item"><?= FormatHelper::nText($item) ?></li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    <?php endif ?>

    <?php if (!empty($system->files)): ?>
        <div class="system__docs">
            <div class="docs is-list">
                <ul class="docs__list grid is-columns">
                    <?php foreach ($system->files as $file): ?>
                        <li class="docs__item col-12">
                            <?= SystemFileWidget::widget(compact('file')) ?>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    <?php endif ?>
    <footer class="system__footer">
        <div class="system__button">
            <button class="button js-button is-wide is-multistring"
                    area-label="&lt;i&gt;<?= Html::encode($system->title) ?>&lt;/i&gt;Заказать мониторинг"
                    data-modal="order"
                    data-info='<?= Json::encode([ 'id' => $system->id, 'title' => $system->title]) ?>'>
                <i><?= $system->title ?></i>Заказать мониторинг
            </button>
        </div>
    </footer>
</article>

<?php endif ?>