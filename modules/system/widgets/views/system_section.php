<?php

use app\modules\system\widgets\SystemWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\system\models\System[] $systems
 */

?>

<?php if (!empty($systems)): ?>
    <section class="section is-grey">
        <div class="container">
            <header class="section__header">
                <h2 class="section__title">Системы мониторинга</h2>
            </header>
            <div class="section__body">
                <div class="systems">
                    <ul class="systems__list grid is-columns">
                        <?php foreach ($systems as $system): ?>
                            <li class="systems__item col-6 col-m-12">
                                <?= SystemWidget::widget(compact('system')) ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>