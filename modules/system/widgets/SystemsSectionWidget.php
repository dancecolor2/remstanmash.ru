<?php

namespace app\modules\system\widgets;

use app\modules\system\models\System;
use yii\base\Widget;

class SystemsSectionWidget extends Widget
{
    public function run()
    {
        $systems = System::find()->active()->orderBy('position')->all();

        return $this->render('system_section', compact('systems'));
    }
}