<?php

namespace app\modules\system\widgets;

use app\modules\system\models\SystemFile;
use yii\base\Widget;

/**
 * @property SystemFile $file
 */
class SystemFileWidget extends Widget
{
    public $file;

    public function run()
    {
        return $this->render('file',[
            'file' => $this->file,
        ]);
    }
}