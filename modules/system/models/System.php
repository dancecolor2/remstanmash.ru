<?php

namespace app\modules\system\models;

use PHPThumb\GD;
use yii\db\ActiveRecord;
use app\modules\admin\traits\QueryExceptions;
use yii\behaviors\TimestampBehavior;
use app\traits\active\ActiveTrait;
use yii\web\UploadedFile;
use yii2tech\ar\position\PositionBehavior;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * @property int $id
 * @property int $created_at Дата создания
 * @property int $updated_at Дата редактирования
 * @property int $active
 * @property string $title
 * @property string $image Картинка
 * @property string $image_hash
 * @property string $content Текст
 * @property string $subtitle Заголовок списка
 * @property string $list Список
 *
 * @property SystemFile[] $files
 *
 * @mixin ImageUploadBehavior
 * @mixin PositionBehavior
 */
class System extends ActiveRecord
{
    use QueryExceptions;
    use ActiveTrait;

    public function behaviors()
    {
        return [
            PositionBehavior::class,
            TimestampBehavior::class,
            'image' => [
                'class' => ImageUploadBehavior::class,
                'createThumbsOnRequest' => true,
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['processor' => function (GD $thumb) {
                        $thumb->resize(470, 52);
                    }],
                ],
                'filePath' => '@webroot/uploads/system/[[pk]]_[[attribute_image_hash]].[[extension]]',
                'fileUrl' => '/uploads/system/[[pk]]_[[attribute_image_hash]].[[extension]]',
                'thumbPath' => '@webroot/uploads/cache/system/[[pk]]_[[profile]]_[[attribute_image_hash]].[[extension]]',
                'thumbUrl' => '/uploads/cache/system/[[pk]]_[[profile]]_[[attribute_image_hash]].[[extension]]',
            ],
        ];
    }

    public static function tableName()
    {
        return 'system';
    }

    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['title'], 'required'],
            [['content', 'list'], 'string'],
            [['title', 'subtitle'], 'string', 'max' => 255],
            ['image', 'image', 'extensions' => ['png', 'jpg', 'jpeg'], 'checkExtensionByMimeType' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'active' => 'Активность',
            'title' => 'Заголовок',
            'image' => 'Картинка',
            'content' => 'Текст',
            'subtitle' => 'Заголовок списка',
            'list' => 'Список',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SystemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemQuery(get_called_class());
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function beforeSave($insert)
    {
        if ($this->image instanceof UploadedFile) {
            $this->image_hash = uniqid();
        }
        return parent::beforeSave($insert);
    }

    public function deleteImage()
    {
        /** @var ImageUploadBehavior $imageBehavior */
        $imageBehavior = $this->getBehavior('image');
        $imageBehavior->cleanFiles();
        $this->updateAttributes(['image' => null, 'image_hash' => null]);
    }

    public function hasImage()
    {
        return !empty($this->image) && is_file($this->getUploadedFilePath('image'));
    }

    public function getFiles() : SystemFileQuery
    {
        /** @var SystemFileQuery $query */
        $query = $this->hasMany(SystemFile::class, ['system_id' => 'id']);
        return $query->orderBy(['position' => SORT_ASC])->active();
    }
}
