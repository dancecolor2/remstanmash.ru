<?php

namespace app\modules\system\models;

use app\traits\active\ActiveQueryTrait;
/**
 * @see System
 */
class SystemQuery extends \yii\db\ActiveQuery
{
    use ActiveQueryTrait;
}
