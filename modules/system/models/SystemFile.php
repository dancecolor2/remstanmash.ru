<?php

namespace app\modules\system\models;

use app\modules\admin\traits\QueryExceptions;
use app\traits\active\ActiveTrait;
use yii\web\UploadedFile;
use yii2tech\ar\position\PositionBehavior;
use yiidreamteam\upload\FileUploadBehavior;

/**
 * This is the model class for table "systems_files".
 *
 * @property int $id
 * @property string $title
 * @property string $file
 * @property int $position
 * @property int $active
 * @property int $system_id
 * @property string $file_hash
 *
 * @mixin FileUploadBehavior
 * @mixin PositionBehavior
 */
class SystemFile extends \yii\db\ActiveRecord
{
    use ActiveTrait;
    use QueryExceptions;

    const SCENARIO_CREATE = 'scenario_create';

    public function behaviors()
    {
        return [
            PositionBehavior::class,
            [
                'class' => FileUploadBehavior::class,
                'attribute' => 'file',
                'fileUrl' => '/uploads/system_files/[[pk]]_[[attribute_file_hash]].[[extension]]',
                'filePath' => '@webroot/uploads/system_files/[[pk]]_[[attribute_file_hash]].[[extension]]',
            ]
        ];
    }

    public static function tableName()
    {
        return 'system_file';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['active'], 'integer'],
            [['title', 'file_hash'], 'string', 'max' => 255],
            [['file'], 'file', 'skipOnEmpty' => true],
            [['file'], 'file', 'skipOnEmpty' => false, 'on' => self::SCENARIO_CREATE],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'file' => 'Файл',
            'active' => 'Активность',
        ];
    }

    public static function find()
    {
        return new SystemFileQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if ($this->file instanceof UploadedFile) {
            $this->file_hash = uniqid();
        }
        return parent::beforeSave($insert);
    }

    public function hasFile()
    {
        return !empty($this->file) && is_file($this->getUploadedFilePath('file'));
    }

}
