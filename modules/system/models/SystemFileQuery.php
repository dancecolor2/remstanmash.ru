<?php

namespace app\modules\system\models;

use app\traits\active\ActiveQueryTrait;

/**
 * @see SystemFile
 */
class SystemFileQuery extends \yii\db\ActiveQuery
{
    use ActiveQueryTrait;

}
