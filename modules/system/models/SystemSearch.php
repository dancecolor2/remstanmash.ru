<?php

namespace app\modules\system\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\system\models\System;

/**
 * SystemSearch represents the model behind the search form of `app\modules\system\models\System`.
 */
class SystemSearch extends System
{
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'active'], 'integer'],
            [['title', 'image', 'image_hash', 'content', 'subtitle', 'list'], 'safe'],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = System::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['position' => SORT_ASC]],
        ]);

        if (!($this->load($params) && !$this->validate())) {

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
