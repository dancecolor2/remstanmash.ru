<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\system\models\SystemFile $systemFile
 */

$this->title = 'Редактирование файла';

?>

<?= $this->render('_form', compact('systemFile')); ?>