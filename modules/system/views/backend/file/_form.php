<?php

use app\modules\setting\helpers\FileSettingHelper;
use app\modules\system\models\SystemFile;
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var SystemFile $systemFile
 */

?>

<?php $form = ActiveForm::begin(['options' => ['id' => 'js-modal-form']]) ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?= $this->title ?></h4>
</div>
<div class="modal-body">
    <?= $form->field($systemFile, 'active')->checkbox() ?>
    <?= $form->field($systemFile, 'title') ?>
    <?= $form->field($systemFile, 'file')->widget( FileInput::class, [
        'pluginOptions' => [
            'fileActionSettings' => [
                'showDelete' => false,
                'showDrag' => false,
                'showUpload' => false,
            ],
            'layoutTemplates' => ['actionDelete' => ''],
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'showClose' => false,
            'showCancel' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
            'browseLabel' =>  'Выберите файл',
            'pluginOptions' => ['previewFileType' => 'any'],
            'initialPreviewConfig' => [
                $systemFile->hasFile() ? [
                    'type'=> FileSettingHelper::getType($systemFile->getUploadedFilePath('file')),
                    'caption' => $systemFile->file,
                    'downloadUrl' => $systemFile->getUploadedFileUrl('file'),
                    'size' => filesize($systemFile->getUploadedFilePath('file')),
                ] : [],
            ],
            'initialPreview'=> $systemFile->hasFile() ? [$systemFile->getUploadedFileUrl('file')] : [],
            'initialPreviewAsData'=>true,
        ],
    ]);?>
</div>
<div class="modal-footer">
    <button class="btn btn-success">Сохранить</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
</div>
<?php ActiveForm::end() ?>
