<?php

use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var app\modules\system\models\System $system */

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <?= $form->field($system, 'active')->checkbox() ?>
        <?= $form->field($system, 'title')->textInput(['maxlength' => true]) ?>
        <div class="single-kartik-image">
            <?= $form->field($system, 'image')->widget(FileInput::class, [
                'pluginOptions' => [
                    'fileActionSettings' => [
                        'showDrag' => false,
                        'showZoom' => true,
                        'showUpload' => false,
                        'showDelete' => false,
                        'showDownload' => true,
                    ],
                    'initialPreviewShowDelete' => false,
                    'initialPreviewDownloadUrl' => $system->getUploadedFileUrl('image'),
                    'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'showClose' => false,
                    'showCancel' => false,
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                    'browseLabel' =>  'Выберите файл',
                    'deleteUrl' => Url::to(['delete-image', 'id' => $system->id]),
                    'initialPreview' => [
                        $system->getUploadedFileUrl('image'),
                    ],
                    'initialPreviewConfig' => [!$system->hasImage() ? [] :
                        [
                            'caption' => $system->image,
                            'size' => filesize($system->getUploadedFilePath('image')),
                        ]
                    ],
                    'initialPreviewAsData' => true,
                ],
            ]);?>
        </div>
        <?= $form->field($system, 'content')->widget(CKEditor::class) ?>
        <?= $form->field($system, 'subtitle')->textInput(['maxlength' => true]) ?>
        <?= $form->field($system, 'list')->textarea(['rows' => 6])->hint('Каждый элемент с новой строки', ['class' => 'text-muted']) ?>
    </div>
    <div class="box-footer">
        <?=  Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>

