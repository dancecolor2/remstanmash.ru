<?php

/**
 * @var yii\web\View $this
 * @var $system app\modules\system\models\System
 */

$this->title = 'Новая система мониторинга';
$this->params['breadcrumbs'][] = ['label' => 'Системы мониторинга', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">
    <?= $this->render('_form', compact('system')) ?>
</div>

