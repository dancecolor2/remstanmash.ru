<?php

/**
 * @var yii\web\View $this
 * @var app\modules\system\models\System $system
 */

$this->title = $system->getTitle();
?>

<?php $this->beginContent('@app/modules/system/views/backend/default/layout.php', ['system' => $system]); ?>

    <?= $this->render('_form', compact('system')) ?>

<?php $this->endContent() ?>



