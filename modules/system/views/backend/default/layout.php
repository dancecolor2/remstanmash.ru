<?php
use yii\bootstrap\Tabs;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $content
 * @var app\modules\system\models\System $system * @var array $breadcrumbs
 */

$this->title = $system->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Системы мониторинга', 'url' => ['/system/backend/default/index']];

if (Yii::$app->controller->action->id == 'update') {
    $this->params['breadcrumbs'][] = $system->getTitle();
} else {
    $this->params['breadcrumbs'][] = [
        'label' => $system->getTitle(),
        'url' => ['/system/backend/default/update', 'id' => $system->id],
    ];
}

if (!empty($breadcrumbs)) {
    foreach ($breadcrumbs as $breadcrumb) {
        $this->params['breadcrumbs'][] = $breadcrumb;
    }
}

?>
<div class="nav-tabs-custom">
    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'url' => ['/system/backend/default/update', 'id' => $system->id],
                'active' => Yii::$app->controller->action->id == 'update',
            ],
            [
                'label' => 'Файлы (' . $system->getFiles()->where('1=1')->select('id')->count() . ')',
                'url' => ['/system/backend/file/index', 'id' => $system->id],
                'active' => Yii::$app->controller->id == 'backend/file',
            ],
            [
                'label' => 'Действия',
                'headerOptions' => ['class' => 'pull-right'],
                'items' => [
                    [
                        'encode' => false,
                        'label' => '<i class="fa fa-remove text-danger" aria-hidden="true"></i>Удалить систему мониторинга',
                        'url' => Url::to(['/system/backend/default/delete', 'id' => $system->id]),
                        'linkOptions' => [
                            'class' => 'text-danger',
                            'data-method' => 'post',
                            'data-confirm' => 'Вы уверены?',
                        ],
                    ],
                ],
            ],
        ]
    ]) ?>

    <?= $content ?>

</div>
