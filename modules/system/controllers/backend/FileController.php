<?php

namespace app\modules\system\controllers\backend;

use app\modules\admin\components\BalletController;
use app\modules\system\models\System;
use app\modules\system\models\SystemFile;
use app\modules\system\models\SystemFileSearch;
use Yii;
use yii\web\Response;

class FileController extends BalletController
{
    public function actionIndex($id)
    {
        $system = System::findOneOrException($id);
        $searchModel = new SystemFileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['system_id' => $system->id]);

        return $this->render('index', compact('dataProvider', 'searchModel', 'system'));
    }

    public function actionCreate($id)
    {
        $system = System::findOneOrException($id);
        $systemFile = new SystemFile([
            'system_id' => $system->id,
            'scenario' => SystemFile::SCENARIO_CREATE,
            'active' => true,
        ]);
        if($systemFile->load(Yii::$app->request->post()) && $systemFile->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['message' => 'success'];
        }

        return $this->renderAjax('create', compact('systemFile'));
    }

    public function actionUpdate($id)
    {
        $systemFile = SystemFile::findOneOrException($id);
        if($systemFile->load(Yii::$app->request->post()) && $systemFile->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['message' => 'success'];
        }

        return $this->renderAjax('update', compact('systemFile'));
    }

    public function actionDelete($id)
    {
        SystemFile::findOneOrException($id)->delete();
    }

    public function actionMoveUp($id)
    {
        SystemFile::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        SystemFile::findOneOrException($id)->moveNext();
    }
}