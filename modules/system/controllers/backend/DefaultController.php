<?php

namespace app\modules\system\controllers\backend;

use Yii;
use app\modules\admin\components\BalletController;
use app\modules\system\models\System;
use app\modules\system\models\SystemSearch;
use yii\filters\VerbFilter;
use yii\web\Response;

class DefaultController extends BalletController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SystemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    public function actionCreate()
    {
        $system = new System(['active' => true]);

        if ($system->load(Yii::$app->request->post()) && $system->save()) {
            Yii::$app->session->setFlash('success', 'Система мониторинга создана');

            return $this->redirect(['update', 'id' => $system->id]);
        }

        return $this->render('create', [
            'system' => $system,
        ]);
    }

    public function actionUpdate($id)
    {
        $system = System::findOneOrException($id);

        if ($system->load(Yii::$app->request->post()) && $system->save()) {

            Yii::$app->session->setFlash('success', 'Система мониторинга обновлена');
            return $this->refresh();
        }

        return $this->render('update', compact('system'));
    }

    public function actionDelete($id)
    {
        System::findOneOrException($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImage($id)
    {
        System::findOneOrException($id)->deleteImage();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['message' => 'success'];
    }

    public function actionMoveUp($id)
    {
        System::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        System::findOneOrException($id)->moveNext();
    }
}
