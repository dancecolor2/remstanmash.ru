<?php

namespace app\modules\filter\widgets;

use app\modules\machine\models\MachineType;
use yii\base\Widget;

/**
 * @property $filterMachines
 */
class FilterMachinesWidget extends Widget
{
    public $filterMachines;

    public function run()
    {
        return $this->render('filter_machines', [
            'filterMachines' => $this->filterMachines,
            'types' => $this->getTypes(),
        ]);
    }

    private function getTypes()
    {
        return MachineType::find()->innerJoinWith('machines')->orderBy('machine_type.title')->where([
            'machine.active' => true,
        ])->all();
    }
}