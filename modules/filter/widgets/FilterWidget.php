<?php

namespace app\modules\filter\widgets;

use app\modules\filter\forms\FilterForm;
use app\modules\vendor\models\Vendor;
use yii\base\Widget;

/**
 * @property FilterForm $filterForm
 */
class FilterWidget extends Widget
{
    public $filterForm;

    public function run()
    {
        $vendors = Vendor::find()
            ->innerJoinWith('products')
            ->where(['product.category_id' => $this->filterForm->getCategoryIds()])
            ->orderBy(['vendor.position' => SORT_ASC])
            ->all();

        return $this->render('filter', [
            'vendors' => $vendors,
            'filterForm' => $this->filterForm
        ]);
    }
}