<?php

/**
 * @var \app\modules\filter\forms\FilterForm $filterForm
 * @var \app\modules\vendor\models\Vendor[] $vendors
 */

?>

<?php if (!empty($vendors)): ?>
    <form class="filter js-filter">
	    <input type="hidden" name="sort" value="<?= Yii::$app->request->get('sort', 'position') ?>"><!-- Нужно для сохранения сортировки при фильтрации -->
	    <input type="hidden" name="layout" value="<?= Yii::$app->request->get('layout', 'tile') ?>"><!-- Нужно для сохранения вида товаров при фильтрации -->
        <div class="filter__body">
            <fieldset class="filter__fieldset js-filter-fieldset is-open">
                <div class="filter__header">
                    <legend class="filter__title">Производитель</legend>
                    <button class="filter__toggle js-filter-toggle" type="button" area-label="Развернуть/Свернуть">
	                    <span class="filter__cross"></span>
                    </button>
                </div>
                <div class="filter__fields js-filter-fields">
                    <?php foreach ($vendors as $vendor): ?>
                        <div class="filter__field">
                            <label class="checkbox">
                                <input class="checkbox__input" type="checkbox" name="vendor_ids[]" data-depend="" value="<?= $vendor->id ?>" <?= in_array($vendor->id, $filterForm->vendor_ids) ? 'checked' : '' ?>/>
                                <span class="checkbox__label"><?= $vendor->title ?></span>
                                <span class="checkbox__amount"><?= $filterForm->getCount($vendor) ?></span>
                            </label>
                        </div>
                    <?php endforeach ?>
                </div>
            </fieldset>
        </div>
        <div class="filter__footer">
            <div class="filter__button is-hidden-with-js">
                <button class="button js-button is-wide is-lower" area-label="отправить">отправить</button>
            </div>
            <div class="filter__button">
                <button class="button js-button is-hollow is-lower is-grey is-wide" area-label="сбросить" type="reset">сбросить</button>
            </div>
        </div>
    </form>
<?php endif ?>
