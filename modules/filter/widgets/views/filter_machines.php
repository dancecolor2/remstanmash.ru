<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\machine\models\MachineType[] $types
 * @var \app\modules\filter\forms\FilterMachines $filterMachines
 */

?>

<form class="filter js-filter">
    <div class="filter__body">
        <fieldset class="filter__fieldset js-filter-fieldset is-open">
            <div class="filter__header">
                <legend class="filter__title">тип станка</legend>
                <button class="filter__toggle js-filter-toggle" type="button" area-label="Развернуть/Свернуть"><span class="filter__cross"></span></button>
            </div>
            <div class="filter__fields js-filter-fields">
                <?php foreach ($types as $type): ?>
                    <div class="filter__field">
                        <label class="checkbox">
                            <input class="checkbox__input" type="checkbox" name="type_ids[]" value="<?= $type->id ?>" data-depend="" <?= in_array($type->id, $filterMachines->type_ids) ? 'checked' : '' ?> />
                            <span class="checkbox__label"><?= $type->title ?></span>
                            <span class="checkbox__amount"><?= $filterMachines->getCount($type) ?></span>
                        </label>
                    </div>
                <?php endforeach ?>
            </div>
        </fieldset>
    </div>
    <div class="filter__footer">
        <div class="filter__button is-hidden-with-js">
            <button class="button js-button is-wide is-lower" area-label="отправить">отправить</button>
        </div>
        <div class="filter__button">
            <button class="button js-button is-hollow is-lower is-grey is-wide" area-label="сбросить" type="reset">сбросить</button>
        </div>
    </div>
</form>
