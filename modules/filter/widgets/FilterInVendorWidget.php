<?php

namespace app\modules\filter\widgets;

use app\modules\category\models\Category;
use app\modules\filter\forms\FilterInVendor;
use app\modules\product\models\Product;
use yii\base\Widget;

/**
 * @property FilterInVendor $filterInVendor
 */
class FilterInVendorWidget extends Widget
{
    public $filterInVendor;

    public function run()
    {
        $categories = Category::find()->where(['depth' => 1])->indexBy('id')->all();
        $counts = $this->getCounts($categories);

        $categories = array_filter($categories, function(Category $category) use ($counts) {
            return $counts[$category->id] > 0;
        });

        return $this->render('filter_in_vendor', [
            'filterInVendor' => $this->filterInVendor,
            'categories' => $categories,
            'counts' => $counts,
        ]);
    }

    private function getCounts($categories)
    {
        return array_map(function(Category $category) {
            return Product::find()
                ->where(['vendor_id' => $this->filterInVendor->vendor->id])
                ->active()
                ->inCategory($category)->select('id')->count();
        }, $categories);
    }
}