<?php

namespace app\modules\filter\controllers;

use app\helpers\ContentHelper;
use app\modules\category\models\Category;
use app\modules\filter\forms\FilterForm;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

class FrontendController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'count' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return null|string
     */
    public function actionCount()
    {
        $category = Category::findOne(Yii::$app->request->post('category_id', null));

        if (null !== $category) {
            $filterForm = new FilterForm($category);
            $filterForm->load(Yii::$app->request->post());
            $filterForm->category_id = array_map('intval', array_merge([$category->id], $category->children()->select(['id'])->column()));
            $filterForm->validate();
            $dataProvider = $filterForm->filter($filterForm);
            $count = $dataProvider->getTotalCount();

            return ContentHelper::addLabel($count);
        }

        return null;
    }
}
