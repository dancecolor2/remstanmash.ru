<?php

namespace app\modules\filter\forms;

use app\modules\category\models\Category;
use app\modules\product\models\Product;
use app\modules\vendor\models\Vendor;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @property Vendor $vendor
 * @property array $category_ids
 */
class FilterInVendor extends Model
{
    public $vendor;
    public $category_ids = [];

    public function rules() : array
    {
        return [
            ['category_ids', 'safe']
        ];
    }

    public function formName() : string
    {
        return '';
    }

    public function filter() : ActiveDataProvider
    {
        $query = Product::find()->active()->andWhere([
            'vendor_id' => $this->vendor->id,
        ])->andFilterWhere([
            'category_id' => $this->getCategoryIds(),
        ]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => Product::COUNT_ON_PAGE, 'forcePageParam' => false],
            'sort' => [
                'defaultOrder' => ['position' => SORT_ASC],
            ]
        ]);
    }

    public function getCategoryIds()
    {
        if (empty($this->category_ids)) {
            return [];
        }

        $catIds = [];
        $categories = Category::find()->where(['id' => $this->category_ids])->all();

        foreach($categories as $category) {
            $catIds[] = $category->id;
            foreach($category->children()->all() as $child) {
                $catIds[] = $child->id;
            };
        }

        return $catIds;
    }
}