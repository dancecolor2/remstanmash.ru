<?php

namespace app\modules\filter\forms;

use app\modules\category\models\Category;
use app\modules\product\models\Product;
use app\modules\vendor\models\Vendor;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @property Category $category
 * @property array $vendor_ids
 */
class FilterForm extends Model
{
    public $category;
    public $vendor_ids = [];
    public $category_ids = [];

    private $catIds = null;

    public function rules() : array
    {
        return [
            ['vendor_ids', 'safe']
        ];
    }

    public function formName() : string
    {
        return '';
    }

    public function filter() : ActiveDataProvider
    {
        $query = Product::find()->active()->with(['images', 'eavAttributes'])->andWhere([
            'category_id' => $this->getCategoryIds(),
            'parent_id' => null,
        ])->andFilterWhere([
            'vendor_id' => $this->vendor_ids,
        ]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => Product::COUNT_ON_PAGE, 'forcePageParam' => false],
            'sort' => [
                'defaultOrder' => ['position' => SORT_ASC],
            ]
        ]);
    }

    public function getCategoryIds ()
    {
        if (is_null($this->catIds)) {
            $this->catIds = $this->category->children()->select('id')->column();
            $this->catIds[] = $this->category->id;
        }

        return $this->catIds;
    }

    public function getCount(Vendor $vendor)
    {
        return Product::find()->active()->andWhere([
            'category_id' => $this->getCategoryIds(),
            'vendor_id' => $vendor->id,
        ])->select('id')->count();
    }
}
