<?php

namespace app\modules\filter\forms;

use app\modules\machine\models\Machine;
use app\modules\machine\models\MachineType;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @property array $type_ids
 */
class FilterMachines extends Model
{
    public $type_ids = [];

    public function rules() : array
    {
        return [
            ['type_ids', 'safe']
        ];
    }

    public function formName() : string
    {
        return '';
    }

    public function filter() : ActiveDataProvider
    {
        $query = Machine::find()->active()->andFilterWhere([
            'type_id' => $this->type_ids,
        ]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => Machine::COUNT_ON_PAGE, 'forcePageParam' => false],
            'sort' => [
                'defaultOrder' => ['position' => SORT_ASC],
            ]
        ]);
    }

    public function getCount(MachineType $type)
    {
        return Machine::find()->active()->andWhere([
            'type_id' => $type->id,
        ])->select('id')->count();
    }
}