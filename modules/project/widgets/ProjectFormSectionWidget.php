<?php

namespace app\modules\project\widgets;

use app\modules\project\models\Project;
use app\modules\setting\components\Settings;
use yii\base\Widget;

/**
 * @property Project $project
 */
class ProjectFormSectionWidget extends Widget
{
    public $project;

    public function run()
    {
        if ($this->project->type_id == Project::TYPE_REPAIR) {
            $title = Settings::getValue('project_form_title_repair');
            $text = Settings::getValue('project_form_text_repair');
        } else {
            $title = Settings::getValue('project_form_title');
            $text = Settings::getValue('project_form_text');
        }

        return $this->render('project_form_section', [
            'project' => $this->project,
            'title' => $title,
            'text' => $text,
        ]);
    }
}