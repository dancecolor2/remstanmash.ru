<?php

use app\modules\feedback\forms\ProjectForm;
use app\modules\feedback\widgets\OrderFormWidget;

/**
 * @var \yii\web\View $this
 * @var string $title
 * @var string $text
 * @var \app\modules\project\models\Project $project
 */

?>

<section class="section">
    <div class="container">
        <div class="section__body">
            <div class="ticket has-arrow">
                <div class="grid">
                    <div class="col-5 col-l-6 col-md-12">
                        <div class="ticket__left">
                            <div class="ticket__header">
                                <h3 class="ticket__title"><?= $title ?></h3>
                                <p class="ticket__caption"><?= $text ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-7 col-l-6 col-md-12">
                        <div class="ticket__right">
                            <div class="ticket__form">
                                <?= OrderFormWidget::widget([
                                    'type' => ProjectForm::TYPE,
                                    'id' => $project->id,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>