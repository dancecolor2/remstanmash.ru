<?php

use app\modules\project\widgets\ProjectWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\project\models\Project[] $projects
 * @var string $caption
 */

?>

<?php if (!empty($projects)): ?>
    <section class="section has-roll is-grey">
        <div class="container">
            <header class="section__header">
                <?php if (!empty($caption)): ?>
                    <p class="section__caption"><?= $caption ?></p>
                <?php endif ?>
                <h2 class="section__title">Реализованные проекты</h2>
            </header>
            <div class="section__body">
                <div class="roll js-roll is-posts" data-options="{&quot;slidesPerView&quot;:3}">
                    <div class="roll__control js-roll-control">
                        <div class="control">
                            <button class="control__arrow is-prev js-roll-prev" type="button" area-label="Назад">
                            <span class="arrow is-left">
                                <svg class="arrow__icon"><use xlink:href="/img/sprite.svg#arrow-alt"></use></svg>
                            </span>
                            </button>
                            <button class="control__arrow is-next js-roll-next" type="button" area-label="Вперёд">
                            <span class="arrow is-right">
                                <svg class="arrow__icon"><use xlink:href="/img/sprite.svg#arrow-alt"></use></svg>
                            </span>
                            </button>
                        </div>
                    </div>
                    <div class="roll__inner js-roll-swiper">
                        <ul class="roll__list js-roll-list">
                            <?php foreach ($projects as $project): ?>
                                <li class="roll__item js-roll-item">
                                    <?= ProjectWidget::widget(compact('project')) ?>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>