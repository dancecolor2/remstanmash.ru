<?php

use app\helpers\DateHelper;

/**
 * @var $this \yii\web\View
 * @var \app\modules\project\models\Project $project
 */

?>

<article class="post">
    <div class="post__cover">
        <img class="post__image" src="<?= $project->getThumbFileUrl('image') ?>" alt="<?= $project->title ?>"/>
    </div>
    <div class="post__body">
        <time class="post__date"><?= DateHelper::forHumanShortMonth($project->created_at) ?></time>
        <h3 class="post__title">
            <a class="post__link" href="<?= $project->getHref() ?>" title="<?= $project->title ?>"><?= $project->title ?></a>
        </h3>
    </div>
</article>
