<?php

namespace app\modules\project\widgets;

use app\modules\project\models\Project;
use yii\base\Widget;

/**
 * @property int $type
 */
class ProjectsWidget extends Widget
{
    public $type;
    public $caption;

    public function run()
    {
        $query = Project::find()->active()->orderBy(['created_at' => SORT_DESC]);

        if (!empty($this->type)) {
            $query->withType($this->type);
        }

        $projects = $query->all();

        return $this->render('projects', [
            'projects' => $projects,
            'caption' => $this->caption,
        ]);
    }
}