<?php

namespace app\modules\project\controllers;

use app\modules\project\models\Project;
use yii\web\Controller;

class FrontendController extends Controller
{
    public function actionView($alias)
    {
        $project = Project::findOneOrException([
            'alias' => $alias,
            'active' => Project::ACTIVE_ON,
        ]);

        $products = $project->products;

        return $this->render('view', compact('project', 'products'));
    }
}


