<?php

namespace app\modules\project\controllers;

use app\modules\admin\components\BalletController;
use app\modules\project\models\ProjectSearch;
use app\modules\project\models\Project;
use app\modules\product\models\Product;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yiidreamteam\upload\ImageUploadBehavior;

class BackendController extends BalletController
{
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', compact('dataProvider', 'searchModel'));
    }

    public function actionCreate()
    {
        $project = new Project(['active' => true]);

        if (Yii::$app->request->isAjax && $project->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($project);
        }

        if ($project->load(Yii::$app->request->post()) && $project->save()) {
            return $this->redirect(['update', 'id' => $project->id]);
        }

        $products = [];

        return $this->render('create', compact('project', 'products'));
    }

    public function actionUpdate($id)
    {
        $project = Project::findOneOrException($id);

        if (Yii::$app->request->isAjax && $project->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($project);
        }

        if ($project->load(Yii::$app->request->post()) && $project->save()) {
            return $this->refresh();
        }

        $products = $project->getProducts()->orderBy(['title' => SORT_ASC])->select(['title'])->indexBy('id')->column();

        return $this->render('update', compact('project', 'products'));
    }

    public function actionDelete($id)
    {
        $project = Project::findOneOrException($id);
        $project->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImage($id, $key)
    {
        $project = Project::findOneOrException($id);

        /** @var ImageUploadBehavior $imageBehavior */
        $imageBehavior = $project->getBehavior($key);
        $imageBehavior->cleanFiles();

        $project->updateAttributes([$key => null, $key . '_hash' => null]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['success' => true];
    }

    public function actionSeo($id)
    {
        $project = Project::findOneOrException($id);

        if ($project->load(Yii::$app->request->post()) && $project->save()) {
            return $this->refresh();
        }

        return $this->render('seo', compact('project'));
    }

	public function actionProducts($q)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		return [
			'results' => array_map(function($product) {
				return [
					'id' => $product['id'],
					'text' => $product['title'],
				];
			},
			Product::find()->where(['like', 'title', $q])->select(['id', 'title'])->orderBy(['title' => SORT_ASC])->asArray()->all())
		];
	}
}
