<?php

use app\helpers\ContentHelper;
use app\helpers\DateHelper;
use app\modules\page\components\Pages;
use app\modules\product\widgets\ProductCompactWidget;
use app\modules\project\models\Project;
use app\modules\project\widgets\ProjectFormSectionWidget;
use app\widgets\ArticleWidget;

/**
 * @var yii\web\View $this
 * @var Project $project
 * @var \app\modules\product\models\Product[] $products
 */

$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs($project->type_id == Project::TYPE_REPAIR ? 'repair' : 'upgrade');
$project->generateMetaTags();
$this->params['h1'] = $project->getH1();
$this->params['headlineClass'] = 'is-margin-bottom-120 has-cover';
$this->params['headlineImage'] = $project->getThumbFileUrl('image', 'origin');
$this->params['headlineDate'] = DateHelper::forHumanShortMonth($project->created_at);

$this->params['activeMenuItem'] = $project->type_id == Project::TYPE_REPAIR ? 'repair' : 'upgrade';

?>

<?php if ($project->hasImage('image_before') || $project->hasImage('image_after')): ?>
    <section class="section">
        <div class="container">
            <div class="section__body">
                <div class="compare">
                    <div class="grid is-row">
                        <?php if ($project->hasImage('image_before')): ?>
                            <div class="col-6">
                                <figure class="compare__cover">
                                    <img class="compare__image" src="<?= $project->getThumbFileUrl('image_before') ?>" alt="alt"/>
                                    <figcaption class="compare__caption">до <?= $project->type_id == Project::TYPE_REPAIR ? 'ремонта' : 'модернизации' ?> </figcaption>
                                </figure>
                            </div>
                        <?php endif ?>
                        <?php if ($project->hasImage('image_after')): ?>
                            <div class="col-6">
                                <figure class="compare__cover">
                                    <img class="compare__image" src="<?= $project->getThumbFileUrl('image_after') ?>" alt="alt"/>
                                    <figcaption class="compare__caption">после <?= $project->type_id == Project::TYPE_REPAIR ? 'ремонта' : 'модернизации' ?></figcaption>
                                </figure>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<section class="section">
    <div class="container">
        <div class="section__body">
            <div class="grid is-row">
                <div class="col-10 shift-1">
                    <div class="text">
                        <?= ArticleWidget::widget(['article' => $project->content]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if ($project->price): ?>
    <section class="section">
        <div class="container">
            <div class="section__body">
                <div class="grid is-row">
                    <div class="col-5">
                        <section class="card is-alt has-right-shadow has-cost">
                            <h4 class="card__title">итого за проект</h4>
                            <div class="card__body">
                                <p class="card__cost"><?= number_format($project->price, 0, '.', ' ') ?> <i>₽</i></p>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?php if (!empty($project->tasks) || !empty($project->works)): ?>
    <section class="section is-white with-padding is-padding-80">
        <div class="container">
            <div class="section__body">
                <div class="grid is-row">
                    <?php if (!empty($project->tasks)): ?>
                        <div class="col-6">
                            <div class="stages">
                                <h3 class="stages__title">Задачи</h3>
                                <ul class="stages__list">
                                    <?php foreach (ContentHelper::splitText($project->tasks) as $task): ?>
                                        <li class="stages__item"><?= $task ?></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php if (!empty($project->tasks)): ?>
                        <div class="col-6">
                            <div class="stages is-solutions">
                                <h3 class="stages__title">Было сделано</h3>
                                <ul class="stages__list">
                                    <?php foreach (ContentHelper::splitText($project->works) as $work): ?>
                                        <li class="stages__item"><?= $work ?></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?php if (!empty($products)): ?>
    <section class="section has-roll is-grey">
        <div class="container">
            <header class="section__header">
                <p class="section__caption">В данной статье</p>
                <h2 class="section__title">Использовались комплектующие</h2>
            </header>
            <div class="section__body">
                <div class="roll js-roll is-products">
                    <div class="roll__control js-roll-control">
                        <div class="control">
                            <button class="control__arrow is-prev js-roll-prev" type="button" area-label="Назад">
                                <span class="arrow is-left">
                                    <svg class="arrow__icon"><use xlink:href="/img/sprite.svg#arrow-alt"></use></svg>
                                </span>
                            </button>
                            <button class="control__arrow is-next js-roll-next" type="button" area-label="Вперёд">
                                <span class="arrow is-right">
                                    <svg class="arrow__icon"><use xlink:href="/img/sprite.svg#arrow-alt"></use></svg>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="roll__inner js-roll-swiper">
                        <ul class="roll__list js-roll-list">
                            <?php foreach ($products as $product): ?>
                                <li class="roll__item js-roll-item">
                                    <?= ProductCompactWidget::widget(compact('product')) ?>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?= ProjectFormSectionWidget::widget(compact('project')) ?>
