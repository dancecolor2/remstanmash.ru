<?php

/**
 * @var $this yii\web\View
 * @var \app\modules\project\models\Project $project
 * @var array $products
 */

$this->title = 'Редактирование проекта';
$this->params['breadcrumbs'][] = ['label' => 'Проекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $project->title];

?>

<?php $this->beginContent('@app/modules/project/views/backend/layout.php', compact('project')) ?>

    <?= $this->render('_form', compact('project', 'products')) ?>

<?php $this->endContent() ?>