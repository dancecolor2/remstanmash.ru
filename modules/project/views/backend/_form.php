<?php

use app\modules\project\models\Project;
use kartik\file\FileInput;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var Project $project
 * @var array $products
 */

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="box-body">
        <?= $form->field($project, 'active')->checkbox() ?>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?= $form->field($project, 'title')->textInput(['class' => 'form-control transIt']) ?>
                <?= $form->field($project, 'alias')->textInput($project->isNewRecord ? [
                    'class' => 'form-control transTo',
                ] : []) ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <?= $form->field($project, 'type_id')->dropDownList([Project::TYPE_MODERNIZATION => 'Модернизация', Project::TYPE_REPAIR => 'Ремонт'], ['prompt' => '']) ?>
                <?= $form->field($project, 'price') ?>
            </div>
            <div class="col-xs-12 row-kartik-images">
                <div class="single-kartik-image">
                    <?= $form->field($project, 'image')->widget(FileInput::class, [
                        'pluginOptions' => [
                            'fileActionSettings' => [
                                'showDrag' => false,
                                'showZoom' => true,
                                'showUpload' => false,
                                'showDelete' => false,
                                'showDownload' => true,
                            ],
                            'initialPreviewDownloadUrl' => $project->getUploadedFileUrl('image'),
                            'deleteUrl' => Url::to(['delete-image', 'id' => $project->id, 'key' => 'image']),
                            'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'showClose' => false,
                            'showCancel' => false,
                            'browseClass' => 'btn btn-primary btn-block',
                            'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                            'browseLabel' => 'Выберите файл',
                            'initialPreview' => [
                                $project->getUploadedFileUrl('image'),
                            ],
                            'initialPreviewAsData' => true,
                        ],
                    ]) ?>
                </div>
                <div class="single-kartik-image">
                    <?= $form->field($project, 'image_before')->widget(FileInput::class, [
                        'pluginOptions' => [
                            'fileActionSettings' => [
                                'showDrag' => false,
                                'showZoom' => true,
                                'showUpload' => false,
                                'showDelete' => false,
                                'showDownload' => true,
                            ],
                            'initialPreviewDownloadUrl' => $project->getUploadedFileUrl('image_before'),
                            'deleteUrl' => Url::to(['delete-image', 'id' => $project->id, 'key' => 'image_before']),
                            'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'showClose' => false,
                            'showCancel' => false,
                            'browseClass' => 'btn btn-primary btn-block',
                            'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                            'browseLabel' => 'Выберите файл',
                            'initialPreview' => [
                                $project->getUploadedFileUrl('image_before'),
                            ],
                            'initialPreviewAsData' => true,
                        ],
                    ]) ?>
                </div>
                <div class="single-kartik-image">
                    <?= $form->field($project, 'image_after')->widget(FileInput::class, [
                        'pluginOptions' => [
                            'fileActionSettings' => [
                                'showDrag' => false,
                                'showZoom' => true,
                                'showUpload' => false,
                                'showDelete' => false,
                                'showDownload' => true,
                            ],
                            'initialPreviewDownloadUrl' => $project->getUploadedFileUrl('image_after'),
                            'deleteUrl' => Url::to(['delete-image', 'id' => $project->id, 'key' => 'image_after']),
                            'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'showClose' => false,
                            'showCancel' => false,
                            'browseClass' => 'btn btn-primary btn-block',
                            'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                            'browseLabel' => 'Выберите файл',
                            'initialPreview' => [
                                $project->getUploadedFileUrl('image_after'),
                            ],
                            'initialPreviewAsData' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>

        <?= $form->field($project, 'content')->widget(CKEditor::class) ?>
        <div class="row">
            <div class="col-xs-12 col-md-6"><?= $form->field($project, 'tasks')->textarea(['rows' => 6])->hint('Каждая фраза с новой строки', ['class' => 'text-muted']) ?></div>
            <div class="col-xs-12 col-md-6"><?= $form->field($project, 'works')->textarea(['rows' => 6])->hint('Каждая фраза с новой строки', ['class' => 'text-muted']) ?></div>
        </div>

        <?= $form->field($project, 'productsIds')->widget(Select2::class, [
            'data' => $products,
            'options' => ['placeholder' => '...', 'autocomplete' => 'off'],
            'showToggleAll' => false,
            'theme' => Select2::THEME_BOOTSTRAP,
	        'pluginOptions' => [
		        'multiple' => true,
		        'allowClear' => false,
		        'minimumInputLength' => 3,
		        'ajax' => [
			        'url' => Url::to(['/project/backend/products']),
			        'dataType' => 'json',
			        'cache' => false,
		        ],
	        ],
        ]) ?>
    </div>
    <div class="box-footer with-border">
        <button class="btn btn-success">Сохранить</button>
        <a class="btn btn-default" href="<?= Url::to(['index']) ?>">Отмена</a>
    </div>
<?php ActiveForm::end() ?>
