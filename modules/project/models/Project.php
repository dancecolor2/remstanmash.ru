<?php

namespace app\modules\project\models;

use app\modules\admin\behaviors\SeoBehavior;
use app\modules\admin\traits\QueryExceptions;
use app\modules\product\models\Product;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii2tech\ar\linkmany\LinkManyBehavior;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $active
 * @property int $type_id
 * @property int $alias
 * @property string $title
 * @property string $image
 * @property string $image_hash
 * @property string $image_before
 * @property string $image_before_hash
 * @property string $image_after
 * @property string $image_after_hash
 * @property string $content
 * @property string $tasks
 * @property string $works
 * @property string $price
 * @property string $h1
 * @property string $meta_t
 * @property string $meta_d
 * @property string $meta_k
 *
 * @property Product[] $products
 *
 * @mixin ImageUploadBehavior
 * @mixin SeoBehavior
 */
class Project extends ActiveRecord
{
    use QueryExceptions;

    const ACTIVE_ON = 1;
    const ACTIVE_OFF = 0;
    const TYPE_MODERNIZATION = 1;
    const TYPE_REPAIR = 2;

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            SeoBehavior::class,
            'image' => [
                'class' => ImageUploadBehavior::class,
                'createThumbsOnRequest' => true,
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 370, 'height' => 220],
                    'origin' => ['width' => 1425, 'height' => 432],
                ],
                'filePath' => '@webroot/uploads/projects/[[pk]]_[[attribute_image_hash]].[[extension]]',
                'fileUrl' => '/uploads/projects/[[pk]]_[[attribute_image_hash]].[[extension]]',
                'thumbPath' => '@webroot/uploads/cache/projects/[[pk]]_[[profile]]_[[attribute_image_hash]].[[extension]]',
                'thumbUrl' => '/uploads/cache/projects/[[pk]]_[[profile]]_[[attribute_image_hash]].[[extension]]',
            ],
            'image_before' => [
                'class' => ImageUploadBehavior::class,
                'createThumbsOnRequest' => true,
                'attribute' => 'image_before',
                'thumbs' => [
                    'thumb' => ['width' => 570, 'height' => 380],
                ],
                'filePath' => '@webroot/uploads/projects_before/[[pk]]_[[attribute_image_before_hash]].[[extension]]',
                'fileUrl' => '/uploads/projects_before/[[pk]]_[[attribute_image_before_hash]].[[extension]]',
                'thumbPath' => '@webroot/uploads/cache/projects_before/[[pk]]_[[profile]]_[[attribute_image_before_hash]].[[extension]]',
                'thumbUrl' => '/uploads/cache/projects_before/[[pk]]_[[profile]]_[[attribute_image_before_hash]].[[extension]]',
            ],
            'image_after' => [
                'class' => ImageUploadBehavior::class,
                'createThumbsOnRequest' => true,
                'attribute' => 'image_after',
                'thumbs' => [
                    'thumb' => ['width' => 570, 'height' => 380],
                ],
                'filePath' => '@webroot/uploads/projects_after/[[pk]]_[[attribute_image_after_hash]].[[extension]]',
                'fileUrl' => '/uploads/projects_after/[[pk]]_[[attribute_image_after_hash]].[[extension]]',
                'thumbPath' => '@webroot/uploads/cache/projects_after/[[pk]]_[[profile]]_[[attribute_image_after_hash]].[[extension]]',
                'thumbUrl' => '/uploads/cache/projects_after/[[pk]]_[[profile]]_[[attribute_image_after_hash]].[[extension]]',
            ],
            [
                'class' => LinkManyBehavior::class,
                'relation' => 'products',
                'relationReferenceAttribute' => 'productsIds',
            ]

        ];
    }

    public static function tableName()
    {
        return 'projects';
    }

    public static function find()
    {
        return new ProjectQuery(get_called_class());
    }

    public function rules()
    {
        return [
            [['title', 'alias', 'content'], 'required'],
            ['alias', 'unique'],
            [['active'], 'boolean'],
            [['price', 'type_id'], 'integer'],
            [['content', 'tasks', 'works'], 'string'],
            [['title', 'h1', 'meta_t', 'meta_d', 'meta_k'], 'string', 'max' => 255],
            [['image', 'image_before', 'image_after'], 'image', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'jpeg']],
            [['productsIds'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'active' => 'Активность',
            'type_id' => 'Тип',
            'title' => 'Заголовок',
            'alias' => 'Алиас',
            'image' => 'Детальная картинка',
            'image_before' => 'Фото "До"',
            'image_after' => 'Фото "После"',
            'content' => 'Контент',
            'tasks' => 'Задачи',
            'works' => 'Было сделано',
            'price' => 'Стоимость проекта',
            'h1' => 'H1',
            'meta_t' => 'Заголовок страницы',
            'meta_d' => 'Описание страницы',
            'meta_k' => 'Ключевые слова',
            'productsIds' => 'Товары',
        ];
    }

    public function getHref()
    {
        return Url::to(['/project/frontend/view', 'alias' => $this->alias]);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function isActive()
    {
        return $this->active == self::ACTIVE_ON;
    }

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id'])->viaTable('projects_products', ['project_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if ($this->image instanceof UploadedFile) {
            $this->image_hash = uniqid();
        }
        if ($this->image_before instanceof UploadedFile) {
            $this->image_before_hash = uniqid();
        }
        if ($this->image_after instanceof UploadedFile) {
            $this->image_after_hash = uniqid();
        }
        return parent::beforeSave($insert);
    }

    public function deleteImage($name = 'image')
    {
        /** @var ImageUploadBehavior $imageBehavior */
        $imageBehavior = $this->getBehavior($name);
        $imageBehavior->cleanFiles();
        $this->updateAttributes([$name => null, $name . '_hash' => null]);
    }

    public function hasImage($name = 'image')
    {
        return !empty($this->$name) && is_file($this->getUploadedFilePath($name));
    }
}
