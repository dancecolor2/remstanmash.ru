<?php

namespace app\modules\project\models;

use yii\db\ActiveQuery;

class ProjectQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['active' => Project::ACTIVE_ON]);
    }

    public function withType($type)
    {
        return $this->andWhere(['type_id' => $type]);
    }
}
