<?php

namespace app\modules\review\forms;

use app\modules\review\models\Review;
use yii\base\Model;

class ReviewEditForm extends Model
{
	public $name;
	public $company;
	public $email;
	public $message;
	public $rating;
	public $product_id;

	public function __construct(Review $review)
	{
		$this->name = $review->name;
		$this->company = $review->company;
		$this->email = $review->email;
		$this->message = $review->message;
		$this->rating = $review->rating;
		$this->product_id = $review->product_id;
		parent::__construct();
	}

	public function rules()
	{
		return [
			[['name', 'message', 'rating', 'product_id'], 'required'],
			[['name', 'email', 'company'], 'string', 'max' => 255],
			[['message'], 'string', 'max' => 500],
			[['rating', 'product_id'], 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'product_id' => 'ID товара',
			'rating' => 'Рейтинг',
			'name' => 'Имя',
			'company' => 'Компания',
			'email' => 'Email',
			'message' => 'Отзыв',
		];
	}
}