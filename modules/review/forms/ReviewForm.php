<?php

namespace app\modules\review\forms;

use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use yii\base\Model;

class ReviewForm extends Model
{
	public $name;
	public $company;
	public $email;
	public $message;
	public $rating = 5;
	public $product_id;
	public $reCaptcha;

	public function rules()
	{
		return [
			[['name', 'message', 'rating', 'product_id', 'email'], 'required'],
			[['name', 'email', 'company'], 'string', 'max' => 255],
			['email', 'email'],
			[['message'], 'string', 'max' => 500],
			[['rating', 'product_id'], 'integer'],
			[['reCaptcha'], ReCaptchaValidator2::class, 'uncheckedMessage' => 'Пожалуйста, подтвердите, что вы не робот.'],
		];
	}

	public function attributeLabels()
	{
		return [
			'name' => 'Ваше имя',
			'company' => 'Ваша компания',
			'email' => 'Email',
			'message' => 'Отзыв',
			'rating' => 'Рейтинг',
		];
	}
}