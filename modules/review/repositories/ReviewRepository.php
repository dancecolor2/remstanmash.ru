<?php

namespace app\modules\review\repositories;

use app\modules\review\models\Review;

class ReviewRepository
{
	public function getReviewById($id): Review
	{
		if (!$review = Review::findOne($id)) {
			throw new \DomainException('Review not found');
		}

		return $review;
	}

	public function save(Review $review)
	{
		if (!$review->save()) {
			throw new \RuntimeException('Review saving error');
		}
	}

	public function delete(Review $review)
	{
		if ($review->delete() === false) {
			throw new \RuntimeException('Review delete error');
		}
	}
}