<?php

use app\helpers\DateHelper;
use app\modules\review\helpers\ReviewHelper;

/**
 * @var \app\modules\review\models\Review[] $reviews
 */

?>

<?php foreach ($reviews as $review): ?>
    <div class="review">
        <div class="review__header">
            <div class="review__info">
                <div class="review__name">
                    <p><?= $review->name ?></p>
                    <p class="review__date"><?= DateHelper::forHumanShortMonth($review->created_at) ?></p>
                </div>
                <p class="review__company"><?= $review->company ?></p>
            </div>
            <div class="review__rate">
            <?php foreach (ReviewHelper::getStars($review->rating) as $star): ?>
                <?= $star ?>
            <?php endforeach; ?>
            </div>
        </div>
        <p class="review__body"><?= $review->message ?></p>
    </div>
<?php endforeach; ?>