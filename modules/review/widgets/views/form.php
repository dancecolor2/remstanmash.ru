<?php

use app\modules\page\components\Pages;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \app\modules\review\forms\ReviewForm $reviewForm
 * @var integer $product_id
 */

?>

<?php $form = ActiveForm::begin([
	'action' => ['/review/frontend/create'],
	'options' => ['class' => 'review__form', 'role' => 'form'],
]) ?>
    <h2 class="review__title">Оставить свой отзыв</h2>
    <input type="hidden" name="ReviewForm[product_id]" value="<?= $product_id ?>">
    <?= $form->field($reviewForm, 'name', [
        'options' => ['class' => 'form__group'],
        'labelOptions' => ['class' => 'review__item'],
        'inputOptions' => ['class' => 'review__input', 'placeholder' => 'Ваше имя'],
		'errorOptions' => ['class' => 'form__message'],
		'template' => '{input}{error}'
    ]) ?>
    <?= $form->field($reviewForm, 'company', [
		'options' => ['class' => 'form__group'],
        'labelOptions' => ['class' => 'review__item'],
        'inputOptions' => ['class' => 'review__input', 'placeholder' => 'Ваша компании'],
		'errorOptions' => ['class' => 'form__message'],
		'template' => '{input}{error}'
    ]) ?>
    <?= $form->field($reviewForm, 'email', [
		'options' => ['class' => 'form__group'],
        'labelOptions' => ['class' => 'review__item'],
        'inputOptions' => ['class' => 'review__input', 'type' => 'email', 'placeholder' => 'Е-mail'],
		'errorOptions' => ['class' => 'form__message'],
		'template' => '{input}{error}'
    ]) ?>
    <?= $form->field($reviewForm, 'message', [
		'options' => ['class' => 'form__group'],
        'labelOptions' => ['class' => 'review__item'],
        'inputOptions' => ['class' => 'review__input', 'placeholder' => 'Сообщение'],
		'errorOptions' => ['class' => 'form__message'],
		'template' => '{input}{error}'
    ])->textarea() ?>
    <?= $form->field($reviewForm, 'rating', [
        'options' => ['class' => 'b-rating-pick'],
        'template' => "<p class='review__text'>Оценка</p><div class='b-rating-pick__inner'>{input}</div>",
    ])->radioList(array_reverse([1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5], true), [
        'item' => function($index, $label, $name, $checked, $value) {
            $inputId = strtolower(str_replace(['[', ']'], ['', ''], $name)) . '_' . $value;
            return Html::radio($name, $checked, ['class' => 'b-rating-pick__field', 'value' => $value, 'id' => $inputId])
                . Html::label('', $inputId, ['class' => 'b-rating-pick__label']);
        },
        'unselect' => null,
        'tag' => null,
    ]) ?>
    <?= $form->field($reviewForm, 'reCaptcha', [
        'options' => ['class' => 'form__group', 'style' => 'margin-bottom: 20px'],
        'errorOptions' => ['class' => 'form__message'],
    ])->widget(ReCaptcha2::class)->label(false) ?>

    <button class="button">отправить сообщение</button>
    <p class="review__policy">Отправляю данную форму, даю согласие на <a href="<?= Pages::getPage('privacy')->getHref() ?>" target="_blank">обработку персональных данных</a></p>
<?php ActiveForm::end() ?>
