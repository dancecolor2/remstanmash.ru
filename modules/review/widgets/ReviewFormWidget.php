<?php

namespace app\modules\review\widgets;

use app\modules\review\forms\ReviewForm;
use yii\base\Widget;

class ReviewFormWidget extends Widget
{
	public $product_id;

	public function run()
	{
		$reviewForm = new ReviewForm();
		$product_id = $this->product_id;

		return $this->render('form', compact('reviewForm', 'product_id'));
	}
}