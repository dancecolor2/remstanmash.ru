<?php

namespace app\modules\review\widgets;

use app\modules\review\models\Review;
use yii\base\Widget;

class ReviewsWidget extends Widget
{
	public $product_id;

	public function run()
	{
		$reviews = Review::find()->forProduct($this->product_id)->active()->orderBy(['created_at'=> SORT_DESC])->all();

		return $this->render('reviews', compact('reviews'));
	}
}