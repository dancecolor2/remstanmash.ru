<?php

namespace app\modules\review\service;

use app\modules\review\forms\ReviewEditForm;
use app\modules\review\forms\ReviewForm;
use app\modules\review\models\Review;
use app\modules\review\repositories\ReviewRepository;

class ReviewService
{
	/**
	 * @var ReviewRepository
	 */
	private $repository;

	public function __construct(ReviewRepository $repository)
	{
		$this->repository = $repository;
	}

	public function activate($id)
	{
		$review = $this->repository->getReviewById($id);
		$review->activate();
		$this->repository->save($review);
	}

	public function deactivate($id)
	{
		$review = $this->repository->getReviewById($id);
		$review->deactivate();
		$this->repository->save($review);
	}

	public function delete($id)
	{
		$review = $this->repository->getReviewById($id);

		if (!$review->canBeDeleted()) {
			throw new \DomainException('Отзыв не может быть удален');
		}

		$this->repository->delete($review);
	}

	public function create(ReviewForm $form)
	{
		$review = new Review();

		$review->create(
			$form->name,
			$form->company,
			$form->email,
			$form->message,
			$form->rating,
			$form->product_id
		);

		$this->repository->save($review);
	}

	public function update(int $id, ReviewEditForm $form)
	{
		$review = $this->repository->getReviewById($id);

		$review->edit(
			$form->name,
			$form->company,
			$form->email,
			$form->message,
			$form->rating,
			$form->product_id
		);

		$this->repository->save($review);
	}
}