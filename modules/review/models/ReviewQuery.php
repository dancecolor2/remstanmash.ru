<?php

namespace app\modules\review\models;

use yii\db\ActiveQuery;

class ReviewQuery extends ActiveQuery
{
	public function active()
	{
		return $this->andWhere(['is_active' => 1]);
	}

	public function forProduct($id)
	{
		return $this->andWhere(['product_id' => $id]);
	}
}