<?php

namespace app\modules\review\models;

use app\modules\admin\traits\QueryExceptions;
use app\modules\product\models\Product;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int $id [int(11)]
 * @property int $product_id [int(11)]
 * @property int $created_at [int(11)]
 * @property int $updated_at [int(11)]
 * @property string $name [varchar(255)]
 * @property string $company [varchar(255)]
 * @property string $email [varchar(255)]
 * @property string $message [varchar(500)]
 * @property int $rating [int(11)]
 * @property bool $is_active [tinyint(1)]
 *
 * @property Product $product
 */
class Review extends ActiveRecord
{
	use QueryExceptions;

	public function behaviors()
	{
		return [
			TimestampBehavior::class
		];
	}

	public function rules()
	{
		return [
			[['product_id', 'rating'], 'integer'],
			[['name', 'email', 'company'], 'string', 'max' => 255],
			['email', 'email'],
			['message', 'string', 'max' => 500],
			['is_active', 'boolean']
		];
	}

	public static function tableName()
	{
		return 'reviews';
	}

	public static function find()
	{
		return new ReviewQuery(static::class);
	}

	public function attributeLabels()
	{
		return [
			'product_id' => 'ID товара',
			'rating' => 'Рейтинг',
			'name' => 'Имя',
			'company' => 'Компания',
			'email' => 'Email',
			'message' => 'Отзыв',
			'is_active' => 'Активность',
			'created_at' => 'Дата добавления'
		];
	}

	public function getProduct()
	{
		return $this->hasOne(Product::class, ['id' => 'product_id']);
	}

	public function isActive()
	{
		return $this->is_active ? true : false;
	}

	public function canBeDeleted()
	{
		return $this->isInactive();
	}

	private function isInactive()
	{
		return !$this->is_active;
	}

	public function activate()
	{
		$this->is_active = true;
	}

	public function deactivate()
	{
		$this->is_active = false;
	}

	public function edit(string $name, string $company, string $email, string $message, int $rating, int $product_id)
	{
		$this->name = $name;
		$this->company = $company;
		$this->email = $email;
		$this->message = $message;
		$this->rating = $rating;
		$this->product_id = $product_id;
	}

	public function hasProduct()
	{
		return $this->getProduct()->exists();
	}

	public function create(string $name, string $company, string $email, string $message, int $rating, int $product_id)
	{
		$this->name = $name;
		$this->company = $company;
		$this->email = $email;
		$this->message = $message;
		$this->rating = $rating;
		$this->product_id = $product_id;
	}
}