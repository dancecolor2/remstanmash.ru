<?php

namespace app\modules\review\models;

use yii\data\ActiveDataProvider;

class ReviewSearch extends Review
{
	public function rules()
	{
		return [
			[['id', 'product_id', 'rating', 'is_active', 'created_at'], 'integer'],
			[['name', 'email', 'message'], 'string']
		];
	}

    public function search($params): ActiveDataProvider
	{
		$query = Review::find();

		if ($this->load($params) && $this->validate()) {
			$query->andFilterWhere(['id' => $this->id]);
			$query->andFilterWhere(['product_id' => $this->product_id]);
			$query->andFilterWhere(['is_active' => $this->is_active]);
			$query->andFilterWhere(['rating' => $this->rating]);
			$query->andFilterWhere(['like', 'name', $this->name]);
			$query->andFilterWhere(['like', 'email', $this->email]);
			$query->andFilterWhere(['like', 'message', $this->message]);
		}

		return new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => ['defaultPageSize' => 50],
		]);
	}
}