<?php

namespace app\modules\review\helpers;

use yii\helpers\ArrayHelper;

class ReviewHelper
{
	public static function getDropDownList()
	{
		return [
			1 => '1',
			2 => '2',
			3 => '3',
			4 => '4',
			5 => '5'
		];
	}

	public static function getRatingLabel(int $rating)
	{
		return ArrayHelper::getValue(self::getDropDownList(), $rating, '-');
	}

	public static function getClassForRating(int $rating)
	{
		switch ($rating) {
			case 1:
			case 2:
				return 'label label-danger';
			case 3:
			case 4:
				return 'label label-warning';
			case 5:
				return 'label label-success';
			default:
				return 'label label-default';
		}
	}

	public static function getStars(int $rating): array
	{
		return array_pad(
			array_fill(0, $rating, '<div class="review__star is-active"></div>'),
			5,
			'<div class="review__star"></div>'
		);
	}
}