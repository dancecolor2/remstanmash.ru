<?php

namespace app\modules\review\controllers;

use app\modules\review\forms\ReviewForm;
use app\modules\review\service\ReviewService;
use Exception;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{
	private $service;

	public function __construct($id, $module, ReviewService $service, $config = [])
	{
		parent::__construct($id, $module, $config);
		$this->service = $service;
	}

	public function actionCreate()
	{
		$formModel = new ReviewForm();

		if ($formModel->load(Yii::$app->request->post()) && $formModel->validate()) {

			try {
				$this->service->create($formModel);
				Yii::$app->session->setFlash('review-success');
				return $this->redirect(['success', 'id' => $formModel->product_id]);
			} catch (Exception $e) {
				Yii::$app->errorHandler->logException($e);
			}
		}

		Yii::$app->session->setFlash('review-error');
		return $this->redirect(['error']);
	}

	public function actionSuccess($id)
	{
		if (Yii::$app->session->hasFlash('review-success')) {
			return $this->render('success', compact('id'));
		}

		return $this->goHome();
	}

	public function actionError()
	{
		if (Yii::$app->session->getFlash('review-error')) {
			return $this->render('error');
		}

		return $this->goHome();
	}
}