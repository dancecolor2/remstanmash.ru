<?php

namespace app\modules\review\controllers;

use app\modules\admin\components\BalletController;
use app\modules\review\forms\ReviewEditForm;
use app\modules\review\models\Review;
use app\modules\review\models\ReviewSearch;
use app\modules\review\service\ReviewService;
use DomainException;
use Exception;
use RuntimeException;
use Yii;
use yii\filters\VerbFilter;

class BackendController extends BalletController
{
	private $service;

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'delete' => ['POST'],
					'activate' => ['POST'],
					'inactive' => ['POST'],
					'enable-hit' => ['POST'],
					'disable-hit' => ['POST'],
				],
			],
		];
	}

	public function __construct($id, $module, ReviewService $service, $config = [])
	{
		parent::__construct($id, $module, $config);
		$this->service = $service;
	}

	public function actionIndex()
	{
		$searchModel = new ReviewSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->get());

		return $this->render('index', compact('dataProvider', 'searchModel'));
	}

	public function actionView($id)
	{
		$review = Review::findOne(['id' => $id]);

		return $this->render('view', compact('review'));
	}

	public function actionUpdate($id)
	{
		$review = Review::findOneOrException($id);
		$editForm = new ReviewEditForm($review);

		if ($editForm->load(Yii::$app->request->post()) && $editForm->validate()) {
			try {
				$this->service->update($review->id, $editForm);
				return $this->redirect(['view', 'id' => $review->id]);
			} catch (Exception $e) {
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash($e->getMessage());
			}
		}

		return $this->render('update', compact('review', 'editForm'));
	}

    public function actionActivate($id)
	{
		$review = Review::findOne(['id' => $id]);

		try {
			$this->service->activate($review->id);
			Yii::$app->session->setFlash('success', 'Отзыв активирован');
		} catch (DomainException $e) {
			Yii::$app->session->setFlash('error', $e->getMessage());
		} catch (RuntimeException $e) {
			Yii::$app->errorHandler->logException($e);
			Yii::$app->session->setFlash('error', 'Техническая ошибка');
		}

		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionDeactivate($id)
	{
		$review = Review::findOne(['id' => $id]);

		try {
			$this->service->deactivate($review->id);
			Yii::$app->session->setFlash('success', 'Отзыв заблокирован');
		} catch (DomainException $e) {
			Yii::$app->session->setFlash('error', $e->getMessage());
		} catch (RuntimeException $e) {
			Yii::$app->errorHandler->logException($e);
			Yii::$app->session->setFlash('error', 'Техническая ошибка');
		}

		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionDelete($id)
	{
		$review = Review::findOne(['id' => $id]);

		try {
			$this->service->delete($review->id);
			Yii::$app->session->setFlash('success', 'Отзыв удален');
			return $this->redirect(['index']);
		} catch (DomainException $e) {
			Yii::$app->session->setFlash('error', $e->getMessage());
		} catch (RuntimeException $e) {
			Yii::$app->errorHandler->logException($e);
			Yii::$app->session->setFlash('error', 'Техническая ошибка');
		}

		return $this->redirect(Yii::$app->request->referrer);
	}
}