<?php

use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var int $id
 */

$this->title = 'Заявка отправлена';

?>

<section class="section is-margin-top-60">
    <div class="container">
        <div class="section__body">
            <div class="grid is-row">
                <div class="col-6 shift-3">
                    <div class="response">
                        <h1 class="response__title">Мы получили ваш отзыв</h1>
                        <p class="response__text">После модерации он обязательно появится на сайте<br> <strong>Спасибо за доверие!</strong></p>
                        <div class="response__button">
                            <a class="button js-button is-wide" area-label="вернуться на главную" href="<?= Url::to(['/product/frontend/view', 'id' => $id]) ?>">вернуться к товару</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

