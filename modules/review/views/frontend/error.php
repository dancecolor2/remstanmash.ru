<?php

/**
 * @var $this \yii\web\View
 */

$this->title = 'Ошибка :(';

?>

<section class="section is-margin-top-60">
    <div class="container">
        <div class="section__body">
            <div class="grid is-row">
                <div class="col-6 shift-3">
                    <div class="response">
                        <h1 class="response__title">Ошибка.</h1>
                        <p class="response__text">Что-то пошло не так.</p>
                        <div class="response__button">
                            <a class="button js-button is-wide" area-label="вернуться на главную" href="/">вернуться на главную</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>