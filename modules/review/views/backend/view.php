<?php

/**
 * @var \yii\web\View $this
 * @var Review $review
 */

use app\helpers\DateHelper;
use app\modules\review\models\Review;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = 'Отзыв ' . $review->name;
$this->params['breadcrumbs'][] = ['label' => 'Отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $review->name;

?>

<div class="box">
    <div class="box-body">
        <div class="form-group">
			<?= Html::a('Редактировать', ['update', 'id' => $review->id], ['class' => 'btn btn-primary']) ?>

            <div class="btn-group">
                <a class="btn disabled <?= $review->isActive() ? 'btn-success' : 'btn-default' ?>"><?= $review->isActive() ? 'Активный' : 'Неактивный' ?></a>
                <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <?php if ($review->isActive()): ?>
                        <li><a data-method="post" href="<?= Url::to(['deactivate', 'id' => $review->id]) ?>">Заблокировать</a></li>
                    <?php else: ?>
                        <li><a data-method="post" href="<?= Url::to(['activate', 'id' => $review->id]) ?>">Активировать</a></li>
                    <?php endif ?>
                </ul>
            </div>

			<?php if($review->canBeDeleted()): ?>
				<?= Html::a('Удалить', ['delete', 'id' => $review->id], [
					'class' => 'btn btn-danger',
					'data-method' => 'post',
					'data-confirm' => 'Подтвердите удаление отзыва',
				]) ?>
			<?php endif ?>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid box-default">
                    <div class="box-header">
                        <h3 class="box-title">Отзыв</h3>
                    </div>
                    <div class="box-body">
						<?= $review->message ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-solid box-default">
                    <div class="box-header">
                        <h3 class="box-title">Информация</h3>
                    </div>
                    <div class="box-body">
						<?= DetailView::widget([
							'model' => $review,
							'attributes' => [
								[
									'attribute' => 'created_at',
									'value' => DateHelper::forHuman($review->created_at)
								],
								'name',
								'company',
								'email',
								'rating',
								[
									'attribute' => 'product_id',
									'format' => 'raw',
									'value' => function(Review $review) {
						                return Html::a($review->product->title, ['/product/backend/default/update', 'id' => $review->product_id], ['target' => '_blank']);
                                    },
								],
							]
						]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>