<?php

use app\modules\review\helpers\ReviewHelper;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var \yii\web\View $this
 * @var \app\modules\review\forms\ReviewEditForm $editForm
 * @var \app\modules\review\models\Review $review
 */

$this->title = 'Редактирование отзыва от ' . $review->name;
$this->params['breadcrumbs'][] = ['label' => 'Отзыв ' . $review->name, 'url' => ['view', 'id' => $review->id]];
$this->params['breadcrumbs'][] = $review->name;

$formatJs = <<< 'JS'
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    
    let markup = '';
    
    if (repo.image) {
        markup = '<img src="' + repo.image + '" style="max-width: 60px; max-height: 60px; margin-right:10px; float:left;">'
    }
    markup += '<b> ' + repo.text + '</b>' 
        + '<br><small>Артикул: <b>' + repo.code + '</b></small>'
        + '<br><small>id: <b>' + repo.id +  '</b></small>';

        
    return '<div style="overflow:hidden; border:1px solid #337ab726; padding: 5px;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return  repo.text;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data) {
    return {
        results: data.results
    };
}
JS;

?>

<div class="box box-primary">
	<?php $form = ActiveForm::begin() ?>

	<div class="box-body">
		<div class="row">
			<div class="col-xs-6">
				<?= $form->field($editForm, 'name') ?>
				<?= $form->field($editForm, 'company') ?>
				<?= $form->field($editForm, 'email') ?>
			</div>
            <div class="col-xs-6">
				<?= $form->field($editForm, 'rating')->dropDownList(ReviewHelper::getDropDownList()) ?>
				<?= $form->field($editForm, 'product_id')->widget(Select2::class, [
					'showToggleAll' => false,
					'data' => $review->hasProduct() ? [$review->product_id => $review->product->title] : [],
					'initValueText' => $review->hasProduct() ? [$review->product->title] : [],
					'options' => ['placeholder' => 'Выберите товары'],
					'pluginOptions' => [
						'allowClear' => true,
						'minimumInputLength' => 3,
						'ajax' => [
							'url' => Url::to(['/product/backend/default/product-list']),
							'dataType' => 'json',
							'processResults' => new JsExpression($resultsJs),
						],
						'templateResult' => new JsExpression('formatRepo'),
						'templateSelection' => new JsExpression('formatRepoSelection'),
						'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					],
				]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
				<?= $form->field($editForm, 'message')->textarea(['rows' => 6]) ?>
            </div>
        </div>
	</div>

	<div class="box-footer">
		<button class="btn btn-success">Сохранить</button>
	</div>

	<?php $form::end() ?>
</div>