<?php

use app\modules\admin\helpers\GridHelper;
use app\modules\review\helpers\ReviewHelper;
use app\modules\review\models\Review;
use kartik\grid\ActionColumn;
use kartik\grid\DataColumn;
use kartik\icons\Icon;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var ActiveDataProvider $dataProvider
 * @var \app\modules\review\models\ReviewSearch $searchModel
*/

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;

?>

<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'summaryOptions' => ['class' => 'text-right'],
	'pjax' => true,
	'pjaxSettings' => ['options' => ['id' => 'pjax-widget']],
	'bordered' => false,
	'striped' => false,
	'hover' => true,
	'panel' => ['before'],
	'toolbar' => [
		['content'=>
			Html::a(
				Icon::show('arrow-sync-outline'),
				['index'],
				[
					'data-pjax' => 0,
					'class' => 'btn btn-default',
				]
			)
		],
		'{toggleData}',
	],
	'toggleDataOptions' => [
		'all' => [
			'icon' => 'resize-full',
			'label' => 'Показать все',
			'class' => 'btn btn-default',
			'title' => 'Показать все'
		],
		'page' => [
			'icon' => 'resize-small',
			'label' => 'Страницы',
			'class' => 'btn btn-default',
			'title' => 'Постаничная разбивка'
		],
	],
	'export' => false,
	'columns' => [
		[
			'class' => DataColumn::class,
			'vAlign' => GridView::ALIGN_MIDDLE,
			'attribute' => 'id',
			'width' => '80px',
		],
		[
			'class' => DataColumn::class,
			'vAlign' => GridView::ALIGN_MIDDLE,
			'attribute' => 'product_id',
			'width' => '80px',
		],
		[
			'class' => DataColumn::class,
			'vAlign' => GridView::ALIGN_MIDDLE,
			'attribute' => 'name',
			'width' => '250px',
		],
		[
			'class' => DataColumn::class,
			'vAlign' => GridView::ALIGN_MIDDLE,
			'attribute' => 'email',
			'width' => '300px',
		],
		[
			'class' => DataColumn::class,
			'vAlign' => GridView::ALIGN_MIDDLE,
			'attribute' => 'message'
		],
		[
			'class' => DataColumn::class,
			'vAlign' => GridView::ALIGN_MIDDLE,
			'attribute' => 'rating',
			'format' => 'raw',
			'width' => '50px',
			'filter' => ReviewHelper::getDropDownList(),
			'value' => function(Review $review) {
				return Html::tag('span', ReviewHelper::getRatingLabel($review->rating), ['class' => ReviewHelper::getClassForRating($review->rating)]);
			}
		],
		[
			'class' => DataColumn::class,
			'vAlign' => GridView::ALIGN_MIDDLE,
			'hAlign' => GridView::ALIGN_CENTER,
			'attribute' => 'is_active',
			'width' => '50px',
			'format' => 'html',
			'filter' => [1 => 'Да', 0 => 'Нет'],
			'value' => function(Review $review) {
				return GridHelper::getActiveIcon($review->isActive());
			}
		],
		[
			'class' => ActionColumn::class,
			'template' => '{view}',
			'width' => '50px',
			'mergeHeader' => false,
			'buttons' => [
				'view' => function ($url) {
					return Html::a('<i class="fa fa-eye"></i> Просмотр', $url, [
						'class' => 'btn btn-xs btn-primary',
						'data-pjax' => '0',
					]);
				},
			],
		],
	],
]) ?>