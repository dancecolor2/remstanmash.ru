<?php

namespace app\modules\review;

use yii\base\BootstrapInterface;
use yii\web\GroupUrlRule;

class Module extends \yii\base\Module implements BootstrapInterface
{
	public function bootstrap($app)
	{
		$app->get('urlManager')->rules[] = new GroupUrlRule([
			'rules' => [
				'review' => '/review/frontend/index',
				'/review/success' => '/review/frontend/success',
				'/review/error' => '/review/frontend/error',
			]
		]);
	}
}