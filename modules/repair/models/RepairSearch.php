<?php

namespace app\modules\repair\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\repair\models\Repair;

/**
 * RepairSearch represents the model behind the search form of `app\modules\repair\models\Repair`.
 */
class RepairSearch extends Repair
{
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'position', 'active'], 'integer'],
            [['title', 'icon', 'icon_hash', 'text'], 'safe'],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Repair::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && !$this->validate())) {

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'icon_hash', $this->icon_hash])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
