<?php

namespace app\modules\repair\models;

use yii\db\ActiveRecord;
use app\modules\admin\traits\QueryExceptions;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use yii2tech\ar\position\PositionBehavior;
use app\traits\active\ActiveTrait;
use yiidreamteam\upload\FileUploadBehavior;

/**
 * @property int $id
 * @property int $created_at Дата создания
 * @property int $updated_at Дата редактирования
 * @property int $position
 * @property int $active Активность
 * @property string $title Название
 * @property string $icon Иконка
 * @property string $icon_hash
 * @property string $text Текст
 *
 * @mixin PositionBehavior
 * @mixin FileUploadBehavior
 */
class Repair extends ActiveRecord
{
    use QueryExceptions;
    use ActiveTrait;

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            PositionBehavior::class,
            'icon' => [
                'class' => FileUploadBehavior::class,
                'attribute' => 'icon',
                'filePath' => '@webroot/uploads/repair/[[pk]]_[[attribute_icon_hash]].[[extension]]',
                'fileUrl' => '/uploads/repair/[[pk]]_[[attribute_icon_hash]].[[extension]]',
            ],
        ];
    }

    public static function tableName()
    {
        return 'repair';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['active'], 'integer'],
            [['text'], 'string'],
            ['icon', 'file', 'skipOnEmpty' => true],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'active' => 'Активность',
            'title' => 'Название',
            'icon' => 'Иконка',
            'icon_hash' => 'Icon Hash',
            'text' => 'Текст',
        ];
    }

    /**
     * {@inheritdoc}
     * @return RepairQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RepairQuery(get_called_class());
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function beforeSave($insert)
    {
        if ($this->icon instanceof UploadedFile) {
            $this->icon_hash = uniqid();
        }
        return parent::beforeSave($insert);
    }

    public function deleteIcon()
    {
        /** @var FileUploadBehavior $fileBehavior */
        $fileBehavior = $this->getBehavior('icon');
        $fileBehavior->cleanFiles();
        $this->updateAttributes([
            'icon' => null,
            'icon_hash' => null,
        ]);
    }

    public function showIcon()
    {
        if ($this->hasIcon()) {
            readfile($this->getUploadedFilePath('icon'));
        }
    }

    public function hasIcon()
    {
        return !empty($this->icon) && file_exists($this->getUploadedFilePath('icon'));
    }

}
