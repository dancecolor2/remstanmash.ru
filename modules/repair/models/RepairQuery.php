<?php

namespace app\modules\repair\models;

use app\traits\active\ActiveQueryTrait;
/**
 * @see Repair
 */
class RepairQuery extends \yii\db\ActiveQuery
{
    use ActiveQueryTrait;
}
