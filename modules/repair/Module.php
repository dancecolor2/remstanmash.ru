<?php

namespace app\modules\repair;

use app\modules\repair\models\Repair;
use enshrined\svgSanitize\Sanitizer;
use yii\base\Event;
use yiidreamteam\upload\FileUploadBehavior;

class Module extends \yii\base\Module
{
    public function bootstrap($app)
    {
        Event::on(Repair::class, FileUploadBehavior::EVENT_AFTER_FILE_SAVE, [$this, 'clearSvg']);
    }

    public function clearSvg(Event $event)
    {
        /** @var Repair $repair */
        $repair = $event->sender;
        $svgFile = $repair->getUploadedFilePath('icon');

        if (is_file($svgFile)) {
            $sanitizer = new Sanitizer();
            $sanitizer->removeRemoteReferences(true);
            $dirtySVG = file_get_contents($svgFile);
            $cleanSVG = $sanitizer->sanitize($dirtySVG);
            file_put_contents($svgFile, $cleanSVG);
        }
    }
}