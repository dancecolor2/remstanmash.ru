<?php

namespace app\modules\repair\controllers\backend;

use Yii;
use app\modules\admin\components\BalletController;
use app\modules\repair\models\Repair;
use app\modules\repair\models\RepairSearch;
use yii\filters\VerbFilter;
use yii\web\Response;

class DefaultController extends BalletController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'delete-icon' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new RepairSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    public function actionCreate()
    {
        $repair = new Repair(['active' => true]);

        if ($repair->load(Yii::$app->request->post()) && $repair->save()) {
            Yii::$app->session->setFlash('success', 'Услуга создана');

            return $this->redirect(['update', 'id' => $repair->id]);
        }

        return $this->render('create', [
            'repair' => $repair,
        ]);
    }

    public function actionUpdate($id)
    {
        $repair = Repair::findOneOrException($id);

        if ($repair->load(Yii::$app->request->post()) && $repair->save()) {

            Yii::$app->session->setFlash('success', 'Услуга обновлена');
            return $this->refresh();
        }

        return $this->render('update', compact('repair'));
    }

    public function actionDelete($id)
    {
        Repair::findOneOrException($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMoveUp($id)
    {
        Repair::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        Repair::findOneOrException($id)->moveNext();
    }

    public function actionDeleteIcon($id)
    {
        Repair::findOneOrException($id)->deleteIcon();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['success' => true];
    }

}
