<?php

use app\helpers\FormatHelper;
use app\modules\feedback\forms\RepairForm;
use yii\helpers\Json;

/**
 * @var \yii\web\View $this
 * @var \app\modules\repair\models\Repair $repair
 */

?>

<?php if (!empty($repair)): ?>
    <article class="service">
        <header class="service__header">
            <?php if ($repair->hasIcon()): ?>
                <div class="service__icon">
                    <?php $repair->showIcon() ?>
                </div>
            <?php endif ?>
            <h3 class="service__title"><?= $repair->title ?></h3>
        </header>
        <div class="service__body">
            <p class="service__text">
                <?= FormatHelper::nText($repair->text) ?>
            </p>
        </div>
        <footer class="service__footer">
            <div class="service__button">
                <button class="button js-button is-wide is-lower" area-label="Заказать услугу" data-modal="order"
                        data-info='<?= Json::encode([
                            'id' => $repair->id,
                            'title' => $repair->title,
                            'type' => RepairForm::TYPE,
                        ]) ?>'>Заказать услугу</button>
            </div>
        </footer>
    </article>
<?php endif ?>

