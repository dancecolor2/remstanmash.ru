<?php

use app\modules\repair\widgets\RepairWidget;

/**
 * @var \yii\web\View $this
 * @var \app\modules\repair\models\Repair[] $repairs
 */

?>

<?php if (!empty($repairs)): ?>
    <section class="section">
        <div class="container">
            <div class="section__body">
                <div class="services">
                    <ul class="services__list grid">
                        <?php foreach ($repairs as $repair): ?>
                            <li class="services__item col-4 col-md-6 col-s-12">
                                <?= RepairWidget::widget(compact('repair')) ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>