<?php

namespace app\modules\repair\widgets;

use app\modules\repair\models\Repair;
use yii\base\Widget;

/**
 * @property Repair $repair
 */
class RepairWidget extends Widget
{
    public $repair;

    public function run()
    {
        return $this->render('repair', [
            'repair' => $this->repair,
        ]);
    }
}