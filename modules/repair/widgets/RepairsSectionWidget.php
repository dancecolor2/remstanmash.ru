<?php

namespace app\modules\repair\widgets;

use app\modules\repair\models\Repair;
use yii\base\Widget;

class RepairsSectionWidget extends Widget
{
    public function run()
    {
        $repairs = Repair::find()->active()->orderBy('position')->all();

        return $this->render('repairs_section', compact('repairs'));
    }
}