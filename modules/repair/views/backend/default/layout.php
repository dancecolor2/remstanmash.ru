<?php
use yii\bootstrap\Tabs;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $content
 * @var app\modules\repair\models\Repair $repair * @var array $breadcrumbs
 */

$this->title = $repair->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Ремонт оборудования', 'url' => ['/repair/backend/default/index']];

if (Yii::$app->controller->action->id == 'update') {
    $this->params['breadcrumbs'][] = $repair->getTitle();
} else {
    $this->params['breadcrumbs'][] = [
        'label' => $repair->getTitle(),
        'url' => ['/repair/backend/default/update', 'id' => $repair->id],
    ];
}

if (!empty($breadcrumbs)) {
    foreach ($breadcrumbs as $breadcrumb) {
        $this->params['breadcrumbs'][] = $breadcrumb;
    }
}

?>
<div class="nav-tabs-custom">
    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'url' => ['/repair/backend/default/update', 'id' => $repair->id],
                'active' => Yii::$app->controller->action->id == 'update',
            ],
            [
                'label' => 'Действия',
                'headerOptions' => ['class' => 'pull-right'],
                'items' => [
                    [
                        'encode' => false,
                        'label' => '<i class="fa fa-remove text-danger" aria-hidden="true"></i>Удалить услугу',
                        'url' => Url::to(['/repair/backend/default/delete', 'id' => $repair->id]),
                        'linkOptions' => [
                            'class' => 'text-danger',
                            'data-method' => 'post',
                            'data-confirm' => 'Вы уверены?',
                        ],
                    ],
                ],
            ],
        ]
    ]) ?>

    <?= $content ?>

</div>
