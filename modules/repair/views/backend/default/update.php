<?php

/**
 * @var yii\web\View $this
 * @var app\modules\repair\models\Repair $repair
 */

$this->title = $repair->getTitle();
?>

<?php $this->beginContent('@app/modules/repair/views/backend/default/layout.php', ['repair' => $repair]); ?>

    <?= $this->render('_form', compact('repair')) ?>

<?php $this->endContent() ?>



