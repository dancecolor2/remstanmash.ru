<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var app\modules\repair\models\Repair $repair
 */

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($repair, 'active')->checkbox() ?>
                <?= $form->field($repair, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($repair, 'text')->textarea(['rows' => 6]) ?>
            </div>
            <div class="col-xs-4">
                <div class="single-kartik-image">
                    <?= $form->field($repair, 'icon')->widget(FileInput::class, [
                        'pluginOptions' => [
                            'fileActionSettings' => [
                                'showDrag' => false,
                                'showZoom' => true,
                                'showUpload' => false,
                                'showDownload' => true,
                            ],
                            'initialPreviewDownloadUrl' => $repair->getUploadedFileUrl('icon'),
                            'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'showClose' => false,
                            'showCancel' => false,
                            'browseClass' => 'btn btn-primary btn-block',
                            'browseIcon' => '<i class="glyphicon glyphicon-download-alt"></i>',
                            'browseLabel' =>  'Выберите файл',
                            'deleteUrl' => Url::to(['delete-icon', 'id' => $repair->id]),
                            'initialPreview' => [
                                $repair->hasIcon() ? $repair->getUploadedFileUrl('icon') : null,
                            ],
                            'initialPreviewConfig' => [
                                $repair->hasIcon() ? [
                                    'caption' => $repair->icon,
                                    'size' => filesize($repair->getUploadedFilePath('icon')),
                                    'downloadUrl' => $repair->getUploadedFileUrl('icon'),
                                ] : [],
                            ],
                            'initialPreviewAsData' => true,
                        ],
                        'options' => ['accept' => 'image/svg+xml'],
                    ]);?>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?=  Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>

