<?php

/**
 * @var yii\web\View $this
 * @var $repair app\modules\repair\models\Repair
 */

$this->title = 'Новая услуга';
$this->params['breadcrumbs'][] = ['label' => 'Ремонт оборудования', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">
    <?= $this->render('_form', compact('repair')) ?>
</div>

