const notify = require('gulp-notify');

module.exports = function(...arg) {
  const args = Array.prototype.slice.call(arg);
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>',
    sound: 'Submarine'
  }).apply(this, args);
  this.emit('end');
};
