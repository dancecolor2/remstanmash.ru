const fs = require('fs');

const components = [];
const data = fs.readdirSync('frontend/components');

data.forEach((component) => {
  if (component.indexOf('.') < 0) components.push(component);
});

const newPackageJson = JSON.stringify(components, '', 2);

fs.writeFileSync(
  'frontend/components/components.json',
  newPackageJson
);
