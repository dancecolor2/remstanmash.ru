const stage = process.env.STAGE || 'backend';
const environment = process.env.NODE_ENV || 'development';

const isProduction = environment === 'production';
const isFrontend = stage === 'frontend';

const root = {
  frontend: 'frontend',
  backend: 'src'
};

const dest = {
  frontend: isProduction ? 'build' : 'public',
  backend: 'web'
};

module.exports = {
  isProduction,
  isFrontend,
  stage,
  environment,
  src: {
    root: `${root[stage]}`,
    assets: `${root[stage]}/assets`,
    components: `${root[stage]}/components`,
    pages: `${root[stage]}/pages`,
    styles: `${root[stage]}/scss`,
    js: `${root[stage]}/js`,
    img: `${root[stage]}/img`,
    icons: `${root[stage]}/icons`,
    fonts: `${root[stage]}/fonts`
  },
  dest: {
    root: dest[stage],
    html: dest[stage],
    css: `${dest[stage]}/css`,
    js: `${dest[stage]}/js`,
    img: `${dest[stage]}/img`,
    fonts: `${dest[stage]}/fonts`,
    icons: `${dest[stage]}/img/icons`
  },
  errorHandler: require('./util/handleErrors')
};
