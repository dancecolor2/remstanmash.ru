const gulp = require('gulp');
const util = require('gulp-util');
const notify = require('gulp-notify');
const webpack = require('webpack');
const config = require('../../webpack.config');
const server = require('./server');

function handler(err, stats, callback) {
  const errors = stats.compilation.errors;

  if (err) throw new util.PluginError('webpack', err);

  if (errors.length > 0) {
    notify
      .onError({
        title: 'Webpack Error',
        message: '<%= error.message %>'
      })
      .call(null, errors[0]);
  }

  util.log(
    '[webpack]',
    stats.toString({
      colors: true,
      modules: false,
      version: false,
      hash: false
    })
  );

  server.reload();
  if (typeof callback === 'function') callback();
}

gulp.task('webpack', callback => {
  webpack(config).run((err, stats) => {
    handler(err, stats, callback);
  });
});

gulp.task('webpack:watch', () => {
  webpack(config).watch(
    {
      aggregateTimeout: 100,
      poll: false
    },
    handler
  );
});
