const gulp = require('gulp');
const sequence = require('run-sequence');
const config = require('../config');

gulp.task('watch', next => {
  if (config.isFrontend) {
    sequence(
      'html:watch',
      'html:data:watch',
      'styles:watch',
      'copy:img:watch',
      'copy:js:watch',
      'webpack:watch',
      next
    );
  } else {
    sequence(
    	'styles:watch',
    	'copy:js:watch',
    	'webpack:watch',
    	next
    );
  }
});
