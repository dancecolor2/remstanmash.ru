const autoprefixer = require('autoprefixer');
const cleanss = require('gulp-cleancss');
const debug = require('gulp-debug');
const gulp = require('gulp');
const gulpIf = require('gulp-if');
const imageInliner = require('postcss-image-inliner');
const objectFitImages = require('postcss-object-fit-images');
const inlineSVG = require('postcss-inline-svg');
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sequence = require('run-sequence');
const size = require('gulp-size');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const server = require('./server');

const { isProduction, src, dest, errorHandler } = require('../config');

gulp.task('styles', () => {
  // prettier-ignore

  const _postCss = postcss(
    isProduction
      ? [
        objectFitImages(),
        autoprefixer({ browsers: ['last 2 version'] }),
        inlineSVG(),
        imageInliner({
          assetPaths: [`${src.components}/**/`]
        })
      ]
      : [
        objectFitImages()
      ]
  );
  const _sass = sass({
    includePaths: ['node_modules/lightgallery/src/sass/']
  });
  const _plumber = plumber({ errorHandler });
  const _sourcemapInit = gulpIf(!isProduction, sourcemaps.init());
  const _cleanCss = gulpIf(isProduction, cleanss());
  const _rename = rename('style.min.css');
  const _sourcemap = gulpIf(!isProduction, sourcemaps.write('/'));
  const _size = size({ title: 'Style' });

  return gulp
    .src(`${src.styles}/style.scss`)
    .pipe(_plumber)
    .pipe(_sourcemapInit)
    .pipe(_sass)
    .pipe(_postCss)
    .pipe(_cleanCss)
    .pipe(_rename)
    .pipe(_sourcemap)
    .pipe(_size)
    .pipe(gulp.dest(dest.css))
    .pipe(server.stream({ match: '**/*.css' }));
});

gulp.task('styles:watch', () => {
  watch(
    [
      `!${src.styles}/**/_*.scss`,
      `${src.styles}/**/*.scss`,
      `!${src.components}/**/_*.scss`,
      `${src.components}/**/*.scss`
    ],
    () => {
      sequence('styles');
    }
  ).pipe(debug({ title: '[Style]:' }));
});
