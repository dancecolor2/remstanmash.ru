const gulp = require('gulp');
const pug = require('gulp-pug');
const progeny = require('gulp-progeny');
const progeny2 = require('progeny');
const cached = require('gulp-cached');
const plumber = require('gulp-plumber');
const debug = require('gulp-debug');
const watch = require('gulp-watch');
const sequence = require('run-sequence');
const fs = require('fs');
// const gulpif = require('gulp-if');
// const filter = require('gulp-filter');
const numberFormat = require('../util/numberFormat');

const { src, dest, errorHandler } = require('../config');

function getData() {
  try {
    return JSON.parse(fs.readFileSync(`${src.assets}/data.json`, 'utf8'));
  } catch (e) {
    return {};
  }
}

const data = {
  json: getData()
};

gulp.task('html', () => {
  return gulp
    .src([
      `${src.pages}/*.pug`,
      `!${src.pages}/_*.pug`,
    ])
    .pipe(plumber({ errorHandler }))
    // .pipe(cached('pug', { optimizeMemory: true }))
    .pipe(progeny())
    .pipe(
      pug({
        data: data.json,
        pretty: true,
        locals: {
          numberFormat: number => numberFormat(number, 0, ',', ' ')
        },
        basedir: `${src.root}`,
        compileDebug: true,
        cache: true
      })
    )
    .pipe(gulp.dest(dest.root));
});

gulp.task('html:watch', () => {
  watch(
    [
      `${src.pages}/**/*.pug`,
      `!${src.pages}/**/_*.pug`,
      `${src.components}/**/*.pug`,
      `!${src.components}/**/_*.pug`
    ],
    () => {
      sequence('html');
    }
  ).pipe(debug({ title: '[Template]:' }));
});

gulp.task('html:data:watch', () => {
  watch(`${src.assets}/data.json`, () => {
    console.log('Templates data updated');
    data.json = getData();
    sequence('html');
  });
});
