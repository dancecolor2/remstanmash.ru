const gulp = require('gulp');
const sequence = require('run-sequence');
const svgstore = require('gulp-svgstore');
const svgmin = require('gulp-svgmin');
const cheerio = require('gulp-cheerio');
const config = require('../config');
const size = require('gulp-size');
const rename = require('gulp-rename');
const spritesmith = require('gulp.spritesmith');
const buffer = require('vinyl-buffer');
const merge = require('merge-stream');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');

const spriteSvgPath = `${config.src.icons}/svg/`;
const spritePngPath = `${config.src.icons}/png/`;

gulp.task('sprite', callback => {
  sequence('sprite:svg', 'sprite:png', callback);
});

gulp.task('sprite:svg', () =>
  gulp
    .src(`${spriteSvgPath}*.svg`)
    .pipe(
      svgmin(() => ({
        plugins: [
          {
            cleanupIDs: {
              minify: true
            }
          },
          {
            removeStyleElement: true
          }
        ]
      }))
    )
    .pipe(svgstore({ inlineSvg: true }))
    .pipe(
      cheerio({
        run: $ => {
          $('svg').attr('style', 'display:none');
          $('symbol *').removeAttr('class');
        },
        parserOptions: {
          xmlMode: true
        }
      })
    )
    .pipe(rename('sprite.svg'))
    .pipe(
      size({
        title: 'Icons',
        showFiles: true,
        showTotal: false
      })
    )
    .pipe(gulp.dest(`${config.src.img}`))
);

gulp.task('sprite:png', () => {
  const fileName = 'sprite.png';
  const spriteData = gulp.src(`${spritePngPath}*.png`).pipe(
    spritesmith({
      imgName: fileName,
      cssName: 'icons.scss',
      padding: 2,
      imgPath: `/img/icons/${fileName}`
    })
  );

  const imgStream = spriteData.img
    .pipe(buffer())
    .pipe(
      imagemin({
        use: [pngquant()]
      })
    )
    .pipe(gulp.dest(config.dest.icons));

  const cssStream = spriteData.css.pipe(gulp.dest(config.src.styles));

  return merge(imgStream, cssStream);
});
