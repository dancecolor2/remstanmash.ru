const gulp = require('gulp');
const sequence = require('run-sequence');
const config = require('../config');

gulp.task('build', next => {
  if (config.isFrontend) {
    if (config.isProduction) {
      sequence(
        'clean',
        'html',
        'styles',
        'webpack',
        'copy:img',
        'copy:fonts',
        'copy:js',
        next
      );
    } else {
      sequence(
        'clean',
        'html',
        'styles',
        'webpack',
        'copy:img',
        'copy:fonts',
        'copy:js',
        next
      );
    }
  } else {
    sequence('styles', 'webpack', 'copy:js', next);
  }
});
