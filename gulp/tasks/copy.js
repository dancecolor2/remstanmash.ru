const gulp = require('gulp');
const cached = require('gulp-cached');
const size = require('gulp-size');
const watch = require('gulp-watch');
const babel = require('gulp-babel');
const sequence = require('run-sequence');

const {
	src,
	dest
} = require('../config');

gulp.task('copy:img', () =>
  gulp
    .src(`${src.img}/*.{png,jpeg,svg,jpg,gif,ico,cur}`)
    .pipe(cached('img', { optimizeMemory: true }))
    .pipe(
      size({
        title: 'Image',
        showFiles: true,
        showTotal: false
      })
    )
    .pipe(gulp.dest(dest.img))
);

gulp.task('copy:img:watch', () => {
  watch([`${src.img}/*.{png,jpeg,svg,jpg,gif,ico,cur}`], () => {
    sequence('copy:img');
  });
});

gulp.task('copy:fonts', () =>
  gulp
    .src(`${src.fonts}/*.{ttf,woff,woff2,eot,svg}`)
    .pipe(cached('fonts', { optimizeMemory: true }))
    .pipe(
      size({
        title: 'Font',
        showFiles: true,
        showTotal: false
      })
    )
    .pipe(gulp.dest(dest.fonts))
);

gulp.task('copy:fonts:watch', () => {
  watch([`${src.fonts}/*.{ttf,woff,woff2,eot,svg}`], () => {
    sequence('copy:fonts');
  });
});

gulp.task('copy:js', () =>
  gulp
    .src([`${src.js}/_*.js`, `${src.components}/**/_*.js`])
    .pipe(cached('js', { optimizeMemory: true }))
    .pipe(
      babel({
        presets: ['es2015-ie']
      })
    )
    .pipe(
      size({
        title: 'JS',
        showFiles: true,
        showTotal: false
      })
    )
    .pipe(gulp.dest(dest.js))
);

gulp.task('copy:js:watch', () => {
  watch([`${src.js}/_*.js`, `${src.components}/**/_*.js`], () => {
    sequence('copy:js');
  });
});
