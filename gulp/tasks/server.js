const gulp = require('gulp');
const server = require('browser-sync').create();
const config = require('../config');

gulp.task('server', () => {
  server.init({
    server: {
      baseDir: config.dest.root,
      serveStaticOptions: {
        extensions: ['html']
      }
    },
    files: [
      `${config.dest.html}/*.html`,
      `${config.dest.css}/*.css`,
      `${config.dest.img}/**/*`
    ],
    startPath: 'index.html',
    open: true,
    port: 8080,
    logLevel: 'info', // 'debug', 'info', 'silent', 'warn'
    logConnections: false,
    logFileChanges: false,
    notify: false,
    ghostMode: false
  });
});

gulp.task('server:reload', () => server.reload());

module.exports = server;
