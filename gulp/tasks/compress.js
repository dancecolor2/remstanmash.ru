const gulp = require('gulp');
const tinypng = require('gulp-tinypng');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const config = require('../config');

gulp.task('img:compress', () =>
  gulp
    .src([`${config.src.img}/*`, `!${config.src.img}/sprite.svg`])
    .pipe(
      imagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false, removeTitle: true }],
        use: [pngquant()]
      })
    )
    .pipe(gulp.dest(config.dest.img))
);

gulp.task('img:tiny', () => {
  gulp
    .src(`${config.src.img}/*.{jpg,jpeg,png}`)
    .pipe(tinypng('6Ili_B2bO9-zM6Z18V-Q8CsT1UQes5nX'))
    .pipe(gulp.dest(config.dest.img));
  gulp
    .src([`${config.src.img}/*.{gif,svg,ico}`, `!${config.src.img}/sprite.svg`])
    .pipe(
      imagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false, removeTitle: true }],
        use: [pngquant()]
      })
    )
    .pipe(gulp.dest(config.dest.img));
});
