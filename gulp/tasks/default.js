const gulp = require('gulp');
const sequence = require('run-sequence');
const config = require('../config');

gulp.task('default', next => {
  if (config.isFrontend) {
    sequence('build', 'watch', 'server', next);
  } else {
    sequence('build', 'watch', next);
  }
});
