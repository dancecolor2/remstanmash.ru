import "js/lib/polyfills";
import "js/lib/svg";
import "js/lib/objectFit";
import "js/lib/hello";
import "js/lib/focus";
import "js/lib/scroll";
import "js/lib/ie";
import "~/menu/menu";
import "~/dropmenu/dropmenu";
import "~/header-search/header-search";
import "~/overlay/overlay";
import "~/collection/collection";
import "~/params/params";
import "~/product/product";
import "~/slider/slider";
import "~/filter/filter";
import "~/tabs/tabs";
import "~/roll/roll";
import "~/clients-roll/clients-roll";
import "~/equipment/equipment";
import "~/image/image";
import "~/map/map";
import "~/posts/posts";
import "~/form/form";
import "~/modal/modal";
import "~/loader/loader";
import "~/cats/cats";

import $ from "jquery";

// import 'js/lib/scrollbar';
// import 'js/lib/touch';

/* Add jQuery to global scope */
window.jQuery = window.$ = $;
