import $ from 'jquery';

const userAgent = navigator.userAgent;
const isIE =
  userAgent.indexOf('MSIE ') > -1 ||
  userAgent.indexOf('Trident/') > -1 ||
  userAgent.indexOf('Edge/') > -1;

if (isIE) {
  $('body').addClass('is-ie');
}

export default isIE;
