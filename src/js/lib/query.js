// Query.js
//
// Iterates through a given, or current query string and
// caches the results from both the parser and the decoder.
//
// Examples:
//
//     // Build Query String
//     // ?name=Query.js&tags=query&tags=string&tags=encode&tags=builder
//     query.build({ name: 'Query.js', tags: [ 'query', 'string', 'encode', 'builder' ]});
//
//     // Parse the current query string, returns entire object.
//     query.parse();
//
//     // Parses query string and fetch specific parameter assumes query.parse was never ran.
//     query.get('param');
//     query.get('anotherParam'); // This will be cached result.
//
//     // After it has been ran and you wish to rebuild the cache?
//     // Good for when the url may have changed or custom query.
//     query.parse({ rebuild: true });
//     query.get('param', true);
//
//     // Custom Query String?
//     query.parse({ rebuild: true, query: 'param=not+again&timestamp=250826092386' });
//     query.get('param');
//
//     // Return param, with rebuilding of cache, and custom query all in one go?
//     query.parse({ name: 'param', rebuild: true, query: 'param=not+again&timestamp=250826092386' });
//
// @version 0.0.2
// @author Nijiko Yonskai
// @copyright 2013 Nijiko Yonskai

class Query {
  constructor() {
    this.store = {};
    this.decodeStore = {};
    this.built = false;
    this.queryString = undefined;
    this.re = /([^&=]+)=?([^&]*)/g;
    this.m = null;
  }
  merge(obj1, obj2) {
    if (typeof obj1 === 'string') {
      obj1 = this.parse({ rebuild: true, query: obj1 });
    }

    if (typeof obj2 === 'string') {
      obj2 = this.parse({ rebuild: true, query: obj2 });
    }

    return Object.assign({}, obj1, obj2);
  }

  build(data, parent) {
    if (Object.prototype.toString.call(data) !== '[object Object]') return '';

    parent = parent || '';

    let query = parent ? '' : '?',
      okey,
      value,
      l,
      i = 0;

    for (var key in data) {
      okey = parent ? parent + '[' + this.encode(key) + ']' : this.encode(key);

      if ((value = data[key])) {
        if (Object.prototype.toString.call(value) === '[object Array]') {
          for (i = 0, l = value.length; i < l; ++i) {
            if (typeof value[i] === 'object')
              query += this.build(value[i], okey + '[' + i + ']');
            else query += okey + '=' + this.encode(value[i]) + '&';
          }
        } else if (
          Object.prototype.toString.call(value) === '[object Object]'
        ) {
          for (i in value) {
            if (typeof value[i] === 'object')
              query += this.build(value[i], okey + '[' + i + ']');
            else query += okey + '[' + i + ']=' + this.encode(value[i]) + '&';
          }
        } else {
          query += okey + '=' + this.encode(value.toString()) + '&';
        }
      } else {
        query += okey + '&';
      }
    }

    return query.substring(0, query.length - 1);
  }

  getQueryString() {
    const search = window.location.search.substring(1);
    const hash = window.location.hash.split('?');

    hash.shift();

    return search && search !== ''
      ? search
      : hash.length > 0
      ? hash.join('?')
      : undefined;
  }

  encode(str) {
    return encodeURIComponent(str.toString());
  }

  decode(str) {
    if (!this.decodeStore[str])
      this.decodeStore[str] = decodeURIComponent(str.replace(/\+/g, ' '));

    return this.decodeStore[str];
  }

  get(name, rebuild) {
    name = String(name).replace(/[.*+?|()\[\]{}\\]/g, '\\$&');
    return this.parse({ name, rebuild });
  }

  parse(opts) {
    opts = opts || {};

    if (!this.built || opts.rebuild) {
      this.queryString =
        typeof opts.query === 'string' ? opts.query : this.getQueryString();

      if (typeof this.queryString === 'string' && this.queryString.length > 0) {
        var index, aname, pname, $key, $value, $decodeKey, $decodeValue;

        if (this.queryString[0] === '?')
          this.queryString = this.queryString.substring(1);

        this.store = {};
        this.decodeStore = {};

        while ((this.m = this.re.exec(this.queryString))) {
          $key = this.m[1];
          $value = this.m[2];
          $decodeKey = this.decode($key);
          $decodeValue = this.decode($value);

          if (this.m[1].indexOf('[') === -1)
            if (!($decodeKey in this.store))
              this.store[$decodeKey] = $decodeValue;
            else if (typeof this.store[$decodeKey] !== 'object')
              this.store[$decodeKey] = [this.store[$decodeKey], $decodeValue];
            else
              Array.prototype.push.call(this.store[$decodeKey], $decodeValue);
          else {
            index = $key.indexOf('[');
            aname = this.decode(
              $key.slice(index + 1, $key.indexOf(']', index))
            );
            pname = this.decode($key.slice(0, index));

            if (typeof this.store[pname] !== 'object') {
              this.store[pname] = {};
              this.store[pname].length = 0;
            }

            if (aname) this.store[pname][aname] = $decodeValue;
            else Array.prototype.push.call(this.store[pname], $decodeValue);
          }
        }
      } else return undefined;

      this.built = true;
    }

    return opts.name
      ? opts.name in this.store
        ? this.store[opts.name]
        : undefined
      : this.store;
  }
}

export default new Query();
