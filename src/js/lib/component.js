/* global $ */

export const components = {};
export const subscribers = {};

Array.prototype._one = function() {
  return this[0] || [];
};

export default class Component {
  constructor(element, options) {
    this.selector = options.classList.root || '';
    this.name = options.name || this.selector.substr(4);
    this.state = Object.assign({}, options.state) || {};

    if ('dependencies' in options && options.dependencies.length) {
      options.dependencies.forEach(dependency => {
        this[dependency.name] = dependency;
      });
    }

    this.$ = {
      window: $(window),
      document: $(document),
      body: $('.js-body'),
      page: $('.js-page'),
      root: $(element)
    };

    for (let element in options.classList) {
      if (element !== 'root') {
        this.$[element] = this.$.root.find(options.classList[element]);
      }
    }

    if (this.$.root.length) {
      this.$.document.ready(() => {
        this.init && this.init();
        this.events && this.events();
        this.inited && this.inited();
      });
    }
  }
  nextTick(time = 0) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve();
      }, time);
    });
  }
  trottle(fn, delay) {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(fn.bind(this, ...args), delay);
    };
  }
  subscribe(name, fn) {
    if (!subscribers[name]) {
      subscribers[name] = [];
    }

    subscribers[name].push(fn);
  }
  publish(name, data = {}) {
    if (subscribers[name]) {
      subscribers[name].forEach(subscriber => {
        subscriber(data);
      });
    }
  }
  parents(name) {
    return this.traversal('up', name);
  }
  childs(name) {
    return this.traversal('down', name);
  }
  traversal(direction, name) {
    const directions = {
      up: 'parents',
      down: 'find'
    };

    const _components = Array.from(
      this.$.root[directions[direction]]('[class*="js-"]')
    )
      .filter(node => Boolean(node.component))
      .map(item => item.component);

    return name
      ? _components.filter(component => (name ? component.name === name : true))
      : _components;
  }
  static mount(Constructor, options) {
    const $elements = $(options.classList.root);

    if (!Boolean($elements.length)) return {};

    const init = element => {
      const id = Math.random()
        .toString(16)
        .slice(2);

      element.selector = options.classList.root;
      element.componentId = id;

      try {
        return {
          id,
          component: new Constructor(element, options)
        };
      } catch (error) {
        return console.error(error), {};
      }
    };

    if (options.singleton) {
      const element = $elements.get(0);
      const { id, component } = init(element);

      components[id] = component;
      element.component = component;

      return component;
    } else {
      $elements.map((i, element) => {
        const { id, component } = init(element);

        components[id] = component;
        element.component = component;
      });
    }
  }
}

window.components = components;
