/* global $ */

import Component from 'js/lib/component';
import anime from 'animejs';

const classList = {
  root: '.js-button'
};

export default class Button extends Component {
  init() {
    this.split();
  }
  events() {
    this.$.root.on('mouseenter', () => {
      if (this.state.animate) return;

      const targets = this.$.root.find('.js-button-char').get();

      const offset = 15;
      const delay = 25;
      const easing = 'cubicBezier(0.5,-1, 0.5, 2)';
      const animationsOut = [];
      const animationsIn = [];

      this.state.animate = true;

      Array.from(targets).forEach((char, i) => {
        animationsOut.push(
          anime({
            targets: char,
            translateY: offset,
            easing: 'easeInCubic',
            duration: 200,
            opacity: 0,
            complete: () => {
              anime.set(char, { translateY: offset * -1 });

              animationsIn.push(
                anime({
                  targets: char,
                  translateY: 0,
                  easing,
                  delay: i * delay,
                  duration: 300,
                  opacity: 1
                }).finished
              );
            }
          }).finished
        );
      });

      Promise.all(animationsOut).then(() => {
        this.nextTick(200).then(() => {
          Promise.all(animationsIn).then(() => {
            this.nextTick(200).then(() => {
              this.state.animate = false;
            });
          });
        });
      });
    });
  }
  split() {
    const label = this.$.root.text();
    const arr = label.split('');
    const className = 'button__char js-button-char';
    const html = arr.map(
      char =>
        `<span class="${className}">${char === ' ' ? '&nbsp;' : char}</span>`
    );

    this.$.root.html(html);
  }
  static get selector() {
    return classList.root;
  }
}

Component.mount(Button, {
  name: 'Button',
  classList,
  state: { animate: false }
});
