/**
 * menu
 */

import $ from "jquery";
import Component from "js/lib/component";

const classList = {
  root: ".js-menu",
  trigger: ".js-menu-link",
  dropmenu: ".js-menu-drop",
};

export default class Menu extends Component {
  init() {
    $(".js-menu-burger").on("click", function() {
      $(this)
        .next()
        .toggleClass("is-active");
    });

    $(".js-header-second").on("click", () => {
      $(".js-header-second-body").toggleClass("is-active");
    });
  }
  events() {
    this.$.document.on("click", (e) => this.handler(e));
    this.$.window.on("keydown", (e) => this.handler(e));
    this.$.trigger.on("mouseenter", (e) => this.handler(e));
  }
  handler(e) {
    const methods = {
      click: this.click,
      mouseenter: this.mouseenter,
      keydown: this.keydown,
    };

    const type = e.type;
    const method = methods[type];

    method.call(this, e);
  }
  click(e) {
    const $target = $(e.target);

    if ($target.is(this.$.trigger)) {
      e.preventDefault();

      const $drop = $target.siblings().filter(this.$.dropmenu);
      this.state.focus = true;

      if (this.isOpen()) {
        this.state.focus = false;
        this.close($target, $drop);

        return;
      }

      this.closeAll();
      this.open($target, $drop);
    } else {
      if (!$target.closest(this.$.dropmenu).length) {
        this.state.focus = false;
        this.closeAll();
      }
    }
  }
  mouseenter(e) {
    if (!this.state.focus) return;

    const $target = $(e.target);
    const $drop = $target.siblings().filter(this.$.dropmenu);

    if (!this.isOpen()) {
      this.closeAll();
      this.open($target, $drop);
    }
  }
  keydown(e) {
    if (e.keyCode === 27) {
      this.closeAll();
    }
  }
  isOpen() {
    return this.state.open;
  }
  open($target, $drop) {
    this.state.open = open;
    this.$.root.addClass("is-open");

    $target.addClass("is-open");
    $drop.show().addClass("is-open");
  }
  close($target, $drop) {
    if ($(document).width() > 1000) {
      this.state.open = false;
      this.$.root.removeClass("is-open");
      $target.removeClass("is-open");
      $drop.hide().removeClass("is-open");
      this.closeSubmenu();
    }
  }
  closeAll() {
    if ($(document).width() > 1000) {
      this.state.focus = false;
      this.state.open = false;
      this.$.root.removeClass("is-open");
      this.$.trigger.removeClass("is-open");
      this.$.dropmenu
        .filter(".is-open")
        .hide()
        .removeClass("is-open");
      this.closeSubmenu();
    }
  }
  closeSubmenu() {
    this.childs("Dropmenu")
      ._one()
      .closeSubmenu();
  }
}

Component.mount(Menu, {
  name: "Menu",
  classList,
  state: {
    focus: false,
    open: false,
  },
});
