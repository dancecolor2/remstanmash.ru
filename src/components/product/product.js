/* global $ */

import Component from 'js/lib/component';

const classList = {
  root: '.js-product',
  image: '.js-product-image',
  code: '.js-product-code',
  title: '.js-product-title',
  cost: '.js-product-cost'
};

export default class Product extends Component {
  init() {}
  events() {}
  fill({ image, code, title, link, cost }) {
    this.$.image.attr('src', image);
    this.$.code.text(code);
    this.$.title.text(title);
    this.$.cost.text(cost);
  }
}

Component.mount(Product, {
  name: 'Product',
  classList,
  state: {}
});
