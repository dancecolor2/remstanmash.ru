/* global $ */

import Component from "js/lib/component";

const classList = {
  root: ".js-form",
  group: ".js-form-group",
  field: ".js-form-field",
};

export default class Form extends Component {
  init() {
    this.$.field.each((i, field) => {
      const $field = $(field);
      const $group = $field.closest(classList.group);
      const value = $field.val();

      if (value.length) {
        $group.addClass("is-filled");
      } else {
        $group.removeClass("is-filled");
      }
    });
  }
  events() {
    this.$.field.on("blur", (e) => {
      const $target = $(e.target);
      const $group = $target.closest(classList.group);
      const value = $target.val();

      if (value.length) {
        $group.addClass("is-filled");
      } else {
        $group.removeClass("is-filled");
      }
    });

    this.$.root.on("transitionend", (e) => {
      e.stopPropagation();
    });
  }
  insertField({ type, name, value }) {
    const $field = $(`
      <input
        type='${type}'
        value='${value}'
        name='${name}'
      >
    `);

    this.$.root.append($field);

    return $field;
  }
}

Component.mount(Form, {
  name: "Form",
  classList,
});
