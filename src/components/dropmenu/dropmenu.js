/* global $ */

import Component from 'js/lib/component';

const classList = {
  root: '.js-dropmenu',
  link: '.js-dropmenu-link',
  wrap: '.js-dropmenu-wrap',
  subitem: '.js-dropmenu-subitem'
};

export default class Dropmenu extends Component {
  init() {
    this.$.wrap.hide();
  }
  events() {
    this.$.link.on('click', e => {
      e.preventDefault();

      const $target = $(e.currentTarget);
      const $parent = $target.parent();
      const $wrap = $parent.find(this.$.wrap);
      const $items = $wrap.find(classList.subitem);
      const isOpen = $parent.is('.is-open');

      if (!isOpen) {
        this.closeSubmenu();
        $parent.addClass('is-open');
        $wrap
          .show()
          .toggleClass('is-wide', this.getItemsHeight($items) > $wrap.height())
          .css('opacity', 1);
      }
    });
  }
  closeSubmenu() {
    this.$.wrap.css('opacity', 0).hide();
    this.$.link.parent().removeClass('is-open');
  }
  getItemsHeight($items) {
    return $items
      .map((i, item) => $(item).outerHeight(true))
      .toArray()
      .reduce((acc, height) => acc + height, 0);
  }
}

Component.mount(Dropmenu, {
  name: 'Dropmenu',
  classList,
  state: {}
});
