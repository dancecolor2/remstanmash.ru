/* global $ */

import Component from "js/lib/component";
import Query from "js/lib/query";
import cookie from "cookie";

//import mockcontent from './products';

const classList = {
  root: ".js-collection",
  body: ".js-collection-body",
};

export default class Collection extends Component {
  events() {
    this.subscribe("filter:change", ({ data, params }) => {
      this.state.params = params;
      this.update(data);
      this.setQuery();
    });
    this.switchView();
    this.sorting();
  }
  update(data) {
    this.setLoading();
    $.ajax(location.pathname, { data, cache: false }).done(({ html }) => {
      this.unsetLoading();
      this.$.body.html(html);
    });
  }
  sorting() {
    $(document).on("click", ".js-collection-sorting", function() {
      // $(this)
      //   .next()
      //   .slideToggle();
      $(".js-collection-box").toggleClass("is-hidden");
    });
  }
  setLoading() {
    this.$.root.addClass("is-loading");
  }
  unsetLoading() {
    this.$.root.removeClass("is-loading");
  }

  getParams() {
    const params = Object.assign({}, this.state.params);

    // clear empty keys
    for (const key in params) {
      if (params.hasOwnProperty(key) && !params[key]) {
        delete params[key];
      }
    }

    return params;
  }
  setQuery() {
    const { origin, pathname } = location;
    const params = this.getParams();
    const query = Query.build(params);

    history.replaceState("", "", `${origin}${pathname}${query}`);
  }
  switchView() {
    let list = $(".js-collection-list");
    $(".js-collection-toggle").on("click", function() {
      $(this)
        .addClass("is-active")
        .siblings("button")
        .removeClass("is-active");
      let type = $(this).data("type");
      document.cookie = `keyboardLayout=${type}`;
      type == "tile" ? list.addClass("grid") : list.removeClass("grid");
    });
  }
}

Component.mount(Collection, {
  name: "Collection",
  classList,
  state: {
    params: {},
  },
});
