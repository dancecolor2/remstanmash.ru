export default `<div class="collection__body js-collection-body">
<div class="collection__cats">
  <div class="cats">
    <ul class="cats__list grid">
      <li class="cats__item col-4">
        <article class="cat is-small">
          <div class="cat__icon"><svg width="87" height="90" viewBox="0 0 87 90" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="35.9631" y="9.93515" width="4.15801" height="12.4203" fill="#D3D8E8"></rect>
<path d="M47.3977 22.3554H51.5557V34.7757L47.3977 30.6356V22.3554Z" fill="#D3D8E8"></path>
<rect x="2.69873" y="8.90012" width="4.15801" height="72.4515" fill="#D3D8E8"></rect>
<path d="M45.7861 69.6214L32.6758 55.8211L36.1259 54.4411L49.9261 68.9314V82.0416H45.7861V69.6214Z" fill="#D3D8E8"></path>
<path d="M64.6827 61.813C59.2121 64.2936 59.2121 72.7988 64.6827 75.2795C55.7929 73.5076 55.7929 62.8761 64.6827 61.813Z" fill="#D3D8E8"></path>
<path d="M14.7101 9.03815H2V82.0417H44.9327V68.2828L26.4679 49.8042V39.247L35.7693 29.9456V2H20.8512L14.7101 9.03815Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M35.7693 9.63156H60.941L66.8613 15.5381V22.4934H35.7693V9.63156Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M47.4858 22.4934V30.4286L51.1429 35.9763H56.8286L60.4995 30.4286V22.4934H47.4858Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M45.9399 30.2768H62.0311" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M31.3948 54.7309H64.9295C72.4644 54.7309 78.5779 60.8444 78.5779 68.3793C78.5779 75.9143 72.4644 82.0278 64.9295 82.0278H44.9329" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10"></path>
<path d="M64.8879 75.2795C68.6835 75.2795 71.7605 72.1964 71.7605 68.3932C71.7605 64.59 68.6835 61.5068 64.8879 61.5068C61.0923 61.5068 58.0154 64.59 58.0154 68.3932C58.0154 72.1964 61.0923 75.2795 64.8879 75.2795Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M25.1706 70.9048C26.5577 70.9048 27.6822 69.7803 27.6822 68.3932C27.6822 67.006 26.5577 65.8815 25.1706 65.8815C23.7834 65.8815 22.6589 67.006 22.6589 68.3932C22.6589 69.7803 23.7834 70.9048 25.1706 70.9048Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M64.8879 70.9048C66.275 70.9048 67.3995 69.7803 67.3995 68.3932C67.3995 67.006 66.275 65.8815 64.8879 65.8815C63.5007 65.8815 62.3762 67.006 62.3762 68.3932C62.3762 69.7803 63.5007 70.9048 64.8879 70.9048Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M25.1706 18.5741C26.5577 18.5741 27.6822 17.4496 27.6822 16.0625C27.6822 14.6753 26.5577 13.5508 25.1706 13.5508C23.7834 13.5508 22.6589 14.6753 22.6589 16.0625C22.6589 17.4496 23.7834 18.5741 25.1706 18.5741Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M13.5646 70.9048C14.9518 70.9048 16.0763 69.7803 16.0763 68.3932C16.0763 67.006 14.9518 65.8815 13.5646 65.8815C12.1775 65.8815 11.053 67.006 11.053 68.3932C11.053 69.7803 12.1775 70.9048 13.5646 70.9048Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M58.0017 68.3932H44.9329" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M44.9327 77.2936L2 77.2936" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M10.9705 28.2481H17.4152" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M35.7693 16.1453H43.7109" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M10.9705 32.2088H17.4152" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M10.9705 36.1557H17.4152" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M53.9858 42.3244C52.5782 42.3244 51.4465 41.1928 51.4465 39.7852V38.833H56.525V39.7852C56.525 41.1928 55.3796 42.3244 53.9858 42.3244Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
</svg>
          </div>
          <header class="cat__header">
            <h3 class="cat__title"><a class="cat__link" href="category.html" title="Станочная оснастка">Станочная оснастка</a></h3><span class="cat__arrow">
              <svg viewBox="0 0 19.4 22.8">
                <path stroke="currentColor" stroke-width="2" fill="none" d="M9,20.4v-18l9,9L9,20.4z"></path>
                <path stroke="currentColor" stroke-width="2" fill="none" d="M18,11.4H0"></path>
              </svg></span>
          </header>
        </article>
      </li>
      <li class="cats__item col-4">
        <article class="cat is-small">
          <div class="cat__icon"><svg width="83" height="87" viewBox="0 0 83 87" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M2.99993 53.283H7.31162C7.31162 53.283 7.43733 62.3952 11.6352 69.4978C15.833 76.6004 27.0975 82.3203 27.0975 82.3203C27.0975 82.3203 17.2293 81.4403 10.6295 74.1491C4.02969 66.8579 2.20679 57.3668 2.20679 57.3668L2.99993 53.283Z" fill="#D3D8E8"></path>
<path d="M48.154 53.4698H43.0627C43.0627 53.4698 42.7485 57.1468 40.4228 59.8181C38.0971 62.4895 33.0059 64.5951 33.0059 64.5951C33.0059 64.5951 39.3543 64.5951 43.0627 61.2638C46.7711 57.9325 48.154 53.4698 48.154 53.4698Z" fill="#D3D8E8"></path>
<path d="M15.0691 63.6489H16.9722V71.2224H15.0691V63.6489Z" fill="#D3D8E8"></path>
<path d="M44.5889 63.6489H46.7768V71.2224H44.5889V63.6489Z" fill="#D3D8E8"></path>
<path d="M30 48C2.00001 41.9474 1.99997 5.63158 30 2C21.5377 6.16157 14.6503 14.7867 14.5 24C14.3442 33.5466 21.2316 43.6879 30 48Z" fill="#D3D8E8"></path>
<path d="M42.6823 53.283C42.6823 59.19 37.8983 63.9811 32 63.9811C26.1017 63.9811 21.3177 59.19 21.3177 53.283H2C2 68.372 13.1062 80.8652 27.5793 83V80.5377C27.5793 78.0876 29.5656 76.1105 32 76.1105C34.4344 76.1105 36.4207 78.0997 36.4207 80.5377V83C50.8938 80.8531 62 68.372 62 53.283H42.6823Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M4.36182 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M7.99512 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M11.6284 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M15.2617 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M18.8955 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M45.1895 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M48.8228 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M52.4563 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M56.0896 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M59.7229 53.283V48.88" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M22.3108 71.2224H25.3144" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M29.9409 71.2224H32.9324" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M37.5713 71.2224H40.5628" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M48.1863 64.5299H49.8715" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M48.1863 66.5363H49.8715" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M48.1863 68.5426H49.8715" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M48.1863 70.5489H49.8715" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M13.0024 64.5299H14.6876" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M13.0024 66.5363H14.6876" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M13.0024 68.5426H14.6876" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M13.0024 70.5489H14.6876" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M3 57H22" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M42 57L62 57" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M48.1862 63.5445H44.1895V71.2224H48.1862V63.5445Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M18.6843 63.5445H14.6875V71.2224H18.6843V63.5445Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M31.9998 47.655C44.5884 47.655 54.7935 37.4348 54.7935 24.8275C54.7935 12.2202 44.5884 2 31.9998 2C19.4111 2 9.20605 12.2202 9.20605 24.8275C9.20605 37.4348 19.4111 47.655 31.9998 47.655Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M31.9999 42.403C41.6922 42.403 49.5493 34.5342 49.5493 24.8275C49.5493 15.1208 41.6922 7.25201 31.9999 7.25201C22.3076 7.25201 14.4504 15.1208 14.4504 24.8275C14.4504 34.5342 22.3076 42.403 31.9999 42.403Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M42.8884 24.8275C42.8884 18.8113 38.0196 13.9232 32.0002 13.9232C25.9809 13.9232 21.1121 18.7992 21.1121 24.8275C21.1121 28.624 23.062 31.9717 26.0051 33.9245C26.0051 33.9609 25.993 33.9973 25.993 34.0458C25.993 35.3558 27.0588 36.4232 28.3668 36.4232H35.6337C36.9417 36.4232 38.0075 35.3558 38.0075 34.0458C38.0075 34.0094 37.9954 33.9609 37.9954 33.9245C40.9506 31.9717 42.8884 28.624 42.8884 24.8275Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M32.0003 11.6467C32.711 11.6467 33.2871 11.0706 33.2871 10.36C33.2871 9.64932 32.711 9.07324 32.0003 9.07324C31.2897 9.07324 30.7136 9.64932 30.7136 10.36C30.7136 11.0706 31.2897 11.6467 32.0003 11.6467Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M42.0001 36.8121C42.7107 36.8121 43.2868 36.2361 43.2868 35.5254C43.2868 34.8148 42.7107 34.2387 42.0001 34.2387C41.2895 34.2387 40.7134 34.8148 40.7134 35.5254C40.7134 36.2361 41.2895 36.8121 42.0001 36.8121Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M22.0001 36.8121C22.7107 36.8121 23.2868 36.2361 23.2868 35.5254C23.2868 34.8148 22.7107 34.2387 22.0001 34.2387C21.2895 34.2387 20.7134 34.8148 20.7134 35.5254C20.7134 36.2361 21.2895 36.8121 22.0001 36.8121Z" stroke="#334DA6" stroke-miterlimit="10"></path>
</svg>
          </div>
          <header class="cat__header">
            <h3 class="cat__title"><a class="cat__link" href="category.html" title="Комплектующие для станков">Комплектующие для станков</a></h3><span class="cat__arrow">
              <svg viewBox="0 0 19.4 22.8">
                <path stroke="currentColor" stroke-width="2" fill="none" d="M9,20.4v-18l9,9L9,20.4z"></path>
                <path stroke="currentColor" stroke-width="2" fill="none" d="M18,11.4H0"></path>
              </svg></span>
          </header>
        </article>
      </li>
      <li class="cats__item col-4">
        <article class="cat is-small">
          <div class="cat__icon"><svg width="83" height="86" viewBox="0 0 83 86" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="3.0498" y="2.77759" width="3.33972" height="57.0134" fill="#D3D8E8"></rect>
<rect x="10.3965" y="11.0704" width="3.14867" height="26.9518" fill="#D3D8E8"></rect>
<rect x="42.2393" y="11.0704" width="3.14867" height="15.4033" fill="#D3D8E8"></rect>
<path d="M73.0396 60.05H4.42933C3.08952 60.05 2 59.0308 2 57.7604V4.30357C2 3.03312 3.08952 2 4.42933 2H73.0396C74.3794 2 75.4689 3.03312 75.4689 4.30357V57.7604C75.4689 59.0308 74.3794 60.05 73.0396 60.05Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M36.0551 10.9211H10.2158V38.5361H36.0551V10.9211Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M68.079 10.9211H42.2397V26.4737H68.079V10.9211Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M45.2134 19.3815H52.8547L54.651 14.3136L55.6669 23.8909L57.36 19.3815H64.7953" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M2 45.7539H75.4689" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M10.2158 54.7029H15.3837" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M62.7622 54.7029H67.9153" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M49.6309 54.7029H54.784" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M36.4971 54.7029H41.6502" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M2 49.5723L75.4687 49.5723" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M10.2158 34.285L36.0551 34.285" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M23.3491 54.7029H28.517" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M45.2131 38.2848C46.937 38.2848 48.3344 36.9597 48.3344 35.3251C48.3344 33.6905 46.937 32.3654 45.2131 32.3654C43.4893 32.3654 42.0918 33.6905 42.0918 35.3251C42.0918 36.9597 43.4893 38.2848 45.2131 38.2848Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M45.2134 35.3251L47.4071 33.2449" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M55.0036 38.2848C56.7275 38.2848 58.125 36.9597 58.125 35.3251C58.125 33.6905 56.7275 32.3654 55.0036 32.3654C53.2798 32.3654 51.8823 33.6905 51.8823 35.3251C51.8823 36.9597 53.2798 38.2848 55.0036 38.2848Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M55.0039 35.3251L57.2124 33.2449" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
<path d="M64.7942 38.2848C66.518 38.2848 67.9155 36.9597 67.9155 35.3251C67.9155 33.6905 66.518 32.3654 64.7942 32.3654C63.0703 32.3654 61.6729 33.6905 61.6729 35.3251C61.6729 36.9597 63.0703 38.2848 64.7942 38.2848Z" stroke="#334DA6" stroke-miterlimit="10"></path>
<path d="M64.7944 35.3251L67.0029 33.2449" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"></path>
</svg>
          </div>
          <header class="cat__header">
            <h3 class="cat__title"><a class="cat__link" href="category.html" title="Приборы для контроля точности">Приборы для контроля точности</a></h3><span class="cat__arrow">
              <svg viewBox="0 0 19.4 22.8">
                <path stroke="currentColor" stroke-width="2" fill="none" d="M9,20.4v-18l9,9L9,20.4z"></path>
                <path stroke="currentColor" stroke-width="2" fill="none" d="M18,11.4H0"></path>
              </svg></span>
          </header>
        </article>
      </li>
    </ul>
  </div>
</div>
<ul class="collection__list">
  <li class="collection__item">
    <article class="product">
      <div class="product__cover"><img class="product__image" src="/img/product-1.jpg" alt="Штревель NORGAU MAS-BT ISO40 M16 30° с отверстием"></div>
      <div class="product__body">
        <header class="product__header">
          <p class="product__code">Арт. 031030630</p>
          <h3 class="product__title"><a class="product__link" href="product.html" title="Штревель NORGAU MAS-BT ISO40 M16 30° с отверстием">Штревель NORGAU MAS-BT ISO40 M16 30° с отверстием</a></h3>
        </header>
        <div class="product__params">
          <div class="params">
            <dl class="params__list">
              <div class="params__item">
                <dt class="params__key">F, мм</dt>
                <dd class="params__value">4</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">D1</dt>
                <dd class="params__value">4</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">BT</dt>
                <dd class="params__value">40</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">L, длина</dt>
                <dd class="params__value">60</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">С, диаметр</dt>
                <dd class="params__value">19</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">D3</dt>
                <dd class="params__value">19</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">M гайки</dt>
                <dd class="params__value">M16</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">L1, длина</dt>
                <dd class="params__value">35</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">D, диаметр</dt>
                <dd class="params__value">23</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">D4</dt>
                <dd class="params__value">10</dd>
              </div>
            </dl>
          </div>
        </div>
      </div>
      <footer class="product__footer">
        <div class="product__instock">Товар в наличии</div>
        <div class="product__cost">
          <div class="cost">
            <div class="cost__caption">Цена</div>
            <div class="cost__value">733 <i>₽</i></div>
          </div>
        </div>
        <div class="product__button">
          <button class="button js-button is-small is-wide" area-label="Заказать">Заказать</button>
        </div>
      </footer>
    </article>
  </li>
  <li class="collection__item">
    <article class="product">
      <div class="product__cover"><img class="product__image" src="/img/product-2.jpg" alt="Штревель NORGAU DIN69872 ISO40 с отверстием"></div>
      <div class="product__body">
        <header class="product__header">
          <p class="product__code">Арт. 031030630</p>
          <h3 class="product__title"><a class="product__link" href="product.html" title="Штревель NORGAU DIN69872 ISO40 с отверстием">Штревель NORGAU DIN69872 ISO40 с отверстием</a></h3>
        </header>
        <div class="product__params">
          <div class="params">
            <dl class="params__list">
              <div class="params__item">
                <dt class="params__key">F, мм</dt>
                <dd class="params__value">4</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">D1</dt>
                <dd class="params__value">4</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">BT</dt>
                <dd class="params__value">40</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">L, длина</dt>
                <dd class="params__value">60</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">С, диаметр</dt>
                <dd class="params__value">19</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">D3</dt>
                <dd class="params__value">19</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">M гайки</dt>
                <dd class="params__value">M16</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">L1, длина</dt>
                <dd class="params__value">35</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">D, диаметр</dt>
                <dd class="params__value">23</dd>
              </div>
              <div class="params__item">
                <dt class="params__key">D4</dt>
                <dd class="params__value">10</dd>
              </div>
            </dl>
          </div>
        </div>
      </div>
      <footer class="product__footer">
        <div class="product__instock">Товар в наличии</div>
        <div class="product__cost">
          <div class="cost">
            <div class="cost__caption">Цена</div>
            <div class="cost__value">694 <i>₽</i></div>
          </div>
        </div>
        <div class="product__button">
          <button class="button js-button is-small is-wide" area-label="Заказать">Заказать</button>
        </div>
      </footer>
    </article>
  </li>
</ul>
</div>`;
