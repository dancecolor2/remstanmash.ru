/**
 * roll
 */

import Component from "js/lib/component";

const classList = {
  root: ".js-cats",
};

export default class Cats extends Component {
  init() {
    $(".js-cats-button").on("click", this.toggleTypes);
  }

  toggleTypes() {
    $(".js-cats-button").toggleClass("is-open");
    $(".js-cats-type").toggleClass("is-open");
  }
}

Component.mount(Cats, {
  name: "Cats",
  classList,
  state: {
    params: {},
  },
});
