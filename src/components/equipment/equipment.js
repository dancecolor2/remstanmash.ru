/* global $ */

import Component from 'js/lib/component';
import Tabs from '~/tabs/tabs';

const classList = {
  root: '.js-equipment',
  tab: '.js-equipment-item',
  label: '.js-equipment-label',
  section: '.js-equipment-section',
  caption: '.js-equipment-caption',
  button: '.js-equipment-button',
  cost: '.js-equipment-cost'
};

export default class Equipment extends Tabs {
  init() {
    this.render();
  }
  onChange($label, $tab) {
    const info = $tab.data('info');

    if (typeof info === 'object') {
      this.$.caption.text(info.caption);
      this.$.cost.text(info.cost);
      this.$.button.attr(
        'data-info',
        `{ "id": ${info.id}, "title": "${info.caption}" }`
      );
    }
  }
}

Component.mount(Equipment, {
  name: 'Equipment',
  classList,
  state: {}
});
