/**
 * product-slider
 */

import Component from 'js/lib/component';

import Swiper from 'swiper';
import $ from 'jquery';

import 'lightgallery';

const classList = {
  root: '.js-slider',
  item: '.js-slider-item',
  wrapper: '.js-slider-wrapper',
  image: '.js-slider-image',
  container: '.js-slider-container',
  thumbnails: '.js-slider-thumbnails'
};

export default class Slider extends Component {
  init() {
    this.slider();
    this.gallery();
  }
  events() {
    this._swiper.on('slideChange', () => {
      this.setCurrentItem();
    });

    this.$.document.on('click', classList.item, e => {
      const $target = $(e.target);
      const $current = $target.is(classList.item)
        ? $target
        : $target.closest(this.$.item);

      const $items = this.$.item;
      const $item = $items.filter($current);
      const index = $items.index($item);

      this._swiper.slideTo(index, 500);
    });
  }
  slider() {
    this._swiper = new Swiper(classList.container, {
      speed: 500,
      wrapperClass: classList.wrapper.substr(1),
      slideClass: classList.image.substr(1),
      on: {
        init: () => {
          this.$.root.addClass('is-inited');
        }
      }
    });

    this.roll();
    this.setCurrentItem();
  }
  roll() {
    this._roll = new Swiper(classList.thumbnails, {
      speed: 500,
      spaceBetween: 5,
      slidesPerView: 3,
      wrapperClass: classList.wrapper.substr(1),
      slideClass: classList.item.substr(1)
    });
  }
  gallery() {
    const _options = {
      selector: classList.image,
      download: false,
      counter: false,
      startClass: 'lg-fade',
      hideBarsDelay: 1000000000,
      getCaptionFromTitleOrAlt: false
    };

    this.$.root.lightGallery(_options);
  }
  setCurrentItem() {
    const index = this._swiper.realIndex;

    const $items = this.$.item.not('.is-video');
    const $item = $items.eq(index);

    $items.removeClass('is-active');
    $item.addClass('is-active');

    this._roll.slideTo(index, 500);
  }
}

Component.mount(Slider, {
  name: 'Sider',
  classList,
  state: {}
});
