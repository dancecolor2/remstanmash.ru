/**
 * overlay
 */

/* global $ */

import Component from "js/lib/component";
import isIE from "js/lib/ie";
import paper, { Path, Group, view, project } from "paper";
import * as dat from "dat.gui";

const guiEnabled = false;

const classList = {
  root: ".js-overlay",
  canvas: ".js-overlay-canvas",
};

export default class Overlay extends Component {
  init() {
    if (isIE) return;

    // paper.install(window);
    paper.setup(this.$.canvas.get(0));

    this.canvas = {
      width: this.$.canvas.width(),
      height: this.$.canvas.height(),
    };

    this.width = 96;
    this.height = 96;
    this.amountX = Math.ceil(this.canvas.width / this.width);
    this.amountY = Math.ceil(this.canvas.height / this.height);
    this.delay = 120; // ms
    this.speed = 2000; //ms
    this.maxItemsToShow = 50;
    this.prevTime = 0;
    this.counter = 0;
    this.squares = [];
    this.coord = {
      x: 0,
      y: 0,
    };

    this.drawElements();

    if (guiEnabled) {
      const gui = new dat.GUI({ closed: true });

      gui.add(this, "delay", 5, 250);
      gui.add(this, "speed", 100, 5000);
      gui.add(this, "maxItemsToShow", 10, this.squares.length);

      $(".dg").css("z-index", 200);
    }
  }
  events() {
    if (isIE) return;
    view.onFrame = this.onFrame.bind(this);
    view.onMouseMove = this.trottle(this.onMouseMove.bind(this), 0);
    this.$.window.on("resize", this.onResize.bind(this));
  }
  reinit() {
    this.canvas = {
      width: this.$.canvas.width(),
      height: this.$.canvas.height(),
    };

    view.viewSize = {
      width: this.canvas.width,
      height: this.canvas.height,
    };

    this.amountX = Math.ceil(this.canvas.width / this.width);
    this.amountY = Math.ceil(this.canvas.height / this.height);
    this.counter = 0;
    this.squares = [];
    this.coord = {
      x: 0,
      y: 0,
    };

    project.clear();

    this.drawElements();
  }
  drawElements() {
    for (var i = 0; i < this.amountX; i++) {
      this.coord.y = 0;

      for (var j = 0; j < this.amountY; j++) {
        this.addSquare(this.coord);
        this.addCross(this.coord);
        this.coord.y += this.height;
      }

      this.coord.x += this.width;
      this.addSquare(this.coord);
      this.addCross(this.coord);
    }
  }
  addSquare(coord) {
    const rectangle = new Path.Rectangle(
      coord.x,
      coord.y,
      this.width,
      this.height
    );

    rectangle.fillColor = "rgba(255, 255, 255, 0.12)";
    rectangle.opacity = 0;
    rectangle.isActive = false;
    rectangle.isSquare = true;

    this.squares.push(rectangle);
  }
  addCross(coord) {
    const size = 9;
    const rectangle = new Path.Rectangle(
      coord.x,
      coord.y - size / 2,
      1,
      size + 1
    );
    const rectangle2 = new Path.Rectangle(
      coord.x - size / 2,
      coord.y,
      size + 1,
      1
    );

    new Group({
      children: [rectangle, rectangle2],
      fillColor: "rgba(255, 255, 255, 0.3)",
    });
  }
  animate(square, speed = this.speed) {
    square.isActive = true;
    square.tween({ opacity: 0 }, { opacity: 1 }, speed / 1.5).then(() => {
      square.tween({ opacity: 1 }, { opacity: 0 }, speed).then(() => {
        square.isActive = false;
        this.counter -= 1;
      });
    });
  }
  getRandomSquare() {
    const size = this.squares.length;
    const index = Math.round(Math.random() * size);
    const square = this.squares[index];

    return square;
  }
  getClosest(point) {
    const closestMap = [
      { x: this.width * -1, y: this.height * -1 },
      { x: 0, y: this.height * -1 },
      { x: this.width, y: this.height * -1 },
      { x: this.width * -1, y: 0 },
      { x: 0, y: 0 },
      { x: this.width, y: 0 },
      { x: this.width, y: this.height * -1 },
      { x: this.width, y: 0 },
      { x: this.width, y: this.height },
    ];

    return closestMap.map((shift) => {
      const result = project.hitTest({
        x: shift.x + point.x,
        y: shift.y + point.y,
      });

      return result ? result.item : {};
    });
  }
  onFrame(event) {
    const offset = event.time - this.prevTime;
    const delayMs = this.delay / 1000;

    if (offset < delayMs) return;

    if (this.counter <= this.maxItemsToShow) {
      const square = this.getRandomSquare();

      if (square && !square.isActive) {
        this.animate(square);
        this.counter += 1;
      }
    }

    this.prevTime = event.time;
  }
  onMouseMove(event) {
    const point = event.point;
    const closest = this.getClosest(point);

    closest.forEach((item) => {
      if (item.isSquare && !item.isActive && Math.round(Math.random() - 0.4)) {
        this.animate(item, this.speed / 2);
      }
    });
  }
  onResize() {
    this.reinit();
  }
}

Component.mount(Overlay, {
  name: "Overlay",
  classList,
  state: {},
});
