/**
 * product-slider
 */

import Component from 'js/lib/component';

import 'lightgallery';

const classList = {
  root: '.js-image',
  image: '.js-image-src'
};

export default class Image extends Component {
  init() {
    this.lightbox();
  }
  lightbox() {
    const _options = {
      selector: classList.image,
      download: false,
      counter: false,
      startClass: 'lg-fade',
      getCaptionFromTitleOrAlt: false
    };

    this.$.root.lightGallery(_options);
  }
}

Component.mount(Image, {
  name: 'Image',
  classList,
  state: {}
});
