/**
 * header-search
 */

import Component from 'js/lib/component';

/* global $ */

const classList = {
  root: '.js-header-search',
  label: '.js-header-search-label',
  field: '.js-header-search-field',
  close: '.js-header-search-close'
};

export default class HeaderSearch extends Component {
  events() {
    this.$.label.on('click', () => this.open());
    this.$.close.on('click', () => this.close());
    this.$.document.on('click', e => this.close(e));
    this.subscribe('page:esc', () => this.close());
  }
  open() {
    this.$.root.addClass('is-open');
    this.$.field.focus();

    this.nextTick().then(() => {
      this.state.open = true;
    });
  }
  close(e) {
    if (e) {
      const $target = $(e.target);

      const isRoot = $target.is(classList.root);
      const isChilds = $target.closest(classList.root).length > 0;

      if (isRoot || isChilds) return;
    }

    if (this.state.open) {
      this.$.root.removeClass('is-open');
      this.$.field.val('');
      this.state.open = false;
    }
  }
}

Component.mount(HeaderSearch, {
  name: 'HeaderSearch',
  classList,
  state: {
    open: false
  }
});
