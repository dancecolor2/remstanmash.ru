import Component from "js/lib/component";

class Header extends Component {
  init() {
    this.state = {
      fixed: false,
      height: {
        default: this.$.root.outerHeight(),
      },
    };
  }
  getFixedHeight() {
    this.$.root.addClass("is-fixed");
    const height = this.$.root.outerHeight();
    this.$.root.removeClass("is-fixed");

    return height;
  }
  events() {
    this.$.window.on("scroll", () => this.handler());
    this.$.document.ready(() => this.handler());
  }
  handler() {
    const offset = window.pageYOffset;
    const state = this.state.fixed;
    const bound = this.state.height.default - this.state.height.fixed;

    if (offset > bound && !state) {
      this.fix();
    }

    if (offset < bound && state) {
      this.unfix();
    }
  }
  fix() {
    this.$.root.addClass("is-fixed");
    this.$.page.css({ paddingTop: this.state.height.default });
    this.state.fixed = true;
  }
  unfix() {
    this.$.page.removeAttr("style");
    this.$.root.removeAttr("style").removeClass("is-fixed");
    this.state.fixed = false;
  }
  shift(distance) {
    if (this.state.fixed) {
      this.$.root.css("left", distance / -2);
    }
  }
  unshift() {
    if (this.state.fixed) {
      this.$.root.css("left", "");
    }
  }

  get isFixed() {
    return this.state.fixed;
  }
}

export default Component.mount(Header, {
  name: "Header",
  classList: { root: ".js-header" },
  singleton: true,
  state: {
    fixed: false,
  },
});
