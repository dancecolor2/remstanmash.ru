/**
 * modal
 */

import Component from 'js/lib/component';
import Page from '~/page/page';

import $ from 'jquery';
import VanillaModal from 'vanilla-modal';

const focusableList = 'a, button, input, textarea, select';

const classList = {
  root: '.js-modal',
  default: '.js-modal-default',
  container: '.js-modal-container',
  inner: '.js-modal-inner',
  content: '.js-modal-content',
  product: '.js-modal-product',
  title: '.js-modal-title'
};

class Modal extends Component {
  init() {
    this.modal = new VanillaModal({
      modal: classList.container,
      modalInner: classList.inner,
      modalContent: classList.content,
      loadClass: 'modal-init',
      onBeforeOpen: () => {
        Page.bodyOverflowEnable();
        this.onTransition().then(() => {
          this.setFocus();
        });
      },
      onBeforeClose: () => {
        this.onTransition().then(() => {
          Page.bodyOverflowDisable();
          this.unsetFocus();
          this.$.root.trigger('modal:close');
        });
      }
    });

    this.$.focusable = $(focusableList);
    this.$.modal = this.$.default;
    this.state.init = true;
  }
  events() {
    this.$.container.children().on('transitionend', e => {
      e.stopPropagation();
    });
    this.$.document.on('click.modal', '[data-modal-close]', () => this.close());
    this.$.document.on('click.modal', '[data-modal]', e => this.handler(e));
  }
  handler(e) {
    e.preventDefault();

    const $target = $(e.currentTarget);
    const data = $target.data('modal');

    if (name.indexOf('/') === -1) {
      const modalId = `#modal-${data}`;
      const dataInfo = $target.attr('data-info') || '{}';
      const $title = $(modalId).find(classList.title);
      const $product = $(modalId).find(classList.product);

      try {
        const info = JSON.parse(dataInfo);

        if (typeof info === 'object') {
          if ('id' in info) {
            const Form = this.childs('Form')
              .filter(c => c.$.root.closest(modalId).length > 0)
              ._one();

            const $hiddenField = Form.insertField({
              type: 'hidden',
              name: 'item_id',
              value: info.id
            });

            this.$.root.one('modal:close', () => {
              $hiddenField.remove();
            });
          }

          if ('type' in info) {
            const Form = this.childs('Form')
                .filter(c => c.$.root.closest(modalId).length > 0)
                ._one();

            const $hiddenField = Form.insertField({
              type: 'hidden',
              name: 'type',
              value: info.type
            });

            this.$.root.one('modal:close', () => {
              $hiddenField.remove();
            });
          }

          if (
            'title' in info &&
            'code' in info &&
            'image' in info &&
            'cost' in info
          ) {
            const Product = this.childs('Product')
              .filter(c => c.$.root.closest(modalId).length > 0)
              ._one();

            this.state.hasProduct = true;

            Product.fill(info);

            $product.show();
          }

          if ('title' in info && !this.state.hasProduct) {
            $title.show();
            $title.text(info.title);
          }

          this.$.root.one('modal:close', () => {
            this.state.hasProduct = false;
            $product.hide();
            $title.hide();
          });
        }
      } catch (error) {
        console.error(error);
      }

      this.open(modalId);
    } else {
      const timeout = setTimeout(() => {
        $target.addClass('is-loading');
      }, 800);
      this.getContent(data)
        .then(() => {
          this.open('#modal');
          this.onTransition().then(() => {
            clearTimeout(timeout);
            $target.removeClass('is-loading');
          });
        })
        .fail(err => {
          clearTimeout(timeout);
          $target.removeClass('is-loading');
          throw new Error(err);
        });
    }
  }
  getContent(url) {
    return $.ajax(url).then(({ content }) => {
      this.setContent(content);
    });
  }
  setContent(content) {
    if (this.$.modal.length) {
      this.$.modal.append(content);
    }
  }
  open(id) {
    if (this.isOpen()) {
      this.close().then(() => {
        this.modal.open(id);
      });
    } else {
      this.modal.open(id);
    }
  }
  close() {
    if (!this.isOpen()) return Promise.resolve();

    return this.nextTick().then(() => {
      this.modal.close();
      return this.onTransition();
    });
  }
  onTransition() {
    return new Promise(resolve => {
      this.$.container.one('transitionend', () => {
        resolve();
      });
    });
  }
  isOpen() {
    return this.modal.isOpen;
  }
  setFocus() {
    const $focusableInModal = this.$.container.find(focusableList);

    this.$.focusable.attr('tabindex', -1);
    $focusableInModal.removeAttr('tabindex');
  }
  unsetFocus() {
    this.$.focusable.removeAttr('tabindex');
  }
  destroy() {
    if (!this.state.init) return;

    this.modal.destroy();

    this.$.document.off('click.modal');
    this.$.document.off('click.modal');

    delete this.modal;
    delete this.state;
  }
}

export default Component.mount(Modal, {
  name: 'Modal',
  classList,
  singleton: true,
  state: { init: false }
});
