/* global $ */

import Component from 'js/lib/component';
import anime from 'animejs';

const classList = {
  root: '.js-loader'
};

export default class Loader extends Component {
  init() {}
  events() {
    if (!this.$.body.is('.is-loaded')) {
      this.$.document.ready(() => {
        this.nextTick(600).then(() => {
          this.hide();
        });
      });
    }
  }
  hide() {
    const root = this.$.root.get(0);
    const page = this.$.page.get(0);
    const windowHeight = this.$.window.height();

    this.$.page.css({
      opacity: 0,
      transform: `translateY(${windowHeight}px)`
    });

    anime({
      targets: root,
      translateY: -120,
      duration: 1000,
      easing: 'easeInOutCubic',
      complete: () => {
        anime({
          targets: root,
          opacity: 0,
          translateY: windowHeight * -1,
          duration: 1000,
          delay: 100,
          easing: 'easeInOutCubic'
        });
        anime({
          targets: page,
          opacity: 1,
          translateY: 0,
          duration: 1000,
          delay: 100,
          easing: 'easeInOutCubic',
          complete: () => {
            sessionStorage.setItem('loaded', true);
            this.$.root.remove();
            this.$.body.addClass('is-loaded');
          }
        });
      }
    });
  }
}

Component.mount(Loader, {
  name: 'Loader',
  classList,
  state: {}
});
