/**
 * roll
 */

import Component from "js/lib/component";
import Swiper from "swiper";

const classList = {
  root: ".js-roll",
  swiper: ".js-roll-swiper",
  list: ".js-roll-list",
  item: ".js-roll-item",
  control: ".js-roll-control",
  prev: ".js-roll-prev",
  next: ".js-roll-next",
};

export default class Roll extends Component {
  init() {
    const defaults = {
      width: 290,
      spaceBetween: 30,
      speed: 1000,
      slidesPerGroup: 1,
      wrapperClass: classList.list.substr(1),
      slideClass: classList.item.substr(1),
      navigation: {
        prevEl: classList.prev,
        nextEl: classList.next,
      },
      on: {
        init: () => {
          this.$.root.addClass("is-inited");
          this.state.init = true;
        },
      },
    };

    const options = Object.assign(defaults, this.$.root.data("options"));

    this.swiper(options);

    if (this.$.item.length > options.slidesPerView) {
      this.showControls();
    }
  }
  showControls() {
    this.$.control.addClass("is-visible");
  }
  swiper(options) {
    this.swiper = new Swiper(classList.swiper, options);
  }
}

Component.mount(Roll, {
  name: "Roll",
  state: { init: false },
  classList,
});
