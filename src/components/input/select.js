/**
 * Select
 */

import SlimSelect from 'slim-select';
import Component from 'js/lib/component';

export default class Select extends Component {
  init() {
    this.select = new SlimSelect({
      select: this.$.root.get(0),
      placeholder: this.$.root.attr('placeholder') || '',
      showSearch: false,
      onChange: () => {
        this.$.root.trigger('change');
      }
    });
  }
}

Component.mount(Select, {
  name: 'Select',
  classList: { root: '.js-select' }
});
