/**
 * filter
 */

/* global $ */

import Component from "js/lib/component";
import Query from "js/lib/query";

const classList = {
  root: ".js-filter",
  fieldset: ".js-filter-fieldset",
  toggle: ".js-filter-toggle",
  fields: ".js-filter-fields",
};

export default class Filter extends Component {
  init() {
    this.$.fieldset
      .filter((i, fieldset) => !$(fieldset).is(".is-open"))
      .find(this.$.fields)
      .hide();

    this.$.root.addClass("is-inited");

    if ($(window).width() < 768) {
      $(".js-filter-fieldset").removeClass("is-open");
    }
  }
  events() {
    this.$.toggle.on("click", (e) => {
      const $target = $(e.currentTarget);
      const $fieldset = $target.closest(classList.fieldset);
      const $fields = $fieldset.find(classList.fields);

      $fieldset.toggleClass("is-open");
      $fields.slideToggle();
    });

    this.$.root.on("change", () => {
      this.publish("filter:change", {
        data: this.$.root.serializeArray(),
        params: this.getParams(),
      });
    });

    this.$.root.on("reset", () => {
      this.reset();
      this.publish("filter:change", {
        data: {},
        params: {},
      });
    });
  }
  reset() {
    const $checkbox = this.$.root.find('input[type="checkbox"]');
    $checkbox.prop("checked", false);
  }
  getParams() {
    const params = Query.merge({}, this.$.root.serialize());

    // remove empty keys
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const param = params[key];

        if (param.length < 1) {
          delete params[key];
        }
      }
    }

    return params;
  }
}

Component.mount(Filter, {
  name: "Filter",
  classList,
  state: {
    params: {},
  },
});
