/* global $ */

import "jquery.easing";

import Component from "js/lib/component";

const classList = {
  item: ".js-params-item",
  list: ".js-params-list",
  root: ".js-params",
  toggle: ".js-params-toggle",
};

export default class Params extends Component {
  init() {
    this.height = {
      default: this.$.list.outerHeight(),
      collapsed: this.getHeight(),
    };

    this.setHeight(this.height.collapsed);
    this.state.open = false;
    this.$.root.addClass("is-inited");
  }
  events() {
    this.$.toggle.on("click", () => {
      if (this.state.open) {
        this.close();
      } else {
        this.open();
      }
    });
  }
  close() {
    this.$.root.removeClass("is-open");
    this.$.list.animate(
      { height: this.height.collapsed },
      450,
      "easeOutCubic",
      () => {
        this.state.open = false;
      }
    );
  }
  open() {
    this.$.root.addClass("is-open");
    this.$.list.animate(
      { height: this.height.default },
      450,
      "easeOutCubic",
      () => {
        this.state.open = true;
      }
    );
  }
  getHeight() {
    const $filtred = this.$.item.filter((index) => index < 10);
    const itemsArray = $filtred.toArray();
    const height = itemsArray.reduce(
      (acc, item) => acc + $(item).outerHeight(),
      0
    );

    return height;
  }
  setHeight(height) {
    this.$.list.height(height);
  }
}

Component.mount(Params, {
  name: "Params",
  classList,
  state: {
    open: true,
  },
});
