/**
 * clients-roll
 */

import Component from 'js/lib/component';
import Swiper from 'swiper';

const classList = {
  root: '.js-clients-roll',
  swiper: '.js-clients-roll-swiper',
  list: '.js-clients-roll-list',
  item: '.js-clients-roll-item',
  dots: '.js-clients-roll-dots'
};

export default class ClientsRoll extends Component {
  init() {
    const defaults = {
      slidesPerView: 2,
      slidesPerColumn: 4,
      slidesPerGroup: 2,
      spaceBetween: 0,
      speed: 1000,
      wrapperClass: classList.list.substr(1),
      slideClass: classList.item.substr(1),
      pagination: {
        el: classList.dots,
        clickable: true,
        bulletActiveClass: 'is-active',
        currentClass: 'is-current',
        bulletClass: 'clients-roll__dot',
        modifierClass: 'clients-roll__'
      },
      on: {
        init: () => {
          this.$.root.addClass('is-inited');
          this.state.init = true;
        }
      }
    };

    const options = Object.assign(defaults, this.$.root.data('options'));

    this.swiper(options);

    // if (this.$.item.length > options.slidesPerView) {
    //   this.showControls();
    // }
  }
  showControls() {
    this.$.control.addClass('is-visible');
  }
  swiper(options) {
    this.swiper = new Swiper(classList.swiper, options);
  }
}

Component.mount(ClientsRoll, {
  name: 'ClientsRoll',
  state: { init: false },
  classList
});
