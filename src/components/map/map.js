/**
 * Map
 */

import $ from 'jquery';
import Component from 'js/lib/component';

/* global ymaps */

const classList = {
  root: '.js-map'
};

export default class Map extends Component {
  init() {
    this.param = {};
    this.collections = [];

    $.getScript('//api-maps.yandex.ru/2.1/?lang=ru_RU', () => {
      ymaps.ready(() => {
        this.setParams();
        this.initMap();
      });
    });
  }
  update(data) {
    this.setParams(data);
    this.removeAllMarkers();
    this.generateMarkers();
    this.setCenter(this.param.center);
    this.setZoom(this.param.zoom);
  }
  setParams(data) {
    const param = data || this.$.root.data('options');

    this.markers = param.markers || [];
    this.param.center = param.center || [54.765961, 32.033613];
    this.param.zoom = param.zoom || 15;
    this.param.container = param.container || '.js-map-container';
  }
  initMap() {
    this.$.container = $(`${this.param.container}`);
    this.$.container.html('');

    this.map = new ymaps.Map(
      this.$.container.get(0),
      {
        center: this.param.center,
        zoom: this.param.zoom,
        controls: []
      },
      {
        searchControlProvider: false
      }
    );

    const zoomControl = new ymaps.control.ZoomControl({
      options: {
        size: 'small',
        position: {
          left: 'auto',
          right: 20,
          top: 250
        }
      }
    });

    this.map.controls.add(zoomControl);
    this.map.behaviors.disable('scrollZoom');
    this.generateMarkers();
    this.state.init = true;
  }
  generateMarkers() {
    this.markers.forEach(marker => {
      this.setMarker(marker);
    });
  }
  setMarker({ title, coords, image, address }) {
    const marker = new ymaps.Placemark(
      coords,
      {
        hintContent: address,
        balloonContent: title
      },
      {
        iconImageHref: image.href || '/img/marker.svg',
        iconLayout: 'default#image',
        iconImageSize: image.size || [56, 97],
        iconImageOffset: image.offset || [-28, -94]
      }
    );

    this.map.geoObjects.add(marker);
    this.collections.push(marker);
  }
}

Component.mount(Map, {
  name: 'Map',
  classList,
  state: { init: false }
});
