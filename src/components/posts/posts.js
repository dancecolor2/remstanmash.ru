/**
 * roll
 */

import Component from "js/lib/component";
import Swiper from "swiper";

const classList = {
  root: ".js-posts",
  swiper: ".js-posts-swiper",
  list: ".js-posts-list",
  item: ".js-posts-item",
};

export default class Posts extends Component {
  init() {
    const defaults = {
      width: 370,
      spaceBetween: 30,
      slidesPerGroup: 1,
      speed: 1000,
      wrapperClass: classList.list.substr(1),
      slideClass: classList.item.substr(1),
      on: {
        init: () => {
          this.$.root.addClass("is-inited");
          this.state.init = true;
        },
      },
    };

    const options = Object.assign(defaults, this.$.root.data("options"));

    this.swiper(options);
  }
  swiper(options) {
    this.swiper = new Swiper(classList.swiper, options);
  }
}

Component.mount(Posts, {
  name: "Posts",
  state: { init: false },
  classList,
});
