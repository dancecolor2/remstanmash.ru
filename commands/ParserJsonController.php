<?php

namespace app\commands;

use app\modules\category\models\Category;
use app\modules\eav\models\EavAttribute;
use app\modules\eav\models\EavAttributeValue;
use app\modules\product\models\Product;
use app\modules\product\models\ProductFile;
use app\modules\product\models\ProductImage;
use app\modules\vendor\models\Vendor;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Yii;
use yii\console\Controller;
use yii\helpers\Json;

class ParserJsonController extends Controller
{
    public function actionIndex()
    {
    	$this->stdout('>');
	    $items = Json::decode(file_get_contents(Yii::getAlias('@app/mirisoru_products.json')));
        $vendor = Vendor::findOne(['title' => 'FANUC']);
        $categories = Category::find()->indexBy('id')->all();

        if (empty($items)) {
            throw new FileNotFoundException('Файл не найден или пуст');
        }

	    foreach ($items as $item) {
		    $categoryId = (int) $item['category_id'];

            if (!isset($categories[$categoryId])) {
            	$this->stdout('C');
                continue;
            }

            if (!$this->createProduct($item, $categoryId, $vendor)) {
	            $this->stdout('S');
                continue;
            }

            $this->stdout('.');
        }

	    $this->stdout(PHP_EOL . 'Finish');
    }

    private function createProduct(array $item, $categoryId, Vendor $vendor)
    {
        $product = new Product(['category_id' => $categoryId]);
        $product->title = $item['title'];
        $product->code = $item['article'];
        $product->active = 0;
        $product->price = null;
        $product->vendor_id = $vendor->id;
        $product->description = $item['description'] . '<p><strong>Ремстанмаш</strong> официальный дилер компании <strong>FANUC</strong></p>';

        if (!$product->save()) {
            return false;
        }

	    if (!empty($item['images'])) {
		    foreach ($item['images'] as $image) {
			    $imageProduct = new ProductImage(['product_id' => $product->id, 'image_hash' => uniqid()]);
			    $imageProduct->save();
			    $name = $imageProduct->id . '_' . $imageProduct->image_hash . '.' . substr($image, strripos($image, '.') + 1);
			    $path = Yii::getAlias('@app/web/uploads/product_image/' . $name);
			    try {
				    copy($image, $path);
			    } catch (\Exception $e) {
				    $imageProduct->delete();
				    return true;
			    }
			    $imageProduct->updateAttributes(['image' => $name]);
		    }
	    }

//        if (!empty($item['characteristics'])) {
//            foreach ($item['characteristics'] as $attribute) {
//                $eavAttribute = EavAttribute::findOne(['title' => $attribute[0]]);
//
//                if (empty($eavAttribute)) {
//                    $eavAttribute = new EavAttribute(['title' => $attribute[0], 'type_id' => 1, 'name' => $this->translit($attribute[0])]);
//                    if (!$eavAttribute->save()) {
//	                    $this->stdout("[EAV1]");
//                        continue;
//                    }
//                }
//
//                $val = EavAttributeValue::findOne(['attribute_id' => $eavAttribute->id, 'entity_id' => $product->id]);
//
//                if (empty($val)) {
//                    $val = new EavAttributeValue(['attribute_id' => $eavAttribute->id, 'entity_id' => $product->id]);
//                    if (!$val->save()) {
//                        $this->stdout("[EAV2]");
//                        continue;
//                    }
//                }
//
//                $val->updateAttributes(['value' => $attribute[1]]);
//            }
//        }

//        if (!empty($item['pdf'])) {
//            $attachmentProduct = new ProductFile([
//                'product_id' => $product->id,
//                'file_hash' => uniqid(),
//                'title' => 'Технический чертеж',
//            ]);
//            $attachmentProduct->save();
//            $name = $attachmentProduct->id . '_' . $attachmentProduct->file_hash . '.' . substr($item['pdf'], strripos($item['pdf'], '.') + 1);
//            try {
//	            copy($item['pdf'], Yii::getAlias('@app/web/uploads/product_files/' . $name));
//            } catch (\Exception $e) {
//	            return true;
//            }
//            $attachmentProduct->updateAttributes(['file' => $name]);
//        }

        return true;
    }

    private function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "_", $s);
        $s = str_replace("-", "_", $s);

        return $s; // возвращаем результат
    }
}
