<?php

namespace app\commands;

use app\helpers\DateHelper;
use app\modules\category\models\Category;
use app\modules\machine\models\Machine;
use app\modules\news\models\News;
use app\modules\page\models\Page;
use app\modules\product\models\Product;
use app\modules\vendor\models\Vendor;
use yii\console\Controller;

class SitemapController extends Controller
{
    public function actionIndex()
    {
        $xml = $this->generate();
        file_put_contents(dirname(__DIR__) . '/web/sitemap.xml', $xml);
    }

    private function generate(): string
    {
        ob_start();

        $writer = new \XMLWriter();
        $writer->openURI('php://output');

        $writer->startDocument('1.0', 'UTF-8');
            $writer->startElement('urlset');
				$writer->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

			    foreach (Category::find()->andWhere(['>=', 'depth', 1])->each() as $category) {
				    $writer->startElement('url');
				        $writer->writeElement('loc', $this->getSiteName() . $category->getHref());
				        $writer->writeElement('lastmod', DateHelper::forSitemap($category->updated_at));
				        $writer->writeElement('changefreq', 'monthly');
				        $writer->writeElement('priority', '0.9');
				    $writer->endElement();
				}

			    foreach (Product::find()->each() as $product) {
				    $writer->startElement('url');
				        $writer->writeElement('loc', $this->getSiteName() . $product->getHref());
				        $writer->writeElement('lastmod', DateHelper::forSitemap($product->updated_at));
				        $writer->writeElement('changefreq', 'weekly');
				        $writer->writeElement('priority', '1');
				    $writer->endElement();
				}

			    foreach (Vendor::find()->each() as $vendor) {
				    $writer->startElement('url');
				        $writer->writeElement('loc', $this->getSiteName() . $vendor->getHref());
				        $writer->writeElement('changefreq', 'monthly');
				        $writer->writeElement('priority', '0.8');
				    $writer->endElement();
				}

			    foreach (Machine::find()->each() as $machine) {
				    $writer->startElement('url');
				        $writer->writeElement('loc', $this->getSiteName() . $machine->getHref());
				        $writer->writeElement('changefreq', 'monthly');
				        $writer->writeElement('priority', '0.9');
				    $writer->endElement();
				}

			    foreach (News::find()->each() as $news) {
				    $writer->startElement('url');
				        $writer->writeElement('loc', $this->getSiteName() . $news->getHref());
				        $writer->writeElement('lastmod', DateHelper::forSitemap($news->updated_at));
				        $writer->writeElement('changefreq', 'yearly');
				        $writer->writeElement('priority', '0.7');
				    $writer->endElement();
				}

			    foreach (Page::find()->andWhere(['!=', 'id', 'root'])->each() as $page) {
				    $writer->startElement('url');
				        $writer->writeElement('loc', $this->getSiteName() . $page->getHref());
				        $writer->writeElement('lastmod', DateHelper::forSitemap($page->updated_at));
				        $writer->writeElement('changefreq', 'monthly');
				        $writer->writeElement('priority', '0.7');
				    $writer->endElement();
				}

            $writer->endElement();
        $writer->endDocument();

        return ob_get_clean();
    }

    private function getSiteName()
    {
        return 'https://cnchelp.ru';
    }
}