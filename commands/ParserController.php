<?php

namespace app\commands;

use app\modules\category\models\Category;
use app\modules\eav\models\EavAttribute;
use app\modules\eav\models\EavAttributeValue;
use app\modules\product\models\Product;
use app\modules\product\models\ProductFile;
use app\modules\product\models\ProductImage;
use app\modules\vendor\models\Vendor;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Yii;
use yii\console\Controller;

require __DIR__ . '/../helpers/phpQuery-onefile.php';

class ParserController extends Controller
{
    public function actionDropFiles($id)
    {
        $startTime = microtime(true);
        $query = ProductFile::find()->innerJoinWith(['product'])->andWhere(['product.vendor_id' => $id]);

        /** @var ProductFile $file */
        foreach ($query->each() as $file) {
            $this->stdout($file->id . PHP_EOL);
            $file->delete();
        }

        $finishTime = microtime(true);
        $this->stdout($finishTime - $startTime);
    }

    public function actionIndex()
    {
        $startTime = microtime(true);
        $row = 1;
        $result = [];
        $client = new \yii\httpclient\Client();
        $handle = fopen(Yii::getAlias('@app/urls.txt'), "r");
//        $category = Category::findOne($id);

//        if (empty($category)) {
//            throw new \DomainException("No category with id $id");
//        }

        $vendor = Vendor::findOne(['title' => 'Autonics']);
        Yii::info('Start parsing ' . Yii::getAlias('@app/urls.txt'), 'parser');

        if (!$handle) {
            throw new FileNotFoundException(Yii::getAlias('@app/urls.txt') . ' не найден');
        }

        while (($line = fgets($handle)) !== false) {
            $line = trim($line);
            [$categoryId, $url] = explode('@', $line);

            if (!Category::find()->where(['id' => $categoryId])->exists()) {
                $this->stdout("Category $categoryId does not exists" . PHP_EOL);
                $row++;
                continue;
            }

            $this->stdout($row . ' - ' . $url . PHP_EOL);
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setUrl($url)
                ->setFormat(\yii\httpclient\Client::FORMAT_URLENCODED)
                ->setHeaders(['user-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'])
                ->send();

            if (!$response->isOk) {
                Yii::info('Error parsing url (' . $row . ')', 'parser');
                $row++;
                continue;
            }

            $html = $response->getContent();
            $dom = \phpQuery::newDocument($html);
            $result['title'] = $dom->find('.product_title')->text();

            if (Product::find()->where(['title' => $result['title'], 'category_id' => $categoryId])->exists()) {
                $this->stdout("--== Skip ==--" . PHP_EOL);
                $row++;
                continue;
            }

            $result['code'] = $dom->find('.sku')->text();
            $result['description'] = $dom->find('.woocommerce-product-details__short-description')->text();
            $result['features_description'] = $dom->find('#tab-description .wc-tab-inner div')->html();
            $result['image'] = $dom->find('.wp-post-image')->attr('src');
//            $result['attachments'] = [];
            $result['eav'] = [];

            foreach ($dom->find('.entry-summary .shop_attributes tr') as $el) {
                $result['eav'][] = [
                    'attribute' => pq($el)->find('th')->text(),
                    'value' => pq($el)->find('td p a')->text(),
                ];
            }

//            foreach ($dom->find('#link-prodct-dwnl li a') as $attachment) {
//                $result['attachments'][] = [
//                    'href' => 'https://autonics.su' . pq($attachment)->attr('href'),
//                    'title' => pq($attachment)->find('span')->text(),
//                ];
//            }

            if (!$this->createProduct($result, $categoryId, $vendor)) {
                Yii::info('Product save failed (' . $row . ')', 'parser');
                $row++;
                continue;
            }

            $this->stdout("Add" . PHP_EOL);
            $row++;
        }

        fclose($handle);
        $finishTime = microtime(true);

        Yii::info('Finish parsing file', 'parser');

        $this->stdout($finishTime - $startTime);
    }

    private function createProduct(array $result, $categoryId, Vendor $vendor)
    {
        $product = new Product(['category_id' => $categoryId]);
        $product->title = $result['title'];
        $product->code = $result['code'];
        $product->price = null;
        $product->vendor_id = $vendor->id;
        $product->description = $result['description'] . '<p><strong>Ремстанмаш </strong>официальный партнёр <strong>Autonics</strong></p>';
        $product->features_description = $result['features_description'];

        if (!$product->save()) {
            return false;
        }

        if (!empty($result['image'])) {
            $imageProduct = new ProductImage(['product_id' => $product->id, 'image_hash' => uniqid()]);
            $imageProduct->save();
            $name = $imageProduct->id . '_' . $imageProduct->image_hash . '.' . substr($result['image'], strripos($result['image'], '.') + 1);
            copy($result['image'], Yii::getAlias('@app/web/uploads/product_image/' . $name));
            $imageProduct->updateAttributes(['image' => $name]);
        }

        if (!empty($result['eav'])) {
            foreach ($result['eav'] as $attribute) {
                $eavAttribute = EavAttribute::findOne(['title' => $attribute['attribute']]);

                if (empty($eavAttribute)) {
                    $eavAttribute = new EavAttribute(['title' => $attribute['attribute'], 'type_id' => 1, 'name' => $this->translit($attribute['attribute'])]);
                    if (!$eavAttribute->save()) {
                        $this->stdout("Error EavAttribute {$attribute['attribute']} save " . PHP_EOL);
                        continue;
                    }
                }

                $val = EavAttributeValue::findOne(['attribute_id' => $eavAttribute->id, 'entity_id' => $product->id]);

                if (empty($val)) {
                    $val = new EavAttributeValue(['attribute_id' => $eavAttribute->id, 'entity_id' => $product->id]);
                    if (!$val->save()) {
                        $this->stdout("Error EavAttributeValue {$attribute['attribute']} {$attribute['value']} save " . PHP_EOL);
                        continue;
                    }
                }

                $val->updateAttributes(['value' => $attribute['value']]);
            }
        }

//        if (!empty($result['attachments'])) {
//            foreach ($result['attachments'] as $attachment) {
//                $attachmentProduct = new ProductFile([
//                    'product_id' => $product->id,
//                    'file_hash' => uniqid(),
//                    'title' => $attachment['title'],
//                ]);
//                $attachmentProduct->save();
//                $name = $attachmentProduct->id . '_' . $attachmentProduct->file_hash . '.' . substr($attachment['href'], strripos($attachment['href'], '.') + 1);
//                copy($attachment['href'], Yii::getAlias('@app/web/uploads/product_files/' . $name));
//                $attachmentProduct->updateAttributes(['file' => $name]);
//            }
//        }

        return true;
    }

    private function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "_", $s);
        $s = str_replace("-", "_", $s);

        return $s; // возвращаем результат
    }
}
