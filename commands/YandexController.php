<?php

namespace app\commands;

use app\modules\category\models\Category;
use app\modules\product\models\Product;
use app\modules\product\repositories\ProductRepository;
use yii\console\Controller;
use yii\helpers\Html;

class YandexController extends Controller
{
    private $products;

    public function __construct($id, $module, ProductRepository $products, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->products = $products;
    }

    public function actionIndex()
	{
		$urlParams = [
			'utm_source' => 'YandexMarket',
			'utm_medium' => 'cpc',
		];
		$xml = $this->generate($urlParams);
		file_put_contents(dirname(__DIR__) . '/web/yandex.xml', $xml);
	}

	/**
	 * @param array $urlParams
	 * @return string
	 */
	private function generate($urlParams)
	{
		ob_start();

		$writer = new \XMLWriter();
		$writer->openURI('php://output');

		$writer->startDocument('1.0', 'UTF-8');
		$writer->startDTD('yml_catalog SYSTEM "shops.dtd"');
		$writer->endDTD();

		$writer->startElement('yml_catalog');
		$writer->writeAttribute('date', date('Y-m-d H:i'));

		$writer->startElement('shop');
			$writer->writeElement('name', 'РЕМСТАНМАШ');
			$writer->writeElement('company', 'ООО «РЕМСТАНМАШ»');
			$writer->writeElement('url', $this->getSiteName());

			$writer->startElement('currencies');
				$writer->startElement('currency');
					$writer->writeAttribute('id', 'RUR');
					$writer->writeAttribute('rate', 1);
				$writer->endElement();
			$writer->endElement();

			$writer->startElement('categories');
				/** @var Category $category */
				foreach (Category::find()->where(['>', 'depth', 0])->orderBy(['lft' => SORT_ASC])->each() as $category) {
					$writer->startElement('category');
						$writer->writeAttribute('id', $category->id);

						if ($category->parents(1)->andWhere(['>', 'depth', 0])->exists()) {
							$writer->writeAttribute('parentId', $category->parents(1)->andWhere(['>', 'depth', 0])->select(['id'])->scalar());
						}

						$writer->writeRaw(Html::encode($category->getTitle()));
					$writer->endElement();
				}
			$writer->endElement();

		$writer->startElement('offers');
			/** @var Product $product */
			foreach ($this->products->getIteratorForYandexMarket() as $product) {
				$pictures = $product->getImages()->limit(10)->all();

				if (empty($pictures)) {
					continue;
				}

				$urlParams['utm_content'] = $product->id;
				$writer->startElement('offer');
					$writer->writeAttribute('id', $product->id);
					$writer->writeAttribute('available', $product->in_stock ? 'true' : 'false');
					$writer->writeElement('url', $this->getSiteName() . $product->getHref() . '?' . http_build_query($urlParams));
					$writer->writeElement('price', $product->getPrice());
					$writer->writeElement('currencyId', 'RUR');
					$writer->writeElement('categoryId', $product->category_id);

					foreach ($pictures as $picture) {
						try {
							$writer->writeElement('picture', $this->getSiteName() . $picture->getThumbFileUrl('image'));
						} catch (\Exception|\Throwable $e) {}
					}

					$writer->writeElement('name', $product->getTitle());

					if ($vendor = $product->vendor) {
						$writer->writeElement('vendor',  $vendor->getTitle());
					}

					$writer->writeElement('vendorCode', $product->code);
					$writer->writeElement('description', strip_tags($product->description));

					$options = $product->eavAttributes;
					if (!empty($options)) {
						foreach ($options as $option) {
							$writer->startElement('param');
							$writer->writeAttribute('name', $option->getLabel());
							if ($option->unit) {
								$writer->writeAttribute('unit', $option->unit);
							}
							$writer->text($product->{$option->name}->getRealValue());
							$writer->endElement();
						}
					}

				$writer->endElement();
			}
		$writer->endElement();
		$writer->fullEndElement();
		$writer->fullEndElement();
		$writer->endDocument();

		return ob_get_clean();
	}

	private function getSiteName()
	{
		return 'https://cnchelp.ru';
	}
}