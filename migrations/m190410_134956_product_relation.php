<?php

use yii\db\Migration;

/**
 * Class m190410_134956_product_relation
 */
class m190410_134956_product_relation extends Migration
{
    public function safeUp()
    {
        $this->createTable('product_relation', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'relation_id' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('product_relation');
    }
}
