<?php

use yii\db\Migration;

class m150701_072555_category extends Migration
{
    public function up()
    {
        $tableOptions = $this->db->driverName === 'mysql'
            ? 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB'
            : null;

        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'lft' => $this->integer(11)->notNull(),
            'rgt' => $this->integer(11)->notNull(),
            'depth' => $this->integer(11)->notNull(),
            'title' => $this->string(255)->notNull(),
            'alias' => $this->string(255)->notNull(),
            'icon' => $this->string(255),
            'icon_hash' => $this->string(255),
            'content' => 'mediumtext',
            'meta_t' => $this->string(255),
            'meta_d' => $this->string(255),
            'meta_k' => $this->string(255),
            'h1' => $this->string(255),
        ], $tableOptions);
        $this->insert('category', [
            'id' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'lft' => 1,
            'rgt' => 2,
            'depth' => 0,
            'title' => '',
            'alias' => '',
        ]);
    }

    public function down()
    {
        $this->dropTable('category');
    }
}
