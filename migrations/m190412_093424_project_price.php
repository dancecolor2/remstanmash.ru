<?php

use yii\db\Migration;

/**
 * Class m190412_093424_project_price
 */
class m190412_093424_project_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'price', $this->integer(11)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190412_093424_project_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190412_093424_project_price cannot be reverted.\n";

        return false;
    }
    */
}
