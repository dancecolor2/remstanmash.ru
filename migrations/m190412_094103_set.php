<?php

use yii\db\Migration;

/**
 * Class m190412_094103_set
 */
class m190412_094103_set extends Migration
{
    public function safeUp()
    {
        $this->createTable('set', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'machine_id' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(1),
            'title' => $this->string(255)->notNull(),
            'position' => $this->integer()->notNull(),
            'cost' => $this->integer(),
        ]);

        $this->createTable('set_item', [
            'id' => $this->primaryKey(),
            'set_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'position' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-set_item-set',
            'set_item',
            'set_id',
            'set',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-set_item-product',
            'set_item',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-set-machine',
            'set',
            'machine_id',
            'machine',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-set-machine', 'set');
        $this->dropForeignKey('fk-set_item-product', 'set_item');
        $this->dropForeignKey('fk-set_item-set', 'set_item');
        $this->dropTable('set_item');
        $this->dropTable('set');
    }
}
