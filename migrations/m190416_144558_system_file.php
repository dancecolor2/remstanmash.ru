<?php

use yii\db\Migration;

/**
 * Class m190416_144558_system_file
 */
class m190416_144558_system_file extends Migration
{
    public function safeUp()
    {
        $this->createTable('system_file', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'file' => $this->string(500),
            'position' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(1),
            'system_id' => $this->integer()->notNull(),
            'file_hash' => $this->string(255),
        ]);

        $this->addForeignKey(
            'fk-system_file-system',
            'system_file',
            'system_id',
            'system',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-system_file-system', 'system_file');
        $this->dropTable('system_file');
    }
}
