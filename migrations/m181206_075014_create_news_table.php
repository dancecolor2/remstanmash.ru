<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m181206_075014_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer()->notNull()->comment('Дата редактирования'),
            'title' => $this->string(255)->notNull()->comment('Заголовок'),
            'alias' => $this->string(255)->notNull()->comment('Алиас'),
            'image' => $this->string(500)->comment('Картинка для анонса'),
            'description' => $this->text()->notNull()->comment('Описание для анонса'),
            'content' => $this->text()->notNull()->comment('Контент'),
            'active' => $this->integer()->defaultValue(1)->comment('Активность'),
            'h1' => $this->string(255),
            'meta_t' => $this->string(255)->comment('Заголовок страницы'),
            'meta_d' => $this->string(255)->comment('Описание страницы'),
            'meta_k' => $this->string(255)->comment('Ключевые слова'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('news');
    }
}
