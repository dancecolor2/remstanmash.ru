<?php

use yii\db\Migration;

/**
 * Class m190422_062921_product_position
 */
class m190422_062921_product_position extends Migration
{
    public function safeUp()
    {
        $products = \app\modules\product\models\Product::find()->orderBy([
            'category_id' => SORT_ASC,
            'id' => SORT_ASC,
        ])->all();

        $position = 1;
        $category_id = null;
        foreach ($products as $product) {
            if ($product->category_id !== $category_id) {
                $category_id = $product->category_id;
                $position = 1;
            }
            $product->updateAttributes(['position' => $position++]);
        }
    }

    public function safeDown()
    {
        echo "m190422_062921_product_position cannot be reverted.\n";

        return false;
    }
}
