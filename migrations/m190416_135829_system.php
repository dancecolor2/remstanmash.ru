<?php

use yii\db\Migration;

class m190416_135829_system extends Migration
{
    public function safeUp()
    {
        $this->createTable('system', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer()->notNull()->comment('Дата редактирования'),
            'active' => $this->boolean()->defaultValue(1),
            'title' => $this->string(255)->notNull(),
            'image' => $this->string(500)->comment('Картинка'),
            'image_hash' => $this->string(255),
            'content' => $this->text()->comment('Текст'),
            'subtitle' => $this->string(255)->comment('Заголовок списка'),
            'list' => $this->text()->comment('Список'),
            'position' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('system');
    }
}
