<?php

use app\modules\eav\models\EavAttributeValue;
use app\modules\product\models\Product;
use yii\db\Migration;

/**
 * Class m200428_192904_funac_yap
 */
class m200428_192904_funac_yap extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$products = Product::find()->andWhere(['vendor_id' => 1])->all();

    	if (empty($products)) {
    		return true;
	    }

	    foreach ($products as $product) {
		    $eavv = new EavAttributeValue();
		    $eavv->entity_id = $product->id;
		    $eavv->attribute_id = 43;
		    $eavv->value = 'Япония';

	    	try {
			    $eavv->save();
		    } catch (Exception|Throwable $e) {}
		}
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200428_192904_funac_yap cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200428_192904_funac_yap cannot be reverted.\n";

        return false;
    }
    */
}
