<?php

use yii\db\Migration;

class m190418_131144_projects_before_after_images extends Migration
{
    public function safeUp()
    {
        $this->addColumn('projects', 'image_hash', $this->string(255));
        $this->addColumn('projects', 'image_before', $this->string(500)->comment('Фото "До"'));
        $this->addColumn('projects', 'image_before_hash', $this->string(255));
        $this->addColumn('projects', 'image_after', $this->string(500)->comment('Фото "После"'));
        $this->addColumn('projects', 'image_after_hash', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('projects', 'image_after_hash');
        $this->dropColumn('projects', 'image_after');
        $this->dropColumn('projects', 'image_before_hash');
        $this->dropColumn('projects', 'image_before');
        $this->dropColumn('projects', 'image_hash');
    }
}
