<?php

use yii\db\Migration;

/**
 * Class m190412_081943_mashine
 */
class m190412_081943_machine extends Migration
{
    public function safeUp()
    {
        $this->createTable('machine', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'position' => $this->integer(),
            'active' => $this->boolean()->defaultValue(1),
            'code' => $this->string(255),
            'title' => $this->string(255)->notNull(),
            'preview_text' => $this->text(),
            'detail_text' => $this->text(),
            'description' => $this->text(),
            'meta_d' => $this->string(255),
            'meta_k' => $this->string(255),
            'meta_t' => $this->string(255),
            'h1' => $this->string(255),
            'type_id' => $this->integer(),
        ]);

        $this->createTable('machine_type', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull()
        ]);

        $this->createTable('machine_image', [
            'id' => $this->primaryKey(),
            'machine_id' => $this->integer()->notNull(),
            'position' => $this->integer(),
            'image' => $this->string(500),
            'image_hash' => $this->string(255),
        ]);

        $this->addForeignKey(
            'fk-machine-machine_type',
            'machine',
            'type_id',
            'machine_type',
            'id'
        );

        $this->addForeignKey(
            'fk-machine_image-machine',
            'machine_image',
            'machine_id',
            'machine',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-machine_image-machine', 'machine_image');
        $this->dropForeignKey('fk-machine-machine_type', 'machine');

        $this->dropTable('machine_image');
        $this->dropTable('machine_type');
        $this->dropTable('machine');
    }
}
