<?php

use yii\db\Migration;

/**
 * Class m190412_095316_project_type_id
 */
class m190412_095316_project_type_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'type_id', $this->integer(11)->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190412_095316_project_type_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190412_095316_project_type_id cannot be reverted.\n";

        return false;
    }
    */
}
