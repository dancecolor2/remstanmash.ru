<?php

use app\traits\MigrateTrait;
use yii\db\Migration;

/**
 * Class m190417_074009_repair
 */
class m190417_074009_repair extends Migration
{
    use MigrateTrait;

    public function safeUp()
    {
        $this->createTable('repair', array_merge(
            $this->defaultColumns(),
            $this->columns(['icon']),
            [
                'text' => $this->text()->comment('Текст'),
            ]
        ));
    }

    public function safeDown()
    {
        $this->dropTable('repair');
    }
}
