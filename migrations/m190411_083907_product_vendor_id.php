<?php

use yii\db\Migration;

class m190411_083907_product_vendor_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'vendor_id', $this->integer());
        $this->addForeignKey(
            'fk-product-vendor',
            'product',
            'vendor_id',
            'vendor',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-product-vendor', 'product');
        $this->dropColumn('product', 'vendor_id');
    }
}
