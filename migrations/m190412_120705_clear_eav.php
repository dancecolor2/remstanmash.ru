<?php

use yii\db\Migration;

/**
 * Class m190412_120705_clear_eav
 */
class m190412_120705_clear_eav extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('eav_entity');
        $this->dropColumn('eav_attribute', 'entity_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190412_120705_clear_eav cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190412_120705_clear_eav cannot be reverted.\n";

        return false;
    }
    */
}
