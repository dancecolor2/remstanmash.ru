<?php

use yii\db\Migration;

/**
 * Class m190515_130703_product_options
 */
class m190515_130703_product_options extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'options_mask', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'options_mask');
    }
}
