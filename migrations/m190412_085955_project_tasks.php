<?php

use yii\db\Migration;

/**
 * Class m190412_085955_project_tasks
 */
class m190412_085955_project_tasks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'tasks', $this->text()->defaultValue(null));
        $this->addColumn('projects', 'works', $this->text()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190412_085955_project_tasks cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190412_085955_project_tasks cannot be reverted.\n";

        return false;
    }
    */
}
