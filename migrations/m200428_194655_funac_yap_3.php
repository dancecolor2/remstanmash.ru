<?php

use app\modules\eav\models\EavAttributeValue;
use app\modules\product\models\Product;
use yii\db\Migration;

/**
 * Class m200428_194655_funac_yap_3
 */
class m200428_194655_funac_yap_3 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $products = Product::find()->andWhere(['vendor_id' => 1])->all();

	    if (empty($products)) {
		    return true;
	    }

	    foreach ($products as $product) {
		    try {
		        EavAttributeValue::deleteAll(['entity_id' => $product->id, 'attribute_id' => 43, 'value' => 'Япония']);
		    } catch (Exception|Throwable $e) {}
	    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200428_194655_funac_yap_3 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200428_194655_funac_yap_3 cannot be reverted.\n";

        return false;
    }
    */
}
