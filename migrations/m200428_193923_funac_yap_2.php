<?php

use app\modules\product\models\Product;
use yii\db\Migration;

/**
 * Class m200428_193923_funac_yap_2
 */
class m200428_193923_funac_yap_2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $products = Product::find()->andWhere(['vendor_id' => 1])->all();

	    if (empty($products)) {
		    return true;
	    }

	    foreach ($products as $product) {
		    $eavv = \app\modules\eav\models\EavAttributeValue::findOne(['entity_id' => $product->id, 'attribute_id' => 43]);

		    try {
			    $eavv->value = '';
			    $eavv->option_id = 34;
			    $eavv->save();
		    } catch (Exception|Throwable $e) {}
	    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200428_193923_funac_yap_2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200428_193923_funac_yap_2 cannot be reverted.\n";

        return false;
    }
    */
}
