<?php

use yii\db\Migration;

/**
 * Class m190409_220142_vendor
 */
class m190409_220142_vendor extends Migration
{
    public function safeUp()
    {
        $this->createTable('vendor', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'alias' => $this->string(255)->notNull(),
            'content' => $this->text()->defaultValue(null),
            'hint' => $this->text()->defaultValue(null),
            'image' => $this->string(500)->defaultValue(null),
            'image_hash' => $this->string(255)->defaultValue(null),
            'h1' => $this->string(255)->defaultValue(null),
            'meta_t' => $this->string(255)->defaultValue(null),
            'meta_d' => $this->string(255)->defaultValue(null),
            'meta_k' => $this->string(255)->defaultValue(null),
            'position' => $this->integer(11)->defaultValue(null),
            'active' => $this->integer(1)->defaultValue(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('vendor');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190409_220142_vendor cannot be reverted.\n";

        return false;
    }
    */
}
