<?php

use yii\db\Migration;

/**
 * Class m200706_130740_product_parent
 */
class m200706_130740_product_parent extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'parent_id', $this->integer()->defaultValue(null));
        $this->createIndex('idx-product-parent_id', 'product', 'parent_id');
        $this->addForeignKey('fk-product_parent_id', 'product', 'parent_id', 'product', 'id');
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'parent_id');
    }
}
