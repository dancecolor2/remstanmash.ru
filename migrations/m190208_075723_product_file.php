<?php

use yii\db\Migration;

class m190208_075723_product_file extends Migration
{
    public function safeUp()
    {
        $this->createTable('product_file', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'file' => $this->string(500),
            'position' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(1),
            'product_id' => $this->integer()->notNull(),
            'file_hash' => $this->string(255),
        ]);

        $this->addForeignKey(
            'fk-product_file-product',
            'product_file',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-product_file-product', 'product_file');
        $this->dropTable('product_file');
    }
}
