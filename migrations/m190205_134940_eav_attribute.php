<?php

use yii\db\Migration;

/**
 * Class m190205_134940_eav_attribute
 */
class m190205_134940_eav_attribute extends Migration
{
    public function safeUp()
    {
        $this->createTable('eav_attribute', [
            'id' => $this->primaryKey(),
            'entity_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->defaultValue(1),
            'name' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'unit' => $this->string(50),
            'default_value' => $this->string(255),
            'default_option_id' => $this->integer(),
            'required' => $this->boolean()->defaultValue(0),
            'description' => $this->string(255),
            'order' => $this->integer(3),
            'is_important' => $this->integer(1)->defaultValue(0),
            'label' => $this->string(255),
            'in_filter' => $this->boolean()->defaultValue(0),
            'position' => $this->integer()->notNull(),
        ]);

        $this->createTable('eav_attribute_option', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer()->notNull(),
            'value' => $this->string(255),
            'default_option_id' => $this->boolean(),
        ]);

        $this->createTable('eav_attribute_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'handler_class' => $this->string(255)->notNull(),
            'store_type' => $this->integer(2)->defaultValue(0),
        ]);

        $this->createTable('eav_attribute_value', [
            'id' => $this->primaryKey(),
            'entity_id' => $this->integer()->notNull(),
            'attribute_id' => $this->integer()->notNull(),
            'value' => $this->string(255),
            'option_id' => $this->integer(2),
        ]);

        $this->createTable('eav_entity', [
            'id' => $this->primaryKey(),
            'entity_name' => $this->string(255),
            'entity_model' => $this->string(255),
            'category_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-eav_attribute-eav_attribute_type',
            'eav_attribute',
            'type_id',
            'eav_attribute_type',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-eav_attribute_option-eav_attribute',
            'eav_attribute_option',
            'attribute_id',
            'eav_attribute',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-eav_attribute_value-eav_attribute',
            'eav_attribute_value',
            'attribute_id',
            'eav_attribute',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-eav_attribute_value-eav_attribute_option',
            'eav_attribute_value',
            'option_id',
            'eav_attribute_option',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-eav_attribute_value-product',
            'eav_attribute_value',
            'entity_id',
            'product',
            'id',
            'CASCADE',
            'CASCADE'
        );

//        $this->addForeignKey(
//            'fk-eav_attribute_value-category',
//            'eav_entity',
//            'category_id',
//            'categories',
//            'id',
//            'CASCADE'
//        );

        $this->batchInsert('eav_attribute_type', ['name', 'handler_class', 'store_type'], [
            ['text', '\app\modules\eav\widgets\TextInput', 0],
            ['option', '\app\modules\eav\widgets\DropDownList', 1],
            ['checkbox', '\app\modules\eav\widgets\CheckBoxList', 2],
        ]);

//        $this->batchInsert('eav_entity', ['entity_name', 'entity_model', 'category_id'], [
//            ['product', 'app\modules\product\models\Product', 52],
//        ]);

    }

    public function safeDown()
    {
        //$this->dropForeignKey('fk-eav_attribute_value-category', 'eav_attribute_value');
        $this->dropForeignKey('fk-eav_attribute_value-product', 'eav_attribute_value');
        $this->dropForeignKey('fk-eav_attribute_value-eav_attribute_option', 'eav_attribute_value');
        $this->dropForeignKey('fk-eav_attribute_value-eav_attribute', 'eav_attribute_value');
        $this->dropForeignKey('fk-eav_attribute_option-eav_attribute', 'eav_attribute_option');
        $this->dropForeignKey('fk-eav_attribute-eav_attribute_type', 'eav_attribute');
        $this->dropTable('eav_entity');
        $this->dropTable('eav_attribute_value');
        $this->dropTable('eav_attribute_type');
        $this->dropTable('eav_attribute_option');
        $this->dropTable('eav_attribute');
    }
}
