<?php

use yii\db\Migration;

/**
 * Class m190205_122936_product
 */
class m190205_122936_product extends Migration
{
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'code' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'price' => $this->integer(),
            'h1' => $this->string(255),
            'meta_t' => $this->string(255),
            'meta_d' => $this->string(255),
            'meta_k' => $this->string(255),
            'active' => $this->boolean()->defaultValue(1)->notNull(),
            'position' => $this->integer(),
            'description' => $this->text(),
            'order_description' => $this->text(),
            'features_description' => $this->text(),
            'in_stock' => $this->boolean()->defaultValue(1),
        ]);

        $this->createTable('product_image', [
            'id' => $this->primaryKey(),
            'image' => $this->string(500),
            'image_hash' => $this->string(255),
            'product_id' => $this->integer()->notNull(),
            'position' => $this->integer(),
            'alt' => $this->string(255),
        ]);

        $this->addForeignKey(
            'fk-product_image-product',
            'product_image',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-product_image-product', 'product_image');
        $this->dropTable('product_image');
        $this->dropTable('product');
    }
}
