<?php

use yii\db\Migration;

/**
 * Class m200707_083416_eav_is_important_child
 */
class m200707_083416_eav_is_important_child extends Migration
{
    public function safeUp()
    {
        $this->addColumn('eav_attribute', 'is_child_preview', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('eav_attribute', 'is_child_preview');
    }
}
