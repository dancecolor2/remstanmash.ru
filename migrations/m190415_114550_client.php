<?php

use yii\db\Migration;

/**
 * Class m190415_114550_client
 */
class m190415_114550_client extends Migration
{
    public function safeUp()
    {
        $this->createTable('client', [
            'created_at' => $this->integer()->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer()->notNull()->comment('Дата редактирования'),
            'active' => $this->boolean()->defaultValue(1)->comment('Активность'),
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull()->comment('Название'),
            'caption' => $this->string(255)->comment('Подпись'),
            'position' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('client');
    }
}
