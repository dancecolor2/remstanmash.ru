<?php

use yii\db\Migration;

/**
 * Class m200702_154055_reviews
 */
class m200702_154055_reviews extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reviews', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'company' => $this->string()->defaultValue(null),
            'email' => $this->string()->defaultValue(null),
            'message' => $this->string(500)->notNull(),
            'rating' => $this->integer()->notNull(),
            'is_active' => $this->boolean()->defaultValue(false),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('reviews');
    }
}
