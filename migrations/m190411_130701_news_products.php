<?php

use yii\db\Migration;

/**
 * Class m190411_130701_news_products
 */
class m190411_130701_news_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news_products', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190411_130701_news_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190411_130701_news_products cannot be reverted.\n";

        return false;
    }
    */
}
