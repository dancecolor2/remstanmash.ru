<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projects`.
 */
class m181206_075014_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer()->notNull()->comment('Дата редактирования'),
            'title' => $this->string(255)->notNull()->comment('Заголовок'),
            'alias' => $this->string(255)->notNull()->comment('Алиас'),
            'image' => $this->string(500)->comment('Картинка для анонса'),
            'content' => $this->text()->notNull()->comment('Контент'),
            'active' => $this->integer()->defaultValue(1)->comment('Активность'),
            'h1' => $this->string(255),
            'meta_t' => $this->string(255)->comment('Заголовок страницы'),
            'meta_d' => $this->string(255)->comment('Описание страницы'),
            'meta_k' => $this->string(255)->comment('Ключевые слова'),
        ]);

        $this->createTable('projects_products', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull(),
        ]);

        $this->addForeignKey('fk_project_product', 'projects_products', 'project_id', 'projects', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_product_project', 'projects_products', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_project_product', 'projects_products');
        $this->dropForeignKey('fk_product_project', 'projects_products');
        $this->dropTable('projects_products');
        $this->dropTable('projects');
    }
}
