<?php

use yii\db\Migration;

class m190212_095157_feedback extends Migration
{
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'name' => $this->string(255),
            'organization' => $this->string(255),
            'phone' => $this->string(255),
            'email' => $this->string(255),
            'comment' => $this->text(),
            'item_id' => $this->integer(),
            'item_text' => $this->string(500),
            'type' => $this->string(255),
            'ref' => $this->string(500),
            'status' => $this->integer()->defaultValue(1),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('feedback');
    }
}
