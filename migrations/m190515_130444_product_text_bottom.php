<?php

use yii\db\Migration;

/**
 * Class m190515_130444_product_text_bottom
 */
class m190515_130444_product_text_bottom extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'text_bottom_title', $this->string(255));
        $this->addColumn('product', 'text_bottom', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'text_bottom_title');
        $this->dropColumn('product', 'text_bottom');
    }
}
