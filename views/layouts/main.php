<?php

use app\modules\feedback\widgets\CallbackFormWidget;
use app\modules\feedback\widgets\OrderFormWidget;
use app\modules\page\components\Pages;
use app\modules\search\widgets\SearchWidget;
use app\modules\setting\components\Settings;
use app\widgets\MenuFooterWidget;
use app\widgets\MenuHeaderBottom;
use app\widgets\MenuHeaderTopWidget;
use app\widgets\SocialWidget;
use app\widgets\WorktimeWidget;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/**
 * @var \yii\web\View $this
 * @var string $content
 */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="html" lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="/img/logo.svg"/>
    <meta property="og:site_name" content="РЕМСТАНМАШ"/>
    <meta property="og:locale" content="ru_RU"/>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon"/>
    <link rel="preload" as="script" href="/js/app.js"/>
    <?= Settings::getRealValue('meta_tags') ?>
    <?php $this->head() ?>
</head>
<body class="body js-body">
<script>document.querySelector('.js-body').classList.add('js-init');</script>
<?php $this->beginBody() ?>
<div class="loader js-loader">
    <script>
        if (sessionStorage.getItem('loaded')) {
            document.querySelector('.js-body').classList.add('is-loaded');
        }
    </script>
</div>
<div class="page js-page" id="page">
    <header class="page__header">
        <div class="header">
            <div class="header__top">
                <div class="container">
                    <div class="header__inner">
                        <div class="header__logo">
                            <a class="logo" href="/" title="Главная страница"></a>
                        </div>
                        <div class="header__nav">
                           <?= MenuHeaderTopWidget::widget() ?>
                        </div>
                        <div class="header__contact">
                            <address class="contact">
                                <div class="contact__group">
                                    <?= WorktimeWidget::widget() ?>
                                    <?php if (!empty(Settings::getArray('phones'))): ?>
                                        <ul class="contact__phones">
                                            <li class="contact__phone">
                                                <a class="phone" href="tel:<?= Settings::getArray('phones')[0] ?>"><?= Settings::getArray('phones')[0] ?></a>
                                            </li>
                                        </ul>
                                    <?php endif ?>
                                    <a href="mailto:info@cnchelp.ru" class="contact__email">info@cnchelp.ru</a>
                                </div>
                            </address>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header__bottom">
                <div class="container">
                    <div class="header__inner">
                        <div class="header__menu">
                            <?= MenuHeaderBottom::widget(['activeItem' => $this->params['activeMenuItem'] ?? null]) ?>
                            <div class="header__second">
                                <button class="header__second-burger js-header-second">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                                <div class="header__second-body js-header-second-body">
                                    
                                    <?= MenuHeaderTopWidget::widget() ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="header__search">
                            <?= SearchWidget::widget() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($this->params['breadcrumbs'] ?? $this->params['h1'] ?? $this->params['headlineCaption'] ?? $this->params['headlineDate'] ?? $this->params['headlineText'] ?? false): ?>
            <div class="headline js-headline <?= $this->params['headlineClass'] ?? '' ?>">
                <?php if (!empty($this->params['headlineImage'])): ?>
                    <div class="headline__cover">
                        <div class="headline__image" style="background-image: url(<?= $this->params['headlineImage'] ?>)"></div>
                    </div>
                    <div class="headline__overlay">
                        <div class="overlay js-overlay">
                            <canvas class="overlay__canvas js-overlay-canvas"></canvas>
                        </div>
                    </div>
                <?php endif ?>
                <div class="container">
                    <div class="headline__inner">
                        <div class="headline__group">
                            <?php if (!empty($this->params['breadcrumbs'])): ?>
                                <div class="headline__breadcrumb">
                                    <?= Breadcrumbs::widget([
                                        'itemTemplate' => "<li class='breadcrumb__item'>{link}</li>",
                                        'activeItemTemplate' => "<li class='breadcrumb__item'>{link}</li>",
                                        'homeLink' => false,
                                        'links' => $this->params['breadcrumbs'],
                                    ]) ?>
                                </div>
                            <?php endif ?>
                            <?php if (!empty($this->params['headlineCaption'])): ?>
                                <p class="headline__caption"><?= $this->params['headlineCaption'] ?></p>
                            <?php endif ?>
                            <?php if (!empty($this->params['headlineDate'])): ?>
                                <time class="headline__date"><?= $this->params['headlineDate'] ?></time>
                            <?php endif ?>
                            <?php if (!empty($this->params['h1'])): ?>
                                <h1 class="headline__title"><?= $this->params['h1'] ?></h1>
                            <?php endif ?>
                        </div>
                        <?php if (!empty($this->params['headlineText'])): ?>
                            <div class="headline__text">
                                <div class="text">
                                    <?= $this->params['headlineText'] ?>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <?= $this->blocks['endHeader'] ?>
    </header>
    <main class="page__content">
        <?= $content ?>
    </main>
    <footer class="page__footer">
        <div class="footer">
            <div class="footer__top">
                <div class="container">
                    <div class="footer__inner">
                        <div class="footer__logo">
                            <div class="logo"></div>
                        </div>
                        <div class="footer__links">
                            <?= MenuFooterWidget::widget() ?>
                        </div>
                        <div class="footer__social">
                            <?= SocialWidget::widget() ?>
                        </div>
                        <div class="footer__totop">
                            <a class="totop" data-scroll-to="data-scroll-to" href="#page" area-label="Вверх">
                                <span class="arrow is-up">
                                    <svg class="arrow__icon"><use xlink:href="/img/sprite.svg#arrow-alt"></use></svg>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="container">
                    <div class="footer__inner">
                        <div class="footer__copy"><?= Settings::getValue('copyright') ?></div>
                        <div class="footer__text"><a href="<?= Pages::getHref('privacy') ?>" title="Политика конфиденциальности">Политика конфиденциальности</a></div>
                        <!-- <div class="footer__dc">
                            <a class="dc" href="https://dancecolor.ru/" title="Сделано для продвижения DANCECOLOR" target="_blank">
                                <div class="dc__logo"></div>
                                <div class="dc__group">
                                    <div class="dc__caption">cделано для продвижения</div>
                                    <div class="dc__title">DANCECOLOR</div>
                                </div>
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal js-modal">
        <div class="modal__container js-modal-container">
            <button class="modal__close" data-modal-close="data-modal-close">Закрыть</button>
            <div class="modal__inner js-modal-inner">
                <div class="modal__content js-modal-content"></div>
            </div>
        </div>
        <div class="modal__list">
            <div id="modal-callback">
                <div class="modal__header">
                    <h3 class="modal__title">Обратный звонок</h3>
                </div>
                <div class="modal__body">
                    <?= CallbackFormWidget::widget() ?>
                </div>
            </div>
            <div id="modal-order">
                <div class="modal__header">
                    <h3 class="modal__title js-modal-title"></h3>
                    <div class="modal__product js-modal-product">
                        <article class="product is-small js-product">
                            <div class="product__cover"><img class="product__image js-product-image" src="" alt="alt"/></div>
                            <div class="product__body">
                                <header class="product__header">
                                    <p class="product__code js-product-code"></p>
                                    <h3 class="product__title js-product-title"></h3>
                                </header>
                                <div class="product__cost js-product-cost"></div>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="modal__body">
                    <?= OrderFormWidget::widget(['inModal' => true]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= Settings::getRealValue('metrika_code') ?>
<?= Settings::getRealValue('analitics_code') ?>
<?= Settings::getRealValue('callback_code') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
