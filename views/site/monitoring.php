<?php

use app\helpers\ContentHelper;
use app\helpers\FormatHelper;
use app\modules\page\components\Pages;
use app\modules\system\widgets\SystemsSectionWidget;
use app\widgets\CallbackSectionWidget;

/**
 * @var \yii\web\View $this
 */

Pages::getCurrentPage()->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs();
$this->params['h1'] = Pages::getCurrentPage()->getH1();
$this->params['headlineClass'] = 'is-margin-bottom-120 has-cover';
$this->params['headlineImage'] = '/img/monitoring-cover.jpg';
$this->params['headlineText'] = Pages::getElementValue('headline_text');

?>

<section class="section">
    <div class="container">
        <div class="section__body">
            <div class="grid is-row">
                <div class="col-8 shift-1 col-m-12 shift-m-0">
                    <div class="text">
                        <?= Pages::getCurrentPage()->content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if (!empty(Pages::getElementValue('task_text')) || !empty(Pages::getElementValue('decision_text'))): ?>
    <section class="section is-white with-padding is-padding-80">
        <div class="container">
            <div class="section__body">
                <div class="grid is-row">
                    <div class="col-6 col-m-12">
                        <div class="stages">
                            <h3 class="stages__title"><?= Pages::getElementValue('task_title') ?></h3>
                            <ul class="stages__list">
                                <?php foreach (ContentHelper::splitText(Pages::getElementValue('task_text')) as $item): ?>
                                    <li class="stages__item"><?= FormatHelper::nText($item) ?></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-m-12">
                        <div class="stages is-solutions">
                            <h3 class="stages__title"><?= Pages::getElementValue('decision_title') ?></h3>
                            <ul class="stages__list">
                                <?php foreach (ContentHelper::splitText(Pages::getElementValue('decision_text')) as $item): ?>
                                    <li class="stages__item"><?= FormatHelper::nText($item) ?></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?= SystemsSectionWidget::widget() ?>

<?php if (!empty(Pages::getElementValue('bottom_text'))): ?>
    <section class="section is-white with-padding is-padding-90 is-layout-4">
        <div class="container">
            <div class="grid is-row">
                <div class="col-5 col-m-12">
                    <div class="section__content"><img src="/img/monitoring-system.jpg" alt="Системы мониторинга">
                    </div>
                </div>
                <div class="col-7 col-m-12">
                    <header class="section__header">
                        <h2 class="section__title"><?= FormatHelper::nText(Pages::getElementValue('bottom_title')) ?></h2>
                    </header>
                    <div class="section__body">
                        <div class="section__text text">
                            <?= Pages::getElementValue('bottom_text') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?= CallbackSectionWidget::widget() ?>