<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = 'Страница не найдена';

?>

<section class="section is-margin-top-60">
    <div class="container">
        <div class="section__body">
            <div class="grid is-row">
                <div class="col-6 shift-3">
                    <div class="status" data-code="404">
                        <h1 class="status__title">Страница не найдена</h1>
                        <div class="status__button"><a class="button js-button is-wide" area-label="вернуться на главную" href="/">вернуться на главную</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
