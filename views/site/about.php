<?php

use app\helpers\FormatHelper;
use app\modules\client\widgets\ClientWidget;
use app\modules\page\components\Pages;
use app\modules\setting\components\Settings;
use app\modules\vendor\widgets\VendorsWidget;
use app\widgets\RequisitesWidget;
use app\widgets\WorktimeWidget;

/**
 * @var $this yii\web\View
 * @var \app\modules\client\models\Client[] $clients
 */

Pages::getCurrentPage()->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs();
$this->params['h1'] = Pages::getCurrentPage()->getH1();
$this->params['headlineClass'] = 'is-margin-bottom-140 is-about has-cover';
$this->params['headlineImage'] = '/img/hero-cover.jpg';
$this->params['headlineCaption'] = 'Добро пожаловать';
$this->params['headlineText'] = Pages::getElementValue('headline_text');

?>

<section class="section is-margin-140 is-layout-5">
    <div class="container">
        <div class="grid is-row">
            <div class="col-8 shift-1 shift-md-0 col-md-12">
                <header class="section__header">
                    <p class="section__caption">наши стремления</p>
                    <h2 class="section__title"><?= FormatHelper::nText(Pages::getElementValue('title_content')) ?></h2>
                </header>
                <div class="section__body">
                    <div class="text">
                        <?= Pages::getCurrentPage()->content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section is-margin-140 is-layout-1">
    <div class="container">
        <div class="grid is-row">
            <div class="col-6 col-md-12">
                <header class="section__header">
                    <p class="section__caption">что мы делаем</p>
                    <h2 class="section__title"><?= FormatHelper::nText(Pages::getElementValue('what_we_do_title')) ?></h2>
                </header>
                <div class="section__body">
                    <div class="section__text text">
                        <?= Pages::getElementValue('what_we_do_content') ?>
                    </div>
                </div>
            </div>
            <div class="col-5 shift-1 col-md-12 shift-md-0">
                <div class="section__content">
                    <section class="card has-left-shadow">
                        <h4 class="card__title"><?= FormatHelper::nText(Pages::getElementValue('card_title')) ?></h4>
                        <div class="card__body">
                            <p class="card__caption"><?= FormatHelper::nText(Pages::getElementValue('card_caption')) ?></p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clients">
    <div class="clients__cover"></div>
    <div class="clients__body">
        <div class="container">
            <div class="grid">
                <?php if (!empty($clients)): ?>
                    <div class="clients__roll">
                        <div class="clients-roll js-clients-roll">
                            <div class="clients-roll__inner js-clients-roll-swiper">
                                <ul class="clients-roll__list js-clients-roll-list">
                                    <?php foreach ($clients as $client): ?>
                                        <li class="clients-roll__item js-clients-roll-item">
                                            <?= ClientWidget::widget(compact('client')) ?>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                                <div class="clients-roll__dots js-clients-roll-dots"></div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
                <?php if (!empty(Pages::getElementValue('clients_text'))): ?>
                    <div class="clients__info">
                        <h4 class="clients__title"><?= FormatHelper::nText(Pages::getElementValue('clients_title')) ?></h4>
                        <p class="clients__text"><?= FormatHelper::nText(Pages::getElementValue('clients_text')) ?></p>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>

<?= VendorsWidget::widget() ?>

<section class="section with-padding is-padding-140">
    <div class="container">
        <div class="section__body">
            <div class="grid is-row is-middle">
                <div class="col-4 col-md-12">
                    <section class="card is-small has-image has-right-shadow">
                        <h4 class="card__title">Контакты</h4>
                        <div class="card__body">
                            <address class="contact is-light">
                                <div class="contact__group">
                                    <?= WorktimeWidget::widget() ?>
                                    <ul class="contact__phones">
                                        <?php foreach (Settings::getArray('phones') as $phone): ?>
                                            <li class="contact__phone">
                                            <a class="phone" href="tel:<?= $phone ?>"><?= $phone ?></a>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                                <div class="contact__address"><?= Settings::getValue('address') ?></div>
                            </address>
                        </div>
                    </section>
                </div>
                <div class="col-6 shift-2 col-md-12 shift-md-0">
                    <?= RequisitesWidget::widget() ?>
                </div>
            </div>
        </div>
    </div>
</section>
