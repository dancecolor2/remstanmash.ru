<?php

use app\helpers\FormatHelper;
use app\modules\eav\helpers\Html;
use app\modules\page\components\Pages;
use app\modules\project\models\Project;
use app\modules\project\widgets\ProjectsWidget;
use app\modules\repair\widgets\RepairsSectionWidget;
use app\widgets\ArticleWidget;
use app\widgets\CallbackSectionWidget;
use app\widgets\TextBottomWidget;

/**
 * @var \yii\web\View $this
 */

Pages::getCurrentPage()->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs();
$this->params['h1'] = Pages::getCurrentPage()->getH1();
$this->params['headlineClass'] = 'is-margin-bottom-120 has-cover';
$this->params['headlineImage'] = '/img/upgrade-cover.jpg';
$this->params['headlineText'] = FormatHelper::nText(Pages::getElementValue('headline_text'));

?>

<?= RepairsSectionWidget::widget() ?>

<?php if (!empty(Pages::getCurrentPage()->content)): ?>
    <section class="section">
        <div class="container">
            <div class="section__body">
                <div class="col-10 shift-1 col-l-12 shift-l-0">
                    <div class="text">
                        <?= ArticleWidget::widget(['article' => Pages::getCurrentPage()->content]) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

<?= ProjectsWidget::widget([
    'type' => Project::TYPE_REPAIR,
    'caption' => 'Ремонт оборудования',
]) ?>

<?php if (!empty(Pages::getElementValue('is_show_text_bottom'))): ?>
    <?= TextBottomWidget::widget([
        'title' => Pages::getElementValue('text_bottom_title'),
        'text' => Pages::getElementValue('text_bottom'),
    ]) ?>
<?php endif ?>

<?= CallbackSectionWidget::widget() ?>