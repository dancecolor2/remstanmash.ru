<?php

use app\modules\feedback\widgets\FeedbackFormWidget;
use app\modules\page\components\Pages;
use app\modules\setting\components\Settings;
use app\widgets\RequisitesWidget;
use app\widgets\WorktimeWidget;
use yii\helpers\Json;

/**
 * @var \yii\web\View $this
 * @var array $map
 */

Pages::getCurrentPage()->generateMetaTags();
$this->params['breadcrumbs'] = Pages::getParentBreadcrumbs();
$this->params['h1'] = Pages::getCurrentPage()->getH1();

$map = Settings::getArray('map');
$dataMap = [
    'center' => $map['coords'],
    'zoom' => $map['zoom'],
    'markers' => [
        [
            'address' => Settings::getRealValue('address'),
            'coords' => $map['coords'],
            'image' => [
                'href' => '/img/marker.svg',
                'size' => [108,88],'offset' => [-54,-84],
            ]
        ]
    ]
]

?>

<section class="section is-collapse">
    <div class="container">
        <div class="section__body">
            <div class="ticket">
                <div class="grid">
                    <div class="col-5 col-l-6 col-md-12">
                        <div class="ticket__left">
                            <div class="ticket__info">
                                <div class="ticket__contacts">
                                    <address class="contact is-light">
                                        <?php if (!empty(Settings::getValue('address'))): ?>
                                            <dl class="contact__list">
                                                <div class="contact__item">
                                                    <dt class="contact__label">Адрес</dt>
                                                    <dd class="contact__value"><?= Settings::getValue('address') ?></dd>
                                                </div>
                                            </dl>
                                        <?php endif ?>
                                        <div class="contact__group">
                                            <?= WorktimeWidget::widget() ?>
                                            <ul class="contact__phones">
                                                <?php foreach (Settings::getArray('phones') as $phone): ?>
                                                    <li class="contact__phone is-alt">
                                                    <a class="phone" href="tel:<?= $phone ?>"><?= $phone ?></a>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                        </div>
                                    </address>
                                </div>
                                <div class="ticket__requisites">
                                    <?= RequisitesWidget::widget(['isLight' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-7 col-l-6 col-md-12">
                        <div class="ticket__right">
                            <div class="ticket__form">
                                <?= FeedbackFormWidget::widget() ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section is-margin-top-60">
    <div class="container">
        <div class="section__body">
            <div class="map js-map" data-options='<?= Json::encode($dataMap) ?>'>
                <div class="map__container js-map-container"></div>
            </div>
        </div>
    </div>
</section>