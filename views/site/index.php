<?php

use app\helpers\FormatHelper;
use app\modules\category\widgets\CategoriesSectionWidget;
use app\modules\news\widgets\LastNewsWidget;
use app\modules\page\components\Pages;
use app\modules\vendor\widgets\VendorsWidget;
use app\widgets\CallbackSectionWidget;
use app\widgets\ServicesSectionWidget;
use app\widgets\TextBottomWidget;

/**
 * @var $this yii\web\View
 */

Pages::getCurrentPage()->generateMetaTags();

?>

<?php $this->beginBlock('endHeader'); ?>
    <div class="hero" style="background-image: url(/img/hero-cover.jpg)">
        <div class="hero__overlay">
            <div class="overlay js-overlay">
                <canvas class="overlay__canvas js-overlay-canvas"></canvas>
            </div>
        </div>
        <div class="container">
            <div class="hero__inner">
                <div class="hero__body">
                    <h1 class="hero__title"><?= Pages::getCurrentPage()->getH1() ?></h1>
                    <div class="hero__button"><a class="button js-button" area-label="перейти к каталогу" href="<?= Pages::getHref('catalog') ?>" title="Перейти к каталогу">перейти к каталогу</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->endBlock();?>

<?= CategoriesSectionWidget::widget() ?>

<section class="section is-layout-1">
    <div class="container">
        <div class="grid is-row">
            <div class="col-6 col-m-12">
                <header class="section__header">
                    <p class="section__caption">ремстанмаш</p>
                    <h2 class="section__title"><?= Pages::getElementValue('title_content') ?></h2>
                </header>
                <div class="section__body">
                    <div class="section__text text">
                        <?= Pages::getCurrentPage()->content ?>
                    </div>
                    <div class="section__link">
                        <a class="link" href="<?= Pages::getHref('about') ?>" title="">
                            <span class="link__label">подробнее о компании</span>
                            <svg class="link__arrow">
                                <use xlink:href="/img/sprite.svg#arrow"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-5 shift-1 shift-l-0 col-l-6 col-m-12">
                <div class="section__content">
                    <section class="card has-left-shadow">
                        <h4 class="card__title"><?= FormatHelper::nText(Pages::getElementValue('card_title')) ?></h4>
                        <div class="card__body">
                            <p class="card__caption"><?= FormatHelper::nText(Pages::getElementValue('card_caption'))  ?></p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>

<?= VendorsWidget::widget() ?>
<?= ServicesSectionWidget::widget() ?>
<?= LastNewsWidget::widget() ?>

<?php if (!empty(Pages::getElementValue('is_show_text_bottom'))): ?>
    <?= TextBottomWidget::widget([
        'title' => Pages::getElementValue('text_bottom_title'),
        'text' => Pages::getElementValue('text_bottom'),
    ]) ?>
<?php endif ?>

<?= CallbackSectionWidget::widget() ?>
