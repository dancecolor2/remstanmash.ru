<?php

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public function init()
    {
        if (isset(Yii::$app->get('assetManager')->bundles[\yii\web\JqueryAsset::class])) {
            Yii::$app->get('assetManager')->bundles[\yii\web\JqueryAsset::class] = ['js' => []];
        }

        if (isset(Yii::$app->get('assetManager')->bundles[\yii\web\YiiAsset::class])) {
            Yii::$app->get('assetManager')->bundles[\yii\web\YiiAsset::class]->depends = [\app\assets\AppAsset::class];
        }

        parent::init();
    }

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.min.css',
    ];
    public $js = [
        'js/app.js'
    ];
}
