const util = require('gulp-util');
const config = require('./gulp/config');

// Console message
util.log(util.colors.white.bgRed(` ${config.environment.toUpperCase()} `));
util.log('');
util.log(`stage: ${util.colors.cyan(`${config.stage}`)}`);
util.log(`src: ${util.colors.green(`/${config.src.root}`)} `);
util.log(`dest: ${util.colors.green(`/${config.dest.root}`)} `);
util.log('');

require('require-dir')('./gulp/tasks', { recurse: true });
