<?php

namespace app\widgets;

use app\helpers\ContentHelper;
use app\modules\setting\components\Settings;
use yii\base\Widget;

class WorktimeWidget extends Widget
{
    public function run()
    {
        $items = Settings::getArray('worktime');

        foreach ($items as &$item) {
            list($day, $time) = ContentHelper::splitText($item, ' ', 2);
            $item = ['day' => $day, 'time' => $time];
        }

        return $this->render('worktime', compact('items'));
    }
}