<?php

namespace app\widgets;

use yii\base\Widget;

class ArticleWidget extends Widget
{
    public $article;

    public function run()
    {
        return $this->parseImages($this->article);
    }

    private function parseImages($content)
    {
        return preg_replace_callback('#<table class=["|\']article__images["|\']>.*<\/table>#sU', function ($table) {
            return preg_replace_callback('#<img[^>]+>#sU', function ($imgTags) {
                $string = $imgTags[0];
                preg_match_all('#src="([^"]*)"#', $string, $srcs);
                $image = current($srcs[1]);
                preg_match_all('#alt="([^"]*)"#', $string, $alts);
                $alt = current($alts[1]);
                return $this->render('article/image', compact('image', 'alt'));
            }, $table[0]);
        }, $content);
    }
}