<?php

namespace app\widgets;

use app\helpers\ContentHelper;
use app\modules\setting\components\Settings;
use yii\base\Widget;

/**
 * @property boolean $isLight
 */
class RequisitesWidget extends Widget
{
    public $isLight = false;

    public function run()
    {
        $requisites = Settings::getArray('requisites');

        foreach ($requisites as &$requisite) {
            list($key, $value) = ContentHelper::splitText($requisite, ' ', 2);
            $requisite = ['key' => $key, 'value' => $value];
        }

        return $this->render('requisites', [
            'requisites' => $requisites,
            'isLight' => $this->isLight,
        ]);
    }
}