<?php

namespace app\widgets;

use yii\base\Widget;

class CallbackSectionWidget extends Widget
{
    public function run()
    {
        return $this->render('callback_section');
    }
}