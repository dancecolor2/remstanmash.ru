<?php

namespace app\widgets;

use app\modules\setting\components\Settings;
use yii\base\Widget;

class SocialWidget extends Widget
{
    public function run()
    {
        $instagram =  Settings::getValue('instagram');
        $facebook =  Settings::getValue('facebook');

        return $this->render('social', compact('instagram', 'facebook'));
    }
}