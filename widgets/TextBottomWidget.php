<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * @property string $title
 * @property string $text
 * @property boolean $is_show
 */
class TextBottomWidget extends Widget
{
    public $title;
    public $text;

    public function run()
    {
        return $this->render('text_bottom', [
            'title' => $this->title,
            'text' => $this->text,
        ]);
    }
}