<?php

namespace app\widgets;

use app\modules\page\components\Pages;
use Yii;
use yii\base\Widget;

class MenuFooterWidget extends Widget
{
    public function run()
    {
        $items = [
            $this->getItem('about'),
            $this->getItem('delivery'),
            $this->getItem('news'),
            $this->getItem('contacts'),
        ];

        return $this->render('menu_footer', compact('items'));
    }

    private function getItem($pageId)
    {
        $page = Pages::getPage($pageId);
        return [
            'label' => $page->getTitle(),
            'url' => $page->getHref(),
            'active' => $page->isActive() || (!empty($moduleIds) && in_array(Yii::$app->controller->module->id, $moduleIds)),
        ];
    }
}