<?php

namespace app\widgets;

use yii\base\Widget;

class ServicesSectionWidget extends Widget
{
    public function run()
    {
        return $this->render('services_section');
    }
}