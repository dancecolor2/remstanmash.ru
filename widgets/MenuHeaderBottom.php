<?php

namespace app\widgets;

use app\helpers\NestedSetsTree;
use app\modules\category\models\Category;
use app\modules\page\components\Pages;
use Yii;
use yii\base\Widget;
use yii\helpers\Url;

/**
 * @property string $activeItem
 */
class MenuHeaderBottom extends Widget
{
    public $activeItem;

    public function run()
    {
        $items = [
            array_merge($this->getItem('catalog', ['category', 'product']), [
                'label' => 'Каталог продукции',
                'class' => 'is-catalog',
                'items' => $this->getCatalogItems(),
            ]),
            array_merge($this->getItem('vendors', ['vendor']), [
                'label' => 'Подбор по производителю'
            ]),
            $this->getItem('upgrade', ['machine', 'set']),
            $this->getItem('monitoring'),
            $this->getItem('repair'),
        ];
        return $this->render('menu_header_bottom', compact('items'));
    }

    private function getItem($pageId, $moduleIds = [])
    {
        $page = Pages::getPage($pageId);
        return [
            'label' => $page->getTitle(),
            'url' => $page->getHref(),
            'active' => $page->isActive() || (!empty($moduleIds) && in_array(Yii::$app->controller->module->id, $moduleIds)) || $pageId == $this->activeItem,
        ];
    }

    private function getCatalogItems()
    {
        $categories = Category::find()->where(['>', 'depth', 0])->orderBy('lft')->asArray()->all();
        $ns = new NestedSetsTree();
        $tree = $ns->tree($categories);

        return array_map(function($category) {
            return [
                'label' => $category["title"],
                'url' => Url::to(['/category/frontend/view', 'alias' => $category['alias']]),
                'items' => array_map(function($category) {
                    return [
                        'label' => $category["title"],
                        'url' => Url::to(['/category/frontend/view', 'alias' => $category['alias']]),
                    ];
                }, $category['children']),
            ];
        }, $tree);
    }
}