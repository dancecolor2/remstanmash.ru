<?php

namespace app\widgets;


use yii\base\Widget;

/**
 * @property \yii\data\Pagination $pagination
 */
class PaginationWidget extends Widget
{
    public $pagination;

    public function run()
    {
        return $this->render('pagination', ['pagination' => $this->pagination]);
    }
}