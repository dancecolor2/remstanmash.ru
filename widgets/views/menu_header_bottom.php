<?php

/**
 * @var \yii\web\View $this
 * @var array $items
 */

?>

<?php if (!empty($items)): ?>

<div class="menu js-menu">
    <button class="menu__burger js-menu-burger">
        <span></span>
        <span></span>
        <span></span>
    </button>
    <ul class="menu__list ">
        <?php foreach ($items as $item): ?>
            <li class="menu__item <?= $item['class'] ?? '' ?>">
                <a class="menu__link<?= !empty($item['items']) ? ' js-menu-link' : '' ?><?= $item['active'] ? ' is-active' : '' ?>" href="<?= $item['url'] ?>" title="<?= $item['label'] ?>"><?= $item['label'] ?></a>
                <?php if (!empty($item['items'])): ?>
                    <div class="menu__drop js-menu-drop">
                        <div class="dropmenu js-dropmenu">
                            <ul class="dropmenu__list">
                                <?php foreach ($item['items'] as $dropMenuItem): ?>
                                    <li class="dropmenu__item <?= !empty($dropMenuItem['items']) ? 'has-children' : '' ?>">
                                        <span class="dropmenu__arrow"></span>
                                        <a class="dropmenu__link <?= !empty($dropMenuItem['items']) ? 'js-dropmenu-link' : '' ?>" href="<?= $dropMenuItem['url'] ?>" title="<?= $dropMenuItem['label'] ?>">
                                            <span><?= $dropMenuItem['label'] ?></span>
                                        </a>
                                        <?php if (!empty($dropMenuItem['items'])): ?>
                                            <div class="dropmenu__wrap js-dropmenu-wrap">
                                                <ul class="dropmenu__sublist">
                                                    <?php foreach ($dropMenuItem['items'] as $subItem): ?>
                                                        <li class="dropmenu__subitem js-dropmenu-subitem">
                                                            <a class="dropmenu__sublink" href="<?= $subItem['url'] ?>" title="<?= $subItem['label'] ?>"><?= $subItem['label'] ?></a>
                                                        </li>
                                                    <?php endforeach ?>
                                                </ul>
                                            </div>
                                        <?php endif ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                <?php endif ?>
            </li>
        <?php endforeach ?>
    </ul>
</div>
<?php endif ?>