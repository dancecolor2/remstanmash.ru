<?php

use app\helpers\FormatHelper;
use app\modules\page\components\Pages;

/**
 * @var \yii\web\View $this
 */

?>

<section class="section is-accent with-padding is-layout-2 is-m-hide">
    <div class="container">
        <div class="grid is-columns">
            <div class="col-7">
                <header class="section__header">
                    <p class="section__caption"><?= FormatHelper::nText(Pages::getElementValue('services_capture')) ?></p>
                    <h2 class="section__title"><?= FormatHelper::nText(Pages::getElementValue('services_title')) ?></h2>
                </header>
            </div>
            <div class="col-5">
                <div class="section__body">
                    <div class="section__text text">
                        <p><?= FormatHelper::nText(Pages::getElementValue('services_text')) ?></p>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="section__content">
                    <div class="cats has-shadow is-alt">
                        <ul class="cats__list grid">
                            <li class="cats__item col-4">
                                <article class="cat is-alt">
                                    <div class="cat__icon">
                                        <svg width="80" height="70" viewBox="0 0 80 70" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <rect x="2" y="10" width="5" height="45" fill="#D3D8E8"/>
                                            <rect x="8" y="55" width="4" height="4" fill="#D3D8E8"/>
                                            <rect x="34" y="55" width="3" height="4" fill="#D3D8E8"/>
                                            <rect x="65" y="19" width="2" height="5" fill="#D3D8E8"/>
                                            <path d="M11.48 20.1367C11.48 19.5844 11.9277 19.1367 12.48 19.1367H13.8183V29.1033H12.48C11.9277 29.1033 11.48 28.6556 11.48 28.1033V20.1367Z" fill="#D3D8E8"/>
                                            <path d="M11.48 35.5167C11.48 34.9644 11.9277 34.5167 12.48 34.5167H13.8183V44.4834H12.48C11.9277 44.4834 11.48 44.0356 11.48 43.4834V35.5167Z" fill="#D3D8E8"/>
                                            <rect x="65" y="30" width="2" height="5" fill="#D3D8E8"/>
                                            <rect x="65" y="40" width="2" height="5" fill="#D3D8E8"/>
                                            <rect x="31" y="22" width="5" height="19" fill="#D3D8E8"/>
                                            <rect x="6" y="10" width="71" height="3" fill="#D3D8E8"/>
                                            <path d="M77.21 10H2V55.15H8.3V58.59H29.8V55.15H34.08V58.59H70.79V55.15H77.21V10Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M11.8 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M14.8 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M17.8 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M20.8 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M23.8 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M26.8 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M37.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M40.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M43.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M46.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M49.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M52.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M34.4399 44.99V41.37" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M37.4399 44.99V41.37" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M40.4399 44.99V41.37" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M43.4399 44.99V41.37" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M46.4399 44.99V41.37" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M34.4399 21.64V18.02" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M37.4399 21.64V18.02" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M40.4399 21.64V18.02" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M43.4399 21.64V18.02" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M46.4399 21.64V18.02" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M26.9502 25.5H30.5702" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M26.9502 28.5H30.5702" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M26.9502 31.5H30.5702" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M26.9502 34.5H30.5702" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M26.9502 37.5H30.5702" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M50.3 25.5H53.92" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M50.3 28.5H53.92" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M50.3 31.5H53.92" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M50.3 34.5H53.92" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M50.3 37.5H53.92" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M55.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M58.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M61.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M64.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M67.4399 58.59V54.97" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M50.2901 21.64H30.5701V41.36H50.2901V21.64Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M13.8 29.23C12.52 29.23 11.48 28.19 11.48 26.91V21.33C11.48 20.05 12.52 19.01 13.8 19.01C15.08 19.01 16.12 20.05 16.12 21.33V26.91C16.12 28.19 15.08 29.23 13.8 29.23Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M69.7501 19.01H65.1201V24.12H69.7501V19.01Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M69.7501 40.07H65.1201V45.18H69.7501V40.07Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M69.7501 29.54H65.1201V34.65H69.7501V29.54Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M13.8 44.61C12.52 44.61 11.48 43.57 11.48 42.29V36.71C11.48 35.43 12.52 34.39 13.8 34.39C15.08 34.39 16.12 35.43 16.12 36.71V42.29C16.12 43.58 15.08 44.61 13.8 44.61Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                    <header class="cat__header">
                                        <div class="cat__group">
                                            <h3 class="cat__title">
                                                <a class="cat__link" href="<?= Pages::getHref('upgrade') ?>" title="Модернизация оборудования">Модернизация оборудования</a>
                                            </h3>
                                        </div><span class="cat__arrow">
                              <svg viewBox="0 0 19.4 22.8">
                                <path stroke="currentColor" stroke-width="2" fill="none" d="M9,20.4v-18l9,9L9,20.4z"></path>
                                <path stroke="currentColor" stroke-width="2" fill="none" d="M18,11.4H0"></path>
                              </svg></span>
                                    </header>
                                </article>
                            </li>
                            <li class="cats__item col-4">
                                <article class="cat is-alt">
                                    <div class="cat__icon"><svg width="83" height="86" viewBox="0 0 83 86" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <rect x="3.0498" y="2.77759" width="3.33972" height="57.0134" fill="#D3D8E8"/>
                                            <rect x="10.3965" y="11.0704" width="3.14867" height="26.9518" fill="#D3D8E8"/>
                                            <rect x="42.2393" y="11.0704" width="3.14867" height="15.4033" fill="#D3D8E8"/>
                                            <path d="M73.0396 60.05H4.42933C3.08952 60.05 2 59.0308 2 57.7604V4.30357C2 3.03312 3.08952 2 4.42933 2H73.0396C74.3794 2 75.4689 3.03312 75.4689 4.30357V57.7604C75.4689 59.0308 74.3794 60.05 73.0396 60.05Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M36.0551 10.9211H10.2158V38.5361H36.0551V10.9211Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M68.079 10.9211H42.2397V26.4737H68.079V10.9211Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M45.2134 19.3815H52.8547L54.651 14.3136L55.6669 23.8909L57.36 19.3815H64.7953" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M2 45.7539H75.4689" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M10.2158 54.7029H15.3837" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M62.7622 54.7029H67.9153" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M49.6309 54.7029H54.784" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M36.4971 54.7029H41.6502" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M2 49.5723L75.4687 49.5723" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M10.2158 34.285L36.0551 34.285" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M23.3491 54.7029H28.517" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M45.2131 38.2848C46.937 38.2848 48.3344 36.9597 48.3344 35.3251C48.3344 33.6905 46.937 32.3654 45.2131 32.3654C43.4893 32.3654 42.0918 33.6905 42.0918 35.3251C42.0918 36.9597 43.4893 38.2848 45.2131 38.2848Z" stroke="#334DA6" stroke-miterlimit="10"/>
                                            <path d="M45.2134 35.3251L47.4071 33.2449" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M55.0036 38.2848C56.7275 38.2848 58.125 36.9597 58.125 35.3251C58.125 33.6905 56.7275 32.3654 55.0036 32.3654C53.2798 32.3654 51.8823 33.6905 51.8823 35.3251C51.8823 36.9597 53.2798 38.2848 55.0036 38.2848Z" stroke="#334DA6" stroke-miterlimit="10"/>
                                            <path d="M55.0039 35.3251L57.2124 33.2449" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M64.7942 38.2848C66.518 38.2848 67.9155 36.9597 67.9155 35.3251C67.9155 33.6905 66.518 32.3654 64.7942 32.3654C63.0703 32.3654 61.6729 33.6905 61.6729 35.3251C61.6729 36.9597 63.0703 38.2848 64.7942 38.2848Z" stroke="#334DA6" stroke-miterlimit="10"/>
                                            <path d="M64.7944 35.3251L67.0029 33.2449" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                    <header class="cat__header">
                                        <div class="cat__group">
                                            <h3 class="cat__title">
                                                <a class="cat__link" href="<?= Pages::getHref('monitoring') ?>" title="Система мониторинга станков и роботов">Система мониторинга станков и роботов</a>
                                            </h3>
                                        </div>
                                        <span class="cat__arrow">
                                            <svg viewBox="0 0 19.4 22.8">
                                                <path stroke="currentColor" stroke-width="2" fill="none" d="M9,20.4v-18l9,9L9,20.4z"></path>
                                                <path stroke="currentColor" stroke-width="2" fill="none" d="M18,11.4H0"></path>
                                            </svg>
                                        </span>
                                    </header>
                                </article>
                            </li>
                            <li class="cats__item col-4">
                                <article class="cat is-alt">
                                    <div class="cat__icon">
                                        <svg width="90" height="84" viewBox="0 0 90 84" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M8.43144 28.6219V17.4016H10.8387V42.0516H6.02417V29.7266L8.43144 28.6219Z" fill="#D3D8E8"/>
                                            <rect x="2.3999" y="43.1481" width="4.26728" height="15.4858" fill="#D3D8E8"/>
                                            <path d="M11.9513 47.174C9.97938 48.3358 9.96425 51.6361 11.9513 53.0205C7.19755 52.2512 7.19755 47.6355 11.9513 47.174Z" fill="#D3D8E8"/>
                                            <path d="M23.5868 47.174C21.6149 48.3358 21.5997 51.6361 23.5868 53.0205C18.8331 52.2512 18.833 47.6355 23.5868 47.174Z" fill="#D3D8E8"/>
                                            <path d="M35.2223 47.174C33.2504 48.3358 33.2352 51.6361 35.2223 53.0205C30.4685 52.2512 30.4685 47.6355 35.2223 47.174Z" fill="#D3D8E8"/>
                                            <rect x="7.09741" y="57.7302" width="77.9186" height="3.18134" fill="#D3D8E8"/>
                                            <path d="M4.53338 57.7302H9.23112V73.672L5.09903 80.3418H1.49609L4.53338 72.8529V57.7302Z" fill="#D3D8E8"/>
                                            <rect x="36.0796" y="17.9095" width="2.41967" height="12.2935" fill="#D3D8E8"/>
                                            <rect x="33.9417" y="6.70569" width="2.84899" height="11.2038" fill="#D3D8E8"/>
                                            <rect x="76.1064" y="18.258" width="3.08011" height="23.8901" fill="#D3D8E8"/>
                                            <path d="M87.7446 58.0952V42.0993H2V58.0952H4.77403V71.8529L2 76.3777V80.9024H18.8561L22.7089 76.3777H67.026L70.8789 80.9024H87.7446V76.3777L84.9706 71.8529V58.0952H87.7446Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M8.64632 17.2563V28.7275L5.85303 30.657V35.6835V42.0992H8.64632H11.43H14.2233V17.2563H8.64632Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M5.85303 35.6836H9.18572" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M81.0987 17.2563V28.7275L83.892 30.657V35.6835V42.0992H81.0987H78.315H75.5217V17.2563H81.0987Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M83.892 35.6836H80.5593" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M35.1729 24.6176H14.2231V17.7144H75.5216V24.6176H45.8356" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M44.3619 5.93964H36.7237C34.595 5.93964 32.8708 7.66658 32.8708 9.79872V18.2308H35.3174V26.6629C35.3174 28.7951 37.0415 30.522 39.1702 30.522H41.9154C44.044 30.522 45.7682 28.7951 45.7682 26.6629V18.2308H48.2147V9.78908C48.2051 7.66658 46.481 5.93964 44.3619 5.93964Z" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M75.5216 17.2564V13.2719H48.2051" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M32.8707 13.2719H26.1475C22.7666 13.2719 20.0215 10.5223 20.0215 7.13595C20.0215 3.7496 22.7666 1 26.1475 1H40.5378V5.92999" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M40.5381 30.5027V34.4776" stroke="#334DA6" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M4.77393 57.5852H84.9705" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M68.4612 50.0973H87.3111" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M22.815 53.0205C24.4269 53.0205 25.7335 51.7117 25.7335 50.0972C25.7335 48.4827 24.4269 47.174 22.815 47.174C21.2031 47.174 19.8965 48.4827 19.8965 50.0972C19.8965 51.7117 21.2031 53.0205 22.815 53.0205Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M11.3045 53.0205C12.9164 53.0205 14.223 51.7117 14.223 50.0972C14.223 48.4827 12.9164 47.174 11.3045 47.174C9.69265 47.174 8.38599 48.4827 8.38599 50.0972C8.38599 51.7117 9.69265 53.0205 11.3045 53.0205Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M34.3255 53.0205C35.9373 53.0205 37.244 51.7117 37.244 50.0972C37.244 48.4827 35.9373 47.174 34.3255 47.174C32.7136 47.174 31.407 48.4827 31.407 50.0972C31.407 51.7117 32.7136 53.0205 34.3255 53.0205Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M61.1699 69.391C62.6115 69.391 63.7801 68.2204 63.7801 66.7765C63.7801 65.3325 62.6115 64.1619 61.1699 64.1619C59.7282 64.1619 58.5596 65.3325 58.5596 66.7765C58.5596 68.2204 59.7282 69.391 61.1699 69.391Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M62.1523 65.9371L64.0306 64.0558" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M73.2001 69.391C74.6418 69.391 75.8104 68.2204 75.8104 66.7765C75.8104 65.3325 74.6418 64.1619 73.2001 64.1619C71.7585 64.1619 70.5898 65.3325 70.5898 66.7765C70.5898 68.2204 71.7585 69.391 73.2001 69.391Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M74.1826 65.9371L76.0609 64.0558" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M65.8896 41.9063V39.726" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M37.8699 41.9064V39.726" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M51.875 41.9064V39.726" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M38.4575 9.57678H42.6186" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M38.4575 11.4196H42.6186" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M38.4575 13.2719H42.6186" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M23.8552 41.9063V39.726" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M13.1541 68.5902V63.4866" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M16.1785 68.5902V63.4866" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M19.2031 68.5902V63.4866" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M22.2273 68.5902V63.4866" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M25.2517 68.5902V63.4866" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M28.2764 68.5902V63.4866" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M31.3008 68.5902V63.4866" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M34.3252 68.5902V63.4866" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M40.5379 20.8402C39.6807 20.8402 38.9775 20.1456 38.9775 19.2773V17.7144H42.0983V19.2773C42.0983 20.1359 41.3952 20.8402 40.5379 20.8402Z" stroke="#334DA6" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                    <header class="cat__header">
                                        <div class="cat__group">
                                            <h3 class="cat__title">
                                                <a class="cat__link" href="<?= Pages::getHref('repair') ?>" title="Ремонт станков">Ремонт станков</a>
                                            </h3>
                                        </div>
                                        <span class="cat__arrow">
                                            <svg viewBox="0 0 19.4 22.8">
                                                <path stroke="currentColor" stroke-width="2" fill="none" d="M9,20.4v-18l9,9L9,20.4z"></path>
                                                <path stroke="currentColor" stroke-width="2" fill="none" d="M18,11.4H0"></path>
                                            </svg>
                                        </span>
                                    </header>
                                </article>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>