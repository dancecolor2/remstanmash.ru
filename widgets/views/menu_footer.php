<?php

/**
 * @var \yii\web\View $this
 * @var array $items
 */

?>

<div class="links">
    <ul class="links__list">
        <?php foreach ($items as $item): ?>
            <li class="links__item">
                <a class="links__link" href="<?= $item['url'] ?>" title="<?= $item['label'] ?>"><?= $item['label'] ?></a>
            </li>
        <?php endforeach ?>
    </ul>
</div>

