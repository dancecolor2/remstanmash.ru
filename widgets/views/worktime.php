<?php

/**
 * @var \yii\web\View $this
 * @var array $items
 */

?>

<?php foreach ($items as $item): ?>
    <div class="contact__worktime">
        <div class="contact__day"><?= $item['day'] ?></div>
        <div class="contact__time"><?= $item['time'] ?></div>
    </div>
<?php endforeach ?>

