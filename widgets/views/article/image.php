<?php

/**
 * @var string $image
 * @var string $alt
 */

?>

<figure class="image js-image">
    <a class="image__link js-image-src" href="<?= $image ?>" title="<?= $alt ?>">
        <img class="image__src" src="<?= $image ?>" alt="<?= $alt ?>">
    </a>
    <figcaption class="image__caption"><?= $alt ?></figcaption>
</figure>