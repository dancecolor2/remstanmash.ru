<?php

use yii\widgets\LinkPager;

/**
 * @var \yii\web\View $this
 * @var \yii\data\Pagination $pagination
 */

?>
<?php if ($pagination->pageCount > 1): ?>
    <div class="pagination">
        <?= LinkPager::widget([
            'pagination' => $pagination,
            'options' =>  ['class' => 'pagination__list'],
            'linkContainerOptions' => ['class' => 'pagination__item'],
            'linkOptions' => ['class' => 'pagination__link'],
            'activePageCssClass' => 'is-current',
            'prevPageLabel' => '<span class="arrow is-left"><svg class="arrow__icon"><use xlink:href="/img/sprite.svg#arrow-narrow"></use></svg></span>',
            'nextPageLabel' => '<span class="arrow is-right"><svg class="arrow__icon"><use xlink:href="/img/sprite.svg#arrow-narrow"></use></svg></span>',
            'prevPageCssClass' => 'is-prev',
            'nextPageCssClass' => 'is-next',
            'maxButtonCount' => 8,
        ]) ?>
    </div>
<?php endif ?>