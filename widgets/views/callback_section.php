<?php

use app\helpers\FormatHelper;
use app\modules\setting\components\Settings;
use app\widgets\WorktimeWidget;

/**
 * @var \yii\web\View $this
 */

?>

<section class="section is-accent is-centred with-padding has-callback is-padding-100">
    <div class="container">
        <header class="section__header">
            <h2 class="section__title"><?= FormatHelper::nText(Settings::getValue('callback_section_title')) ?></h2>
            <p class="section__descr"><?= FormatHelper::nText(Settings::getValue('callback_section_text')) ?></p>
        </header>
        <div class="section__body">
            <div class="callback">
                <div class="callback__contact">
                    <address class="contact is-alt">
                        <div class="contact__group">
                            <?= WorktimeWidget::widget() ?>
                            <ul class="contact__phones">
                                <?php if (!empty($phones = Settings::getArray('phones'))): ?>
                                    <li class="contact__phone is-alt">
                                    <a class="phone" href="tel:<?= $phones[0] ?>"><?= $phones[0] ?></a>
                                    </li>
                                <?php endif ?>
                            </ul>
                        </div>
                    </address>
                </div>
                <div class="callback__button">
                    <button class="button js-button" area-label="заказать обратный звонок" data-modal="callback">заказать обратный звонок</button>
                </div>
            </div>
        </div>
    </div>
</section>