<?php

/**
 * @var \yii\web\View $this
 * @var string $instagram
 * @var string $facebook
 */

?>

<div class="social">
    <ul class="social__list">
        <?php if (!empty($instagram)): ?>
            <li class="social__item">
                <a class="social__link is-instagram" title="instagram" area-label="instagram" href="<?= $instagram ?>" target="_blank">
                    <svg class="social__icon">
                        <use xlink:href="/img/sprite.svg#soc-instagram"></use>
                    </svg>
                </a>
            </li>
        <?php endif ?>
        <?php if (!empty($facebook)): ?>
            <li class="social__item">
                <a class="social__link is-facebook" title="facebook" area-label="facebook" href="<?= $facebook ?>" target="_blank">
                    <svg class="social__icon">
                        <use xlink:href="/img/sprite.svg#soc-facebook"></use>
                    </svg>
                </a>
            </li>
        <?php endif ?>
    </ul>
</div>

