<?php

/**
 * @var \yii\web\View $this
 * @var array $requisites `['key' => '', 'value' => '']`
 * @var boolean $isLight
 */

?>

<?php if (!empty($requisites)): ?>
    <div class="requisites <?= $isLight ? 'is-light' : '' ?>">
        <dl class="requisites__list">
            <?php foreach ($requisites as $requisite): ?>
                <div class="requisites__item">
                    <dt class="requisites__key"><?= $requisite['key'] ?></dt>
                    <dd class="requisites__value"><?= $requisite['value'] ?></dd>
                </div>
            <?php endforeach ?>
        </dl>
    </div>
<?php endif ?>
