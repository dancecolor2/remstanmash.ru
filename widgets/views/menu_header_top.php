<?php

/**
 * @var \yii\web\View $this
 * @var array $items
 */

?>

<?php if (!empty($items)): ?>
    <nav class="nav">
        <ul class="nav__list">
            <?php foreach ($items as $item): ?>
                <li class="nav__item">
                    <a class="nav__link <?= $item['active'] ? 'is-active' : '' ?>" href="<?= $item['url'] ?>" title="<?= $item['label'] ?>"><?= $item['label'] ?></a>
                </li>
            <?php endforeach ?>
        </ul>
    </nav>
<?php endif ?>
