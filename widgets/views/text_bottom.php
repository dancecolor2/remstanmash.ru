<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var string $title
 * @var string $text
 */

?>

<?php if (!empty($text)): ?>
    <section class="section is-white with-padding is-layout-3">
        <div class="container">
            <div class="grid is-row">
                <div class="col-4 col-m-12">
                    <div class="section__content">
                        <section class="card is-alt has-right-shadow">
                            <h4 class="card__title">ремстанмаш — это решение конкретных задач</h4>
                            <div class="card__body">
                                <p class="card__caption">Исключительно профессиональная и&nbsp;информативная поддержка всех наших клиентов</p>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="shift-1 col-7 col-m-12 shift-m-0">
                    <?php if (!empty($title)): ?>
                        <header class="section__header">
                            <h2 class="section__title"><?= Html::encode($title) ?></h2>
                        </header>
                    <?php endif ?>

                    <div class="section__body">
                        <div class="section__text text">
                            <?= $text ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>

