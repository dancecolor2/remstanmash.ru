CKEDITOR.plugins.add('gallery',
    {
        init: function(editor)
        {
            editor.ui.addButton('Галерея',
                {
                    label: 'Галерея',
                    command: 'gallery1',
                    icon: '/img/ckeditor/gallery.png'
                }
            );
            editor.addCommand('gallery1', {
                exec: function(editor) {
                    editor.insertHtml('<table class="article__images"><tr><td></td><td></td></tr></table>');
                }
            });
            editor.addContentsCss('/css/ckeditor/gallery.css');
        }
    }
);