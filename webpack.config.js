const webpack = require('webpack');
const path = require('path');
const config = require('./gulp/config');
// const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
  context: path.join(__dirname, config.src.js),
  entry: {
    app: './app.js'
  },
  mode: config.environment,
  output: {
    path: path.join(__dirname, config.dest.js),
    filename: '[name].js',
    publicPath: 'js/'
  },
  devtool: config.isProduction
    ? '#source-map'
    : '#cheap-module-eval-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      // _: 'lodash',
      Promise: 'es6-promise-promise'
    }),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  resolve: {
    alias: {
      '~': path.resolve(__dirname, config.src.components),
      js: path.resolve(__dirname, config.src.js)
    },
    modules: ['bower_components', 'node_modules'],
    extensions: ['.js']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: [
          path.resolve(__dirname, 'node_modules'),
          path.resolve(__dirname, 'bower_components')
        ]
      },
      {
        test: /(dom7|ssr-window|swiper)/,
        loader: 'babel-loader'
      },
    ]
  }
};
