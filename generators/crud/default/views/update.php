<?php

use app\generators\crud\Generator;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator app\generators\crud\Generator */

$varModel = $generator->getVarModel();
$nameModel = $generator->getNameModel();
$modelClass = StringHelper::basename($generator->modelClass);

$urlParams = $generator->generateUrlParams();
$modelClassName = Inflector::camel2words(StringHelper::basename($generator->modelClass));
$nameAttributeTemplate = $varModel . '->' . $generator->getNameAttribute();

echo "<?php\n";
?>

/**
 * @var yii\web\View $this
 * @var <?= ltrim($generator->modelClass, '\\') ?> <?= $varModel ?>

 */

$this->title = <?= $varModel ?>->getTitle();
?>

<?= '<?php' ?> $this->beginContent('<?= $generator->viewPath ?>/layout.php', ['<?= $nameModel ?>' => <?= $varModel ?>]); ?>

    <?= '<?= ' ?>$this->render('_form', compact('<?= $nameModel ?>')) ?>

<?= '<?php' ?> $this->endContent() ?>



