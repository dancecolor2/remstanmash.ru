<?php

use app\generators\crud\Generator;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator app\generators\crud\Generator */

$varModel = $generator->getVarModel();
$nameModel = $generator->getNameModel();
$modelClass = StringHelper::basename($generator->modelClass);

echo "<?php\n";
?>

/**
 * @var yii\web\View $this
 * @var <?= $varModel ?> <?= ltrim($generator->modelClass, '\\') ?>

 */

$this->title = '<?= $generator->message(Generator::MESSAGE_NEW) ?>';
$this->params['breadcrumbs'][] = ['label' => '<?= $generator->message(Generator::MESSAGE_MULTIPLE) ?>', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">
    <?= "<?= " ?>$this->render('_form', compact('<?= $nameModel ?>')) ?>
</div>

