<?php

use app\generators\crud\Generator;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator app\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$names = $generator->getColumnNames();
$varModel = $generator->getVarModel();
$nameModel = $generator->getNameModel();
$modelClass = StringHelper::basename($generator->modelClass);

echo "<?php\n";
?>

use yii\helpers\Html;
use kartik\grid\ActionColumn;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\icons\Icon;
use <?= $generator->modelClass ?>;

/**
 * @var yii\web\View $this
<?= !empty($generator->searchModelClass) ? " * @var " . ltrim($generator->searchModelClass, '\\') . " \$searchModel\n" : '' ?>
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = '<?= $generator->message(Generator::MESSAGE_MULTIPLE) ?>';
$this->params['breadcrumbs'][] = $this->title;

?>

<?= "<?= " ?>GridView::widget([
    'dataProvider' => $dataProvider,
    'summaryOptions' => ['class' => 'text-right'],
    'bordered' => false,
    'pjax' => <?= $generator->enablePjax ? 'true' : 'false' ?>,
    'pjaxSettings' => ['options' => ['id' => 'pjax-widget']],
    'striped' => false,
    'hover' => true,
    'panel' => [
        'before',
        'after' => false,
    ],
    'toolbar' => [
        [
            'content' =>
                Html::a(
                    Icon::show('plus-outline') . ' <?= $generator->message(Generator::MESSAGE_ADD) ?>',
                    ['create'],
                    [
                        'data-pjax' => 0,
                        'class' => 'btn btn-success'
                    ]
                ) . ' ' .
                Html::a(
                    Icon::show('arrow-sync-outline'),
                    ['index'],
                    [
                        'data-pjax' => 0,
                        'class' => 'btn btn-default',
                        'title' => Yii::t('app', 'Reset')
                    ]
                )
        ],
        '{toggleData}',
    ],
    'toggleDataOptions' => [
        'all' => [
            'icon' => 'resize-full',
            'label' => 'Показать все',
            'class' => 'btn btn-default',
            'title' => 'Показать все'
        ],
    'page' => [
        'icon' => 'resize-small',
            'label' => 'Страницы',
            'class' => 'btn btn-default',
            'title' => 'Постаничная разбивка'
        ],
    ],
    <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n    'columns' => [\n" : "'columns' => [\n"; ?>
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'attribute' => 'id',
            'width' => '100px',
        ],
<?php if (in_array('position', $names)): ?>
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'hAlign' => GridView::ALIGN_CENTER,
            'value' => function (<?= $modelClass ?> <?= $varModel ?>) {
                return
                    Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => <?= $varModel ?>->id], [
                        'class' => 'pjax-action'
                    ]) .
                    Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => <?= $varModel ?>->id], [
                        'class' => 'pjax-action'
                    ]);
            },
            'format' => 'raw',
            'width' => '40px',
        ],
<?php endif ?>
<?php if (in_array('title', $names)): ?>
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'format' => 'raw',
            'attribute' => 'title',
            'value' => function (<?= $modelClass ?> <?= $varModel ?>) {
                return Html::a(<?= $varModel ?>->title, ['update', 'id' => <?= $varModel ?>->id], ['data-pjax' => '0']);
            }
        ],
<?php endif ?>
<?php if (in_array('active', $names)): ?>
        [
            'class' => DataColumn::class,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'attribute' => 'active',
            'format' => 'raw',
            'filter' => <?= $modelClass ?>::activeList(),
            'value' => function (<?= $modelClass ?> <?= $varModel ?>) {
                return <?= $varModel ?>->getActiveIcon();
            },
        ],
<?php endif ?>
        [
            'class' => ActionColumn::class,
            'template' => '{update}',
            'width' => '50px',
            'mergeHeader' => false,
            'buttons' => [
                'update' => function ($url, <?= $modelClass ?> <?= $varModel ?>, $key) {
                    return Html::a('<i class="fa fa-pencil"></i> Редактировать', $url, [
                        'class' => 'btn btn-xs btn-primary',
                        'data-pjax' => '0',
                    ]);
                },
            ],
        ],
    ],
]); ?>