<?php

use app\generators\crud\Generator;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator app\generators\crud\Generator */

$varModel = $generator->getVarModel();
$nameModel = $generator->getNameModel();
$modelClass = StringHelper::basename($generator->modelClass);
$controllerPath = $generator->getControllerPath();

echo '<?php';
?>

use yii\bootstrap\Tabs;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $content
 * @var <?= $generator->modelClass ?> <?= $varModel ?>
 * @var array $breadcrumbs
 */

$this->title = <?= $varModel ?>->getTitle();
$this->params['breadcrumbs'][] = ['label' => '<?= $generator->message(Generator::MESSAGE_MULTIPLE) ?>', 'url' => ['<?= $controllerPath ?>/index']];

if (Yii::$app->controller->action->id == 'update') {
    $this->params['breadcrumbs'][] = <?= $varModel ?>->getTitle();
} else {
    $this->params['breadcrumbs'][] = [
        'label' => <?= $varModel ?>->getTitle(),
        'url' => ['<?= $controllerPath ?>/update', 'id' => <?= $varModel ?>->id],
    ];
}

if (!empty($breadcrumbs)) {
    foreach ($breadcrumbs as $breadcrumb) {
        $this->params['breadcrumbs'][] = $breadcrumb;
    }
}

?>
<div class="nav-tabs-custom">
    <?= '<?=' ?> Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'url' => ['<?= $controllerPath ?>/update', 'id' => <?= $varModel ?>->id],
                'active' => Yii::$app->controller->action->id == 'update',
            ],
            [
                'label' => 'Действия',
                'headerOptions' => ['class' => 'pull-right'],
                'items' => [
                    [
                        'encode' => false,
                        'label' => '<i class="fa fa-remove text-danger" aria-hidden="true"></i><?= $generator->message(Generator::MESSAGE_DELETE) ?>',
                        'url' => Url::to(['<?= $controllerPath ?>/delete', 'id' => <?= $varModel ?>->id]),
                        'linkOptions' => [
                            'class' => 'text-danger',
                            'data-method' => 'post',
                            'data-confirm' => 'Вы уверены?',
                        ],
                    ],
                ],
            ],
        ]
    ]) ?>

    <?= '<?=' ?> $content ?>

</div>
