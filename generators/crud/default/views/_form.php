<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator app\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

$varModel = $generator->getVarModel();
$nameModel = $generator->getNameModel();
$modelClass = StringHelper::basename($generator->modelClass);

echo "<?php\n";
?>

use yii\helpers\Html;
use kartik\form\ActiveForm;

/**
 * @var yii\web\View $this
 * @var <?= ltrim($generator->modelClass, '\\') ?> <?= $varModel ?>
 */

?>

<?= "<?php " ?>$form = ActiveForm::begin(); ?>
    <div class="box-body">
<?php foreach ($generator->getColumnNames() as $attribute) {

        if (substr($attribute, -5)=="_hash") {
            continue;
        }

        if (in_array($attribute, $safeAttributes)) {
        echo "      <?= " . $generator->generateActiveField($attribute) . " ?>\n";
    }
} ?>
    </div>
    <div class="box-footer">
        <?= "<?= " ?> Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
<?= "<?php " ?>ActiveForm::end(); ?>

