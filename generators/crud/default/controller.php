<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use app\generators\crud\Generator;
use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator app\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class = $generator->modelClass;
$pks = $class::primaryKey();
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

$varModel = $generator->getVarModel();
$nameModel = $generator->getNameModel();
$columns = $generator->getColumnNames();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use Yii;
use app\modules\admin\components\BalletController;
use <?= ltrim($generator->modelClass, '\\') ?>;
<?php if (!empty($generator->searchModelClass)): ?>
use <?= ltrim($generator->searchModelClass, '\\') . (isset($searchModelAlias) ? " as $searchModelAlias" : "") ?>;
<?php else: ?>
use yii\data\ActiveDataProvider;
<?php endif; ?>
use yii\filters\VerbFilter;

class <?= $controllerClass ?> extends BalletController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
<?php if (!empty($generator->searchModelClass)): ?>
        $searchModel = new <?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
<?php else: ?>
        $dataProvider = new ActiveDataProvider([
            'query' => <?= $modelClass ?>::find(),
        ]);

        return $this->render('index', compact('dataProvider'));
<?php endif; ?>
    }

    public function actionCreate()
    {
        <?= $varModel ?> = new <?= $modelClass ?>(<?= in_array('active', $generator->getColumnNames()) ? "['active' => true]" : '' ?>);

        if (<?= $varModel ?>->load(Yii::$app->request->post()) && <?= $varModel ?>->save()) {
            Yii::$app->session->setFlash('success', '<?= $generator->message(Generator::MESSAGE_CREATE_SUCCESS) ?>');

            return $this->redirect(['update', <?= $urlParams ?>]);
        }

        return $this->render('create', compact('<?= $nameModel ?>'));
    }

    public function actionUpdate(<?= $actionParams ?>)
    {
        <?= $varModel ?> = <?= $modelClass ?>::findOneOrException(<?= $actionParams ?>);

        if (<?= $varModel ?>->load(Yii::$app->request->post()) && <?= $varModel ?>->save()) {

            Yii::$app->session->setFlash('success', '<?= $generator->message(Generator::MESSAGE_UPDATE_SUCCESS) ?>');
            return $this->refresh();
        }

        return $this->render('update', compact('<?= $nameModel ?>'));
    }

    public function actionDelete(<?= $actionParams ?>)
    {
        <?= $modelClass ?>::findOneOrException(<?= $actionParams ?>)->delete();

        return $this->redirect(['index']);
    }
<?php if(in_array('position', $columns)): ?>

    public function actionMoveUp($id)
    {
        <?= $modelClass ?>::findOneOrException($id)->movePrev();
    }

    public function actionMoveDown($id)
    {
        <?= $modelClass ?>::findOneOrException($id)->moveNext();
    }

<?php endif ?>
}
