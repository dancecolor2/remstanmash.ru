<?php
/**
 * This is the template for generating a controller class within a module.
 */

/* @var $this yii\web\View */
/* @var $generator app\generators\module\Generator */

echo "<?php\n";
?>

namespace <?= $generator->getControllerNamespace() ?>;

use app\modules\admin\components\BalletController;

class DefaultController extends BalletController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
